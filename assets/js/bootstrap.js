+function(){
/**
 * --------------------------------------------------------------------------
 * Bootstrap (v4.0.0-beta): util.js
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * --------------------------------------------------------------------------
 */

var Util = function ($) {

  /**
   * ------------------------------------------------------------------------
   * Private TransitionEnd Helpers
   * ------------------------------------------------------------------------
   */

  var transition = false;

  var MAX_UID = 1000000;

  var TransitionEndEvent = {
    WebkitTransition: 'webkitTransitionEnd',
    MozTransition: 'transitionend',
    OTransition: 'oTransitionEnd otransitionend',
    transition: 'transitionend'

    // shoutout AngusCroll (https://goo.gl/pxwQGp)
  };function toType(obj) {
    return {}.toString.call(obj).match(/\s([a-zA-Z]+)/)[1].toLowerCase();
  }

  function isElement(obj) {
    return (obj[0] || obj).nodeType;
  }

  function getSpecialTransitionEndEvent() {
    return {
      bindType: transition.end,
      delegateType: transition.end,
      handle: function handle(event) {
        if ($(event.target).is(this)) {
          return event.handleObj.handler.apply(this, arguments); // eslint-disable-line prefer-rest-params
        }
        return undefined;
      }
    };
  }

  function transitionEndTest() {
    if (window.QUnit) {
      return false;
    }

    var el = document.createElement('bootstrap');

    for (var name in TransitionEndEvent) {
      if (el.style[name] !== undefined) {
        return {
          end: TransitionEndEvent[name]
        };
      }
    }

    return false;
  }

  function transitionEndEmulator(duration) {
    var _this = this;

    var called = false;

    $(this).one(Util.TRANSITION_END, function () {
      called = true;
    });

    setTimeout(function () {
      if (!called) {
        Util.triggerTransitionEnd(_this);
      }
    }, duration);

    return this;
  }

  function setTransitionEndSupport() {
    transition = transitionEndTest();

    $.fn.emulateTransitionEnd = transitionEndEmulator;

    if (Util.supportsTransitionEnd()) {
      $.event.special[Util.TRANSITION_END] = getSpecialTransitionEndEvent();
    }
  }

  /**
   * --------------------------------------------------------------------------
   * Public Util Api
   * --------------------------------------------------------------------------
   */

  var Util = {

    TRANSITION_END: 'bsTransitionEnd',

    getUID: function getUID(prefix) {
      do {
        // eslint-disable-next-line no-bitwise
        prefix += ~~(Math.random() * MAX_UID); // "~~" acts like a faster Math.floor() here
      } while (document.getElementById(prefix));
      return prefix;
    },
    getSelectorFromElement: function getSelectorFromElement(element) {
      var selector = element.getAttribute('data-target');
      if (!selector || selector === '#') {
        selector = element.getAttribute('href') || '';
      }

      try {
        var $selector = $(selector);
        return $selector.length > 0 ? selector : null;
      } catch (error) {
        return null;
      }
    },
    reflow: function reflow(element) {
      return element.offsetHeight;
    },
    triggerTransitionEnd: function triggerTransitionEnd(element) {
      $(element).trigger(transition.end);
    },
    supportsTransitionEnd: function supportsTransitionEnd() {
      return Boolean(transition);
    },
    typeCheckConfig: function typeCheckConfig(componentName, config, configTypes) {
      for (var property in configTypes) {
        if (configTypes.hasOwnProperty(property)) {
          var expectedTypes = configTypes[property];
          var value = config[property];
          var valueType = value && isElement(value) ? 'element' : toType(value);

          if (!new RegExp(expectedTypes).test(valueType)) {
            throw new Error(componentName.toUpperCase() + ': ' + ('Option "' + property + '" provided type "' + valueType + '" ') + ('but expected type "' + expectedTypes + '".'));
          }
        }
      }
    }
  };

  setTransitionEndSupport();

  return Util;
}(jQuery);
var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/**
 * --------------------------------------------------------------------------
 * Bootstrap (v4.0.0-beta): alert.js
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * --------------------------------------------------------------------------
 */

var Alert = function ($) {

  /**
   * ------------------------------------------------------------------------
   * Constants
   * ------------------------------------------------------------------------
   */

  var NAME = 'alert';
  var VERSION = '4.0.0-beta';
  var DATA_KEY = 'bs.alert';
  var EVENT_KEY = '.' + DATA_KEY;
  var DATA_API_KEY = '.data-api';
  var JQUERY_NO_CONFLICT = $.fn[NAME];
  var TRANSITION_DURATION = 150;

  var Selector = {
    DISMISS: '[data-dismiss="alert"]'
  };

  var Event = {
    CLOSE: 'close' + EVENT_KEY,
    CLOSED: 'closed' + EVENT_KEY,
    CLICK_DATA_API: 'click' + EVENT_KEY + DATA_API_KEY
  };

  var ClassName = {
    ALERT: 'alert',
    FADE: 'fade',
    SHOW: 'show'

    /**
     * ------------------------------------------------------------------------
     * Class Definition
     * ------------------------------------------------------------------------
     */

  };
  var Alert = function () {
    function Alert(element) {
      _classCallCheck(this, Alert);

      this._element = element;
    }

    // getters

    // public

    Alert.prototype.close = function close(element) {
      element = element || this._element;

      var rootElement = this._getRootElement(element);
      var customEvent = this._triggerCloseEvent(rootElement);

      if (customEvent.isDefaultPrevented()) {
        return;
      }

      this._removeElement(rootElement);
    };

    Alert.prototype.dispose = function dispose() {
      $.removeData(this._element, DATA_KEY);
      this._element = null;
    };

    // private

    Alert.prototype._getRootElement = function _getRootElement(element) {
      var selector = Util.getSelectorFromElement(element);
      var parent = false;

      if (selector) {
        parent = $(selector)[0];
      }

      if (!parent) {
        parent = $(element).closest('.' + ClassName.ALERT)[0];
      }

      return parent;
    };

    Alert.prototype._triggerCloseEvent = function _triggerCloseEvent(element) {
      var closeEvent = $.Event(Event.CLOSE);

      $(element).trigger(closeEvent);
      return closeEvent;
    };

    Alert.prototype._removeElement = function _removeElement(element) {
      var _this = this;

      $(element).removeClass(ClassName.SHOW);

      if (!Util.supportsTransitionEnd() || !$(element).hasClass(ClassName.FADE)) {
        this._destroyElement(element);
        return;
      }

      $(element).one(Util.TRANSITION_END, function (event) {
        return _this._destroyElement(element, event);
      }).emulateTransitionEnd(TRANSITION_DURATION);
    };

    Alert.prototype._destroyElement = function _destroyElement(element) {
      $(element).detach().trigger(Event.CLOSED).remove();
    };

    // static

    Alert._jQueryInterface = function _jQueryInterface(config) {
      return this.each(function () {
        var $element = $(this);
        var data = $element.data(DATA_KEY);

        if (!data) {
          data = new Alert(this);
          $element.data(DATA_KEY, data);
        }

        if (config === 'close') {
          data[config](this);
        }
      });
    };

    Alert._handleDismiss = function _handleDismiss(alertInstance) {
      return function (event) {
        if (event) {
          event.preventDefault();
        }

        alertInstance.close(this);
      };
    };

    _createClass(Alert, null, [{
      key: 'VERSION',
      get: function get() {
        return VERSION;
      }
    }]);

    return Alert;
  }();

  /**
   * ------------------------------------------------------------------------
   * Data Api implementation
   * ------------------------------------------------------------------------
   */

  $(document).on(Event.CLICK_DATA_API, Selector.DISMISS, Alert._handleDismiss(new Alert()));

  /**
   * ------------------------------------------------------------------------
   * jQuery
   * ------------------------------------------------------------------------
   */

  $.fn[NAME] = Alert._jQueryInterface;
  $.fn[NAME].Constructor = Alert;
  $.fn[NAME].noConflict = function () {
    $.fn[NAME] = JQUERY_NO_CONFLICT;
    return Alert._jQueryInterface;
  };

  return Alert;
}(jQuery);
var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/**
 * --------------------------------------------------------------------------
 * Bootstrap (v4.0.0-beta): button.js
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * --------------------------------------------------------------------------
 */

var Button = function ($) {

  /**
   * ------------------------------------------------------------------------
   * Constants
   * ------------------------------------------------------------------------
   */

  var NAME = 'button';
  var VERSION = '4.0.0-beta';
  var DATA_KEY = 'bs.button';
  var EVENT_KEY = '.' + DATA_KEY;
  var DATA_API_KEY = '.data-api';
  var JQUERY_NO_CONFLICT = $.fn[NAME];

  var ClassName = {
    ACTIVE: 'active',
    BUTTON: 'btn',
    FOCUS: 'focus'
  };

  var Selector = {
    DATA_TOGGLE_CARROT: '[data-toggle^="button"]',
    DATA_TOGGLE: '[data-toggle="buttons"]',
    INPUT: 'input',
    ACTIVE: '.active',
    BUTTON: '.btn'
  };

  var Event = {
    CLICK_DATA_API: 'click' + EVENT_KEY + DATA_API_KEY,
    FOCUS_BLUR_DATA_API: 'focus' + EVENT_KEY + DATA_API_KEY + ' ' + ('blur' + EVENT_KEY + DATA_API_KEY)

    /**
     * ------------------------------------------------------------------------
     * Class Definition
     * ------------------------------------------------------------------------
     */

  };
  var Button = function () {
    function Button(element) {
      _classCallCheck(this, Button);

      this._element = element;
    }

    // getters

    // public

    Button.prototype.toggle = function toggle() {
      var triggerChangeEvent = true;
      var addAriaPressed = true;
      var rootElement = $(this._element).closest(Selector.DATA_TOGGLE)[0];

      if (rootElement) {
        var input = $(this._element).find(Selector.INPUT)[0];

        if (input) {
          if (input.type === 'radio') {
            if (input.checked && $(this._element).hasClass(ClassName.ACTIVE)) {
              triggerChangeEvent = false;
            } else {
              var activeElement = $(rootElement).find(Selector.ACTIVE)[0];

              if (activeElement) {
                $(activeElement).removeClass(ClassName.ACTIVE);
              }
            }
          }

          if (triggerChangeEvent) {
            if (input.hasAttribute('disabled') || rootElement.hasAttribute('disabled') || input.classList.contains('disabled') || rootElement.classList.contains('disabled')) {
              return;
            }
            input.checked = !$(this._element).hasClass(ClassName.ACTIVE);
            $(input).trigger('change');
          }

          input.focus();
          addAriaPressed = false;
        }
      }

      if (addAriaPressed) {
        this._element.setAttribute('aria-pressed', !$(this._element).hasClass(ClassName.ACTIVE));
      }

      if (triggerChangeEvent) {
        $(this._element).toggleClass(ClassName.ACTIVE);
      }
    };

    Button.prototype.dispose = function dispose() {
      $.removeData(this._element, DATA_KEY);
      this._element = null;
    };

    // static

    Button._jQueryInterface = function _jQueryInterface(config) {
      return this.each(function () {
        var data = $(this).data(DATA_KEY);

        if (!data) {
          data = new Button(this);
          $(this).data(DATA_KEY, data);
        }

        if (config === 'toggle') {
          data[config]();
        }
      });
    };

    _createClass(Button, null, [{
      key: 'VERSION',
      get: function get() {
        return VERSION;
      }
    }]);

    return Button;
  }();

  /**
   * ------------------------------------------------------------------------
   * Data Api implementation
   * ------------------------------------------------------------------------
   */

  $(document).on(Event.CLICK_DATA_API, Selector.DATA_TOGGLE_CARROT, function (event) {
    event.preventDefault();

    var button = event.target;

    if (!$(button).hasClass(ClassName.BUTTON)) {
      button = $(button).closest(Selector.BUTTON);
    }

    Button._jQueryInterface.call($(button), 'toggle');
  }).on(Event.FOCUS_BLUR_DATA_API, Selector.DATA_TOGGLE_CARROT, function (event) {
    var button = $(event.target).closest(Selector.BUTTON)[0];
    $(button).toggleClass(ClassName.FOCUS, /^focus(in)?$/.test(event.type));
  });

  /**
   * ------------------------------------------------------------------------
   * jQuery
   * ------------------------------------------------------------------------
   */

  $.fn[NAME] = Button._jQueryInterface;
  $.fn[NAME].Constructor = Button;
  $.fn[NAME].noConflict = function () {
    $.fn[NAME] = JQUERY_NO_CONFLICT;
    return Button._jQueryInterface;
  };

  return Button;
}(jQuery);
var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/**
 * --------------------------------------------------------------------------
 * Bootstrap (v4.0.0-beta): carousel.js
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * --------------------------------------------------------------------------
 */

var Carousel = function ($) {

  /**
   * ------------------------------------------------------------------------
   * Constants
   * ------------------------------------------------------------------------
   */

  var NAME = 'carousel';
  var VERSION = '4.0.0-beta';
  var DATA_KEY = 'bs.carousel';
  var EVENT_KEY = '.' + DATA_KEY;
  var DATA_API_KEY = '.data-api';
  var JQUERY_NO_CONFLICT = $.fn[NAME];
  var TRANSITION_DURATION = 600;
  var ARROW_LEFT_KEYCODE = 37; // KeyboardEvent.which value for left arrow key
  var ARROW_RIGHT_KEYCODE = 39; // KeyboardEvent.which value for right arrow key
  var TOUCHEVENT_COMPAT_WAIT = 500; // Time for mouse compat events to fire after touch

  var Default = {
    interval: 5000,
    keyboard: true,
    slide: false,
    pause: 'hover',
    wrap: true
  };

  var DefaultType = {
    interval: '(number|boolean)',
    keyboard: 'boolean',
    slide: '(boolean|string)',
    pause: '(string|boolean)',
    wrap: 'boolean'
  };

  var Direction = {
    NEXT: 'next',
    PREV: 'prev',
    LEFT: 'left',
    RIGHT: 'right'
  };

  var Event = {
    SLIDE: 'slide' + EVENT_KEY,
    SLID: 'slid' + EVENT_KEY,
    KEYDOWN: 'keydown' + EVENT_KEY,
    MOUSEENTER: 'mouseenter' + EVENT_KEY,
    MOUSELEAVE: 'mouseleave' + EVENT_KEY,
    TOUCHEND: 'touchend' + EVENT_KEY,
    LOAD_DATA_API: 'load' + EVENT_KEY + DATA_API_KEY,
    CLICK_DATA_API: 'click' + EVENT_KEY + DATA_API_KEY
  };

  var ClassName = {
    CAROUSEL: 'carousel',
    ACTIVE: 'active',
    SLIDE: 'slide',
    RIGHT: 'carousel-item-right',
    LEFT: 'carousel-item-left',
    NEXT: 'carousel-item-next',
    PREV: 'carousel-item-prev',
    ITEM: 'carousel-item'
  };

  var Selector = {
    ACTIVE: '.active',
    ACTIVE_ITEM: '.active.carousel-item',
    ITEM: '.carousel-item',
    NEXT_PREV: '.carousel-item-next, .carousel-item-prev',
    INDICATORS: '.carousel-indicators',
    DATA_SLIDE: '[data-slide], [data-slide-to]',
    DATA_RIDE: '[data-ride="carousel"]'

    /**
     * ------------------------------------------------------------------------
     * Class Definition
     * ------------------------------------------------------------------------
     */

  };
  var Carousel = function () {
    function Carousel(element, config) {
      _classCallCheck(this, Carousel);

      this._items = null;
      this._interval = null;
      this._activeElement = null;

      this._isPaused = false;
      this._isSliding = false;

      this.touchTimeout = null;

      this._config = this._getConfig(config);
      this._element = $(element)[0];
      this._indicatorsElement = $(this._element).find(Selector.INDICATORS)[0];

      this._addEventListeners();
    }

    // getters

    // public

    Carousel.prototype.next = function next() {
      if (!this._isSliding) {
        this._slide(Direction.NEXT);
      }
    };

    Carousel.prototype.nextWhenVisible = function nextWhenVisible() {
      // Don't call next when the page isn't visible
      if (!document.hidden) {
        this.next();
      }
    };

    Carousel.prototype.prev = function prev() {
      if (!this._isSliding) {
        this._slide(Direction.PREV);
      }
    };

    Carousel.prototype.pause = function pause(event) {
      if (!event) {
        this._isPaused = true;
      }

      if ($(this._element).find(Selector.NEXT_PREV)[0] && Util.supportsTransitionEnd()) {
        Util.triggerTransitionEnd(this._element);
        this.cycle(true);
      }

      clearInterval(this._interval);
      this._interval = null;
    };

    Carousel.prototype.cycle = function cycle(event) {
      if (!event) {
        this._isPaused = false;
      }

      if (this._interval) {
        clearInterval(this._interval);
        this._interval = null;
      }

      if (this._config.interval && !this._isPaused) {
        this._interval = setInterval((document.visibilityState ? this.nextWhenVisible : this.next).bind(this), this._config.interval);
      }
    };

    Carousel.prototype.to = function to(index) {
      var _this = this;

      this._activeElement = $(this._element).find(Selector.ACTIVE_ITEM)[0];

      var activeIndex = this._getItemIndex(this._activeElement);

      if (index > this._items.length - 1 || index < 0) {
        return;
      }

      if (this._isSliding) {
        $(this._element).one(Event.SLID, function () {
          return _this.to(index);
        });
        return;
      }

      if (activeIndex === index) {
        this.pause();
        this.cycle();
        return;
      }

      var direction = index > activeIndex ? Direction.NEXT : Direction.PREV;

      this._slide(direction, this._items[index]);
    };

    Carousel.prototype.dispose = function dispose() {
      $(this._element).off(EVENT_KEY);
      $.removeData(this._element, DATA_KEY);

      this._items = null;
      this._config = null;
      this._element = null;
      this._interval = null;
      this._isPaused = null;
      this._isSliding = null;
      this._activeElement = null;
      this._indicatorsElement = null;
    };

    // private

    Carousel.prototype._getConfig = function _getConfig(config) {
      config = $.extend({}, Default, config);
      Util.typeCheckConfig(NAME, config, DefaultType);
      return config;
    };

    Carousel.prototype._addEventListeners = function _addEventListeners() {
      var _this2 = this;

      if (this._config.keyboard) {
        $(this._element).on(Event.KEYDOWN, function (event) {
          return _this2._keydown(event);
        });
      }

      if (this._config.pause === 'hover') {
        $(this._element).on(Event.MOUSEENTER, function (event) {
          return _this2.pause(event);
        }).on(Event.MOUSELEAVE, function (event) {
          return _this2.cycle(event);
        });
        if ('ontouchstart' in document.documentElement) {
          // if it's a touch-enabled device, mouseenter/leave are fired as
          // part of the mouse compatibility events on first tap - the carousel
          // would stop cycling until user tapped out of it;
          // here, we listen for touchend, explicitly pause the carousel
          // (as if it's the second time we tap on it, mouseenter compat event
          // is NOT fired) and after a timeout (to allow for mouse compatibility
          // events to fire) we explicitly restart cycling
          $(this._element).on(Event.TOUCHEND, function () {
            _this2.pause();
            if (_this2.touchTimeout) {
              clearTimeout(_this2.touchTimeout);
            }
            _this2.touchTimeout = setTimeout(function (event) {
              return _this2.cycle(event);
            }, TOUCHEVENT_COMPAT_WAIT + _this2._config.interval);
          });
        }
      }
    };

    Carousel.prototype._keydown = function _keydown(event) {
      if (/input|textarea/i.test(event.target.tagName)) {
        return;
      }

      switch (event.which) {
        case ARROW_LEFT_KEYCODE:
          event.preventDefault();
          this.prev();
          break;
        case ARROW_RIGHT_KEYCODE:
          event.preventDefault();
          this.next();
          break;
        default:
          return;
      }
    };

    Carousel.prototype._getItemIndex = function _getItemIndex(element) {
      this._items = $.makeArray($(element).parent().find(Selector.ITEM));
      return this._items.indexOf(element);
    };

    Carousel.prototype._getItemByDirection = function _getItemByDirection(direction, activeElement) {
      var isNextDirection = direction === Direction.NEXT;
      var isPrevDirection = direction === Direction.PREV;
      var activeIndex = this._getItemIndex(activeElement);
      var lastItemIndex = this._items.length - 1;
      var isGoingToWrap = isPrevDirection && activeIndex === 0 || isNextDirection && activeIndex === lastItemIndex;

      if (isGoingToWrap && !this._config.wrap) {
        return activeElement;
      }

      var delta = direction === Direction.PREV ? -1 : 1;
      var itemIndex = (activeIndex + delta) % this._items.length;

      return itemIndex === -1 ? this._items[this._items.length - 1] : this._items[itemIndex];
    };

    Carousel.prototype._triggerSlideEvent = function _triggerSlideEvent(relatedTarget, eventDirectionName) {
      var targetIndex = this._getItemIndex(relatedTarget);
      var fromIndex = this._getItemIndex($(this._element).find(Selector.ACTIVE_ITEM)[0]);
      var slideEvent = $.Event(Event.SLIDE, {
        relatedTarget: relatedTarget,
        direction: eventDirectionName,
        from: fromIndex,
        to: targetIndex
      });

      $(this._element).trigger(slideEvent);

      return slideEvent;
    };

    Carousel.prototype._setActiveIndicatorElement = function _setActiveIndicatorElement(element) {
      if (this._indicatorsElement) {
        $(this._indicatorsElement).find(Selector.ACTIVE).removeClass(ClassName.ACTIVE);

        var nextIndicator = this._indicatorsElement.children[this._getItemIndex(element)];

        if (nextIndicator) {
          $(nextIndicator).addClass(ClassName.ACTIVE);
        }
      }
    };

    Carousel.prototype._slide = function _slide(direction, element) {
      var _this3 = this;

      var activeElement = $(this._element).find(Selector.ACTIVE_ITEM)[0];
      var activeElementIndex = this._getItemIndex(activeElement);
      var nextElement = element || activeElement && this._getItemByDirection(direction, activeElement);
      var nextElementIndex = this._getItemIndex(nextElement);
      var isCycling = Boolean(this._interval);

      var directionalClassName = void 0;
      var orderClassName = void 0;
      var eventDirectionName = void 0;

      if (direction === Direction.NEXT) {
        directionalClassName = ClassName.LEFT;
        orderClassName = ClassName.NEXT;
        eventDirectionName = Direction.LEFT;
      } else {
        directionalClassName = ClassName.RIGHT;
        orderClassName = ClassName.PREV;
        eventDirectionName = Direction.RIGHT;
      }

      if (nextElement && $(nextElement).hasClass(ClassName.ACTIVE)) {
        this._isSliding = false;
        return;
      }

      var slideEvent = this._triggerSlideEvent(nextElement, eventDirectionName);
      if (slideEvent.isDefaultPrevented()) {
        return;
      }

      if (!activeElement || !nextElement) {
        // some weirdness is happening, so we bail
        return;
      }

      this._isSliding = true;

      if (isCycling) {
        this.pause();
      }

      this._setActiveIndicatorElement(nextElement);

      var slidEvent = $.Event(Event.SLID, {
        relatedTarget: nextElement,
        direction: eventDirectionName,
        from: activeElementIndex,
        to: nextElementIndex
      });

      if (Util.supportsTransitionEnd() && $(this._element).hasClass(ClassName.SLIDE)) {

        $(nextElement).addClass(orderClassName);

        Util.reflow(nextElement);

        $(activeElement).addClass(directionalClassName);
        $(nextElement).addClass(directionalClassName);

        $(activeElement).one(Util.TRANSITION_END, function () {
          $(nextElement).removeClass(directionalClassName + ' ' + orderClassName).addClass(ClassName.ACTIVE);

          $(activeElement).removeClass(ClassName.ACTIVE + ' ' + orderClassName + ' ' + directionalClassName);

          _this3._isSliding = false;

          setTimeout(function () {
            return $(_this3._element).trigger(slidEvent);
          }, 0);
        }).emulateTransitionEnd(TRANSITION_DURATION);
      } else {
        $(activeElement).removeClass(ClassName.ACTIVE);
        $(nextElement).addClass(ClassName.ACTIVE);

        this._isSliding = false;
        $(this._element).trigger(slidEvent);
      }

      if (isCycling) {
        this.cycle();
      }
    };

    // static

    Carousel._jQueryInterface = function _jQueryInterface(config) {
      return this.each(function () {
        var data = $(this).data(DATA_KEY);
        var _config = $.extend({}, Default, $(this).data());

        if ((typeof config === 'undefined' ? 'undefined' : _typeof(config)) === 'object') {
          $.extend(_config, config);
        }

        var action = typeof config === 'string' ? config : _config.slide;

        if (!data) {
          data = new Carousel(this, _config);
          $(this).data(DATA_KEY, data);
        }

        if (typeof config === 'number') {
          data.to(config);
        } else if (typeof action === 'string') {
          if (data[action] === undefined) {
            throw new Error('No method named "' + action + '"');
          }
          data[action]();
        } else if (_config.interval) {
          data.pause();
          data.cycle();
        }
      });
    };

    Carousel._dataApiClickHandler = function _dataApiClickHandler(event) {
      var selector = Util.getSelectorFromElement(this);

      if (!selector) {
        return;
      }

      var target = $(selector)[0];

      if (!target || !$(target).hasClass(ClassName.CAROUSEL)) {
        return;
      }

      var config = $.extend({}, $(target).data(), $(this).data());
      var slideIndex = this.getAttribute('data-slide-to');

      if (slideIndex) {
        config.interval = false;
      }

      Carousel._jQueryInterface.call($(target), config);

      if (slideIndex) {
        $(target).data(DATA_KEY).to(slideIndex);
      }

      event.preventDefault();
    };

    _createClass(Carousel, null, [{
      key: 'VERSION',
      get: function get() {
        return VERSION;
      }
    }, {
      key: 'Default',
      get: function get() {
        return Default;
      }
    }]);

    return Carousel;
  }();

  /**
   * ------------------------------------------------------------------------
   * Data Api implementation
   * ------------------------------------------------------------------------
   */

  $(document).on(Event.CLICK_DATA_API, Selector.DATA_SLIDE, Carousel._dataApiClickHandler);

  $(window).on(Event.LOAD_DATA_API, function () {
    $(Selector.DATA_RIDE).each(function () {
      var $carousel = $(this);
      Carousel._jQueryInterface.call($carousel, $carousel.data());
    });
  });

  /**
   * ------------------------------------------------------------------------
   * jQuery
   * ------------------------------------------------------------------------
   */

  $.fn[NAME] = Carousel._jQueryInterface;
  $.fn[NAME].Constructor = Carousel;
  $.fn[NAME].noConflict = function () {
    $.fn[NAME] = JQUERY_NO_CONFLICT;
    return Carousel._jQueryInterface;
  };

  return Carousel;
}(jQuery);
var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/**
 * --------------------------------------------------------------------------
 * Bootstrap (v4.0.0-beta): collapse.js
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * --------------------------------------------------------------------------
 */

var Collapse = function ($) {

  /**
   * ------------------------------------------------------------------------
   * Constants
   * ------------------------------------------------------------------------
   */

  var NAME = 'collapse';
  var VERSION = '4.0.0-beta';
  var DATA_KEY = 'bs.collapse';
  var EVENT_KEY = '.' + DATA_KEY;
  var DATA_API_KEY = '.data-api';
  var JQUERY_NO_CONFLICT = $.fn[NAME];
  var TRANSITION_DURATION = 600;

  var Default = {
    toggle: true,
    parent: ''
  };

  var DefaultType = {
    toggle: 'boolean',
    parent: 'string'
  };

  var Event = {
    SHOW: 'show' + EVENT_KEY,
    SHOWN: 'shown' + EVENT_KEY,
    HIDE: 'hide' + EVENT_KEY,
    HIDDEN: 'hidden' + EVENT_KEY,
    CLICK_DATA_API: 'click' + EVENT_KEY + DATA_API_KEY
  };

  var ClassName = {
    SHOW: 'show',
    COLLAPSE: 'collapse',
    COLLAPSING: 'collapsing',
    COLLAPSED: 'collapsed'
  };

  var Dimension = {
    WIDTH: 'width',
    HEIGHT: 'height'
  };

  var Selector = {
    ACTIVES: '.show, .collapsing',
    DATA_TOGGLE: '[data-toggle="collapse"]'

    /**
     * ------------------------------------------------------------------------
     * Class Definition
     * ------------------------------------------------------------------------
     */

  };
  var Collapse = function () {
    function Collapse(element, config) {
      _classCallCheck(this, Collapse);

      this._isTransitioning = false;
      this._element = element;
      this._config = this._getConfig(config);
      this._triggerArray = $.makeArray($('[data-toggle="collapse"][href="#' + element.id + '"],' + ('[data-toggle="collapse"][data-target="#' + element.id + '"]')));
      var tabToggles = $(Selector.DATA_TOGGLE);
      for (var i = 0; i < tabToggles.length; i++) {
        var elem = tabToggles[i];
        var selector = Util.getSelectorFromElement(elem);
        if (selector !== null && $(selector).filter(element).length > 0) {
          this._triggerArray.push(elem);
        }
      }

      this._parent = this._config.parent ? this._getParent() : null;

      if (!this._config.parent) {
        this._addAriaAndCollapsedClass(this._element, this._triggerArray);
      }

      if (this._config.toggle) {
        this.toggle();
      }
    }

    // getters

    // public

    Collapse.prototype.toggle = function toggle() {
      if ($(this._element).hasClass(ClassName.SHOW)) {
        this.hide();
      } else {
        this.show();
      }
    };

    Collapse.prototype.show = function show() {
      var _this = this;

      if (this._isTransitioning || $(this._element).hasClass(ClassName.SHOW)) {
        return;
      }

      var actives = void 0;
      var activesData = void 0;

      if (this._parent) {
        actives = $.makeArray($(this._parent).children().children(Selector.ACTIVES));
        if (!actives.length) {
          actives = null;
        }
      }

      if (actives) {
        activesData = $(actives).data(DATA_KEY);
        if (activesData && activesData._isTransitioning) {
          return;
        }
      }

      var startEvent = $.Event(Event.SHOW);
      $(this._element).trigger(startEvent);
      if (startEvent.isDefaultPrevented()) {
        return;
      }

      if (actives) {
        Collapse._jQueryInterface.call($(actives), 'hide');
        if (!activesData) {
          $(actives).data(DATA_KEY, null);
        }
      }

      var dimension = this._getDimension();

      $(this._element).removeClass(ClassName.COLLAPSE).addClass(ClassName.COLLAPSING);

      this._element.style[dimension] = 0;

      if (this._triggerArray.length) {
        $(this._triggerArray).removeClass(ClassName.COLLAPSED).attr('aria-expanded', true);
      }

      this.setTransitioning(true);

      var complete = function complete() {
        $(_this._element).removeClass(ClassName.COLLAPSING).addClass(ClassName.COLLAPSE).addClass(ClassName.SHOW);

        _this._element.style[dimension] = '';

        _this.setTransitioning(false);

        $(_this._element).trigger(Event.SHOWN);
      };

      if (!Util.supportsTransitionEnd()) {
        complete();
        return;
      }

      var capitalizedDimension = dimension[0].toUpperCase() + dimension.slice(1);
      var scrollSize = 'scroll' + capitalizedDimension;

      $(this._element).one(Util.TRANSITION_END, complete).emulateTransitionEnd(TRANSITION_DURATION);

      this._element.style[dimension] = this._element[scrollSize] + 'px';
    };

    Collapse.prototype.hide = function hide() {
      var _this2 = this;

      if (this._isTransitioning || !$(this._element).hasClass(ClassName.SHOW)) {
        return;
      }

      var startEvent = $.Event(Event.HIDE);
      $(this._element).trigger(startEvent);
      if (startEvent.isDefaultPrevented()) {
        return;
      }

      var dimension = this._getDimension();

      this._element.style[dimension] = this._element.getBoundingClientRect()[dimension] + 'px';

      Util.reflow(this._element);

      $(this._element).addClass(ClassName.COLLAPSING).removeClass(ClassName.COLLAPSE).removeClass(ClassName.SHOW);

      if (this._triggerArray.length) {
        for (var i = 0; i < this._triggerArray.length; i++) {
          var trigger = this._triggerArray[i];
          var selector = Util.getSelectorFromElement(trigger);
          if (selector !== null) {
            var $elem = $(selector);
            if (!$elem.hasClass(ClassName.SHOW)) {
              $(trigger).addClass(ClassName.COLLAPSED).attr('aria-expanded', false);
            }
          }
        }
      }

      this.setTransitioning(true);

      var complete = function complete() {
        _this2.setTransitioning(false);
        $(_this2._element).removeClass(ClassName.COLLAPSING).addClass(ClassName.COLLAPSE).trigger(Event.HIDDEN);
      };

      this._element.style[dimension] = '';

      if (!Util.supportsTransitionEnd()) {
        complete();
        return;
      }

      $(this._element).one(Util.TRANSITION_END, complete).emulateTransitionEnd(TRANSITION_DURATION);
    };

    Collapse.prototype.setTransitioning = function setTransitioning(isTransitioning) {
      this._isTransitioning = isTransitioning;
    };

    Collapse.prototype.dispose = function dispose() {
      $.removeData(this._element, DATA_KEY);

      this._config = null;
      this._parent = null;
      this._element = null;
      this._triggerArray = null;
      this._isTransitioning = null;
    };

    // private

    Collapse.prototype._getConfig = function _getConfig(config) {
      config = $.extend({}, Default, config);
      config.toggle = Boolean(config.toggle); // coerce string values
      Util.typeCheckConfig(NAME, config, DefaultType);
      return config;
    };

    Collapse.prototype._getDimension = function _getDimension() {
      var hasWidth = $(this._element).hasClass(Dimension.WIDTH);
      return hasWidth ? Dimension.WIDTH : Dimension.HEIGHT;
    };

    Collapse.prototype._getParent = function _getParent() {
      var _this3 = this;

      var parent = $(this._config.parent)[0];
      var selector = '[data-toggle="collapse"][data-parent="' + this._config.parent + '"]';

      $(parent).find(selector).each(function (i, element) {
        _this3._addAriaAndCollapsedClass(Collapse._getTargetFromElement(element), [element]);
      });

      return parent;
    };

    Collapse.prototype._addAriaAndCollapsedClass = function _addAriaAndCollapsedClass(element, triggerArray) {
      if (element) {
        var isOpen = $(element).hasClass(ClassName.SHOW);

        if (triggerArray.length) {
          $(triggerArray).toggleClass(ClassName.COLLAPSED, !isOpen).attr('aria-expanded', isOpen);
        }
      }
    };

    // static

    Collapse._getTargetFromElement = function _getTargetFromElement(element) {
      var selector = Util.getSelectorFromElement(element);
      return selector ? $(selector)[0] : null;
    };

    Collapse._jQueryInterface = function _jQueryInterface(config) {
      return this.each(function () {
        var $this = $(this);
        var data = $this.data(DATA_KEY);
        var _config = $.extend({}, Default, $this.data(), (typeof config === 'undefined' ? 'undefined' : _typeof(config)) === 'object' && config);

        if (!data && _config.toggle && /show|hide/.test(config)) {
          _config.toggle = false;
        }

        if (!data) {
          data = new Collapse(this, _config);
          $this.data(DATA_KEY, data);
        }

        if (typeof config === 'string') {
          if (data[config] === undefined) {
            throw new Error('No method named "' + config + '"');
          }
          data[config]();
        }
      });
    };

    _createClass(Collapse, null, [{
      key: 'VERSION',
      get: function get() {
        return VERSION;
      }
    }, {
      key: 'Default',
      get: function get() {
        return Default;
      }
    }]);

    return Collapse;
  }();

  /**
   * ------------------------------------------------------------------------
   * Data Api implementation
   * ------------------------------------------------------------------------
   */

  $(document).on(Event.CLICK_DATA_API, Selector.DATA_TOGGLE, function (event) {
    if (!/input|textarea/i.test(event.target.tagName)) {
      event.preventDefault();
    }

    var $trigger = $(this);
    var selector = Util.getSelectorFromElement(this);
    $(selector).each(function () {
      var $target = $(this);
      var data = $target.data(DATA_KEY);
      var config = data ? 'toggle' : $trigger.data();
      Collapse._jQueryInterface.call($target, config);
    });
  });

  /**
   * ------------------------------------------------------------------------
   * jQuery
   * ------------------------------------------------------------------------
   */

  $.fn[NAME] = Collapse._jQueryInterface;
  $.fn[NAME].Constructor = Collapse;
  $.fn[NAME].noConflict = function () {
    $.fn[NAME] = JQUERY_NO_CONFLICT;
    return Collapse._jQueryInterface;
  };

  return Collapse;
}(jQuery);
var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/**
 * --------------------------------------------------------------------------
 * Bootstrap (v4.0.0-beta): dropdown.js
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * --------------------------------------------------------------------------
 */

var Dropdown = function ($) {

  /**
   * Check for Popper dependency
   * Popper - https://popper.js.org
   */
  if (typeof Popper === 'undefined') {
    throw new Error('Bootstrap dropdown require Popper.js (https://popper.js.org)');
  }

  /**
   * ------------------------------------------------------------------------
   * Constants
   * ------------------------------------------------------------------------
   */

  var NAME = 'dropdown';
  var VERSION = '4.0.0-beta';
  var DATA_KEY = 'bs.dropdown';
  var EVENT_KEY = '.' + DATA_KEY;
  var DATA_API_KEY = '.data-api';
  var JQUERY_NO_CONFLICT = $.fn[NAME];
  var ESCAPE_KEYCODE = 27; // KeyboardEvent.which value for Escape (Esc) key
  var SPACE_KEYCODE = 32; // KeyboardEvent.which value for space key
  var TAB_KEYCODE = 9; // KeyboardEvent.which value for tab key
  var ARROW_UP_KEYCODE = 38; // KeyboardEvent.which value for up arrow key
  var ARROW_DOWN_KEYCODE = 40; // KeyboardEvent.which value for down arrow key
  var RIGHT_MOUSE_BUTTON_WHICH = 3; // MouseEvent.which value for the right button (assuming a right-handed mouse)
  var REGEXP_KEYDOWN = new RegExp(ARROW_UP_KEYCODE + '|' + ARROW_DOWN_KEYCODE + '|' + ESCAPE_KEYCODE);

  var Event = {
    HIDE: 'hide' + EVENT_KEY,
    HIDDEN: 'hidden' + EVENT_KEY,
    SHOW: 'show' + EVENT_KEY,
    SHOWN: 'shown' + EVENT_KEY,
    CLICK: 'click' + EVENT_KEY,
    CLICK_DATA_API: 'click' + EVENT_KEY + DATA_API_KEY,
    KEYDOWN_DATA_API: 'keydown' + EVENT_KEY + DATA_API_KEY,
    KEYUP_DATA_API: 'keyup' + EVENT_KEY + DATA_API_KEY
  };

  var ClassName = {
    DISABLED: 'disabled',
    SHOW: 'show',
    DROPUP: 'dropup',
    MENURIGHT: 'dropdown-menu-right',
    MENULEFT: 'dropdown-menu-left'
  };

  var Selector = {
    DATA_TOGGLE: '[data-toggle="dropdown"]',
    FORM_CHILD: '.dropdown form',
    MENU: '.dropdown-menu',
    NAVBAR_NAV: '.navbar-nav',
    VISIBLE_ITEMS: '.dropdown-menu .dropdown-item:not(.disabled)'
  };

  var AttachmentMap = {
    TOP: 'top-start',
    TOPEND: 'top-end',
    BOTTOM: 'bottom-start',
    BOTTOMEND: 'bottom-end'
  };

  var Default = {
    placement: AttachmentMap.BOTTOM,
    offset: 0,
    flip: true
  };

  var DefaultType = {
    placement: 'string',
    offset: '(number|string)',
    flip: 'boolean'

    /**
     * ------------------------------------------------------------------------
     * Class Definition
     * ------------------------------------------------------------------------
     */

  };
  var Dropdown = function () {
    function Dropdown(element, config) {
      _classCallCheck(this, Dropdown);

      this._element = element;
      this._popper = null;
      this._config = this._getConfig(config);
      this._menu = this._getMenuElement();
      this._inNavbar = this._detectNavbar();

      this._addEventListeners();
    }

    // getters

    // public

    Dropdown.prototype.toggle = function toggle() {
      if (this._element.disabled || $(this._element).hasClass(ClassName.DISABLED)) {
        return;
      }

      var parent = Dropdown._getParentFromElement(this._element);
      var isActive = $(this._menu).hasClass(ClassName.SHOW);

      Dropdown._clearMenus();

      if (isActive) {
        return;
      }

      var relatedTarget = {
        relatedTarget: this._element
      };
      var showEvent = $.Event(Event.SHOW, relatedTarget);

      $(parent).trigger(showEvent);

      if (showEvent.isDefaultPrevented()) {
        return;
      }

      var element = this._element;
      // for dropup with alignment we use the parent as popper container
      if ($(parent).hasClass(ClassName.DROPUP)) {
        if ($(this._menu).hasClass(ClassName.MENULEFT) || $(this._menu).hasClass(ClassName.MENURIGHT)) {
          element = parent;
        }
      }
      this._popper = new Popper(element, this._menu, this._getPopperConfig());

      // if this is a touch-enabled device we add extra
      // empty mouseover listeners to the body's immediate children;
      // only needed because of broken event delegation on iOS
      // https://www.quirksmode.org/blog/archives/2014/02/mouse_event_bub.html
      if ('ontouchstart' in document.documentElement && !$(parent).closest(Selector.NAVBAR_NAV).length) {
        $('body').children().on('mouseover', null, $.noop);
      }

      this._element.focus();
      this._element.setAttribute('aria-expanded', true);

      $(this._menu).toggleClass(ClassName.SHOW);
      $(parent).toggleClass(ClassName.SHOW).trigger($.Event(Event.SHOWN, relatedTarget));
    };

    Dropdown.prototype.dispose = function dispose() {
      $.removeData(this._element, DATA_KEY);
      $(this._element).off(EVENT_KEY);
      this._element = null;
      this._menu = null;
      if (this._popper !== null) {
        this._popper.destroy();
      }
      this._popper = null;
    };

    Dropdown.prototype.update = function update() {
      this._inNavbar = this._detectNavbar();
      if (this._popper !== null) {
        this._popper.scheduleUpdate();
      }
    };

    // private

    Dropdown.prototype._addEventListeners = function _addEventListeners() {
      var _this = this;

      $(this._element).on(Event.CLICK, function (event) {
        event.preventDefault();
        event.stopPropagation();
        _this.toggle();
      });
    };

    Dropdown.prototype._getConfig = function _getConfig(config) {
      var elementData = $(this._element).data();
      if (elementData.placement !== undefined) {
        elementData.placement = AttachmentMap[elementData.placement.toUpperCase()];
      }

      config = $.extend({}, this.constructor.Default, $(this._element).data(), config);

      Util.typeCheckConfig(NAME, config, this.constructor.DefaultType);

      return config;
    };

    Dropdown.prototype._getMenuElement = function _getMenuElement() {
      if (!this._menu) {
        var parent = Dropdown._getParentFromElement(this._element);
        this._menu = $(parent).find(Selector.MENU)[0];
      }
      return this._menu;
    };

    Dropdown.prototype._getPlacement = function _getPlacement() {
      var $parentDropdown = $(this._element).parent();
      var placement = this._config.placement;

      // Handle dropup
      if ($parentDropdown.hasClass(ClassName.DROPUP) || this._config.placement === AttachmentMap.TOP) {
        placement = AttachmentMap.TOP;
        if ($(this._menu).hasClass(ClassName.MENURIGHT)) {
          placement = AttachmentMap.TOPEND;
        }
      } else if ($(this._menu).hasClass(ClassName.MENURIGHT)) {
        placement = AttachmentMap.BOTTOMEND;
      }
      return placement;
    };

    Dropdown.prototype._detectNavbar = function _detectNavbar() {
      return $(this._element).closest('.navbar').length > 0;
    };

    Dropdown.prototype._getPopperConfig = function _getPopperConfig() {
      var popperConfig = {
        placement: this._getPlacement(),
        modifiers: {
          offset: {
            offset: this._config.offset
          },
          flip: {
            enabled: this._config.flip
          }
        }

        // Disable Popper.js for Dropdown in Navbar
      };if (this._inNavbar) {
        popperConfig.modifiers.applyStyle = {
          enabled: !this._inNavbar
        };
      }
      return popperConfig;
    };

    // static

    Dropdown._jQueryInterface = function _jQueryInterface(config) {
      return this.each(function () {
        var data = $(this).data(DATA_KEY);
        var _config = (typeof config === 'undefined' ? 'undefined' : _typeof(config)) === 'object' ? config : null;

        if (!data) {
          data = new Dropdown(this, _config);
          $(this).data(DATA_KEY, data);
        }

        if (typeof config === 'string') {
          if (data[config] === undefined) {
            throw new Error('No method named "' + config + '"');
          }
          data[config]();
        }
      });
    };

    Dropdown._clearMenus = function _clearMenus(event) {
      if (event && (event.which === RIGHT_MOUSE_BUTTON_WHICH || event.type === 'keyup' && event.which !== TAB_KEYCODE)) {
        return;
      }

      var toggles = $.makeArray($(Selector.DATA_TOGGLE));
      for (var i = 0; i < toggles.length; i++) {
        var parent = Dropdown._getParentFromElement(toggles[i]);
        var context = $(toggles[i]).data(DATA_KEY);
        var relatedTarget = {
          relatedTarget: toggles[i]
        };

        if (!context) {
          continue;
        }

        var dropdownMenu = context._menu;
        if (!$(parent).hasClass(ClassName.SHOW)) {
          continue;
        }

        if (event && (event.type === 'click' && /input|textarea/i.test(event.target.tagName) || event.type === 'keyup' && event.which === TAB_KEYCODE) && $.contains(parent, event.target)) {
          continue;
        }

        var hideEvent = $.Event(Event.HIDE, relatedTarget);
        $(parent).trigger(hideEvent);
        if (hideEvent.isDefaultPrevented()) {
          continue;
        }

        // if this is a touch-enabled device we remove the extra
        // empty mouseover listeners we added for iOS support
        if ('ontouchstart' in document.documentElement) {
          $('body').children().off('mouseover', null, $.noop);
        }

        toggles[i].setAttribute('aria-expanded', 'false');

        $(dropdownMenu).removeClass(ClassName.SHOW);
        $(parent).removeClass(ClassName.SHOW).trigger($.Event(Event.HIDDEN, relatedTarget));
      }
    };

    Dropdown._getParentFromElement = function _getParentFromElement(element) {
      var parent = void 0;
      var selector = Util.getSelectorFromElement(element);

      if (selector) {
        parent = $(selector)[0];
      }

      return parent || element.parentNode;
    };

    Dropdown._dataApiKeydownHandler = function _dataApiKeydownHandler(event) {
      if (!REGEXP_KEYDOWN.test(event.which) || /button/i.test(event.target.tagName) && event.which === SPACE_KEYCODE || /input|textarea/i.test(event.target.tagName)) {
        return;
      }

      event.preventDefault();
      event.stopPropagation();

      if (this.disabled || $(this).hasClass(ClassName.DISABLED)) {
        return;
      }

      var parent = Dropdown._getParentFromElement(this);
      var isActive = $(parent).hasClass(ClassName.SHOW);

      if (!isActive && (event.which !== ESCAPE_KEYCODE || event.which !== SPACE_KEYCODE) || isActive && (event.which === ESCAPE_KEYCODE || event.which === SPACE_KEYCODE)) {

        if (event.which === ESCAPE_KEYCODE) {
          var toggle = $(parent).find(Selector.DATA_TOGGLE)[0];
          $(toggle).trigger('focus');
        }

        $(this).trigger('click');
        return;
      }

      var items = $(parent).find(Selector.VISIBLE_ITEMS).get();

      if (!items.length) {
        return;
      }

      var index = items.indexOf(event.target);

      if (event.which === ARROW_UP_KEYCODE && index > 0) {
        // up
        index--;
      }

      if (event.which === ARROW_DOWN_KEYCODE && index < items.length - 1) {
        // down
        index++;
      }

      if (index < 0) {
        index = 0;
      }

      items[index].focus();
    };

    _createClass(Dropdown, null, [{
      key: 'VERSION',
      get: function get() {
        return VERSION;
      }
    }, {
      key: 'Default',
      get: function get() {
        return Default;
      }
    }, {
      key: 'DefaultType',
      get: function get() {
        return DefaultType;
      }
    }]);

    return Dropdown;
  }();

  /**
   * ------------------------------------------------------------------------
   * Data Api implementation
   * ------------------------------------------------------------------------
   */

  $(document).on(Event.KEYDOWN_DATA_API, Selector.DATA_TOGGLE, Dropdown._dataApiKeydownHandler).on(Event.KEYDOWN_DATA_API, Selector.MENU, Dropdown._dataApiKeydownHandler).on(Event.CLICK_DATA_API + ' ' + Event.KEYUP_DATA_API, Dropdown._clearMenus).on(Event.CLICK_DATA_API, Selector.DATA_TOGGLE, function (event) {
    event.preventDefault();
    event.stopPropagation();
    Dropdown._jQueryInterface.call($(this), 'toggle');
  }).on(Event.CLICK_DATA_API, Selector.FORM_CHILD, function (e) {
    e.stopPropagation();
  });

  /**
   * ------------------------------------------------------------------------
   * jQuery
   * ------------------------------------------------------------------------
   */

  $.fn[NAME] = Dropdown._jQueryInterface;
  $.fn[NAME].Constructor = Dropdown;
  $.fn[NAME].noConflict = function () {
    $.fn[NAME] = JQUERY_NO_CONFLICT;
    return Dropdown._jQueryInterface;
  };

  return Dropdown;
}(jQuery); /* global Popper */
var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/**
 * --------------------------------------------------------------------------
 * Bootstrap (v4.0.0-beta): modal.js
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * --------------------------------------------------------------------------
 */

var Modal = function ($) {

  /**
   * ------------------------------------------------------------------------
   * Constants
   * ------------------------------------------------------------------------
   */

  var NAME = 'modal';
  var VERSION = '4.0.0-beta';
  var DATA_KEY = 'bs.modal';
  var EVENT_KEY = '.' + DATA_KEY;
  var DATA_API_KEY = '.data-api';
  var JQUERY_NO_CONFLICT = $.fn[NAME];
  var TRANSITION_DURATION = 300;
  var BACKDROP_TRANSITION_DURATION = 150;
  var ESCAPE_KEYCODE = 27; // KeyboardEvent.which value for Escape (Esc) key

  var Default = {
    backdrop: true,
    keyboard: true,
    focus: true,
    show: true
  };

  var DefaultType = {
    backdrop: '(boolean|string)',
    keyboard: 'boolean',
    focus: 'boolean',
    show: 'boolean'
  };

  var Event = {
    HIDE: 'hide' + EVENT_KEY,
    HIDDEN: 'hidden' + EVENT_KEY,
    SHOW: 'show' + EVENT_KEY,
    SHOWN: 'shown' + EVENT_KEY,
    FOCUSIN: 'focusin' + EVENT_KEY,
    RESIZE: 'resize' + EVENT_KEY,
    CLICK_DISMISS: 'click.dismiss' + EVENT_KEY,
    KEYDOWN_DISMISS: 'keydown.dismiss' + EVENT_KEY,
    MOUSEUP_DISMISS: 'mouseup.dismiss' + EVENT_KEY,
    MOUSEDOWN_DISMISS: 'mousedown.dismiss' + EVENT_KEY,
    CLICK_DATA_API: 'click' + EVENT_KEY + DATA_API_KEY
  };

  var ClassName = {
    SCROLLBAR_MEASURER: 'modal-scrollbar-measure',
    BACKDROP: 'modal-backdrop',
    OPEN: 'modal-open',
    FADE: 'fade',
    SHOW: 'show'
  };

  var Selector = {
    DIALOG: '.modal-dialog',
    DATA_TOGGLE: '[data-toggle="modal"]',
    DATA_DISMISS: '[data-dismiss="modal"]',
    FIXED_CONTENT: '.fixed-top, .fixed-bottom, .is-fixed, .sticky-top',
    NAVBAR_TOGGLER: '.navbar-toggler'

    /**
     * ------------------------------------------------------------------------
     * Class Definition
     * ------------------------------------------------------------------------
     */

  };
  var Modal = function () {
    function Modal(element, config) {
      _classCallCheck(this, Modal);

      this._config = this._getConfig(config);
      this._element = element;
      this._dialog = $(element).find(Selector.DIALOG)[0];
      this._backdrop = null;
      this._isShown = false;
      this._isBodyOverflowing = false;
      this._ignoreBackdropClick = false;
      this._originalBodyPadding = 0;
      this._scrollbarWidth = 0;
    }

    // getters

    // public

    Modal.prototype.toggle = function toggle(relatedTarget) {
      return this._isShown ? this.hide() : this.show(relatedTarget);
    };

    Modal.prototype.show = function show(relatedTarget) {
      var _this = this;

      if (this._isTransitioning) {
        return;
      }

      if (Util.supportsTransitionEnd() && $(this._element).hasClass(ClassName.FADE)) {
        this._isTransitioning = true;
      }

      var showEvent = $.Event(Event.SHOW, {
        relatedTarget: relatedTarget
      });

      $(this._element).trigger(showEvent);

      if (this._isShown || showEvent.isDefaultPrevented()) {
        return;
      }

      this._isShown = true;

      this._checkScrollbar();
      this._setScrollbar();

      $(document.body).addClass(ClassName.OPEN);

      this._setEscapeEvent();
      this._setResizeEvent();

      $(this._element).on(Event.CLICK_DISMISS, Selector.DATA_DISMISS, function (event) {
        return _this.hide(event);
      });

      $(this._dialog).on(Event.MOUSEDOWN_DISMISS, function () {
        $(_this._element).one(Event.MOUSEUP_DISMISS, function (event) {
          if ($(event.target).is(_this._element)) {
            _this._ignoreBackdropClick = true;
          }
        });
      });

      this._showBackdrop(function () {
        return _this._showElement(relatedTarget);
      });
    };

    Modal.prototype.hide = function hide(event) {
      var _this2 = this;

      if (event) {
        event.preventDefault();
      }

      if (this._isTransitioning || !this._isShown) {
        return;
      }

      var transition = Util.supportsTransitionEnd() && $(this._element).hasClass(ClassName.FADE);

      if (transition) {
        this._isTransitioning = true;
      }

      var hideEvent = $.Event(Event.HIDE);

      $(this._element).trigger(hideEvent);

      if (!this._isShown || hideEvent.isDefaultPrevented()) {
        return;
      }

      this._isShown = false;

      this._setEscapeEvent();
      this._setResizeEvent();

      $(document).off(Event.FOCUSIN);

      $(this._element).removeClass(ClassName.SHOW);

      $(this._element).off(Event.CLICK_DISMISS);
      $(this._dialog).off(Event.MOUSEDOWN_DISMISS);

      if (transition) {

        $(this._element).one(Util.TRANSITION_END, function (event) {
          return _this2._hideModal(event);
        }).emulateTransitionEnd(TRANSITION_DURATION);
      } else {
        this._hideModal();
      }
    };

    Modal.prototype.dispose = function dispose() {
      $.removeData(this._element, DATA_KEY);

      $(window, document, this._element, this._backdrop).off(EVENT_KEY);

      this._config = null;
      this._element = null;
      this._dialog = null;
      this._backdrop = null;
      this._isShown = null;
      this._isBodyOverflowing = null;
      this._ignoreBackdropClick = null;
      this._scrollbarWidth = null;
    };

    Modal.prototype.handleUpdate = function handleUpdate() {
      this._adjustDialog();
    };

    // private

    Modal.prototype._getConfig = function _getConfig(config) {
      config = $.extend({}, Default, config);
      Util.typeCheckConfig(NAME, config, DefaultType);
      return config;
    };

    Modal.prototype._showElement = function _showElement(relatedTarget) {
      var _this3 = this;

      var transition = Util.supportsTransitionEnd() && $(this._element).hasClass(ClassName.FADE);

      if (!this._element.parentNode || this._element.parentNode.nodeType !== Node.ELEMENT_NODE) {
        // don't move modals dom position
        document.body.appendChild(this._element);
      }

      this._element.style.display = 'block';
      this._element.removeAttribute('aria-hidden');
      this._element.scrollTop = 0;

      if (transition) {
        Util.reflow(this._element);
      }

      $(this._element).addClass(ClassName.SHOW);

      if (this._config.focus) {
        this._enforceFocus();
      }

      var shownEvent = $.Event(Event.SHOWN, {
        relatedTarget: relatedTarget
      });

      var transitionComplete = function transitionComplete() {
        if (_this3._config.focus) {
          _this3._element.focus();
        }
        _this3._isTransitioning = false;
        $(_this3._element).trigger(shownEvent);
      };

      if (transition) {
        $(this._dialog).one(Util.TRANSITION_END, transitionComplete).emulateTransitionEnd(TRANSITION_DURATION);
      } else {
        transitionComplete();
      }
    };

    Modal.prototype._enforceFocus = function _enforceFocus() {
      var _this4 = this;

      $(document).off(Event.FOCUSIN) // guard against infinite focus loop
      .on(Event.FOCUSIN, function (event) {
        if (document !== event.target && _this4._element !== event.target && !$(_this4._element).has(event.target).length) {
          _this4._element.focus();
        }
      });
    };

    Modal.prototype._setEscapeEvent = function _setEscapeEvent() {
      var _this5 = this;

      if (this._isShown && this._config.keyboard) {
        $(this._element).on(Event.KEYDOWN_DISMISS, function (event) {
          if (event.which === ESCAPE_KEYCODE) {
            event.preventDefault();
            _this5.hide();
          }
        });
      } else if (!this._isShown) {
        $(this._element).off(Event.KEYDOWN_DISMISS);
      }
    };

    Modal.prototype._setResizeEvent = function _setResizeEvent() {
      var _this6 = this;

      if (this._isShown) {
        $(window).on(Event.RESIZE, function (event) {
          return _this6.handleUpdate(event);
        });
      } else {
        $(window).off(Event.RESIZE);
      }
    };

    Modal.prototype._hideModal = function _hideModal() {
      var _this7 = this;

      this._element.style.display = 'none';
      this._element.setAttribute('aria-hidden', true);
      this._isTransitioning = false;
      this._showBackdrop(function () {
        $(document.body).removeClass(ClassName.OPEN);
        _this7._resetAdjustments();
        _this7._resetScrollbar();
        $(_this7._element).trigger(Event.HIDDEN);
      });
    };

    Modal.prototype._removeBackdrop = function _removeBackdrop() {
      if (this._backdrop) {
        $(this._backdrop).remove();
        this._backdrop = null;
      }
    };

    Modal.prototype._showBackdrop = function _showBackdrop(callback) {
      var _this8 = this;

      var animate = $(this._element).hasClass(ClassName.FADE) ? ClassName.FADE : '';

      if (this._isShown && this._config.backdrop) {
        var doAnimate = Util.supportsTransitionEnd() && animate;

        this._backdrop = document.createElement('div');
        this._backdrop.className = ClassName.BACKDROP;

        if (animate) {
          $(this._backdrop).addClass(animate);
        }

        $(this._backdrop).appendTo(document.body);

        $(this._element).on(Event.CLICK_DISMISS, function (event) {
          if (_this8._ignoreBackdropClick) {
            _this8._ignoreBackdropClick = false;
            return;
          }
          if (event.target !== event.currentTarget) {
            return;
          }
          if (_this8._config.backdrop === 'static') {
            _this8._element.focus();
          } else {
            _this8.hide();
          }
        });

        if (doAnimate) {
          Util.reflow(this._backdrop);
        }

        $(this._backdrop).addClass(ClassName.SHOW);

        if (!callback) {
          return;
        }

        if (!doAnimate) {
          callback();
          return;
        }

        $(this._backdrop).one(Util.TRANSITION_END, callback).emulateTransitionEnd(BACKDROP_TRANSITION_DURATION);
      } else if (!this._isShown && this._backdrop) {
        $(this._backdrop).removeClass(ClassName.SHOW);

        var callbackRemove = function callbackRemove() {
          _this8._removeBackdrop();
          if (callback) {
            callback();
          }
        };

        if (Util.supportsTransitionEnd() && $(this._element).hasClass(ClassName.FADE)) {
          $(this._backdrop).one(Util.TRANSITION_END, callbackRemove).emulateTransitionEnd(BACKDROP_TRANSITION_DURATION);
        } else {
          callbackRemove();
        }
      } else if (callback) {
        callback();
      }
    };

    // ----------------------------------------------------------------------
    // the following methods are used to handle overflowing modals
    // todo (fat): these should probably be refactored out of modal.js
    // ----------------------------------------------------------------------

    Modal.prototype._adjustDialog = function _adjustDialog() {
      var isModalOverflowing = this._element.scrollHeight > document.documentElement.clientHeight;

      if (!this._isBodyOverflowing && isModalOverflowing) {
        this._element.style.paddingLeft = this._scrollbarWidth + 'px';
      }

      if (this._isBodyOverflowing && !isModalOverflowing) {
        this._element.style.paddingRight = this._scrollbarWidth + 'px';
      }
    };

    Modal.prototype._resetAdjustments = function _resetAdjustments() {
      this._element.style.paddingLeft = '';
      this._element.style.paddingRight = '';
    };

    Modal.prototype._checkScrollbar = function _checkScrollbar() {
      this._isBodyOverflowing = document.body.clientWidth < window.innerWidth;
      this._scrollbarWidth = this._getScrollbarWidth();
    };

    Modal.prototype._setScrollbar = function _setScrollbar() {
      var _this9 = this;

      if (this._isBodyOverflowing) {
        // Note: DOMNode.style.paddingRight returns the actual value or '' if not set
        //   while $(DOMNode).css('padding-right') returns the calculated value or 0 if not set

        // Adjust fixed content padding
        $(Selector.FIXED_CONTENT).each(function (index, element) {
          var actualPadding = $(element)[0].style.paddingRight;
          var calculatedPadding = $(element).css('padding-right');
          $(element).data('padding-right', actualPadding).css('padding-right', parseFloat(calculatedPadding) + _this9._scrollbarWidth + 'px');
        });

        // Adjust navbar-toggler margin
        $(Selector.NAVBAR_TOGGLER).each(function (index, element) {
          var actualMargin = $(element)[0].style.marginRight;
          var calculatedMargin = $(element).css('margin-right');
          $(element).data('margin-right', actualMargin).css('margin-right', parseFloat(calculatedMargin) + _this9._scrollbarWidth + 'px');
        });

        // Adjust body padding
        var actualPadding = document.body.style.paddingRight;
        var calculatedPadding = $('body').css('padding-right');
        $('body').data('padding-right', actualPadding).css('padding-right', parseFloat(calculatedPadding) + this._scrollbarWidth + 'px');
      }
    };

    Modal.prototype._resetScrollbar = function _resetScrollbar() {
      // Restore fixed content padding
      $(Selector.FIXED_CONTENT).each(function (index, element) {
        var padding = $(element).data('padding-right');
        if (typeof padding !== 'undefined') {
          $(element).css('padding-right', padding).removeData('padding-right');
        }
      });

      // Restore navbar-toggler margin
      $(Selector.NAVBAR_TOGGLER).each(function (index, element) {
        var margin = $(element).data('margin-right');
        if (typeof margin !== 'undefined') {
          $(element).css('margin-right', margin).removeData('margin-right');
        }
      });

      // Restore body padding
      var padding = $('body').data('padding-right');
      if (typeof padding !== 'undefined') {
        $('body').css('padding-right', padding).removeData('padding-right');
      }
    };

    Modal.prototype._getScrollbarWidth = function _getScrollbarWidth() {
      // thx d.walsh
      var scrollDiv = document.createElement('div');
      scrollDiv.className = ClassName.SCROLLBAR_MEASURER;
      document.body.appendChild(scrollDiv);
      var scrollbarWidth = scrollDiv.getBoundingClientRect().width - scrollDiv.clientWidth;
      document.body.removeChild(scrollDiv);
      return scrollbarWidth;
    };

    // static

    Modal._jQueryInterface = function _jQueryInterface(config, relatedTarget) {
      return this.each(function () {
        var data = $(this).data(DATA_KEY);
        var _config = $.extend({}, Modal.Default, $(this).data(), (typeof config === 'undefined' ? 'undefined' : _typeof(config)) === 'object' && config);

        if (!data) {
          data = new Modal(this, _config);
          $(this).data(DATA_KEY, data);
        }

        if (typeof config === 'string') {
          if (data[config] === undefined) {
            throw new Error('No method named "' + config + '"');
          }
          data[config](relatedTarget);
        } else if (_config.show) {
          data.show(relatedTarget);
        }
      });
    };

    _createClass(Modal, null, [{
      key: 'VERSION',
      get: function get() {
        return VERSION;
      }
    }, {
      key: 'Default',
      get: function get() {
        return Default;
      }
    }]);

    return Modal;
  }();

  /**
   * ------------------------------------------------------------------------
   * Data Api implementation
   * ------------------------------------------------------------------------
   */

  $(document).on(Event.CLICK_DATA_API, Selector.DATA_TOGGLE, function (event) {
    var _this10 = this;

    var target = void 0;
    var selector = Util.getSelectorFromElement(this);

    if (selector) {
      target = $(selector)[0];
    }

    var config = $(target).data(DATA_KEY) ? 'toggle' : $.extend({}, $(target).data(), $(this).data());

    if (this.tagName === 'A' || this.tagName === 'AREA') {
      event.preventDefault();
    }

    var $target = $(target).one(Event.SHOW, function (showEvent) {
      if (showEvent.isDefaultPrevented()) {
        // only register focus restorer if modal will actually get shown
        return;
      }

      $target.one(Event.HIDDEN, function () {
        if ($(_this10).is(':visible')) {
          _this10.focus();
        }
      });
    });

    Modal._jQueryInterface.call($(target), config, this);
  });

  /**
   * ------------------------------------------------------------------------
   * jQuery
   * ------------------------------------------------------------------------
   */

  $.fn[NAME] = Modal._jQueryInterface;
  $.fn[NAME].Constructor = Modal;
  $.fn[NAME].noConflict = function () {
    $.fn[NAME] = JQUERY_NO_CONFLICT;
    return Modal._jQueryInterface;
  };

  return Modal;
}(jQuery);
var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/**
 * --------------------------------------------------------------------------
 * Bootstrap (v4.0.0-beta): scrollspy.js
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * --------------------------------------------------------------------------
 */

var ScrollSpy = function ($) {

  /**
   * ------------------------------------------------------------------------
   * Constants
   * ------------------------------------------------------------------------
   */

  var NAME = 'scrollspy';
  var VERSION = '4.0.0-beta';
  var DATA_KEY = 'bs.scrollspy';
  var EVENT_KEY = '.' + DATA_KEY;
  var DATA_API_KEY = '.data-api';
  var JQUERY_NO_CONFLICT = $.fn[NAME];

  var Default = {
    offset: 10,
    method: 'auto',
    target: ''
  };

  var DefaultType = {
    offset: 'number',
    method: 'string',
    target: '(string|element)'
  };

  var Event = {
    ACTIVATE: 'activate' + EVENT_KEY,
    SCROLL: 'scroll' + EVENT_KEY,
    LOAD_DATA_API: 'load' + EVENT_KEY + DATA_API_KEY
  };

  var ClassName = {
    DROPDOWN_ITEM: 'dropdown-item',
    DROPDOWN_MENU: 'dropdown-menu',
    ACTIVE: 'active'
  };

  var Selector = {
    DATA_SPY: '[data-spy="scroll"]',
    ACTIVE: '.active',
    NAV_LIST_GROUP: '.nav, .list-group',
    NAV_LINKS: '.nav-link',
    LIST_ITEMS: '.list-group-item',
    DROPDOWN: '.dropdown',
    DROPDOWN_ITEMS: '.dropdown-item',
    DROPDOWN_TOGGLE: '.dropdown-toggle'
  };

  var OffsetMethod = {
    OFFSET: 'offset',
    POSITION: 'position'

    /**
     * ------------------------------------------------------------------------
     * Class Definition
     * ------------------------------------------------------------------------
     */

  };
  var ScrollSpy = function () {
    function ScrollSpy(element, config) {
      var _this = this;

      _classCallCheck(this, ScrollSpy);

      this._element = element;
      this._scrollElement = element.tagName === 'BODY' ? window : element;
      this._config = this._getConfig(config);
      this._selector = this._config.target + ' ' + Selector.NAV_LINKS + ',' + (this._config.target + ' ' + Selector.LIST_ITEMS + ',') + (this._config.target + ' ' + Selector.DROPDOWN_ITEMS);
      this._offsets = [];
      this._targets = [];
      this._activeTarget = null;
      this._scrollHeight = 0;

      $(this._scrollElement).on(Event.SCROLL, function (event) {
        return _this._process(event);
      });

      this.refresh();
      this._process();
    }

    // getters

    // public

    ScrollSpy.prototype.refresh = function refresh() {
      var _this2 = this;

      var autoMethod = this._scrollElement !== this._scrollElement.window ? OffsetMethod.POSITION : OffsetMethod.OFFSET;

      var offsetMethod = this._config.method === 'auto' ? autoMethod : this._config.method;

      var offsetBase = offsetMethod === OffsetMethod.POSITION ? this._getScrollTop() : 0;

      this._offsets = [];
      this._targets = [];

      this._scrollHeight = this._getScrollHeight();

      var targets = $.makeArray($(this._selector));

      targets.map(function (element) {
        var target = void 0;
        var targetSelector = Util.getSelectorFromElement(element);

        if (targetSelector) {
          target = $(targetSelector)[0];
        }

        if (target) {
          var targetBCR = target.getBoundingClientRect();
          if (targetBCR.width || targetBCR.height) {
            // todo (fat): remove sketch reliance on jQuery position/offset
            return [$(target)[offsetMethod]().top + offsetBase, targetSelector];
          }
        }
        return null;
      }).filter(function (item) {
        return item;
      }).sort(function (a, b) {
        return a[0] - b[0];
      }).forEach(function (item) {
        _this2._offsets.push(item[0]);
        _this2._targets.push(item[1]);
      });
    };

    ScrollSpy.prototype.dispose = function dispose() {
      $.removeData(this._element, DATA_KEY);
      $(this._scrollElement).off(EVENT_KEY);

      this._element = null;
      this._scrollElement = null;
      this._config = null;
      this._selector = null;
      this._offsets = null;
      this._targets = null;
      this._activeTarget = null;
      this._scrollHeight = null;
    };

    // private

    ScrollSpy.prototype._getConfig = function _getConfig(config) {
      config = $.extend({}, Default, config);

      if (typeof config.target !== 'string') {
        var id = $(config.target).attr('id');
        if (!id) {
          id = Util.getUID(NAME);
          $(config.target).attr('id', id);
        }
        config.target = '#' + id;
      }

      Util.typeCheckConfig(NAME, config, DefaultType);

      return config;
    };

    ScrollSpy.prototype._getScrollTop = function _getScrollTop() {
      return this._scrollElement === window ? this._scrollElement.pageYOffset : this._scrollElement.scrollTop;
    };

    ScrollSpy.prototype._getScrollHeight = function _getScrollHeight() {
      return this._scrollElement.scrollHeight || Math.max(document.body.scrollHeight, document.documentElement.scrollHeight);
    };

    ScrollSpy.prototype._getOffsetHeight = function _getOffsetHeight() {
      return this._scrollElement === window ? window.innerHeight : this._scrollElement.getBoundingClientRect().height;
    };

    ScrollSpy.prototype._process = function _process() {
      var scrollTop = this._getScrollTop() + this._config.offset;
      var scrollHeight = this._getScrollHeight();
      var maxScroll = this._config.offset + scrollHeight - this._getOffsetHeight();

      if (this._scrollHeight !== scrollHeight) {
        this.refresh();
      }

      if (scrollTop >= maxScroll) {
        var target = this._targets[this._targets.length - 1];

        if (this._activeTarget !== target) {
          this._activate(target);
        }
        return;
      }

      if (this._activeTarget && scrollTop < this._offsets[0] && this._offsets[0] > 0) {
        this._activeTarget = null;
        this._clear();
        return;
      }

      for (var i = this._offsets.length; i--;) {
        var isActiveTarget = this._activeTarget !== this._targets[i] && scrollTop >= this._offsets[i] && (this._offsets[i + 1] === undefined || scrollTop < this._offsets[i + 1]);

        if (isActiveTarget) {
          this._activate(this._targets[i]);
        }
      }
    };

    ScrollSpy.prototype._activate = function _activate(target) {
      this._activeTarget = target;

      this._clear();

      var queries = this._selector.split(',');
      queries = queries.map(function (selector) {
        return selector + '[data-target="' + target + '"],' + (selector + '[href="' + target + '"]');
      });

      var $link = $(queries.join(','));

      if ($link.hasClass(ClassName.DROPDOWN_ITEM)) {
        $link.closest(Selector.DROPDOWN).find(Selector.DROPDOWN_TOGGLE).addClass(ClassName.ACTIVE);
        $link.addClass(ClassName.ACTIVE);
      } else {
        // Set triggered link as active
        $link.addClass(ClassName.ACTIVE);
        // Set triggered links parents as active
        // With both <ul> and <nav> markup a parent is the previous sibling of any nav ancestor
        $link.parents(Selector.NAV_LIST_GROUP).prev(Selector.NAV_LINKS + ', ' + Selector.LIST_ITEMS).addClass(ClassName.ACTIVE);
      }

      $(this._scrollElement).trigger(Event.ACTIVATE, {
        relatedTarget: target
      });
    };

    ScrollSpy.prototype._clear = function _clear() {
      $(this._selector).filter(Selector.ACTIVE).removeClass(ClassName.ACTIVE);
    };

    // static

    ScrollSpy._jQueryInterface = function _jQueryInterface(config) {
      return this.each(function () {
        var data = $(this).data(DATA_KEY);
        var _config = (typeof config === 'undefined' ? 'undefined' : _typeof(config)) === 'object' && config;

        if (!data) {
          data = new ScrollSpy(this, _config);
          $(this).data(DATA_KEY, data);
        }

        if (typeof config === 'string') {
          if (data[config] === undefined) {
            throw new Error('No method named "' + config + '"');
          }
          data[config]();
        }
      });
    };

    _createClass(ScrollSpy, null, [{
      key: 'VERSION',
      get: function get() {
        return VERSION;
      }
    }, {
      key: 'Default',
      get: function get() {
        return Default;
      }
    }]);

    return ScrollSpy;
  }();

  /**
   * ------------------------------------------------------------------------
   * Data Api implementation
   * ------------------------------------------------------------------------
   */

  $(window).on(Event.LOAD_DATA_API, function () {
    var scrollSpys = $.makeArray($(Selector.DATA_SPY));

    for (var i = scrollSpys.length; i--;) {
      var $spy = $(scrollSpys[i]);
      ScrollSpy._jQueryInterface.call($spy, $spy.data());
    }
  });

  /**
   * ------------------------------------------------------------------------
   * jQuery
   * ------------------------------------------------------------------------
   */

  $.fn[NAME] = ScrollSpy._jQueryInterface;
  $.fn[NAME].Constructor = ScrollSpy;
  $.fn[NAME].noConflict = function () {
    $.fn[NAME] = JQUERY_NO_CONFLICT;
    return ScrollSpy._jQueryInterface;
  };

  return ScrollSpy;
}(jQuery);
var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/**
 * --------------------------------------------------------------------------
 * Bootstrap (v4.0.0-beta): tab.js
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * --------------------------------------------------------------------------
 */

var Tab = function ($) {

  /**
   * ------------------------------------------------------------------------
   * Constants
   * ------------------------------------------------------------------------
   */

  var NAME = 'tab';
  var VERSION = '4.0.0-beta';
  var DATA_KEY = 'bs.tab';
  var EVENT_KEY = '.' + DATA_KEY;
  var DATA_API_KEY = '.data-api';
  var JQUERY_NO_CONFLICT = $.fn[NAME];
  var TRANSITION_DURATION = 150;

  var Event = {
    HIDE: 'hide' + EVENT_KEY,
    HIDDEN: 'hidden' + EVENT_KEY,
    SHOW: 'show' + EVENT_KEY,
    SHOWN: 'shown' + EVENT_KEY,
    CLICK_DATA_API: 'click' + EVENT_KEY + DATA_API_KEY
  };

  var ClassName = {
    DROPDOWN_MENU: 'dropdown-menu',
    ACTIVE: 'active',
    DISABLED: 'disabled',
    FADE: 'fade',
    SHOW: 'show'
  };

  var Selector = {
    DROPDOWN: '.dropdown',
    NAV_LIST_GROUP: '.nav, .list-group',
    ACTIVE: '.active',
    DATA_TOGGLE: '[data-toggle="tab"], [data-toggle="pill"], [data-toggle="list"]',
    DROPDOWN_TOGGLE: '.dropdown-toggle',
    DROPDOWN_ACTIVE_CHILD: '> .dropdown-menu .active'

    /**
     * ------------------------------------------------------------------------
     * Class Definition
     * ------------------------------------------------------------------------
     */

  };
  var Tab = function () {
    function Tab(element) {
      _classCallCheck(this, Tab);

      this._element = element;
    }

    // getters

    // public

    Tab.prototype.show = function show() {
      var _this = this;

      if (this._element.parentNode && this._element.parentNode.nodeType === Node.ELEMENT_NODE && $(this._element).hasClass(ClassName.ACTIVE) || $(this._element).hasClass(ClassName.DISABLED)) {
        return;
      }

      var target = void 0;
      var previous = void 0;
      var listElement = $(this._element).closest(Selector.NAV_LIST_GROUP)[0];
      var selector = Util.getSelectorFromElement(this._element);

      if (listElement) {
        previous = $.makeArray($(listElement).find(Selector.ACTIVE));
        previous = previous[previous.length - 1];
      }

      var hideEvent = $.Event(Event.HIDE, {
        relatedTarget: this._element
      });

      var showEvent = $.Event(Event.SHOW, {
        relatedTarget: previous
      });

      if (previous) {
        $(previous).trigger(hideEvent);
      }

      $(this._element).trigger(showEvent);

      if (showEvent.isDefaultPrevented() || hideEvent.isDefaultPrevented()) {
        return;
      }

      if (selector) {
        target = $(selector)[0];
      }

      this._activate(this._element, listElement);

      var complete = function complete() {
        var hiddenEvent = $.Event(Event.HIDDEN, {
          relatedTarget: _this._element
        });

        var shownEvent = $.Event(Event.SHOWN, {
          relatedTarget: previous
        });

        $(previous).trigger(hiddenEvent);
        $(_this._element).trigger(shownEvent);
      };

      if (target) {
        this._activate(target, target.parentNode, complete);
      } else {
        complete();
      }
    };

    Tab.prototype.dispose = function dispose() {
      $.removeData(this._element, DATA_KEY);
      this._element = null;
    };

    // private

    Tab.prototype._activate = function _activate(element, container, callback) {
      var _this2 = this;

      var active = $(container).find(Selector.ACTIVE)[0];
      var isTransitioning = callback && Util.supportsTransitionEnd() && active && $(active).hasClass(ClassName.FADE);

      var complete = function complete() {
        return _this2._transitionComplete(element, active, isTransitioning, callback);
      };

      if (active && isTransitioning) {
        $(active).one(Util.TRANSITION_END, complete).emulateTransitionEnd(TRANSITION_DURATION);
      } else {
        complete();
      }

      if (active) {
        $(active).removeClass(ClassName.SHOW);
      }
    };

    Tab.prototype._transitionComplete = function _transitionComplete(element, active, isTransitioning, callback) {
      if (active) {
        $(active).removeClass(ClassName.ACTIVE);

        var dropdownChild = $(active.parentNode).find(Selector.DROPDOWN_ACTIVE_CHILD)[0];

        if (dropdownChild) {
          $(dropdownChild).removeClass(ClassName.ACTIVE);
        }

        active.setAttribute('aria-expanded', false);
      }

      $(element).addClass(ClassName.ACTIVE);
      element.setAttribute('aria-expanded', true);

      if (isTransitioning) {
        Util.reflow(element);
        $(element).addClass(ClassName.SHOW);
      } else {
        $(element).removeClass(ClassName.FADE);
      }

      if (element.parentNode && $(element.parentNode).hasClass(ClassName.DROPDOWN_MENU)) {

        var dropdownElement = $(element).closest(Selector.DROPDOWN)[0];
        if (dropdownElement) {
          $(dropdownElement).find(Selector.DROPDOWN_TOGGLE).addClass(ClassName.ACTIVE);
        }

        element.setAttribute('aria-expanded', true);
      }

      if (callback) {
        callback();
      }
    };

    // static

    Tab._jQueryInterface = function _jQueryInterface(config) {
      return this.each(function () {
        var $this = $(this);
        var data = $this.data(DATA_KEY);

        if (!data) {
          data = new Tab(this);
          $this.data(DATA_KEY, data);
        }

        if (typeof config === 'string') {
          if (data[config] === undefined) {
            throw new Error('No method named "' + config + '"');
          }
          data[config]();
        }
      });
    };

    _createClass(Tab, null, [{
      key: 'VERSION',
      get: function get() {
        return VERSION;
      }
    }]);

    return Tab;
  }();

  /**
   * ------------------------------------------------------------------------
   * Data Api implementation
   * ------------------------------------------------------------------------
   */

  $(document).on(Event.CLICK_DATA_API, Selector.DATA_TOGGLE, function (event) {
    event.preventDefault();
    Tab._jQueryInterface.call($(this), 'show');
  });

  /**
   * ------------------------------------------------------------------------
   * jQuery
   * ------------------------------------------------------------------------
   */

  $.fn[NAME] = Tab._jQueryInterface;
  $.fn[NAME].Constructor = Tab;
  $.fn[NAME].noConflict = function () {
    $.fn[NAME] = JQUERY_NO_CONFLICT;
    return Tab._jQueryInterface;
  };

  return Tab;
}(jQuery);
var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/**
 * --------------------------------------------------------------------------
 * Bootstrap (v4.0.0-beta): tooltip.js
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * --------------------------------------------------------------------------
 */

var Tooltip = function ($) {

  /**
   * Check for Popper dependency
   * Popper - https://popper.js.org
   */
  if (typeof Popper === 'undefined') {
    throw new Error('Bootstrap tooltips require Popper.js (https://popper.js.org)');
  }

  /**
   * ------------------------------------------------------------------------
   * Constants
   * ------------------------------------------------------------------------
   */

  var NAME = 'tooltip';
  var VERSION = '4.0.0-beta';
  var DATA_KEY = 'bs.tooltip';
  var EVENT_KEY = '.' + DATA_KEY;
  var JQUERY_NO_CONFLICT = $.fn[NAME];
  var TRANSITION_DURATION = 150;
  var CLASS_PREFIX = 'bs-tooltip';
  var BSCLS_PREFIX_REGEX = new RegExp('(^|\\s)' + CLASS_PREFIX + '\\S+', 'g');

  var DefaultType = {
    animation: 'boolean',
    template: 'string',
    title: '(string|element|function)',
    trigger: 'string',
    delay: '(number|object)',
    html: 'boolean',
    selector: '(string|boolean)',
    placement: '(string|function)',
    offset: '(number|string)',
    container: '(string|element|boolean)',
    fallbackPlacement: '(string|array)'
  };

  var AttachmentMap = {
    AUTO: 'auto',
    TOP: 'top',
    RIGHT: 'right',
    BOTTOM: 'bottom',
    LEFT: 'left'
  };

  var Default = {
    animation: true,
    template: '<div class="tooltip" role="tooltip">' + '<div class="arrow"></div>' + '<div class="tooltip-inner"></div></div>',
    trigger: 'hover focus',
    title: '',
    delay: 0,
    html: false,
    selector: false,
    placement: 'top',
    offset: 0,
    container: false,
    fallbackPlacement: 'flip'
  };

  var HoverState = {
    SHOW: 'show',
    OUT: 'out'
  };

  var Event = {
    HIDE: 'hide' + EVENT_KEY,
    HIDDEN: 'hidden' + EVENT_KEY,
    SHOW: 'show' + EVENT_KEY,
    SHOWN: 'shown' + EVENT_KEY,
    INSERTED: 'inserted' + EVENT_KEY,
    CLICK: 'click' + EVENT_KEY,
    FOCUSIN: 'focusin' + EVENT_KEY,
    FOCUSOUT: 'focusout' + EVENT_KEY,
    MOUSEENTER: 'mouseenter' + EVENT_KEY,
    MOUSELEAVE: 'mouseleave' + EVENT_KEY
  };

  var ClassName = {
    FADE: 'fade',
    SHOW: 'show'
  };

  var Selector = {
    TOOLTIP: '.tooltip',
    TOOLTIP_INNER: '.tooltip-inner',
    ARROW: '.arrow'
  };

  var Trigger = {
    HOVER: 'hover',
    FOCUS: 'focus',
    CLICK: 'click',
    MANUAL: 'manual'

    /**
     * ------------------------------------------------------------------------
     * Class Definition
     * ------------------------------------------------------------------------
     */

  };
  var Tooltip = function () {
    function Tooltip(element, config) {
      _classCallCheck(this, Tooltip);

      // private
      this._isEnabled = true;
      this._timeout = 0;
      this._hoverState = '';
      this._activeTrigger = {};
      this._popper = null;

      // protected
      this.element = element;
      this.config = this._getConfig(config);
      this.tip = null;

      this._setListeners();
    }

    // getters

    // public

    Tooltip.prototype.enable = function enable() {
      this._isEnabled = true;
    };

    Tooltip.prototype.disable = function disable() {
      this._isEnabled = false;
    };

    Tooltip.prototype.toggleEnabled = function toggleEnabled() {
      this._isEnabled = !this._isEnabled;
    };

    Tooltip.prototype.toggle = function toggle(event) {
      if (event) {
        var dataKey = this.constructor.DATA_KEY;
        var context = $(event.currentTarget).data(dataKey);

        if (!context) {
          context = new this.constructor(event.currentTarget, this._getDelegateConfig());
          $(event.currentTarget).data(dataKey, context);
        }

        context._activeTrigger.click = !context._activeTrigger.click;

        if (context._isWithActiveTrigger()) {
          context._enter(null, context);
        } else {
          context._leave(null, context);
        }
      } else {

        if ($(this.getTipElement()).hasClass(ClassName.SHOW)) {
          this._leave(null, this);
          return;
        }

        this._enter(null, this);
      }
    };

    Tooltip.prototype.dispose = function dispose() {
      clearTimeout(this._timeout);

      $.removeData(this.element, this.constructor.DATA_KEY);

      $(this.element).off(this.constructor.EVENT_KEY);
      $(this.element).closest('.modal').off('hide.bs.modal');

      if (this.tip) {
        $(this.tip).remove();
      }

      this._isEnabled = null;
      this._timeout = null;
      this._hoverState = null;
      this._activeTrigger = null;
      if (this._popper !== null) {
        this._popper.destroy();
      }
      this._popper = null;

      this.element = null;
      this.config = null;
      this.tip = null;
    };

    Tooltip.prototype.show = function show() {
      var _this = this;

      if ($(this.element).css('display') === 'none') {
        throw new Error('Please use show on visible elements');
      }

      var showEvent = $.Event(this.constructor.Event.SHOW);
      if (this.isWithContent() && this._isEnabled) {
        $(this.element).trigger(showEvent);

        var isInTheDom = $.contains(this.element.ownerDocument.documentElement, this.element);

        if (showEvent.isDefaultPrevented() || !isInTheDom) {
          return;
        }

        var tip = this.getTipElement();
        var tipId = Util.getUID(this.constructor.NAME);

        tip.setAttribute('id', tipId);
        this.element.setAttribute('aria-describedby', tipId);

        this.setContent();

        if (this.config.animation) {
          $(tip).addClass(ClassName.FADE);
        }

        var placement = typeof this.config.placement === 'function' ? this.config.placement.call(this, tip, this.element) : this.config.placement;

        var attachment = this._getAttachment(placement);
        this.addAttachmentClass(attachment);

        var container = this.config.container === false ? document.body : $(this.config.container);

        $(tip).data(this.constructor.DATA_KEY, this);

        if (!$.contains(this.element.ownerDocument.documentElement, this.tip)) {
          $(tip).appendTo(container);
        }

        $(this.element).trigger(this.constructor.Event.INSERTED);

        this._popper = new Popper(this.element, tip, {
          placement: attachment,
          modifiers: {
            offset: {
              offset: this.config.offset
            },
            flip: {
              behavior: this.config.fallbackPlacement
            },
            arrow: {
              element: Selector.ARROW
            }
          },
          onCreate: function onCreate(data) {
            if (data.originalPlacement !== data.placement) {
              _this._handlePopperPlacementChange(data);
            }
          },
          onUpdate: function onUpdate(data) {
            _this._handlePopperPlacementChange(data);
          }
        });

        $(tip).addClass(ClassName.SHOW);

        // if this is a touch-enabled device we add extra
        // empty mouseover listeners to the body's immediate children;
        // only needed because of broken event delegation on iOS
        // https://www.quirksmode.org/blog/archives/2014/02/mouse_event_bub.html
        if ('ontouchstart' in document.documentElement) {
          $('body').children().on('mouseover', null, $.noop);
        }

        var complete = function complete() {
          if (_this.config.animation) {
            _this._fixTransition();
          }
          var prevHoverState = _this._hoverState;
          _this._hoverState = null;

          $(_this.element).trigger(_this.constructor.Event.SHOWN);

          if (prevHoverState === HoverState.OUT) {
            _this._leave(null, _this);
          }
        };

        if (Util.supportsTransitionEnd() && $(this.tip).hasClass(ClassName.FADE)) {
          $(this.tip).one(Util.TRANSITION_END, complete).emulateTransitionEnd(Tooltip._TRANSITION_DURATION);
        } else {
          complete();
        }
      }
    };

    Tooltip.prototype.hide = function hide(callback) {
      var _this2 = this;

      var tip = this.getTipElement();
      var hideEvent = $.Event(this.constructor.Event.HIDE);
      var complete = function complete() {
        if (_this2._hoverState !== HoverState.SHOW && tip.parentNode) {
          tip.parentNode.removeChild(tip);
        }

        _this2._cleanTipClass();
        _this2.element.removeAttribute('aria-describedby');
        $(_this2.element).trigger(_this2.constructor.Event.HIDDEN);
        if (_this2._popper !== null) {
          _this2._popper.destroy();
        }

        if (callback) {
          callback();
        }
      };

      $(this.element).trigger(hideEvent);

      if (hideEvent.isDefaultPrevented()) {
        return;
      }

      $(tip).removeClass(ClassName.SHOW);

      // if this is a touch-enabled device we remove the extra
      // empty mouseover listeners we added for iOS support
      if ('ontouchstart' in document.documentElement) {
        $('body').children().off('mouseover', null, $.noop);
      }

      this._activeTrigger[Trigger.CLICK] = false;
      this._activeTrigger[Trigger.FOCUS] = false;
      this._activeTrigger[Trigger.HOVER] = false;

      if (Util.supportsTransitionEnd() && $(this.tip).hasClass(ClassName.FADE)) {

        $(tip).one(Util.TRANSITION_END, complete).emulateTransitionEnd(TRANSITION_DURATION);
      } else {
        complete();
      }

      this._hoverState = '';
    };

    Tooltip.prototype.update = function update() {
      if (this._popper !== null) {
        this._popper.scheduleUpdate();
      }
    };

    // protected

    Tooltip.prototype.isWithContent = function isWithContent() {
      return Boolean(this.getTitle());
    };

    Tooltip.prototype.addAttachmentClass = function addAttachmentClass(attachment) {
      $(this.getTipElement()).addClass(CLASS_PREFIX + '-' + attachment);
    };

    Tooltip.prototype.getTipElement = function getTipElement() {
      return this.tip = this.tip || $(this.config.template)[0];
    };

    Tooltip.prototype.setContent = function setContent() {
      var $tip = $(this.getTipElement());
      this.setElementContent($tip.find(Selector.TOOLTIP_INNER), this.getTitle());
      $tip.removeClass(ClassName.FADE + ' ' + ClassName.SHOW);
    };

    Tooltip.prototype.setElementContent = function setElementContent($element, content) {
      var html = this.config.html;
      if ((typeof content === 'undefined' ? 'undefined' : _typeof(content)) === 'object' && (content.nodeType || content.jquery)) {
        // content is a DOM node or a jQuery
        if (html) {
          if (!$(content).parent().is($element)) {
            $element.empty().append(content);
          }
        } else {
          $element.text($(content).text());
        }
      } else {
        $element[html ? 'html' : 'text'](content);
      }
    };

    Tooltip.prototype.getTitle = function getTitle() {
      var title = this.element.getAttribute('data-original-title');

      if (!title) {
        title = typeof this.config.title === 'function' ? this.config.title.call(this.element) : this.config.title;
      }

      return title;
    };

    // private

    Tooltip.prototype._getAttachment = function _getAttachment(placement) {
      return AttachmentMap[placement.toUpperCase()];
    };

    Tooltip.prototype._setListeners = function _setListeners() {
      var _this3 = this;

      var triggers = this.config.trigger.split(' ');

      triggers.forEach(function (trigger) {
        if (trigger === 'click') {
          $(_this3.element).on(_this3.constructor.Event.CLICK, _this3.config.selector, function (event) {
            return _this3.toggle(event);
          });
        } else if (trigger !== Trigger.MANUAL) {
          var eventIn = trigger === Trigger.HOVER ? _this3.constructor.Event.MOUSEENTER : _this3.constructor.Event.FOCUSIN;
          var eventOut = trigger === Trigger.HOVER ? _this3.constructor.Event.MOUSELEAVE : _this3.constructor.Event.FOCUSOUT;

          $(_this3.element).on(eventIn, _this3.config.selector, function (event) {
            return _this3._enter(event);
          }).on(eventOut, _this3.config.selector, function (event) {
            return _this3._leave(event);
          });
        }

        $(_this3.element).closest('.modal').on('hide.bs.modal', function () {
          return _this3.hide();
        });
      });

      if (this.config.selector) {
        this.config = $.extend({}, this.config, {
          trigger: 'manual',
          selector: ''
        });
      } else {
        this._fixTitle();
      }
    };

    Tooltip.prototype._fixTitle = function _fixTitle() {
      var titleType = _typeof(this.element.getAttribute('data-original-title'));
      if (this.element.getAttribute('title') || titleType !== 'string') {
        this.element.setAttribute('data-original-title', this.element.getAttribute('title') || '');
        this.element.setAttribute('title', '');
      }
    };

    Tooltip.prototype._enter = function _enter(event, context) {
      var dataKey = this.constructor.DATA_KEY;

      context = context || $(event.currentTarget).data(dataKey);

      if (!context) {
        context = new this.constructor(event.currentTarget, this._getDelegateConfig());
        $(event.currentTarget).data(dataKey, context);
      }

      if (event) {
        context._activeTrigger[event.type === 'focusin' ? Trigger.FOCUS : Trigger.HOVER] = true;
      }

      if ($(context.getTipElement()).hasClass(ClassName.SHOW) || context._hoverState === HoverState.SHOW) {
        context._hoverState = HoverState.SHOW;
        return;
      }

      clearTimeout(context._timeout);

      context._hoverState = HoverState.SHOW;

      if (!context.config.delay || !context.config.delay.show) {
        context.show();
        return;
      }

      context._timeout = setTimeout(function () {
        if (context._hoverState === HoverState.SHOW) {
          context.show();
        }
      }, context.config.delay.show);
    };

    Tooltip.prototype._leave = function _leave(event, context) {
      var dataKey = this.constructor.DATA_KEY;

      context = context || $(event.currentTarget).data(dataKey);

      if (!context) {
        context = new this.constructor(event.currentTarget, this._getDelegateConfig());
        $(event.currentTarget).data(dataKey, context);
      }

      if (event) {
        context._activeTrigger[event.type === 'focusout' ? Trigger.FOCUS : Trigger.HOVER] = false;
      }

      if (context._isWithActiveTrigger()) {
        return;
      }

      clearTimeout(context._timeout);

      context._hoverState = HoverState.OUT;

      if (!context.config.delay || !context.config.delay.hide) {
        context.hide();
        return;
      }

      context._timeout = setTimeout(function () {
        if (context._hoverState === HoverState.OUT) {
          context.hide();
        }
      }, context.config.delay.hide);
    };

    Tooltip.prototype._isWithActiveTrigger = function _isWithActiveTrigger() {
      for (var trigger in this._activeTrigger) {
        if (this._activeTrigger[trigger]) {
          return true;
        }
      }

      return false;
    };

    Tooltip.prototype._getConfig = function _getConfig(config) {
      config = $.extend({}, this.constructor.Default, $(this.element).data(), config);

      if (config.delay && typeof config.delay === 'number') {
        config.delay = {
          show: config.delay,
          hide: config.delay
        };
      }

      if (config.title && typeof config.title === 'number') {
        config.title = config.title.toString();
      }

      if (config.content && typeof config.content === 'number') {
        config.content = config.content.toString();
      }

      Util.typeCheckConfig(NAME, config, this.constructor.DefaultType);

      return config;
    };

    Tooltip.prototype._getDelegateConfig = function _getDelegateConfig() {
      var config = {};

      if (this.config) {
        for (var key in this.config) {
          if (this.constructor.Default[key] !== this.config[key]) {
            config[key] = this.config[key];
          }
        }
      }

      return config;
    };

    Tooltip.prototype._cleanTipClass = function _cleanTipClass() {
      var $tip = $(this.getTipElement());
      var tabClass = $tip.attr('class').match(BSCLS_PREFIX_REGEX);
      if (tabClass !== null && tabClass.length > 0) {
        $tip.removeClass(tabClass.join(''));
      }
    };

    Tooltip.prototype._handlePopperPlacementChange = function _handlePopperPlacementChange(data) {
      this._cleanTipClass();
      this.addAttachmentClass(this._getAttachment(data.placement));
    };

    Tooltip.prototype._fixTransition = function _fixTransition() {
      var tip = this.getTipElement();
      var initConfigAnimation = this.config.animation;
      if (tip.getAttribute('x-placement') !== null) {
        return;
      }
      $(tip).removeClass(ClassName.FADE);
      this.config.animation = false;
      this.hide();
      this.show();
      this.config.animation = initConfigAnimation;
    };

    // static

    Tooltip._jQueryInterface = function _jQueryInterface(config) {
      return this.each(function () {
        var data = $(this).data(DATA_KEY);
        var _config = (typeof config === 'undefined' ? 'undefined' : _typeof(config)) === 'object' && config;

        if (!data && /dispose|hide/.test(config)) {
          return;
        }

        if (!data) {
          data = new Tooltip(this, _config);
          $(this).data(DATA_KEY, data);
        }

        if (typeof config === 'string') {
          if (data[config] === undefined) {
            throw new Error('No method named "' + config + '"');
          }
          data[config]();
        }
      });
    };

    _createClass(Tooltip, null, [{
      key: 'VERSION',
      get: function get() {
        return VERSION;
      }
    }, {
      key: 'Default',
      get: function get() {
        return Default;
      }
    }, {
      key: 'NAME',
      get: function get() {
        return NAME;
      }
    }, {
      key: 'DATA_KEY',
      get: function get() {
        return DATA_KEY;
      }
    }, {
      key: 'Event',
      get: function get() {
        return Event;
      }
    }, {
      key: 'EVENT_KEY',
      get: function get() {
        return EVENT_KEY;
      }
    }, {
      key: 'DefaultType',
      get: function get() {
        return DefaultType;
      }
    }]);

    return Tooltip;
  }();

  /**
   * ------------------------------------------------------------------------
   * jQuery
   * ------------------------------------------------------------------------
   */

  $.fn[NAME] = Tooltip._jQueryInterface;
  $.fn[NAME].Constructor = Tooltip;
  $.fn[NAME].noConflict = function () {
    $.fn[NAME] = JQUERY_NO_CONFLICT;
    return Tooltip._jQueryInterface;
  };

  return Tooltip;
}(jQuery); /* global Popper */
var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

/**
 * --------------------------------------------------------------------------
 * Bootstrap (v4.0.0-beta): popover.js
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * --------------------------------------------------------------------------
 */

var Popover = function ($) {

  /**
   * ------------------------------------------------------------------------
   * Constants
   * ------------------------------------------------------------------------
   */

  var NAME = 'popover';
  var VERSION = '4.0.0-beta';
  var DATA_KEY = 'bs.popover';
  var EVENT_KEY = '.' + DATA_KEY;
  var JQUERY_NO_CONFLICT = $.fn[NAME];
  var CLASS_PREFIX = 'bs-popover';
  var BSCLS_PREFIX_REGEX = new RegExp('(^|\\s)' + CLASS_PREFIX + '\\S+', 'g');

  var Default = $.extend({}, Tooltip.Default, {
    placement: 'right',
    trigger: 'click',
    content: '',
    template: '<div class="popover" role="tooltip">' + '<div class="arrow"></div>' + '<h3 class="popover-header"></h3>' + '<div class="popover-body"></div></div>'
  });

  var DefaultType = $.extend({}, Tooltip.DefaultType, {
    content: '(string|element|function)'
  });

  var ClassName = {
    FADE: 'fade',
    SHOW: 'show'
  };

  var Selector = {
    TITLE: '.popover-header',
    CONTENT: '.popover-body'
  };

  var Event = {
    HIDE: 'hide' + EVENT_KEY,
    HIDDEN: 'hidden' + EVENT_KEY,
    SHOW: 'show' + EVENT_KEY,
    SHOWN: 'shown' + EVENT_KEY,
    INSERTED: 'inserted' + EVENT_KEY,
    CLICK: 'click' + EVENT_KEY,
    FOCUSIN: 'focusin' + EVENT_KEY,
    FOCUSOUT: 'focusout' + EVENT_KEY,
    MOUSEENTER: 'mouseenter' + EVENT_KEY,
    MOUSELEAVE: 'mouseleave' + EVENT_KEY

    /**
     * ------------------------------------------------------------------------
     * Class Definition
     * ------------------------------------------------------------------------
     */

  };
  var Popover = function (_Tooltip) {
    _inherits(Popover, _Tooltip);

    function Popover() {
      _classCallCheck(this, Popover);

      return _possibleConstructorReturn(this, _Tooltip.apply(this, arguments));
    }

    // overrides

    Popover.prototype.isWithContent = function isWithContent() {
      return this.getTitle() || this._getContent();
    };

    Popover.prototype.addAttachmentClass = function addAttachmentClass(attachment) {
      $(this.getTipElement()).addClass(CLASS_PREFIX + '-' + attachment);
    };

    Popover.prototype.getTipElement = function getTipElement() {
      return this.tip = this.tip || $(this.config.template)[0];
    };

    Popover.prototype.setContent = function setContent() {
      var $tip = $(this.getTipElement());

      // we use append for html objects to maintain js events
      this.setElementContent($tip.find(Selector.TITLE), this.getTitle());
      this.setElementContent($tip.find(Selector.CONTENT), this._getContent());

      $tip.removeClass(ClassName.FADE + ' ' + ClassName.SHOW);
    };

    // private

    Popover.prototype._getContent = function _getContent() {
      return this.element.getAttribute('data-content') || (typeof this.config.content === 'function' ? this.config.content.call(this.element) : this.config.content);
    };

    Popover.prototype._cleanTipClass = function _cleanTipClass() {
      var $tip = $(this.getTipElement());
      var tabClass = $tip.attr('class').match(BSCLS_PREFIX_REGEX);
      if (tabClass !== null && tabClass.length > 0) {
        $tip.removeClass(tabClass.join(''));
      }
    };

    // static

    Popover._jQueryInterface = function _jQueryInterface(config) {
      return this.each(function () {
        var data = $(this).data(DATA_KEY);
        var _config = (typeof config === 'undefined' ? 'undefined' : _typeof(config)) === 'object' ? config : null;

        if (!data && /destroy|hide/.test(config)) {
          return;
        }

        if (!data) {
          data = new Popover(this, _config);
          $(this).data(DATA_KEY, data);
        }

        if (typeof config === 'string') {
          if (data[config] === undefined) {
            throw new Error('No method named "' + config + '"');
          }
          data[config]();
        }
      });
    };

    _createClass(Popover, null, [{
      key: 'VERSION',


      // getters

      get: function get() {
        return VERSION;
      }
    }, {
      key: 'Default',
      get: function get() {
        return Default;
      }
    }, {
      key: 'NAME',
      get: function get() {
        return NAME;
      }
    }, {
      key: 'DATA_KEY',
      get: function get() {
        return DATA_KEY;
      }
    }, {
      key: 'Event',
      get: function get() {
        return Event;
      }
    }, {
      key: 'EVENT_KEY',
      get: function get() {
        return EVENT_KEY;
      }
    }, {
      key: 'DefaultType',
      get: function get() {
        return DefaultType;
      }
    }]);

    return Popover;
  }(Tooltip);

  /**
   * ------------------------------------------------------------------------
   * jQuery
   * ------------------------------------------------------------------------
   */

  $.fn[NAME] = Popover._jQueryInterface;
  $.fn[NAME].Constructor = Popover;
  $.fn[NAME].noConflict = function () {
    $.fn[NAME] = JQUERY_NO_CONFLICT;
    return Popover._jQueryInterface;
  };

  return Popover;
}(jQuery);
}()
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInV0aWwuanMiLCJhbGVydC5qcyIsImJ1dHRvbi5qcyIsImNhcm91c2VsLmpzIiwiY29sbGFwc2UuanMiLCJkcm9wZG93bi5qcyIsIm1vZGFsLmpzIiwic2Nyb2xsc3B5LmpzIiwidGFiLmpzIiwidG9vbHRpcC5qcyIsInBvcG92ZXIuanMiXSwibmFtZXMiOlsiVXRpbCIsIiQiLCJ0cmFuc2l0aW9uIiwiTUFYX1VJRCIsIlRyYW5zaXRpb25FbmRFdmVudCIsIldlYmtpdFRyYW5zaXRpb24iLCJNb3pUcmFuc2l0aW9uIiwiT1RyYW5zaXRpb24iLCJ0b1R5cGUiLCJvYmoiLCJ0b1N0cmluZyIsImNhbGwiLCJtYXRjaCIsInRvTG93ZXJDYXNlIiwiaXNFbGVtZW50Iiwibm9kZVR5cGUiLCJnZXRTcGVjaWFsVHJhbnNpdGlvbkVuZEV2ZW50IiwiYmluZFR5cGUiLCJlbmQiLCJkZWxlZ2F0ZVR5cGUiLCJoYW5kbGUiLCJldmVudCIsInRhcmdldCIsImlzIiwiaGFuZGxlT2JqIiwiaGFuZGxlciIsImFwcGx5IiwiYXJndW1lbnRzIiwidW5kZWZpbmVkIiwidHJhbnNpdGlvbkVuZFRlc3QiLCJ3aW5kb3ciLCJRVW5pdCIsImVsIiwiZG9jdW1lbnQiLCJjcmVhdGVFbGVtZW50IiwibmFtZSIsInN0eWxlIiwidHJhbnNpdGlvbkVuZEVtdWxhdG9yIiwiZHVyYXRpb24iLCJjYWxsZWQiLCJvbmUiLCJUUkFOU0lUSU9OX0VORCIsInNldFRpbWVvdXQiLCJ0cmlnZ2VyVHJhbnNpdGlvbkVuZCIsInNldFRyYW5zaXRpb25FbmRTdXBwb3J0IiwiZm4iLCJlbXVsYXRlVHJhbnNpdGlvbkVuZCIsInN1cHBvcnRzVHJhbnNpdGlvbkVuZCIsInNwZWNpYWwiLCJnZXRVSUQiLCJwcmVmaXgiLCJNYXRoIiwicmFuZG9tIiwiZ2V0RWxlbWVudEJ5SWQiLCJnZXRTZWxlY3RvckZyb21FbGVtZW50IiwiZWxlbWVudCIsInNlbGVjdG9yIiwiZ2V0QXR0cmlidXRlIiwiJHNlbGVjdG9yIiwibGVuZ3RoIiwiZXJyb3IiLCJyZWZsb3ciLCJvZmZzZXRIZWlnaHQiLCJ0cmlnZ2VyIiwiQm9vbGVhbiIsInR5cGVDaGVja0NvbmZpZyIsImNvbXBvbmVudE5hbWUiLCJjb25maWciLCJjb25maWdUeXBlcyIsInByb3BlcnR5IiwiaGFzT3duUHJvcGVydHkiLCJleHBlY3RlZFR5cGVzIiwidmFsdWUiLCJ2YWx1ZVR5cGUiLCJSZWdFeHAiLCJ0ZXN0IiwiRXJyb3IiLCJ0b1VwcGVyQ2FzZSIsImpRdWVyeSIsIkFsZXJ0IiwiTkFNRSIsIlZFUlNJT04iLCJEQVRBX0tFWSIsIkVWRU5UX0tFWSIsIkRBVEFfQVBJX0tFWSIsIkpRVUVSWV9OT19DT05GTElDVCIsIlRSQU5TSVRJT05fRFVSQVRJT04iLCJTZWxlY3RvciIsIkRJU01JU1MiLCJFdmVudCIsIkNMT1NFIiwiQ0xPU0VEIiwiQ0xJQ0tfREFUQV9BUEkiLCJDbGFzc05hbWUiLCJBTEVSVCIsIkZBREUiLCJTSE9XIiwiX2VsZW1lbnQiLCJjbG9zZSIsInJvb3RFbGVtZW50IiwiX2dldFJvb3RFbGVtZW50IiwiY3VzdG9tRXZlbnQiLCJfdHJpZ2dlckNsb3NlRXZlbnQiLCJpc0RlZmF1bHRQcmV2ZW50ZWQiLCJfcmVtb3ZlRWxlbWVudCIsImRpc3Bvc2UiLCJyZW1vdmVEYXRhIiwicGFyZW50IiwiY2xvc2VzdCIsImNsb3NlRXZlbnQiLCJyZW1vdmVDbGFzcyIsImhhc0NsYXNzIiwiX2Rlc3Ryb3lFbGVtZW50IiwiZGV0YWNoIiwicmVtb3ZlIiwiX2pRdWVyeUludGVyZmFjZSIsImVhY2giLCIkZWxlbWVudCIsImRhdGEiLCJfaGFuZGxlRGlzbWlzcyIsImFsZXJ0SW5zdGFuY2UiLCJwcmV2ZW50RGVmYXVsdCIsIm9uIiwiQ29uc3RydWN0b3IiLCJub0NvbmZsaWN0IiwiQnV0dG9uIiwiQUNUSVZFIiwiQlVUVE9OIiwiRk9DVVMiLCJEQVRBX1RPR0dMRV9DQVJST1QiLCJEQVRBX1RPR0dMRSIsIklOUFVUIiwiRk9DVVNfQkxVUl9EQVRBX0FQSSIsInRvZ2dsZSIsInRyaWdnZXJDaGFuZ2VFdmVudCIsImFkZEFyaWFQcmVzc2VkIiwiaW5wdXQiLCJmaW5kIiwidHlwZSIsImNoZWNrZWQiLCJhY3RpdmVFbGVtZW50IiwiaGFzQXR0cmlidXRlIiwiY2xhc3NMaXN0IiwiY29udGFpbnMiLCJmb2N1cyIsInNldEF0dHJpYnV0ZSIsInRvZ2dsZUNsYXNzIiwiYnV0dG9uIiwiQ2Fyb3VzZWwiLCJBUlJPV19MRUZUX0tFWUNPREUiLCJBUlJPV19SSUdIVF9LRVlDT0RFIiwiVE9VQ0hFVkVOVF9DT01QQVRfV0FJVCIsIkRlZmF1bHQiLCJpbnRlcnZhbCIsImtleWJvYXJkIiwic2xpZGUiLCJwYXVzZSIsIndyYXAiLCJEZWZhdWx0VHlwZSIsIkRpcmVjdGlvbiIsIk5FWFQiLCJQUkVWIiwiTEVGVCIsIlJJR0hUIiwiU0xJREUiLCJTTElEIiwiS0VZRE9XTiIsIk1PVVNFRU5URVIiLCJNT1VTRUxFQVZFIiwiVE9VQ0hFTkQiLCJMT0FEX0RBVEFfQVBJIiwiQ0FST1VTRUwiLCJJVEVNIiwiQUNUSVZFX0lURU0iLCJORVhUX1BSRVYiLCJJTkRJQ0FUT1JTIiwiREFUQV9TTElERSIsIkRBVEFfUklERSIsIl9pdGVtcyIsIl9pbnRlcnZhbCIsIl9hY3RpdmVFbGVtZW50IiwiX2lzUGF1c2VkIiwiX2lzU2xpZGluZyIsInRvdWNoVGltZW91dCIsIl9jb25maWciLCJfZ2V0Q29uZmlnIiwiX2luZGljYXRvcnNFbGVtZW50IiwiX2FkZEV2ZW50TGlzdGVuZXJzIiwibmV4dCIsIl9zbGlkZSIsIm5leHRXaGVuVmlzaWJsZSIsImhpZGRlbiIsInByZXYiLCJjeWNsZSIsImNsZWFySW50ZXJ2YWwiLCJzZXRJbnRlcnZhbCIsInZpc2liaWxpdHlTdGF0ZSIsImJpbmQiLCJ0byIsImluZGV4IiwiYWN0aXZlSW5kZXgiLCJfZ2V0SXRlbUluZGV4IiwiZGlyZWN0aW9uIiwib2ZmIiwiZXh0ZW5kIiwiX2tleWRvd24iLCJkb2N1bWVudEVsZW1lbnQiLCJjbGVhclRpbWVvdXQiLCJ0YWdOYW1lIiwid2hpY2giLCJtYWtlQXJyYXkiLCJpbmRleE9mIiwiX2dldEl0ZW1CeURpcmVjdGlvbiIsImlzTmV4dERpcmVjdGlvbiIsImlzUHJldkRpcmVjdGlvbiIsImxhc3RJdGVtSW5kZXgiLCJpc0dvaW5nVG9XcmFwIiwiZGVsdGEiLCJpdGVtSW5kZXgiLCJfdHJpZ2dlclNsaWRlRXZlbnQiLCJyZWxhdGVkVGFyZ2V0IiwiZXZlbnREaXJlY3Rpb25OYW1lIiwidGFyZ2V0SW5kZXgiLCJmcm9tSW5kZXgiLCJzbGlkZUV2ZW50IiwiZnJvbSIsIl9zZXRBY3RpdmVJbmRpY2F0b3JFbGVtZW50IiwibmV4dEluZGljYXRvciIsImNoaWxkcmVuIiwiYWRkQ2xhc3MiLCJhY3RpdmVFbGVtZW50SW5kZXgiLCJuZXh0RWxlbWVudCIsIm5leHRFbGVtZW50SW5kZXgiLCJpc0N5Y2xpbmciLCJkaXJlY3Rpb25hbENsYXNzTmFtZSIsIm9yZGVyQ2xhc3NOYW1lIiwic2xpZEV2ZW50IiwiYWN0aW9uIiwiX2RhdGFBcGlDbGlja0hhbmRsZXIiLCJzbGlkZUluZGV4IiwiJGNhcm91c2VsIiwiQ29sbGFwc2UiLCJTSE9XTiIsIkhJREUiLCJISURERU4iLCJDT0xMQVBTRSIsIkNPTExBUFNJTkciLCJDT0xMQVBTRUQiLCJEaW1lbnNpb24iLCJXSURUSCIsIkhFSUdIVCIsIkFDVElWRVMiLCJfaXNUcmFuc2l0aW9uaW5nIiwiX3RyaWdnZXJBcnJheSIsImlkIiwidGFiVG9nZ2xlcyIsImkiLCJlbGVtIiwiZmlsdGVyIiwicHVzaCIsIl9wYXJlbnQiLCJfZ2V0UGFyZW50IiwiX2FkZEFyaWFBbmRDb2xsYXBzZWRDbGFzcyIsImhpZGUiLCJzaG93IiwiYWN0aXZlcyIsImFjdGl2ZXNEYXRhIiwic3RhcnRFdmVudCIsImRpbWVuc2lvbiIsIl9nZXREaW1lbnNpb24iLCJhdHRyIiwic2V0VHJhbnNpdGlvbmluZyIsImNvbXBsZXRlIiwiY2FwaXRhbGl6ZWREaW1lbnNpb24iLCJzbGljZSIsInNjcm9sbFNpemUiLCJnZXRCb3VuZGluZ0NsaWVudFJlY3QiLCIkZWxlbSIsImlzVHJhbnNpdGlvbmluZyIsImhhc1dpZHRoIiwiX2dldFRhcmdldEZyb21FbGVtZW50IiwidHJpZ2dlckFycmF5IiwiaXNPcGVuIiwiJHRoaXMiLCIkdHJpZ2dlciIsIiR0YXJnZXQiLCJEcm9wZG93biIsIlBvcHBlciIsIkVTQ0FQRV9LRVlDT0RFIiwiU1BBQ0VfS0VZQ09ERSIsIlRBQl9LRVlDT0RFIiwiQVJST1dfVVBfS0VZQ09ERSIsIkFSUk9XX0RPV05fS0VZQ09ERSIsIlJJR0hUX01PVVNFX0JVVFRPTl9XSElDSCIsIlJFR0VYUF9LRVlET1dOIiwiQ0xJQ0siLCJLRVlET1dOX0RBVEFfQVBJIiwiS0VZVVBfREFUQV9BUEkiLCJESVNBQkxFRCIsIkRST1BVUCIsIk1FTlVSSUdIVCIsIk1FTlVMRUZUIiwiRk9STV9DSElMRCIsIk1FTlUiLCJOQVZCQVJfTkFWIiwiVklTSUJMRV9JVEVNUyIsIkF0dGFjaG1lbnRNYXAiLCJUT1AiLCJUT1BFTkQiLCJCT1RUT00iLCJCT1RUT01FTkQiLCJwbGFjZW1lbnQiLCJvZmZzZXQiLCJmbGlwIiwiX3BvcHBlciIsIl9tZW51IiwiX2dldE1lbnVFbGVtZW50IiwiX2luTmF2YmFyIiwiX2RldGVjdE5hdmJhciIsImRpc2FibGVkIiwiX2dldFBhcmVudEZyb21FbGVtZW50IiwiaXNBY3RpdmUiLCJfY2xlYXJNZW51cyIsInNob3dFdmVudCIsIl9nZXRQb3BwZXJDb25maWciLCJub29wIiwiZGVzdHJveSIsInVwZGF0ZSIsInNjaGVkdWxlVXBkYXRlIiwic3RvcFByb3BhZ2F0aW9uIiwiZWxlbWVudERhdGEiLCJjb25zdHJ1Y3RvciIsIl9nZXRQbGFjZW1lbnQiLCIkcGFyZW50RHJvcGRvd24iLCJwb3BwZXJDb25maWciLCJtb2RpZmllcnMiLCJlbmFibGVkIiwiYXBwbHlTdHlsZSIsInRvZ2dsZXMiLCJjb250ZXh0IiwiZHJvcGRvd25NZW51IiwiaGlkZUV2ZW50IiwicGFyZW50Tm9kZSIsIl9kYXRhQXBpS2V5ZG93bkhhbmRsZXIiLCJpdGVtcyIsImdldCIsImUiLCJNb2RhbCIsIkJBQ0tEUk9QX1RSQU5TSVRJT05fRFVSQVRJT04iLCJiYWNrZHJvcCIsIkZPQ1VTSU4iLCJSRVNJWkUiLCJDTElDS19ESVNNSVNTIiwiS0VZRE9XTl9ESVNNSVNTIiwiTU9VU0VVUF9ESVNNSVNTIiwiTU9VU0VET1dOX0RJU01JU1MiLCJTQ1JPTExCQVJfTUVBU1VSRVIiLCJCQUNLRFJPUCIsIk9QRU4iLCJESUFMT0ciLCJEQVRBX0RJU01JU1MiLCJGSVhFRF9DT05URU5UIiwiTkFWQkFSX1RPR0dMRVIiLCJfZGlhbG9nIiwiX2JhY2tkcm9wIiwiX2lzU2hvd24iLCJfaXNCb2R5T3ZlcmZsb3dpbmciLCJfaWdub3JlQmFja2Ryb3BDbGljayIsIl9vcmlnaW5hbEJvZHlQYWRkaW5nIiwiX3Njcm9sbGJhcldpZHRoIiwiX2NoZWNrU2Nyb2xsYmFyIiwiX3NldFNjcm9sbGJhciIsImJvZHkiLCJfc2V0RXNjYXBlRXZlbnQiLCJfc2V0UmVzaXplRXZlbnQiLCJfc2hvd0JhY2tkcm9wIiwiX3Nob3dFbGVtZW50IiwiX2hpZGVNb2RhbCIsImhhbmRsZVVwZGF0ZSIsIl9hZGp1c3REaWFsb2ciLCJOb2RlIiwiRUxFTUVOVF9OT0RFIiwiYXBwZW5kQ2hpbGQiLCJkaXNwbGF5IiwicmVtb3ZlQXR0cmlidXRlIiwic2Nyb2xsVG9wIiwiX2VuZm9yY2VGb2N1cyIsInNob3duRXZlbnQiLCJ0cmFuc2l0aW9uQ29tcGxldGUiLCJoYXMiLCJfcmVzZXRBZGp1c3RtZW50cyIsIl9yZXNldFNjcm9sbGJhciIsIl9yZW1vdmVCYWNrZHJvcCIsImNhbGxiYWNrIiwiYW5pbWF0ZSIsImRvQW5pbWF0ZSIsImNsYXNzTmFtZSIsImFwcGVuZFRvIiwiY3VycmVudFRhcmdldCIsImNhbGxiYWNrUmVtb3ZlIiwiaXNNb2RhbE92ZXJmbG93aW5nIiwic2Nyb2xsSGVpZ2h0IiwiY2xpZW50SGVpZ2h0IiwicGFkZGluZ0xlZnQiLCJwYWRkaW5nUmlnaHQiLCJjbGllbnRXaWR0aCIsImlubmVyV2lkdGgiLCJfZ2V0U2Nyb2xsYmFyV2lkdGgiLCJhY3R1YWxQYWRkaW5nIiwiY2FsY3VsYXRlZFBhZGRpbmciLCJjc3MiLCJwYXJzZUZsb2F0IiwiYWN0dWFsTWFyZ2luIiwibWFyZ2luUmlnaHQiLCJjYWxjdWxhdGVkTWFyZ2luIiwicGFkZGluZyIsIm1hcmdpbiIsInNjcm9sbERpdiIsInNjcm9sbGJhcldpZHRoIiwid2lkdGgiLCJyZW1vdmVDaGlsZCIsIlNjcm9sbFNweSIsIm1ldGhvZCIsIkFDVElWQVRFIiwiU0NST0xMIiwiRFJPUERPV05fSVRFTSIsIkRST1BET1dOX01FTlUiLCJEQVRBX1NQWSIsIk5BVl9MSVNUX0dST1VQIiwiTkFWX0xJTktTIiwiTElTVF9JVEVNUyIsIkRST1BET1dOIiwiRFJPUERPV05fSVRFTVMiLCJEUk9QRE9XTl9UT0dHTEUiLCJPZmZzZXRNZXRob2QiLCJPRkZTRVQiLCJQT1NJVElPTiIsIl9zY3JvbGxFbGVtZW50IiwiX3NlbGVjdG9yIiwiX29mZnNldHMiLCJfdGFyZ2V0cyIsIl9hY3RpdmVUYXJnZXQiLCJfc2Nyb2xsSGVpZ2h0IiwiX3Byb2Nlc3MiLCJyZWZyZXNoIiwiYXV0b01ldGhvZCIsIm9mZnNldE1ldGhvZCIsIm9mZnNldEJhc2UiLCJfZ2V0U2Nyb2xsVG9wIiwiX2dldFNjcm9sbEhlaWdodCIsInRhcmdldHMiLCJtYXAiLCJ0YXJnZXRTZWxlY3RvciIsInRhcmdldEJDUiIsImhlaWdodCIsInRvcCIsIml0ZW0iLCJzb3J0IiwiYSIsImIiLCJmb3JFYWNoIiwicGFnZVlPZmZzZXQiLCJtYXgiLCJfZ2V0T2Zmc2V0SGVpZ2h0IiwiaW5uZXJIZWlnaHQiLCJtYXhTY3JvbGwiLCJfYWN0aXZhdGUiLCJfY2xlYXIiLCJpc0FjdGl2ZVRhcmdldCIsInF1ZXJpZXMiLCJzcGxpdCIsIiRsaW5rIiwiam9pbiIsInBhcmVudHMiLCJzY3JvbGxTcHlzIiwiJHNweSIsIlRhYiIsIkRST1BET1dOX0FDVElWRV9DSElMRCIsInByZXZpb3VzIiwibGlzdEVsZW1lbnQiLCJoaWRkZW5FdmVudCIsImNvbnRhaW5lciIsImFjdGl2ZSIsIl90cmFuc2l0aW9uQ29tcGxldGUiLCJkcm9wZG93bkNoaWxkIiwiZHJvcGRvd25FbGVtZW50IiwiVG9vbHRpcCIsIkNMQVNTX1BSRUZJWCIsIkJTQ0xTX1BSRUZJWF9SRUdFWCIsImFuaW1hdGlvbiIsInRlbXBsYXRlIiwidGl0bGUiLCJkZWxheSIsImh0bWwiLCJmYWxsYmFja1BsYWNlbWVudCIsIkFVVE8iLCJIb3ZlclN0YXRlIiwiT1VUIiwiSU5TRVJURUQiLCJGT0NVU09VVCIsIlRPT0xUSVAiLCJUT09MVElQX0lOTkVSIiwiQVJST1ciLCJUcmlnZ2VyIiwiSE9WRVIiLCJNQU5VQUwiLCJfaXNFbmFibGVkIiwiX3RpbWVvdXQiLCJfaG92ZXJTdGF0ZSIsIl9hY3RpdmVUcmlnZ2VyIiwidGlwIiwiX3NldExpc3RlbmVycyIsImVuYWJsZSIsImRpc2FibGUiLCJ0b2dnbGVFbmFibGVkIiwiZGF0YUtleSIsIl9nZXREZWxlZ2F0ZUNvbmZpZyIsImNsaWNrIiwiX2lzV2l0aEFjdGl2ZVRyaWdnZXIiLCJfZW50ZXIiLCJfbGVhdmUiLCJnZXRUaXBFbGVtZW50IiwiaXNXaXRoQ29udGVudCIsImlzSW5UaGVEb20iLCJvd25lckRvY3VtZW50IiwidGlwSWQiLCJzZXRDb250ZW50IiwiYXR0YWNobWVudCIsIl9nZXRBdHRhY2htZW50IiwiYWRkQXR0YWNobWVudENsYXNzIiwiYmVoYXZpb3IiLCJhcnJvdyIsIm9uQ3JlYXRlIiwib3JpZ2luYWxQbGFjZW1lbnQiLCJfaGFuZGxlUG9wcGVyUGxhY2VtZW50Q2hhbmdlIiwib25VcGRhdGUiLCJfZml4VHJhbnNpdGlvbiIsInByZXZIb3ZlclN0YXRlIiwiX1RSQU5TSVRJT05fRFVSQVRJT04iLCJfY2xlYW5UaXBDbGFzcyIsImdldFRpdGxlIiwiJHRpcCIsInNldEVsZW1lbnRDb250ZW50IiwiY29udGVudCIsImpxdWVyeSIsImVtcHR5IiwiYXBwZW5kIiwidGV4dCIsInRyaWdnZXJzIiwiZXZlbnRJbiIsImV2ZW50T3V0IiwiX2ZpeFRpdGxlIiwidGl0bGVUeXBlIiwia2V5IiwidGFiQ2xhc3MiLCJpbml0Q29uZmlnQW5pbWF0aW9uIiwiUG9wb3ZlciIsIlRJVExFIiwiQ09OVEVOVCIsIl9nZXRDb250ZW50Il0sIm1hcHBpbmdzIjoiQUFBQTs7Ozs7OztBQU9BLElBQU1BLE9BQVEsVUFBQ0MsQ0FBRCxFQUFPOztBQUduQjs7Ozs7O0FBTUEsTUFBSUMsYUFBYSxLQUFqQjs7QUFFQSxNQUFNQyxVQUFVLE9BQWhCOztBQUVBLE1BQU1DLHFCQUFxQjtBQUN6QkMsc0JBQW1CLHFCQURNO0FBRXpCQyxtQkFBbUIsZUFGTTtBQUd6QkMsaUJBQW1CLCtCQUhNO0FBSXpCTCxnQkFBbUI7O0FBR3JCO0FBUDJCLEdBQTNCLENBUUEsU0FBU00sTUFBVCxDQUFnQkMsR0FBaEIsRUFBcUI7QUFDbkIsV0FBTyxHQUFHQyxRQUFILENBQVlDLElBQVosQ0FBaUJGLEdBQWpCLEVBQXNCRyxLQUF0QixDQUE0QixlQUE1QixFQUE2QyxDQUE3QyxFQUFnREMsV0FBaEQsRUFBUDtBQUNEOztBQUVELFdBQVNDLFNBQVQsQ0FBbUJMLEdBQW5CLEVBQXdCO0FBQ3RCLFdBQU8sQ0FBQ0EsSUFBSSxDQUFKLEtBQVVBLEdBQVgsRUFBZ0JNLFFBQXZCO0FBQ0Q7O0FBRUQsV0FBU0MsNEJBQVQsR0FBd0M7QUFDdEMsV0FBTztBQUNMQyxnQkFBVWYsV0FBV2dCLEdBRGhCO0FBRUxDLG9CQUFjakIsV0FBV2dCLEdBRnBCO0FBR0xFLFlBSEssa0JBR0VDLEtBSEYsRUFHUztBQUNaLFlBQUlwQixFQUFFb0IsTUFBTUMsTUFBUixFQUFnQkMsRUFBaEIsQ0FBbUIsSUFBbkIsQ0FBSixFQUE4QjtBQUM1QixpQkFBT0YsTUFBTUcsU0FBTixDQUFnQkMsT0FBaEIsQ0FBd0JDLEtBQXhCLENBQThCLElBQTlCLEVBQW9DQyxTQUFwQyxDQUFQLENBRDRCLENBQzBCO0FBQ3ZEO0FBQ0QsZUFBT0MsU0FBUDtBQUNEO0FBUkksS0FBUDtBQVVEOztBQUVELFdBQVNDLGlCQUFULEdBQTZCO0FBQzNCLFFBQUlDLE9BQU9DLEtBQVgsRUFBa0I7QUFDaEIsYUFBTyxLQUFQO0FBQ0Q7O0FBRUQsUUFBTUMsS0FBS0MsU0FBU0MsYUFBVCxDQUF1QixXQUF2QixDQUFYOztBQUVBLFNBQUssSUFBTUMsSUFBWCxJQUFtQi9CLGtCQUFuQixFQUF1QztBQUNyQyxVQUFJNEIsR0FBR0ksS0FBSCxDQUFTRCxJQUFULE1BQW1CUCxTQUF2QixFQUFrQztBQUNoQyxlQUFPO0FBQ0xWLGVBQUtkLG1CQUFtQitCLElBQW5CO0FBREEsU0FBUDtBQUdEO0FBQ0Y7O0FBRUQsV0FBTyxLQUFQO0FBQ0Q7O0FBRUQsV0FBU0UscUJBQVQsQ0FBK0JDLFFBQS9CLEVBQXlDO0FBQUE7O0FBQ3ZDLFFBQUlDLFNBQVMsS0FBYjs7QUFFQXRDLE1BQUUsSUFBRixFQUFRdUMsR0FBUixDQUFZeEMsS0FBS3lDLGNBQWpCLEVBQWlDLFlBQU07QUFDckNGLGVBQVMsSUFBVDtBQUNELEtBRkQ7O0FBSUFHLGVBQVcsWUFBTTtBQUNmLFVBQUksQ0FBQ0gsTUFBTCxFQUFhO0FBQ1h2QyxhQUFLMkMsb0JBQUw7QUFDRDtBQUNGLEtBSkQsRUFJR0wsUUFKSDs7QUFNQSxXQUFPLElBQVA7QUFDRDs7QUFFRCxXQUFTTSx1QkFBVCxHQUFtQztBQUNqQzFDLGlCQUFhMkIsbUJBQWI7O0FBRUE1QixNQUFFNEMsRUFBRixDQUFLQyxvQkFBTCxHQUE0QlQscUJBQTVCOztBQUVBLFFBQUlyQyxLQUFLK0MscUJBQUwsRUFBSixFQUFrQztBQUNoQzlDLFFBQUVvQixLQUFGLENBQVEyQixPQUFSLENBQWdCaEQsS0FBS3lDLGNBQXJCLElBQXVDekIsOEJBQXZDO0FBQ0Q7QUFDRjs7QUFHRDs7Ozs7O0FBTUEsTUFBTWhCLE9BQU87O0FBRVh5QyxvQkFBZ0IsaUJBRkw7O0FBSVhRLFVBSlcsa0JBSUpDLE1BSkksRUFJSTtBQUNiLFNBQUc7QUFDRDtBQUNBQSxrQkFBVSxDQUFDLEVBQUVDLEtBQUtDLE1BQUwsS0FBZ0JqRCxPQUFsQixDQUFYLENBRkMsQ0FFcUM7QUFDdkMsT0FIRCxRQUdTOEIsU0FBU29CLGNBQVQsQ0FBd0JILE1BQXhCLENBSFQ7QUFJQSxhQUFPQSxNQUFQO0FBQ0QsS0FWVTtBQVlYSSwwQkFaVyxrQ0FZWUMsT0FaWixFQVlxQjtBQUM5QixVQUFJQyxXQUFXRCxRQUFRRSxZQUFSLENBQXFCLGFBQXJCLENBQWY7QUFDQSxVQUFJLENBQUNELFFBQUQsSUFBYUEsYUFBYSxHQUE5QixFQUFtQztBQUNqQ0EsbUJBQVdELFFBQVFFLFlBQVIsQ0FBcUIsTUFBckIsS0FBZ0MsRUFBM0M7QUFDRDs7QUFFRCxVQUFJO0FBQ0YsWUFBTUMsWUFBWXpELEVBQUV1RCxRQUFGLENBQWxCO0FBQ0EsZUFBT0UsVUFBVUMsTUFBVixHQUFtQixDQUFuQixHQUF1QkgsUUFBdkIsR0FBa0MsSUFBekM7QUFDRCxPQUhELENBR0UsT0FBT0ksS0FBUCxFQUFjO0FBQ2QsZUFBTyxJQUFQO0FBQ0Q7QUFDRixLQXhCVTtBQTBCWEMsVUExQlcsa0JBMEJKTixPQTFCSSxFQTBCSztBQUNkLGFBQU9BLFFBQVFPLFlBQWY7QUFDRCxLQTVCVTtBQThCWG5CLHdCQTlCVyxnQ0E4QlVZLE9BOUJWLEVBOEJtQjtBQUM1QnRELFFBQUVzRCxPQUFGLEVBQVdRLE9BQVgsQ0FBbUI3RCxXQUFXZ0IsR0FBOUI7QUFDRCxLQWhDVTtBQWtDWDZCLHlCQWxDVyxtQ0FrQ2E7QUFDdEIsYUFBT2lCLFFBQVE5RCxVQUFSLENBQVA7QUFDRCxLQXBDVTtBQXNDWCtELG1CQXRDVywyQkFzQ0tDLGFBdENMLEVBc0NvQkMsTUF0Q3BCLEVBc0M0QkMsV0F0QzVCLEVBc0N5QztBQUNsRCxXQUFLLElBQU1DLFFBQVgsSUFBdUJELFdBQXZCLEVBQW9DO0FBQ2xDLFlBQUlBLFlBQVlFLGNBQVosQ0FBMkJELFFBQTNCLENBQUosRUFBMEM7QUFDeEMsY0FBTUUsZ0JBQWdCSCxZQUFZQyxRQUFaLENBQXRCO0FBQ0EsY0FBTUcsUUFBZ0JMLE9BQU9FLFFBQVAsQ0FBdEI7QUFDQSxjQUFNSSxZQUFnQkQsU0FBUzFELFVBQVUwRCxLQUFWLENBQVQsR0FDQSxTQURBLEdBQ1loRSxPQUFPZ0UsS0FBUCxDQURsQzs7QUFHQSxjQUFJLENBQUMsSUFBSUUsTUFBSixDQUFXSCxhQUFYLEVBQTBCSSxJQUExQixDQUErQkYsU0FBL0IsQ0FBTCxFQUFnRDtBQUM5QyxrQkFBTSxJQUFJRyxLQUFKLENBQ0RWLGNBQWNXLFdBQWQsRUFBSCx3QkFDV1IsUUFEWCx5QkFDdUNJLFNBRHZDLG9DQUVzQkYsYUFGdEIsUUFESSxDQUFOO0FBSUQ7QUFDRjtBQUNGO0FBQ0Y7QUF0RFUsR0FBYjs7QUF5REEzQjs7QUFFQSxTQUFPNUMsSUFBUDtBQUVELENBMUpZLENBMEpWOEUsTUExSlUsQ0FBYjs7Ozs7QUNKQTs7Ozs7OztBQU9BLElBQU1DLFFBQVMsVUFBQzlFLENBQUQsRUFBTzs7QUFHcEI7Ozs7OztBQU1BLE1BQU0rRSxPQUFzQixPQUE1QjtBQUNBLE1BQU1DLFVBQXNCLFlBQTVCO0FBQ0EsTUFBTUMsV0FBc0IsVUFBNUI7QUFDQSxNQUFNQyxrQkFBMEJELFFBQWhDO0FBQ0EsTUFBTUUsZUFBc0IsV0FBNUI7QUFDQSxNQUFNQyxxQkFBc0JwRixFQUFFNEMsRUFBRixDQUFLbUMsSUFBTCxDQUE1QjtBQUNBLE1BQU1NLHNCQUFzQixHQUE1Qjs7QUFFQSxNQUFNQyxXQUFXO0FBQ2ZDLGFBQVU7QUFESyxHQUFqQjs7QUFJQSxNQUFNQyxRQUFRO0FBQ1pDLHFCQUF5QlAsU0FEYjtBQUVaUSx1QkFBMEJSLFNBRmQ7QUFHWlMsOEJBQXlCVCxTQUF6QixHQUFxQ0M7QUFIekIsR0FBZDs7QUFNQSxNQUFNUyxZQUFZO0FBQ2hCQyxXQUFRLE9BRFE7QUFFaEJDLFVBQVEsTUFGUTtBQUdoQkMsVUFBUTs7QUFJVjs7Ozs7O0FBUGtCLEdBQWxCO0FBM0JvQixNQXdDZGpCLEtBeENjO0FBMENsQixtQkFBWXhCLE9BQVosRUFBcUI7QUFBQTs7QUFDbkIsV0FBSzBDLFFBQUwsR0FBZ0IxQyxPQUFoQjtBQUNEOztBQUdEOztBQU9BOztBQXREa0Isb0JBd0RsQjJDLEtBeERrQixrQkF3RFozQyxPQXhEWSxFQXdESDtBQUNiQSxnQkFBVUEsV0FBVyxLQUFLMEMsUUFBMUI7O0FBRUEsVUFBTUUsY0FBYyxLQUFLQyxlQUFMLENBQXFCN0MsT0FBckIsQ0FBcEI7QUFDQSxVQUFNOEMsY0FBYyxLQUFLQyxrQkFBTCxDQUF3QkgsV0FBeEIsQ0FBcEI7O0FBRUEsVUFBSUUsWUFBWUUsa0JBQVosRUFBSixFQUFzQztBQUNwQztBQUNEOztBQUVELFdBQUtDLGNBQUwsQ0FBb0JMLFdBQXBCO0FBQ0QsS0FuRWlCOztBQUFBLG9CQXFFbEJNLE9BckVrQixzQkFxRVI7QUFDUnhHLFFBQUV5RyxVQUFGLENBQWEsS0FBS1QsUUFBbEIsRUFBNEJmLFFBQTVCO0FBQ0EsV0FBS2UsUUFBTCxHQUFnQixJQUFoQjtBQUNELEtBeEVpQjs7QUEyRWxCOztBQTNFa0Isb0JBNkVsQkcsZUE3RWtCLDRCQTZFRjdDLE9BN0VFLEVBNkVPO0FBQ3ZCLFVBQU1DLFdBQVd4RCxLQUFLc0Qsc0JBQUwsQ0FBNEJDLE9BQTVCLENBQWpCO0FBQ0EsVUFBSW9ELFNBQWEsS0FBakI7O0FBRUEsVUFBSW5ELFFBQUosRUFBYztBQUNabUQsaUJBQVMxRyxFQUFFdUQsUUFBRixFQUFZLENBQVosQ0FBVDtBQUNEOztBQUVELFVBQUksQ0FBQ21ELE1BQUwsRUFBYTtBQUNYQSxpQkFBUzFHLEVBQUVzRCxPQUFGLEVBQVdxRCxPQUFYLE9BQXVCZixVQUFVQyxLQUFqQyxFQUEwQyxDQUExQyxDQUFUO0FBQ0Q7O0FBRUQsYUFBT2EsTUFBUDtBQUNELEtBMUZpQjs7QUFBQSxvQkE0RmxCTCxrQkE1RmtCLCtCQTRGQy9DLE9BNUZELEVBNEZVO0FBQzFCLFVBQU1zRCxhQUFhNUcsRUFBRXdGLEtBQUYsQ0FBUUEsTUFBTUMsS0FBZCxDQUFuQjs7QUFFQXpGLFFBQUVzRCxPQUFGLEVBQVdRLE9BQVgsQ0FBbUI4QyxVQUFuQjtBQUNBLGFBQU9BLFVBQVA7QUFDRCxLQWpHaUI7O0FBQUEsb0JBbUdsQkwsY0FuR2tCLDJCQW1HSGpELE9BbkdHLEVBbUdNO0FBQUE7O0FBQ3RCdEQsUUFBRXNELE9BQUYsRUFBV3VELFdBQVgsQ0FBdUJqQixVQUFVRyxJQUFqQzs7QUFFQSxVQUFJLENBQUNoRyxLQUFLK0MscUJBQUwsRUFBRCxJQUNBLENBQUM5QyxFQUFFc0QsT0FBRixFQUFXd0QsUUFBWCxDQUFvQmxCLFVBQVVFLElBQTlCLENBREwsRUFDMEM7QUFDeEMsYUFBS2lCLGVBQUwsQ0FBcUJ6RCxPQUFyQjtBQUNBO0FBQ0Q7O0FBRUR0RCxRQUFFc0QsT0FBRixFQUNHZixHQURILENBQ094QyxLQUFLeUMsY0FEWixFQUM0QixVQUFDcEIsS0FBRDtBQUFBLGVBQVcsTUFBSzJGLGVBQUwsQ0FBcUJ6RCxPQUFyQixFQUE4QmxDLEtBQTlCLENBQVg7QUFBQSxPQUQ1QixFQUVHeUIsb0JBRkgsQ0FFd0J3QyxtQkFGeEI7QUFHRCxLQS9HaUI7O0FBQUEsb0JBaUhsQjBCLGVBakhrQiw0QkFpSEZ6RCxPQWpIRSxFQWlITztBQUN2QnRELFFBQUVzRCxPQUFGLEVBQ0cwRCxNQURILEdBRUdsRCxPQUZILENBRVcwQixNQUFNRSxNQUZqQixFQUdHdUIsTUFISDtBQUlELEtBdEhpQjs7QUF5SGxCOztBQXpIa0IsVUEySFhDLGdCQTNIVyw2QkEySE1oRCxNQTNITixFQTJIYztBQUM5QixhQUFPLEtBQUtpRCxJQUFMLENBQVUsWUFBWTtBQUMzQixZQUFNQyxXQUFXcEgsRUFBRSxJQUFGLENBQWpCO0FBQ0EsWUFBSXFILE9BQWFELFNBQVNDLElBQVQsQ0FBY3BDLFFBQWQsQ0FBakI7O0FBRUEsWUFBSSxDQUFDb0MsSUFBTCxFQUFXO0FBQ1RBLGlCQUFPLElBQUl2QyxLQUFKLENBQVUsSUFBVixDQUFQO0FBQ0FzQyxtQkFBU0MsSUFBVCxDQUFjcEMsUUFBZCxFQUF3Qm9DLElBQXhCO0FBQ0Q7O0FBRUQsWUFBSW5ELFdBQVcsT0FBZixFQUF3QjtBQUN0Qm1ELGVBQUtuRCxNQUFMLEVBQWEsSUFBYjtBQUNEO0FBQ0YsT0FaTSxDQUFQO0FBYUQsS0F6SWlCOztBQUFBLFVBMklYb0QsY0EzSVcsMkJBMklJQyxhQTNJSixFQTJJbUI7QUFDbkMsYUFBTyxVQUFVbkcsS0FBVixFQUFpQjtBQUN0QixZQUFJQSxLQUFKLEVBQVc7QUFDVEEsZ0JBQU1vRyxjQUFOO0FBQ0Q7O0FBRURELHNCQUFjdEIsS0FBZCxDQUFvQixJQUFwQjtBQUNELE9BTkQ7QUFPRCxLQW5KaUI7O0FBQUE7QUFBQTtBQUFBLDBCQWlERztBQUNuQixlQUFPakIsT0FBUDtBQUNEO0FBbkRpQjs7QUFBQTtBQUFBOztBQXdKcEI7Ozs7OztBQU1BaEYsSUFBRWdDLFFBQUYsRUFBWXlGLEVBQVosQ0FDRWpDLE1BQU1HLGNBRFIsRUFFRUwsU0FBU0MsT0FGWCxFQUdFVCxNQUFNd0MsY0FBTixDQUFxQixJQUFJeEMsS0FBSixFQUFyQixDQUhGOztBQU9BOzs7Ozs7QUFNQTlFLElBQUU0QyxFQUFGLENBQUttQyxJQUFMLElBQXlCRCxNQUFNb0MsZ0JBQS9CO0FBQ0FsSCxJQUFFNEMsRUFBRixDQUFLbUMsSUFBTCxFQUFXMkMsV0FBWCxHQUF5QjVDLEtBQXpCO0FBQ0E5RSxJQUFFNEMsRUFBRixDQUFLbUMsSUFBTCxFQUFXNEMsVUFBWCxHQUF5QixZQUFZO0FBQ25DM0gsTUFBRTRDLEVBQUYsQ0FBS21DLElBQUwsSUFBYUssa0JBQWI7QUFDQSxXQUFPTixNQUFNb0MsZ0JBQWI7QUFDRCxHQUhEOztBQUtBLFNBQU9wQyxLQUFQO0FBRUQsQ0FwTGEsQ0FvTFhELE1BcExXLENBQWQ7Ozs7O0FDVkE7Ozs7Ozs7QUFPQSxJQUFNK0MsU0FBVSxVQUFDNUgsQ0FBRCxFQUFPOztBQUdyQjs7Ozs7O0FBTUEsTUFBTStFLE9BQXNCLFFBQTVCO0FBQ0EsTUFBTUMsVUFBc0IsWUFBNUI7QUFDQSxNQUFNQyxXQUFzQixXQUE1QjtBQUNBLE1BQU1DLGtCQUEwQkQsUUFBaEM7QUFDQSxNQUFNRSxlQUFzQixXQUE1QjtBQUNBLE1BQU1DLHFCQUFzQnBGLEVBQUU0QyxFQUFGLENBQUttQyxJQUFMLENBQTVCOztBQUVBLE1BQU1hLFlBQVk7QUFDaEJpQyxZQUFTLFFBRE87QUFFaEJDLFlBQVMsS0FGTztBQUdoQkMsV0FBUztBQUhPLEdBQWxCOztBQU1BLE1BQU16QyxXQUFXO0FBQ2YwQyx3QkFBcUIseUJBRE47QUFFZkMsaUJBQXFCLHlCQUZOO0FBR2ZDLFdBQXFCLE9BSE47QUFJZkwsWUFBcUIsU0FKTjtBQUtmQyxZQUFxQjtBQUxOLEdBQWpCOztBQVFBLE1BQU10QyxRQUFRO0FBQ1pHLDhCQUE4QlQsU0FBOUIsR0FBMENDLFlBRDlCO0FBRVpnRCx5QkFBc0IsVUFBUWpELFNBQVIsR0FBb0JDLFlBQXBCLG1CQUNPRCxTQURQLEdBQ21CQyxZQURuQjs7QUFLeEI7Ozs7OztBQVBjLEdBQWQ7QUE5QnFCLE1BMkNmeUMsTUEzQ2U7QUE2Q25CLG9CQUFZdEUsT0FBWixFQUFxQjtBQUFBOztBQUNuQixXQUFLMEMsUUFBTCxHQUFnQjFDLE9BQWhCO0FBQ0Q7O0FBR0Q7O0FBT0E7O0FBekRtQixxQkEyRG5COEUsTUEzRG1CLHFCQTJEVjtBQUNQLFVBQUlDLHFCQUFxQixJQUF6QjtBQUNBLFVBQUlDLGlCQUFpQixJQUFyQjtBQUNBLFVBQU1wQyxjQUFtQmxHLEVBQUUsS0FBS2dHLFFBQVAsRUFBaUJXLE9BQWpCLENBQ3ZCckIsU0FBUzJDLFdBRGMsRUFFdkIsQ0FGdUIsQ0FBekI7O0FBSUEsVUFBSS9CLFdBQUosRUFBaUI7QUFDZixZQUFNcUMsUUFBUXZJLEVBQUUsS0FBS2dHLFFBQVAsRUFBaUJ3QyxJQUFqQixDQUFzQmxELFNBQVM0QyxLQUEvQixFQUFzQyxDQUF0QyxDQUFkOztBQUVBLFlBQUlLLEtBQUosRUFBVztBQUNULGNBQUlBLE1BQU1FLElBQU4sS0FBZSxPQUFuQixFQUE0QjtBQUMxQixnQkFBSUYsTUFBTUcsT0FBTixJQUNGMUksRUFBRSxLQUFLZ0csUUFBUCxFQUFpQmMsUUFBakIsQ0FBMEJsQixVQUFVaUMsTUFBcEMsQ0FERixFQUMrQztBQUM3Q1EsbUNBQXFCLEtBQXJCO0FBRUQsYUFKRCxNQUlPO0FBQ0wsa0JBQU1NLGdCQUFnQjNJLEVBQUVrRyxXQUFGLEVBQWVzQyxJQUFmLENBQW9CbEQsU0FBU3VDLE1BQTdCLEVBQXFDLENBQXJDLENBQXRCOztBQUVBLGtCQUFJYyxhQUFKLEVBQW1CO0FBQ2pCM0ksa0JBQUUySSxhQUFGLEVBQWlCOUIsV0FBakIsQ0FBNkJqQixVQUFVaUMsTUFBdkM7QUFDRDtBQUNGO0FBQ0Y7O0FBRUQsY0FBSVEsa0JBQUosRUFBd0I7QUFDdEIsZ0JBQUlFLE1BQU1LLFlBQU4sQ0FBbUIsVUFBbkIsS0FDRjFDLFlBQVkwQyxZQUFaLENBQXlCLFVBQXpCLENBREUsSUFFRkwsTUFBTU0sU0FBTixDQUFnQkMsUUFBaEIsQ0FBeUIsVUFBekIsQ0FGRSxJQUdGNUMsWUFBWTJDLFNBQVosQ0FBc0JDLFFBQXRCLENBQStCLFVBQS9CLENBSEYsRUFHOEM7QUFDNUM7QUFDRDtBQUNEUCxrQkFBTUcsT0FBTixHQUFnQixDQUFDMUksRUFBRSxLQUFLZ0csUUFBUCxFQUFpQmMsUUFBakIsQ0FBMEJsQixVQUFVaUMsTUFBcEMsQ0FBakI7QUFDQTdILGNBQUV1SSxLQUFGLEVBQVN6RSxPQUFULENBQWlCLFFBQWpCO0FBQ0Q7O0FBRUR5RSxnQkFBTVEsS0FBTjtBQUNBVCwyQkFBaUIsS0FBakI7QUFDRDtBQUVGOztBQUVELFVBQUlBLGNBQUosRUFBb0I7QUFDbEIsYUFBS3RDLFFBQUwsQ0FBY2dELFlBQWQsQ0FBMkIsY0FBM0IsRUFDRSxDQUFDaEosRUFBRSxLQUFLZ0csUUFBUCxFQUFpQmMsUUFBakIsQ0FBMEJsQixVQUFVaUMsTUFBcEMsQ0FESDtBQUVEOztBQUVELFVBQUlRLGtCQUFKLEVBQXdCO0FBQ3RCckksVUFBRSxLQUFLZ0csUUFBUCxFQUFpQmlELFdBQWpCLENBQTZCckQsVUFBVWlDLE1BQXZDO0FBQ0Q7QUFDRixLQTdHa0I7O0FBQUEscUJBK0duQnJCLE9BL0dtQixzQkErR1Q7QUFDUnhHLFFBQUV5RyxVQUFGLENBQWEsS0FBS1QsUUFBbEIsRUFBNEJmLFFBQTVCO0FBQ0EsV0FBS2UsUUFBTCxHQUFnQixJQUFoQjtBQUNELEtBbEhrQjs7QUFxSG5COztBQXJIbUIsV0F1SFprQixnQkF2SFksNkJBdUhLaEQsTUF2SEwsRUF1SGE7QUFDOUIsYUFBTyxLQUFLaUQsSUFBTCxDQUFVLFlBQVk7QUFDM0IsWUFBSUUsT0FBT3JILEVBQUUsSUFBRixFQUFRcUgsSUFBUixDQUFhcEMsUUFBYixDQUFYOztBQUVBLFlBQUksQ0FBQ29DLElBQUwsRUFBVztBQUNUQSxpQkFBTyxJQUFJTyxNQUFKLENBQVcsSUFBWCxDQUFQO0FBQ0E1SCxZQUFFLElBQUYsRUFBUXFILElBQVIsQ0FBYXBDLFFBQWIsRUFBdUJvQyxJQUF2QjtBQUNEOztBQUVELFlBQUluRCxXQUFXLFFBQWYsRUFBeUI7QUFDdkJtRCxlQUFLbkQsTUFBTDtBQUNEO0FBQ0YsT0FYTSxDQUFQO0FBWUQsS0FwSWtCOztBQUFBO0FBQUE7QUFBQSwwQkFvREU7QUFDbkIsZUFBT2MsT0FBUDtBQUNEO0FBdERrQjs7QUFBQTtBQUFBOztBQXlJckI7Ozs7OztBQU1BaEYsSUFBRWdDLFFBQUYsRUFDR3lGLEVBREgsQ0FDTWpDLE1BQU1HLGNBRFosRUFDNEJMLFNBQVMwQyxrQkFEckMsRUFDeUQsVUFBQzVHLEtBQUQsRUFBVztBQUNoRUEsVUFBTW9HLGNBQU47O0FBRUEsUUFBSTBCLFNBQVM5SCxNQUFNQyxNQUFuQjs7QUFFQSxRQUFJLENBQUNyQixFQUFFa0osTUFBRixFQUFVcEMsUUFBVixDQUFtQmxCLFVBQVVrQyxNQUE3QixDQUFMLEVBQTJDO0FBQ3pDb0IsZUFBU2xKLEVBQUVrSixNQUFGLEVBQVV2QyxPQUFWLENBQWtCckIsU0FBU3dDLE1BQTNCLENBQVQ7QUFDRDs7QUFFREYsV0FBT1YsZ0JBQVAsQ0FBd0J4RyxJQUF4QixDQUE2QlYsRUFBRWtKLE1BQUYsQ0FBN0IsRUFBd0MsUUFBeEM7QUFDRCxHQVhILEVBWUd6QixFQVpILENBWU1qQyxNQUFNMkMsbUJBWlosRUFZaUM3QyxTQUFTMEMsa0JBWjFDLEVBWThELFVBQUM1RyxLQUFELEVBQVc7QUFDckUsUUFBTThILFNBQVNsSixFQUFFb0IsTUFBTUMsTUFBUixFQUFnQnNGLE9BQWhCLENBQXdCckIsU0FBU3dDLE1BQWpDLEVBQXlDLENBQXpDLENBQWY7QUFDQTlILE1BQUVrSixNQUFGLEVBQVVELFdBQVYsQ0FBc0JyRCxVQUFVbUMsS0FBaEMsRUFBdUMsZUFBZXJELElBQWYsQ0FBb0J0RCxNQUFNcUgsSUFBMUIsQ0FBdkM7QUFDRCxHQWZIOztBQWtCQTs7Ozs7O0FBTUF6SSxJQUFFNEMsRUFBRixDQUFLbUMsSUFBTCxJQUF5QjZDLE9BQU9WLGdCQUFoQztBQUNBbEgsSUFBRTRDLEVBQUYsQ0FBS21DLElBQUwsRUFBVzJDLFdBQVgsR0FBeUJFLE1BQXpCO0FBQ0E1SCxJQUFFNEMsRUFBRixDQUFLbUMsSUFBTCxFQUFXNEMsVUFBWCxHQUF5QixZQUFZO0FBQ25DM0gsTUFBRTRDLEVBQUYsQ0FBS21DLElBQUwsSUFBYUssa0JBQWI7QUFDQSxXQUFPd0MsT0FBT1YsZ0JBQWQ7QUFDRCxHQUhEOztBQUtBLFNBQU9VLE1BQVA7QUFFRCxDQWhMYyxDQWdMWi9DLE1BaExZLENBQWY7Ozs7Ozs7QUNKQTs7Ozs7OztBQU9BLElBQU1zRSxXQUFZLFVBQUNuSixDQUFELEVBQU87O0FBR3ZCOzs7Ozs7QUFNQSxNQUFNK0UsT0FBeUIsVUFBL0I7QUFDQSxNQUFNQyxVQUF5QixZQUEvQjtBQUNBLE1BQU1DLFdBQXlCLGFBQS9CO0FBQ0EsTUFBTUMsa0JBQTZCRCxRQUFuQztBQUNBLE1BQU1FLGVBQXlCLFdBQS9CO0FBQ0EsTUFBTUMscUJBQXlCcEYsRUFBRTRDLEVBQUYsQ0FBS21DLElBQUwsQ0FBL0I7QUFDQSxNQUFNTSxzQkFBeUIsR0FBL0I7QUFDQSxNQUFNK0QscUJBQXlCLEVBQS9CLENBaEJ1QixDQWdCVztBQUNsQyxNQUFNQyxzQkFBeUIsRUFBL0IsQ0FqQnVCLENBaUJXO0FBQ2xDLE1BQU1DLHlCQUF5QixHQUEvQixDQWxCdUIsQ0FrQlk7O0FBRW5DLE1BQU1DLFVBQVU7QUFDZEMsY0FBVyxJQURHO0FBRWRDLGNBQVcsSUFGRztBQUdkQyxXQUFXLEtBSEc7QUFJZEMsV0FBVyxPQUpHO0FBS2RDLFVBQVc7QUFMRyxHQUFoQjs7QUFRQSxNQUFNQyxjQUFjO0FBQ2xCTCxjQUFXLGtCQURPO0FBRWxCQyxjQUFXLFNBRk87QUFHbEJDLFdBQVcsa0JBSE87QUFJbEJDLFdBQVcsa0JBSk87QUFLbEJDLFVBQVc7QUFMTyxHQUFwQjs7QUFRQSxNQUFNRSxZQUFZO0FBQ2hCQyxVQUFXLE1BREs7QUFFaEJDLFVBQVcsTUFGSztBQUdoQkMsVUFBVyxNQUhLO0FBSWhCQyxXQUFXO0FBSkssR0FBbEI7O0FBT0EsTUFBTTFFLFFBQVE7QUFDWjJFLHFCQUF5QmpGLFNBRGI7QUFFWmtGLG1CQUF3QmxGLFNBRlo7QUFHWm1GLHlCQUEyQm5GLFNBSGY7QUFJWm9GLCtCQUE4QnBGLFNBSmxCO0FBS1pxRiwrQkFBOEJyRixTQUxsQjtBQU1ac0YsMkJBQTRCdEYsU0FOaEI7QUFPWnVGLDRCQUF3QnZGLFNBQXhCLEdBQW9DQyxZQVB4QjtBQVFaUSw4QkFBeUJULFNBQXpCLEdBQXFDQztBQVJ6QixHQUFkOztBQVdBLE1BQU1TLFlBQVk7QUFDaEI4RSxjQUFXLFVBREs7QUFFaEI3QyxZQUFXLFFBRks7QUFHaEJzQyxXQUFXLE9BSEs7QUFJaEJELFdBQVcscUJBSks7QUFLaEJELFVBQVcsb0JBTEs7QUFNaEJGLFVBQVcsb0JBTks7QUFPaEJDLFVBQVcsb0JBUEs7QUFRaEJXLFVBQVc7QUFSSyxHQUFsQjs7QUFXQSxNQUFNckYsV0FBVztBQUNmdUMsWUFBYyxTQURDO0FBRWYrQyxpQkFBYyx1QkFGQztBQUdmRCxVQUFjLGdCQUhDO0FBSWZFLGVBQWMsMENBSkM7QUFLZkMsZ0JBQWMsc0JBTEM7QUFNZkMsZ0JBQWMsK0JBTkM7QUFPZkMsZUFBYzs7QUFJaEI7Ozs7OztBQVhpQixHQUFqQjtBQWpFdUIsTUFrRmpCN0IsUUFsRmlCO0FBb0ZyQixzQkFBWTdGLE9BQVosRUFBcUJZLE1BQXJCLEVBQTZCO0FBQUE7O0FBQzNCLFdBQUsrRyxNQUFMLEdBQTBCLElBQTFCO0FBQ0EsV0FBS0MsU0FBTCxHQUEwQixJQUExQjtBQUNBLFdBQUtDLGNBQUwsR0FBMEIsSUFBMUI7O0FBRUEsV0FBS0MsU0FBTCxHQUEwQixLQUExQjtBQUNBLFdBQUtDLFVBQUwsR0FBMEIsS0FBMUI7O0FBRUEsV0FBS0MsWUFBTCxHQUEwQixJQUExQjs7QUFFQSxXQUFLQyxPQUFMLEdBQTBCLEtBQUtDLFVBQUwsQ0FBZ0J0SCxNQUFoQixDQUExQjtBQUNBLFdBQUs4QixRQUFMLEdBQTBCaEcsRUFBRXNELE9BQUYsRUFBVyxDQUFYLENBQTFCO0FBQ0EsV0FBS21JLGtCQUFMLEdBQTBCekwsRUFBRSxLQUFLZ0csUUFBUCxFQUFpQndDLElBQWpCLENBQXNCbEQsU0FBU3dGLFVBQS9CLEVBQTJDLENBQTNDLENBQTFCOztBQUVBLFdBQUtZLGtCQUFMO0FBQ0Q7O0FBR0Q7O0FBV0E7O0FBakhxQix1QkFtSHJCQyxJQW5IcUIsbUJBbUhkO0FBQ0wsVUFBSSxDQUFDLEtBQUtOLFVBQVYsRUFBc0I7QUFDcEIsYUFBS08sTUFBTCxDQUFZOUIsVUFBVUMsSUFBdEI7QUFDRDtBQUNGLEtBdkhvQjs7QUFBQSx1QkF5SHJCOEIsZUF6SHFCLDhCQXlISDtBQUNoQjtBQUNBLFVBQUksQ0FBQzdKLFNBQVM4SixNQUFkLEVBQXNCO0FBQ3BCLGFBQUtILElBQUw7QUFDRDtBQUNGLEtBOUhvQjs7QUFBQSx1QkFnSXJCSSxJQWhJcUIsbUJBZ0lkO0FBQ0wsVUFBSSxDQUFDLEtBQUtWLFVBQVYsRUFBc0I7QUFDcEIsYUFBS08sTUFBTCxDQUFZOUIsVUFBVUUsSUFBdEI7QUFDRDtBQUNGLEtBcElvQjs7QUFBQSx1QkFzSXJCTCxLQXRJcUIsa0JBc0lmdkksS0F0SWUsRUFzSVI7QUFDWCxVQUFJLENBQUNBLEtBQUwsRUFBWTtBQUNWLGFBQUtnSyxTQUFMLEdBQWlCLElBQWpCO0FBQ0Q7O0FBRUQsVUFBSXBMLEVBQUUsS0FBS2dHLFFBQVAsRUFBaUJ3QyxJQUFqQixDQUFzQmxELFNBQVN1RixTQUEvQixFQUEwQyxDQUExQyxLQUNGOUssS0FBSytDLHFCQUFMLEVBREYsRUFDZ0M7QUFDOUIvQyxhQUFLMkMsb0JBQUwsQ0FBMEIsS0FBS3NELFFBQS9CO0FBQ0EsYUFBS2dHLEtBQUwsQ0FBVyxJQUFYO0FBQ0Q7O0FBRURDLG9CQUFjLEtBQUtmLFNBQW5CO0FBQ0EsV0FBS0EsU0FBTCxHQUFpQixJQUFqQjtBQUNELEtBbkpvQjs7QUFBQSx1QkFxSnJCYyxLQXJKcUIsa0JBcUpmNUssS0FySmUsRUFxSlI7QUFDWCxVQUFJLENBQUNBLEtBQUwsRUFBWTtBQUNWLGFBQUtnSyxTQUFMLEdBQWlCLEtBQWpCO0FBQ0Q7O0FBRUQsVUFBSSxLQUFLRixTQUFULEVBQW9CO0FBQ2xCZSxzQkFBYyxLQUFLZixTQUFuQjtBQUNBLGFBQUtBLFNBQUwsR0FBaUIsSUFBakI7QUFDRDs7QUFFRCxVQUFJLEtBQUtLLE9BQUwsQ0FBYS9CLFFBQWIsSUFBeUIsQ0FBQyxLQUFLNEIsU0FBbkMsRUFBOEM7QUFDNUMsYUFBS0YsU0FBTCxHQUFpQmdCLFlBQ2YsQ0FBQ2xLLFNBQVNtSyxlQUFULEdBQTJCLEtBQUtOLGVBQWhDLEdBQWtELEtBQUtGLElBQXhELEVBQThEUyxJQUE5RCxDQUFtRSxJQUFuRSxDQURlLEVBRWYsS0FBS2IsT0FBTCxDQUFhL0IsUUFGRSxDQUFqQjtBQUlEO0FBQ0YsS0FyS29COztBQUFBLHVCQXVLckI2QyxFQXZLcUIsZUF1S2xCQyxLQXZLa0IsRUF1S1g7QUFBQTs7QUFDUixXQUFLbkIsY0FBTCxHQUFzQm5MLEVBQUUsS0FBS2dHLFFBQVAsRUFBaUJ3QyxJQUFqQixDQUFzQmxELFNBQVNzRixXQUEvQixFQUE0QyxDQUE1QyxDQUF0Qjs7QUFFQSxVQUFNMkIsY0FBYyxLQUFLQyxhQUFMLENBQW1CLEtBQUtyQixjQUF4QixDQUFwQjs7QUFFQSxVQUFJbUIsUUFBUSxLQUFLckIsTUFBTCxDQUFZdkgsTUFBWixHQUFxQixDQUE3QixJQUFrQzRJLFFBQVEsQ0FBOUMsRUFBaUQ7QUFDL0M7QUFDRDs7QUFFRCxVQUFJLEtBQUtqQixVQUFULEVBQXFCO0FBQ25CckwsVUFBRSxLQUFLZ0csUUFBUCxFQUFpQnpELEdBQWpCLENBQXFCaUQsTUFBTTRFLElBQTNCLEVBQWlDO0FBQUEsaUJBQU0sTUFBS2lDLEVBQUwsQ0FBUUMsS0FBUixDQUFOO0FBQUEsU0FBakM7QUFDQTtBQUNEOztBQUVELFVBQUlDLGdCQUFnQkQsS0FBcEIsRUFBMkI7QUFDekIsYUFBSzNDLEtBQUw7QUFDQSxhQUFLcUMsS0FBTDtBQUNBO0FBQ0Q7O0FBRUQsVUFBTVMsWUFBWUgsUUFBUUMsV0FBUixHQUNoQnpDLFVBQVVDLElBRE0sR0FFaEJELFVBQVVFLElBRlo7O0FBSUEsV0FBSzRCLE1BQUwsQ0FBWWEsU0FBWixFQUF1QixLQUFLeEIsTUFBTCxDQUFZcUIsS0FBWixDQUF2QjtBQUNELEtBaE1vQjs7QUFBQSx1QkFrTXJCOUYsT0FsTXFCLHNCQWtNWDtBQUNSeEcsUUFBRSxLQUFLZ0csUUFBUCxFQUFpQjBHLEdBQWpCLENBQXFCeEgsU0FBckI7QUFDQWxGLFFBQUV5RyxVQUFGLENBQWEsS0FBS1QsUUFBbEIsRUFBNEJmLFFBQTVCOztBQUVBLFdBQUtnRyxNQUFMLEdBQTBCLElBQTFCO0FBQ0EsV0FBS00sT0FBTCxHQUEwQixJQUExQjtBQUNBLFdBQUt2RixRQUFMLEdBQTBCLElBQTFCO0FBQ0EsV0FBS2tGLFNBQUwsR0FBMEIsSUFBMUI7QUFDQSxXQUFLRSxTQUFMLEdBQTBCLElBQTFCO0FBQ0EsV0FBS0MsVUFBTCxHQUEwQixJQUExQjtBQUNBLFdBQUtGLGNBQUwsR0FBMEIsSUFBMUI7QUFDQSxXQUFLTSxrQkFBTCxHQUEwQixJQUExQjtBQUNELEtBOU1vQjs7QUFpTnJCOztBQWpOcUIsdUJBbU5yQkQsVUFuTnFCLHVCQW1OVnRILE1Bbk5VLEVBbU5GO0FBQ2pCQSxlQUFTbEUsRUFBRTJNLE1BQUYsQ0FBUyxFQUFULEVBQWFwRCxPQUFiLEVBQXNCckYsTUFBdEIsQ0FBVDtBQUNBbkUsV0FBS2lFLGVBQUwsQ0FBcUJlLElBQXJCLEVBQTJCYixNQUEzQixFQUFtQzJGLFdBQW5DO0FBQ0EsYUFBTzNGLE1BQVA7QUFDRCxLQXZOb0I7O0FBQUEsdUJBeU5yQndILGtCQXpOcUIsaUNBeU5BO0FBQUE7O0FBQ25CLFVBQUksS0FBS0gsT0FBTCxDQUFhOUIsUUFBakIsRUFBMkI7QUFDekJ6SixVQUFFLEtBQUtnRyxRQUFQLEVBQ0d5QixFQURILENBQ01qQyxNQUFNNkUsT0FEWixFQUNxQixVQUFDakosS0FBRDtBQUFBLGlCQUFXLE9BQUt3TCxRQUFMLENBQWN4TCxLQUFkLENBQVg7QUFBQSxTQURyQjtBQUVEOztBQUVELFVBQUksS0FBS21LLE9BQUwsQ0FBYTVCLEtBQWIsS0FBdUIsT0FBM0IsRUFBb0M7QUFDbEMzSixVQUFFLEtBQUtnRyxRQUFQLEVBQ0d5QixFQURILENBQ01qQyxNQUFNOEUsVUFEWixFQUN3QixVQUFDbEosS0FBRDtBQUFBLGlCQUFXLE9BQUt1SSxLQUFMLENBQVd2SSxLQUFYLENBQVg7QUFBQSxTQUR4QixFQUVHcUcsRUFGSCxDQUVNakMsTUFBTStFLFVBRlosRUFFd0IsVUFBQ25KLEtBQUQ7QUFBQSxpQkFBVyxPQUFLNEssS0FBTCxDQUFXNUssS0FBWCxDQUFYO0FBQUEsU0FGeEI7QUFHQSxZQUFJLGtCQUFrQlksU0FBUzZLLGVBQS9CLEVBQWdEO0FBQzlDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E3TSxZQUFFLEtBQUtnRyxRQUFQLEVBQWlCeUIsRUFBakIsQ0FBb0JqQyxNQUFNZ0YsUUFBMUIsRUFBb0MsWUFBTTtBQUN4QyxtQkFBS2IsS0FBTDtBQUNBLGdCQUFJLE9BQUsyQixZQUFULEVBQXVCO0FBQ3JCd0IsMkJBQWEsT0FBS3hCLFlBQWxCO0FBQ0Q7QUFDRCxtQkFBS0EsWUFBTCxHQUFvQjdJLFdBQVcsVUFBQ3JCLEtBQUQ7QUFBQSxxQkFBVyxPQUFLNEssS0FBTCxDQUFXNUssS0FBWCxDQUFYO0FBQUEsYUFBWCxFQUF5Q2tJLHlCQUF5QixPQUFLaUMsT0FBTCxDQUFhL0IsUUFBL0UsQ0FBcEI7QUFDRCxXQU5EO0FBT0Q7QUFDRjtBQUNGLEtBcFBvQjs7QUFBQSx1QkFzUHJCb0QsUUF0UHFCLHFCQXNQWnhMLEtBdFBZLEVBc1BMO0FBQ2QsVUFBSSxrQkFBa0JzRCxJQUFsQixDQUF1QnRELE1BQU1DLE1BQU4sQ0FBYTBMLE9BQXBDLENBQUosRUFBa0Q7QUFDaEQ7QUFDRDs7QUFFRCxjQUFRM0wsTUFBTTRMLEtBQWQ7QUFDRSxhQUFLNUQsa0JBQUw7QUFDRWhJLGdCQUFNb0csY0FBTjtBQUNBLGVBQUt1RSxJQUFMO0FBQ0E7QUFDRixhQUFLMUMsbUJBQUw7QUFDRWpJLGdCQUFNb0csY0FBTjtBQUNBLGVBQUttRSxJQUFMO0FBQ0E7QUFDRjtBQUNFO0FBVko7QUFZRCxLQXZRb0I7O0FBQUEsdUJBeVFyQmEsYUF6UXFCLDBCQXlRUGxKLE9BelFPLEVBeVFFO0FBQ3JCLFdBQUsySCxNQUFMLEdBQWNqTCxFQUFFaU4sU0FBRixDQUFZak4sRUFBRXNELE9BQUYsRUFBV29ELE1BQVgsR0FBb0I4QixJQUFwQixDQUF5QmxELFNBQVNxRixJQUFsQyxDQUFaLENBQWQ7QUFDQSxhQUFPLEtBQUtNLE1BQUwsQ0FBWWlDLE9BQVosQ0FBb0I1SixPQUFwQixDQUFQO0FBQ0QsS0E1UW9COztBQUFBLHVCQThRckI2SixtQkE5UXFCLGdDQThRRFYsU0E5UUMsRUE4UVU5RCxhQTlRVixFQThReUI7QUFDNUMsVUFBTXlFLGtCQUFrQlgsY0FBYzNDLFVBQVVDLElBQWhEO0FBQ0EsVUFBTXNELGtCQUFrQlosY0FBYzNDLFVBQVVFLElBQWhEO0FBQ0EsVUFBTXVDLGNBQWtCLEtBQUtDLGFBQUwsQ0FBbUI3RCxhQUFuQixDQUF4QjtBQUNBLFVBQU0yRSxnQkFBa0IsS0FBS3JDLE1BQUwsQ0FBWXZILE1BQVosR0FBcUIsQ0FBN0M7QUFDQSxVQUFNNkosZ0JBQWtCRixtQkFBbUJkLGdCQUFnQixDQUFuQyxJQUNBYSxtQkFBbUJiLGdCQUFnQmUsYUFEM0Q7O0FBR0EsVUFBSUMsaUJBQWlCLENBQUMsS0FBS2hDLE9BQUwsQ0FBYTNCLElBQW5DLEVBQXlDO0FBQ3ZDLGVBQU9qQixhQUFQO0FBQ0Q7O0FBRUQsVUFBTTZFLFFBQVlmLGNBQWMzQyxVQUFVRSxJQUF4QixHQUErQixDQUFDLENBQWhDLEdBQW9DLENBQXREO0FBQ0EsVUFBTXlELFlBQVksQ0FBQ2xCLGNBQWNpQixLQUFmLElBQXdCLEtBQUt2QyxNQUFMLENBQVl2SCxNQUF0RDs7QUFFQSxhQUFPK0osY0FBYyxDQUFDLENBQWYsR0FDTCxLQUFLeEMsTUFBTCxDQUFZLEtBQUtBLE1BQUwsQ0FBWXZILE1BQVosR0FBcUIsQ0FBakMsQ0FESyxHQUNpQyxLQUFLdUgsTUFBTCxDQUFZd0MsU0FBWixDQUR4QztBQUVELEtBL1JvQjs7QUFBQSx1QkFrU3JCQyxrQkFsU3FCLCtCQWtTRkMsYUFsU0UsRUFrU2FDLGtCQWxTYixFQWtTaUM7QUFDcEQsVUFBTUMsY0FBYyxLQUFLckIsYUFBTCxDQUFtQm1CLGFBQW5CLENBQXBCO0FBQ0EsVUFBTUcsWUFBWSxLQUFLdEIsYUFBTCxDQUFtQnhNLEVBQUUsS0FBS2dHLFFBQVAsRUFBaUJ3QyxJQUFqQixDQUFzQmxELFNBQVNzRixXQUEvQixFQUE0QyxDQUE1QyxDQUFuQixDQUFsQjtBQUNBLFVBQU1tRCxhQUFhL04sRUFBRXdGLEtBQUYsQ0FBUUEsTUFBTTJFLEtBQWQsRUFBcUI7QUFDdEN3RCxvQ0FEc0M7QUFFdENsQixtQkFBV21CLGtCQUYyQjtBQUd0Q0ksY0FBTUYsU0FIZ0M7QUFJdEN6QixZQUFJd0I7QUFKa0MsT0FBckIsQ0FBbkI7O0FBT0E3TixRQUFFLEtBQUtnRyxRQUFQLEVBQWlCbEMsT0FBakIsQ0FBeUJpSyxVQUF6Qjs7QUFFQSxhQUFPQSxVQUFQO0FBQ0QsS0EvU29COztBQUFBLHVCQWlUckJFLDBCQWpUcUIsdUNBaVRNM0ssT0FqVE4sRUFpVGU7QUFDbEMsVUFBSSxLQUFLbUksa0JBQVQsRUFBNkI7QUFDM0J6TCxVQUFFLEtBQUt5TCxrQkFBUCxFQUNHakQsSUFESCxDQUNRbEQsU0FBU3VDLE1BRGpCLEVBRUdoQixXQUZILENBRWVqQixVQUFVaUMsTUFGekI7O0FBSUEsWUFBTXFHLGdCQUFnQixLQUFLekMsa0JBQUwsQ0FBd0IwQyxRQUF4QixDQUNwQixLQUFLM0IsYUFBTCxDQUFtQmxKLE9BQW5CLENBRG9CLENBQXRCOztBQUlBLFlBQUk0SyxhQUFKLEVBQW1CO0FBQ2pCbE8sWUFBRWtPLGFBQUYsRUFBaUJFLFFBQWpCLENBQTBCeEksVUFBVWlDLE1BQXBDO0FBQ0Q7QUFDRjtBQUNGLEtBL1RvQjs7QUFBQSx1QkFpVXJCK0QsTUFqVXFCLG1CQWlVZGEsU0FqVWMsRUFpVUhuSixPQWpVRyxFQWlVTTtBQUFBOztBQUN6QixVQUFNcUYsZ0JBQWdCM0ksRUFBRSxLQUFLZ0csUUFBUCxFQUFpQndDLElBQWpCLENBQXNCbEQsU0FBU3NGLFdBQS9CLEVBQTRDLENBQTVDLENBQXRCO0FBQ0EsVUFBTXlELHFCQUFxQixLQUFLN0IsYUFBTCxDQUFtQjdELGFBQW5CLENBQTNCO0FBQ0EsVUFBTTJGLGNBQWdCaEwsV0FBV3FGLGlCQUMvQixLQUFLd0UsbUJBQUwsQ0FBeUJWLFNBQXpCLEVBQW9DOUQsYUFBcEMsQ0FERjtBQUVBLFVBQU00RixtQkFBbUIsS0FBSy9CLGFBQUwsQ0FBbUI4QixXQUFuQixDQUF6QjtBQUNBLFVBQU1FLFlBQVl6SyxRQUFRLEtBQUttSCxTQUFiLENBQWxCOztBQUVBLFVBQUl1RCw2QkFBSjtBQUNBLFVBQUlDLHVCQUFKO0FBQ0EsVUFBSWQsMkJBQUo7O0FBRUEsVUFBSW5CLGNBQWMzQyxVQUFVQyxJQUE1QixFQUFrQztBQUNoQzBFLCtCQUF1QjdJLFVBQVVxRSxJQUFqQztBQUNBeUUseUJBQWlCOUksVUFBVW1FLElBQTNCO0FBQ0E2RCw2QkFBcUI5RCxVQUFVRyxJQUEvQjtBQUNELE9BSkQsTUFJTztBQUNMd0UsK0JBQXVCN0ksVUFBVXNFLEtBQWpDO0FBQ0F3RSx5QkFBaUI5SSxVQUFVb0UsSUFBM0I7QUFDQTRELDZCQUFxQjlELFVBQVVJLEtBQS9CO0FBQ0Q7O0FBRUQsVUFBSW9FLGVBQWV0TyxFQUFFc08sV0FBRixFQUFleEgsUUFBZixDQUF3QmxCLFVBQVVpQyxNQUFsQyxDQUFuQixFQUE4RDtBQUM1RCxhQUFLd0QsVUFBTCxHQUFrQixLQUFsQjtBQUNBO0FBQ0Q7O0FBRUQsVUFBTTBDLGFBQWEsS0FBS0wsa0JBQUwsQ0FBd0JZLFdBQXhCLEVBQXFDVixrQkFBckMsQ0FBbkI7QUFDQSxVQUFJRyxXQUFXekgsa0JBQVgsRUFBSixFQUFxQztBQUNuQztBQUNEOztBQUVELFVBQUksQ0FBQ3FDLGFBQUQsSUFBa0IsQ0FBQzJGLFdBQXZCLEVBQW9DO0FBQ2xDO0FBQ0E7QUFDRDs7QUFFRCxXQUFLakQsVUFBTCxHQUFrQixJQUFsQjs7QUFFQSxVQUFJbUQsU0FBSixFQUFlO0FBQ2IsYUFBSzdFLEtBQUw7QUFDRDs7QUFFRCxXQUFLc0UsMEJBQUwsQ0FBZ0NLLFdBQWhDOztBQUVBLFVBQU1LLFlBQVkzTyxFQUFFd0YsS0FBRixDQUFRQSxNQUFNNEUsSUFBZCxFQUFvQjtBQUNwQ3VELHVCQUFlVyxXQURxQjtBQUVwQzdCLG1CQUFXbUIsa0JBRnlCO0FBR3BDSSxjQUFNSyxrQkFIOEI7QUFJcENoQyxZQUFJa0M7QUFKZ0MsT0FBcEIsQ0FBbEI7O0FBT0EsVUFBSXhPLEtBQUsrQyxxQkFBTCxNQUNGOUMsRUFBRSxLQUFLZ0csUUFBUCxFQUFpQmMsUUFBakIsQ0FBMEJsQixVQUFVdUUsS0FBcEMsQ0FERixFQUM4Qzs7QUFFNUNuSyxVQUFFc08sV0FBRixFQUFlRixRQUFmLENBQXdCTSxjQUF4Qjs7QUFFQTNPLGFBQUs2RCxNQUFMLENBQVkwSyxXQUFaOztBQUVBdE8sVUFBRTJJLGFBQUYsRUFBaUJ5RixRQUFqQixDQUEwQkssb0JBQTFCO0FBQ0F6TyxVQUFFc08sV0FBRixFQUFlRixRQUFmLENBQXdCSyxvQkFBeEI7O0FBRUF6TyxVQUFFMkksYUFBRixFQUNHcEcsR0FESCxDQUNPeEMsS0FBS3lDLGNBRFosRUFDNEIsWUFBTTtBQUM5QnhDLFlBQUVzTyxXQUFGLEVBQ0d6SCxXQURILENBQ2tCNEgsb0JBRGxCLFNBQzBDQyxjQUQxQyxFQUVHTixRQUZILENBRVl4SSxVQUFVaUMsTUFGdEI7O0FBSUE3SCxZQUFFMkksYUFBRixFQUFpQjlCLFdBQWpCLENBQWdDakIsVUFBVWlDLE1BQTFDLFNBQW9ENkcsY0FBcEQsU0FBc0VELG9CQUF0RTs7QUFFQSxpQkFBS3BELFVBQUwsR0FBa0IsS0FBbEI7O0FBRUE1SSxxQkFBVztBQUFBLG1CQUFNekMsRUFBRSxPQUFLZ0csUUFBUCxFQUFpQmxDLE9BQWpCLENBQXlCNkssU0FBekIsQ0FBTjtBQUFBLFdBQVgsRUFBc0QsQ0FBdEQ7QUFFRCxTQVpILEVBYUc5TCxvQkFiSCxDQWF3QndDLG1CQWJ4QjtBQWVELE9BekJELE1BeUJPO0FBQ0xyRixVQUFFMkksYUFBRixFQUFpQjlCLFdBQWpCLENBQTZCakIsVUFBVWlDLE1BQXZDO0FBQ0E3SCxVQUFFc08sV0FBRixFQUFlRixRQUFmLENBQXdCeEksVUFBVWlDLE1BQWxDOztBQUVBLGFBQUt3RCxVQUFMLEdBQWtCLEtBQWxCO0FBQ0FyTCxVQUFFLEtBQUtnRyxRQUFQLEVBQWlCbEMsT0FBakIsQ0FBeUI2SyxTQUF6QjtBQUNEOztBQUVELFVBQUlILFNBQUosRUFBZTtBQUNiLGFBQUt4QyxLQUFMO0FBQ0Q7QUFDRixLQXpab0I7O0FBNFpyQjs7QUE1WnFCLGFBOFpkOUUsZ0JBOVpjLDZCQThaR2hELE1BOVpILEVBOFpXO0FBQzlCLGFBQU8sS0FBS2lELElBQUwsQ0FBVSxZQUFZO0FBQzNCLFlBQUlFLE9BQVlySCxFQUFFLElBQUYsRUFBUXFILElBQVIsQ0FBYXBDLFFBQWIsQ0FBaEI7QUFDQSxZQUFNc0csVUFBVXZMLEVBQUUyTSxNQUFGLENBQVMsRUFBVCxFQUFhcEQsT0FBYixFQUFzQnZKLEVBQUUsSUFBRixFQUFRcUgsSUFBUixFQUF0QixDQUFoQjs7QUFFQSxZQUFJLFFBQU9uRCxNQUFQLHlDQUFPQSxNQUFQLE9BQWtCLFFBQXRCLEVBQWdDO0FBQzlCbEUsWUFBRTJNLE1BQUYsQ0FBU3BCLE9BQVQsRUFBa0JySCxNQUFsQjtBQUNEOztBQUVELFlBQU0wSyxTQUFTLE9BQU8xSyxNQUFQLEtBQWtCLFFBQWxCLEdBQTZCQSxNQUE3QixHQUFzQ3FILFFBQVE3QixLQUE3RDs7QUFFQSxZQUFJLENBQUNyQyxJQUFMLEVBQVc7QUFDVEEsaUJBQU8sSUFBSThCLFFBQUosQ0FBYSxJQUFiLEVBQW1Cb0MsT0FBbkIsQ0FBUDtBQUNBdkwsWUFBRSxJQUFGLEVBQVFxSCxJQUFSLENBQWFwQyxRQUFiLEVBQXVCb0MsSUFBdkI7QUFDRDs7QUFFRCxZQUFJLE9BQU9uRCxNQUFQLEtBQWtCLFFBQXRCLEVBQWdDO0FBQzlCbUQsZUFBS2dGLEVBQUwsQ0FBUW5JLE1BQVI7QUFDRCxTQUZELE1BRU8sSUFBSSxPQUFPMEssTUFBUCxLQUFrQixRQUF0QixFQUFnQztBQUNyQyxjQUFJdkgsS0FBS3VILE1BQUwsTUFBaUJqTixTQUFyQixFQUFnQztBQUM5QixrQkFBTSxJQUFJZ0QsS0FBSix1QkFBOEJpSyxNQUE5QixPQUFOO0FBQ0Q7QUFDRHZILGVBQUt1SCxNQUFMO0FBQ0QsU0FMTSxNQUtBLElBQUlyRCxRQUFRL0IsUUFBWixFQUFzQjtBQUMzQm5DLGVBQUtzQyxLQUFMO0FBQ0F0QyxlQUFLMkUsS0FBTDtBQUNEO0FBQ0YsT0ExQk0sQ0FBUDtBQTJCRCxLQTFib0I7O0FBQUEsYUE0YmQ2QyxvQkE1YmMsaUNBNGJPek4sS0E1YlAsRUE0YmM7QUFDakMsVUFBTW1DLFdBQVd4RCxLQUFLc0Qsc0JBQUwsQ0FBNEIsSUFBNUIsQ0FBakI7O0FBRUEsVUFBSSxDQUFDRSxRQUFMLEVBQWU7QUFDYjtBQUNEOztBQUVELFVBQU1sQyxTQUFTckIsRUFBRXVELFFBQUYsRUFBWSxDQUFaLENBQWY7O0FBRUEsVUFBSSxDQUFDbEMsTUFBRCxJQUFXLENBQUNyQixFQUFFcUIsTUFBRixFQUFVeUYsUUFBVixDQUFtQmxCLFVBQVU4RSxRQUE3QixDQUFoQixFQUF3RDtBQUN0RDtBQUNEOztBQUVELFVBQU14RyxTQUFhbEUsRUFBRTJNLE1BQUYsQ0FBUyxFQUFULEVBQWEzTSxFQUFFcUIsTUFBRixFQUFVZ0csSUFBVixFQUFiLEVBQStCckgsRUFBRSxJQUFGLEVBQVFxSCxJQUFSLEVBQS9CLENBQW5CO0FBQ0EsVUFBTXlILGFBQWEsS0FBS3RMLFlBQUwsQ0FBa0IsZUFBbEIsQ0FBbkI7O0FBRUEsVUFBSXNMLFVBQUosRUFBZ0I7QUFDZDVLLGVBQU9zRixRQUFQLEdBQWtCLEtBQWxCO0FBQ0Q7O0FBRURMLGVBQVNqQyxnQkFBVCxDQUEwQnhHLElBQTFCLENBQStCVixFQUFFcUIsTUFBRixDQUEvQixFQUEwQzZDLE1BQTFDOztBQUVBLFVBQUk0SyxVQUFKLEVBQWdCO0FBQ2Q5TyxVQUFFcUIsTUFBRixFQUFVZ0csSUFBVixDQUFlcEMsUUFBZixFQUF5Qm9ILEVBQXpCLENBQTRCeUMsVUFBNUI7QUFDRDs7QUFFRDFOLFlBQU1vRyxjQUFOO0FBQ0QsS0F2ZG9COztBQUFBO0FBQUE7QUFBQSwwQkF3R0E7QUFDbkIsZUFBT3hDLE9BQVA7QUFDRDtBQTFHb0I7QUFBQTtBQUFBLDBCQTRHQTtBQUNuQixlQUFPdUUsT0FBUDtBQUNEO0FBOUdvQjs7QUFBQTtBQUFBOztBQTRkdkI7Ozs7OztBQU1BdkosSUFBRWdDLFFBQUYsRUFDR3lGLEVBREgsQ0FDTWpDLE1BQU1HLGNBRFosRUFDNEJMLFNBQVN5RixVQURyQyxFQUNpRDVCLFNBQVMwRixvQkFEMUQ7O0FBR0E3TyxJQUFFNkIsTUFBRixFQUFVNEYsRUFBVixDQUFhakMsTUFBTWlGLGFBQW5CLEVBQWtDLFlBQU07QUFDdEN6SyxNQUFFc0YsU0FBUzBGLFNBQVgsRUFBc0I3RCxJQUF0QixDQUEyQixZQUFZO0FBQ3JDLFVBQU00SCxZQUFZL08sRUFBRSxJQUFGLENBQWxCO0FBQ0FtSixlQUFTakMsZ0JBQVQsQ0FBMEJ4RyxJQUExQixDQUErQnFPLFNBQS9CLEVBQTBDQSxVQUFVMUgsSUFBVixFQUExQztBQUNELEtBSEQ7QUFJRCxHQUxEOztBQVFBOzs7Ozs7QUFNQXJILElBQUU0QyxFQUFGLENBQUttQyxJQUFMLElBQXlCb0UsU0FBU2pDLGdCQUFsQztBQUNBbEgsSUFBRTRDLEVBQUYsQ0FBS21DLElBQUwsRUFBVzJDLFdBQVgsR0FBeUJ5QixRQUF6QjtBQUNBbkosSUFBRTRDLEVBQUYsQ0FBS21DLElBQUwsRUFBVzRDLFVBQVgsR0FBeUIsWUFBWTtBQUNuQzNILE1BQUU0QyxFQUFGLENBQUttQyxJQUFMLElBQWFLLGtCQUFiO0FBQ0EsV0FBTytELFNBQVNqQyxnQkFBaEI7QUFDRCxHQUhEOztBQUtBLFNBQU9pQyxRQUFQO0FBRUQsQ0E1ZmdCLENBNGZkdEUsTUE1ZmMsQ0FBakI7Ozs7Ozs7QUNQQTs7Ozs7OztBQU9BLElBQU1tSyxXQUFZLFVBQUNoUCxDQUFELEVBQU87O0FBR3ZCOzs7Ozs7QUFNQSxNQUFNK0UsT0FBc0IsVUFBNUI7QUFDQSxNQUFNQyxVQUFzQixZQUE1QjtBQUNBLE1BQU1DLFdBQXNCLGFBQTVCO0FBQ0EsTUFBTUMsa0JBQTBCRCxRQUFoQztBQUNBLE1BQU1FLGVBQXNCLFdBQTVCO0FBQ0EsTUFBTUMscUJBQXNCcEYsRUFBRTRDLEVBQUYsQ0FBS21DLElBQUwsQ0FBNUI7QUFDQSxNQUFNTSxzQkFBc0IsR0FBNUI7O0FBRUEsTUFBTWtFLFVBQVU7QUFDZG5CLFlBQVMsSUFESztBQUVkMUIsWUFBUztBQUZLLEdBQWhCOztBQUtBLE1BQU1tRCxjQUFjO0FBQ2xCekIsWUFBUyxTQURTO0FBRWxCMUIsWUFBUztBQUZTLEdBQXBCOztBQUtBLE1BQU1sQixRQUFRO0FBQ1pPLG1CQUF3QmIsU0FEWjtBQUVaK0oscUJBQXlCL0osU0FGYjtBQUdaZ0ssbUJBQXdCaEssU0FIWjtBQUlaaUssdUJBQTBCakssU0FKZDtBQUtaUyw4QkFBeUJULFNBQXpCLEdBQXFDQztBQUx6QixHQUFkOztBQVFBLE1BQU1TLFlBQVk7QUFDaEJHLFVBQWEsTUFERztBQUVoQnFKLGNBQWEsVUFGRztBQUdoQkMsZ0JBQWEsWUFIRztBQUloQkMsZUFBYTtBQUpHLEdBQWxCOztBQU9BLE1BQU1DLFlBQVk7QUFDaEJDLFdBQVMsT0FETztBQUVoQkMsWUFBUztBQUZPLEdBQWxCOztBQUtBLE1BQU1uSyxXQUFXO0FBQ2ZvSyxhQUFjLG9CQURDO0FBRWZ6SCxpQkFBYzs7QUFJaEI7Ozs7OztBQU5pQixHQUFqQjtBQS9DdUIsTUEyRGpCK0csUUEzRGlCO0FBNkRyQixzQkFBWTFMLE9BQVosRUFBcUJZLE1BQXJCLEVBQTZCO0FBQUE7O0FBQzNCLFdBQUt5TCxnQkFBTCxHQUF3QixLQUF4QjtBQUNBLFdBQUszSixRQUFMLEdBQXdCMUMsT0FBeEI7QUFDQSxXQUFLaUksT0FBTCxHQUF3QixLQUFLQyxVQUFMLENBQWdCdEgsTUFBaEIsQ0FBeEI7QUFDQSxXQUFLMEwsYUFBTCxHQUF3QjVQLEVBQUVpTixTQUFGLENBQVlqTixFQUNsQyxxQ0FBbUNzRCxRQUFRdU0sRUFBM0Msd0RBQzBDdk0sUUFBUXVNLEVBRGxELFFBRGtDLENBQVosQ0FBeEI7QUFJQSxVQUFNQyxhQUFhOVAsRUFBRXNGLFNBQVMyQyxXQUFYLENBQW5CO0FBQ0EsV0FBSyxJQUFJOEgsSUFBSSxDQUFiLEVBQWdCQSxJQUFJRCxXQUFXcE0sTUFBL0IsRUFBdUNxTSxHQUF2QyxFQUE0QztBQUMxQyxZQUFNQyxPQUFPRixXQUFXQyxDQUFYLENBQWI7QUFDQSxZQUFNeE0sV0FBV3hELEtBQUtzRCxzQkFBTCxDQUE0QjJNLElBQTVCLENBQWpCO0FBQ0EsWUFBSXpNLGFBQWEsSUFBYixJQUFxQnZELEVBQUV1RCxRQUFGLEVBQVkwTSxNQUFaLENBQW1CM00sT0FBbkIsRUFBNEJJLE1BQTVCLEdBQXFDLENBQTlELEVBQWlFO0FBQy9ELGVBQUtrTSxhQUFMLENBQW1CTSxJQUFuQixDQUF3QkYsSUFBeEI7QUFDRDtBQUNGOztBQUVELFdBQUtHLE9BQUwsR0FBZSxLQUFLNUUsT0FBTCxDQUFhN0UsTUFBYixHQUFzQixLQUFLMEosVUFBTCxFQUF0QixHQUEwQyxJQUF6RDs7QUFFQSxVQUFJLENBQUMsS0FBSzdFLE9BQUwsQ0FBYTdFLE1BQWxCLEVBQTBCO0FBQ3hCLGFBQUsySix5QkFBTCxDQUErQixLQUFLckssUUFBcEMsRUFBOEMsS0FBSzRKLGFBQW5EO0FBQ0Q7O0FBRUQsVUFBSSxLQUFLckUsT0FBTCxDQUFhbkQsTUFBakIsRUFBeUI7QUFDdkIsYUFBS0EsTUFBTDtBQUNEO0FBQ0Y7O0FBR0Q7O0FBV0E7O0FBckdxQix1QkF1R3JCQSxNQXZHcUIscUJBdUdaO0FBQ1AsVUFBSXBJLEVBQUUsS0FBS2dHLFFBQVAsRUFBaUJjLFFBQWpCLENBQTBCbEIsVUFBVUcsSUFBcEMsQ0FBSixFQUErQztBQUM3QyxhQUFLdUssSUFBTDtBQUNELE9BRkQsTUFFTztBQUNMLGFBQUtDLElBQUw7QUFDRDtBQUNGLEtBN0dvQjs7QUFBQSx1QkErR3JCQSxJQS9HcUIsbUJBK0dkO0FBQUE7O0FBQ0wsVUFBSSxLQUFLWixnQkFBTCxJQUNGM1AsRUFBRSxLQUFLZ0csUUFBUCxFQUFpQmMsUUFBakIsQ0FBMEJsQixVQUFVRyxJQUFwQyxDQURGLEVBQzZDO0FBQzNDO0FBQ0Q7O0FBRUQsVUFBSXlLLGdCQUFKO0FBQ0EsVUFBSUMsb0JBQUo7O0FBRUEsVUFBSSxLQUFLTixPQUFULEVBQWtCO0FBQ2hCSyxrQkFBVXhRLEVBQUVpTixTQUFGLENBQVlqTixFQUFFLEtBQUttUSxPQUFQLEVBQWdCaEMsUUFBaEIsR0FBMkJBLFFBQTNCLENBQW9DN0ksU0FBU29LLE9BQTdDLENBQVosQ0FBVjtBQUNBLFlBQUksQ0FBQ2MsUUFBUTlNLE1BQWIsRUFBcUI7QUFDbkI4TSxvQkFBVSxJQUFWO0FBQ0Q7QUFDRjs7QUFFRCxVQUFJQSxPQUFKLEVBQWE7QUFDWEMsc0JBQWN6USxFQUFFd1EsT0FBRixFQUFXbkosSUFBWCxDQUFnQnBDLFFBQWhCLENBQWQ7QUFDQSxZQUFJd0wsZUFBZUEsWUFBWWQsZ0JBQS9CLEVBQWlEO0FBQy9DO0FBQ0Q7QUFDRjs7QUFFRCxVQUFNZSxhQUFhMVEsRUFBRXdGLEtBQUYsQ0FBUUEsTUFBTU8sSUFBZCxDQUFuQjtBQUNBL0YsUUFBRSxLQUFLZ0csUUFBUCxFQUFpQmxDLE9BQWpCLENBQXlCNE0sVUFBekI7QUFDQSxVQUFJQSxXQUFXcEssa0JBQVgsRUFBSixFQUFxQztBQUNuQztBQUNEOztBQUVELFVBQUlrSyxPQUFKLEVBQWE7QUFDWHhCLGlCQUFTOUgsZ0JBQVQsQ0FBMEJ4RyxJQUExQixDQUErQlYsRUFBRXdRLE9BQUYsQ0FBL0IsRUFBMkMsTUFBM0M7QUFDQSxZQUFJLENBQUNDLFdBQUwsRUFBa0I7QUFDaEJ6USxZQUFFd1EsT0FBRixFQUFXbkosSUFBWCxDQUFnQnBDLFFBQWhCLEVBQTBCLElBQTFCO0FBQ0Q7QUFDRjs7QUFFRCxVQUFNMEwsWUFBWSxLQUFLQyxhQUFMLEVBQWxCOztBQUVBNVEsUUFBRSxLQUFLZ0csUUFBUCxFQUNHYSxXQURILENBQ2VqQixVQUFVd0osUUFEekIsRUFFR2hCLFFBRkgsQ0FFWXhJLFVBQVV5SixVQUZ0Qjs7QUFJQSxXQUFLckosUUFBTCxDQUFjN0QsS0FBZCxDQUFvQndPLFNBQXBCLElBQWlDLENBQWpDOztBQUVBLFVBQUksS0FBS2YsYUFBTCxDQUFtQmxNLE1BQXZCLEVBQStCO0FBQzdCMUQsVUFBRSxLQUFLNFAsYUFBUCxFQUNHL0ksV0FESCxDQUNlakIsVUFBVTBKLFNBRHpCLEVBRUd1QixJQUZILENBRVEsZUFGUixFQUV5QixJQUZ6QjtBQUdEOztBQUVELFdBQUtDLGdCQUFMLENBQXNCLElBQXRCOztBQUVBLFVBQU1DLFdBQVcsU0FBWEEsUUFBVyxHQUFNO0FBQ3JCL1EsVUFBRSxNQUFLZ0csUUFBUCxFQUNHYSxXQURILENBQ2VqQixVQUFVeUosVUFEekIsRUFFR2pCLFFBRkgsQ0FFWXhJLFVBQVV3SixRQUZ0QixFQUdHaEIsUUFISCxDQUdZeEksVUFBVUcsSUFIdEI7O0FBS0EsY0FBS0MsUUFBTCxDQUFjN0QsS0FBZCxDQUFvQndPLFNBQXBCLElBQWlDLEVBQWpDOztBQUVBLGNBQUtHLGdCQUFMLENBQXNCLEtBQXRCOztBQUVBOVEsVUFBRSxNQUFLZ0csUUFBUCxFQUFpQmxDLE9BQWpCLENBQXlCMEIsTUFBTXlKLEtBQS9CO0FBQ0QsT0FYRDs7QUFhQSxVQUFJLENBQUNsUCxLQUFLK0MscUJBQUwsRUFBTCxFQUFtQztBQUNqQ2lPO0FBQ0E7QUFDRDs7QUFFRCxVQUFNQyx1QkFBdUJMLFVBQVUsQ0FBVixFQUFhL0wsV0FBYixLQUE2QitMLFVBQVVNLEtBQVYsQ0FBZ0IsQ0FBaEIsQ0FBMUQ7QUFDQSxVQUFNQyx3QkFBZ0NGLG9CQUF0Qzs7QUFFQWhSLFFBQUUsS0FBS2dHLFFBQVAsRUFDR3pELEdBREgsQ0FDT3hDLEtBQUt5QyxjQURaLEVBQzRCdU8sUUFENUIsRUFFR2xPLG9CQUZILENBRXdCd0MsbUJBRnhCOztBQUlBLFdBQUtXLFFBQUwsQ0FBYzdELEtBQWQsQ0FBb0J3TyxTQUFwQixJQUFvQyxLQUFLM0ssUUFBTCxDQUFja0wsVUFBZCxDQUFwQztBQUNELEtBN0xvQjs7QUFBQSx1QkErTHJCWixJQS9McUIsbUJBK0xkO0FBQUE7O0FBQ0wsVUFBSSxLQUFLWCxnQkFBTCxJQUNGLENBQUMzUCxFQUFFLEtBQUtnRyxRQUFQLEVBQWlCYyxRQUFqQixDQUEwQmxCLFVBQVVHLElBQXBDLENBREgsRUFDOEM7QUFDNUM7QUFDRDs7QUFFRCxVQUFNMkssYUFBYTFRLEVBQUV3RixLQUFGLENBQVFBLE1BQU0wSixJQUFkLENBQW5CO0FBQ0FsUCxRQUFFLEtBQUtnRyxRQUFQLEVBQWlCbEMsT0FBakIsQ0FBeUI0TSxVQUF6QjtBQUNBLFVBQUlBLFdBQVdwSyxrQkFBWCxFQUFKLEVBQXFDO0FBQ25DO0FBQ0Q7O0FBRUQsVUFBTXFLLFlBQWtCLEtBQUtDLGFBQUwsRUFBeEI7O0FBRUEsV0FBSzVLLFFBQUwsQ0FBYzdELEtBQWQsQ0FBb0J3TyxTQUFwQixJQUFvQyxLQUFLM0ssUUFBTCxDQUFjbUwscUJBQWQsR0FBc0NSLFNBQXRDLENBQXBDOztBQUVBNVEsV0FBSzZELE1BQUwsQ0FBWSxLQUFLb0MsUUFBakI7O0FBRUFoRyxRQUFFLEtBQUtnRyxRQUFQLEVBQ0dvSSxRQURILENBQ1l4SSxVQUFVeUosVUFEdEIsRUFFR3hJLFdBRkgsQ0FFZWpCLFVBQVV3SixRQUZ6QixFQUdHdkksV0FISCxDQUdlakIsVUFBVUcsSUFIekI7O0FBS0EsVUFBSSxLQUFLNkosYUFBTCxDQUFtQmxNLE1BQXZCLEVBQStCO0FBQzdCLGFBQUssSUFBSXFNLElBQUksQ0FBYixFQUFnQkEsSUFBSSxLQUFLSCxhQUFMLENBQW1CbE0sTUFBdkMsRUFBK0NxTSxHQUEvQyxFQUFvRDtBQUNsRCxjQUFNak0sVUFBVSxLQUFLOEwsYUFBTCxDQUFtQkcsQ0FBbkIsQ0FBaEI7QUFDQSxjQUFNeE0sV0FBV3hELEtBQUtzRCxzQkFBTCxDQUE0QlMsT0FBNUIsQ0FBakI7QUFDQSxjQUFJUCxhQUFhLElBQWpCLEVBQXVCO0FBQ3JCLGdCQUFNNk4sUUFBUXBSLEVBQUV1RCxRQUFGLENBQWQ7QUFDQSxnQkFBSSxDQUFDNk4sTUFBTXRLLFFBQU4sQ0FBZWxCLFVBQVVHLElBQXpCLENBQUwsRUFBcUM7QUFDbkMvRixnQkFBRThELE9BQUYsRUFBV3NLLFFBQVgsQ0FBb0J4SSxVQUFVMEosU0FBOUIsRUFDTXVCLElBRE4sQ0FDVyxlQURYLEVBQzRCLEtBRDVCO0FBRUQ7QUFDRjtBQUNGO0FBQ0Y7O0FBRUQsV0FBS0MsZ0JBQUwsQ0FBc0IsSUFBdEI7O0FBRUEsVUFBTUMsV0FBVyxTQUFYQSxRQUFXLEdBQU07QUFDckIsZUFBS0QsZ0JBQUwsQ0FBc0IsS0FBdEI7QUFDQTlRLFVBQUUsT0FBS2dHLFFBQVAsRUFDR2EsV0FESCxDQUNlakIsVUFBVXlKLFVBRHpCLEVBRUdqQixRQUZILENBRVl4SSxVQUFVd0osUUFGdEIsRUFHR3RMLE9BSEgsQ0FHVzBCLE1BQU0ySixNQUhqQjtBQUlELE9BTkQ7O0FBUUEsV0FBS25KLFFBQUwsQ0FBYzdELEtBQWQsQ0FBb0J3TyxTQUFwQixJQUFpQyxFQUFqQzs7QUFFQSxVQUFJLENBQUM1USxLQUFLK0MscUJBQUwsRUFBTCxFQUFtQztBQUNqQ2lPO0FBQ0E7QUFDRDs7QUFFRC9RLFFBQUUsS0FBS2dHLFFBQVAsRUFDR3pELEdBREgsQ0FDT3hDLEtBQUt5QyxjQURaLEVBQzRCdU8sUUFENUIsRUFFR2xPLG9CQUZILENBRXdCd0MsbUJBRnhCO0FBR0QsS0F4UG9COztBQUFBLHVCQTBQckJ5TCxnQkExUHFCLDZCQTBQSk8sZUExUEksRUEwUGE7QUFDaEMsV0FBSzFCLGdCQUFMLEdBQXdCMEIsZUFBeEI7QUFDRCxLQTVQb0I7O0FBQUEsdUJBOFByQjdLLE9BOVBxQixzQkE4UFg7QUFDUnhHLFFBQUV5RyxVQUFGLENBQWEsS0FBS1QsUUFBbEIsRUFBNEJmLFFBQTVCOztBQUVBLFdBQUtzRyxPQUFMLEdBQXdCLElBQXhCO0FBQ0EsV0FBSzRFLE9BQUwsR0FBd0IsSUFBeEI7QUFDQSxXQUFLbkssUUFBTCxHQUF3QixJQUF4QjtBQUNBLFdBQUs0SixhQUFMLEdBQXdCLElBQXhCO0FBQ0EsV0FBS0QsZ0JBQUwsR0FBd0IsSUFBeEI7QUFDRCxLQXRRb0I7O0FBeVFyQjs7QUF6UXFCLHVCQTJRckJuRSxVQTNRcUIsdUJBMlFWdEgsTUEzUVUsRUEyUUY7QUFDakJBLGVBQVNsRSxFQUFFMk0sTUFBRixDQUFTLEVBQVQsRUFBYXBELE9BQWIsRUFBc0JyRixNQUF0QixDQUFUO0FBQ0FBLGFBQU9rRSxNQUFQLEdBQWdCckUsUUFBUUcsT0FBT2tFLE1BQWYsQ0FBaEIsQ0FGaUIsQ0FFc0I7QUFDdkNySSxXQUFLaUUsZUFBTCxDQUFxQmUsSUFBckIsRUFBMkJiLE1BQTNCLEVBQW1DMkYsV0FBbkM7QUFDQSxhQUFPM0YsTUFBUDtBQUNELEtBaFJvQjs7QUFBQSx1QkFrUnJCME0sYUFsUnFCLDRCQWtSTDtBQUNkLFVBQU1VLFdBQVd0UixFQUFFLEtBQUtnRyxRQUFQLEVBQWlCYyxRQUFqQixDQUEwQnlJLFVBQVVDLEtBQXBDLENBQWpCO0FBQ0EsYUFBTzhCLFdBQVcvQixVQUFVQyxLQUFyQixHQUE2QkQsVUFBVUUsTUFBOUM7QUFDRCxLQXJSb0I7O0FBQUEsdUJBdVJyQlcsVUF2UnFCLHlCQXVSUjtBQUFBOztBQUNYLFVBQU0xSixTQUFXMUcsRUFBRSxLQUFLdUwsT0FBTCxDQUFhN0UsTUFBZixFQUF1QixDQUF2QixDQUFqQjtBQUNBLFVBQU1uRCxzREFDcUMsS0FBS2dJLE9BQUwsQ0FBYTdFLE1BRGxELE9BQU47O0FBR0ExRyxRQUFFMEcsTUFBRixFQUFVOEIsSUFBVixDQUFlakYsUUFBZixFQUF5QjRELElBQXpCLENBQThCLFVBQUM0SSxDQUFELEVBQUl6TSxPQUFKLEVBQWdCO0FBQzVDLGVBQUsrTSx5QkFBTCxDQUNFckIsU0FBU3VDLHFCQUFULENBQStCak8sT0FBL0IsQ0FERixFQUVFLENBQUNBLE9BQUQsQ0FGRjtBQUlELE9BTEQ7O0FBT0EsYUFBT29ELE1BQVA7QUFDRCxLQXBTb0I7O0FBQUEsdUJBc1NyQjJKLHlCQXRTcUIsc0NBc1NLL00sT0F0U0wsRUFzU2NrTyxZQXRTZCxFQXNTNEI7QUFDL0MsVUFBSWxPLE9BQUosRUFBYTtBQUNYLFlBQU1tTyxTQUFTelIsRUFBRXNELE9BQUYsRUFBV3dELFFBQVgsQ0FBb0JsQixVQUFVRyxJQUE5QixDQUFmOztBQUVBLFlBQUl5TCxhQUFhOU4sTUFBakIsRUFBeUI7QUFDdkIxRCxZQUFFd1IsWUFBRixFQUNHdkksV0FESCxDQUNlckQsVUFBVTBKLFNBRHpCLEVBQ29DLENBQUNtQyxNQURyQyxFQUVHWixJQUZILENBRVEsZUFGUixFQUV5QlksTUFGekI7QUFHRDtBQUNGO0FBQ0YsS0FoVG9COztBQW1UckI7O0FBblRxQixhQXFUZEYscUJBclRjLGtDQXFUUWpPLE9BclRSLEVBcVRpQjtBQUNwQyxVQUFNQyxXQUFXeEQsS0FBS3NELHNCQUFMLENBQTRCQyxPQUE1QixDQUFqQjtBQUNBLGFBQU9DLFdBQVd2RCxFQUFFdUQsUUFBRixFQUFZLENBQVosQ0FBWCxHQUE0QixJQUFuQztBQUNELEtBeFRvQjs7QUFBQSxhQTBUZDJELGdCQTFUYyw2QkEwVEdoRCxNQTFUSCxFQTBUVztBQUM5QixhQUFPLEtBQUtpRCxJQUFMLENBQVUsWUFBWTtBQUMzQixZQUFNdUssUUFBVTFSLEVBQUUsSUFBRixDQUFoQjtBQUNBLFlBQUlxSCxPQUFZcUssTUFBTXJLLElBQU4sQ0FBV3BDLFFBQVgsQ0FBaEI7QUFDQSxZQUFNc0csVUFBVXZMLEVBQUUyTSxNQUFGLENBQ2QsRUFEYyxFQUVkcEQsT0FGYyxFQUdkbUksTUFBTXJLLElBQU4sRUFIYyxFQUlkLFFBQU9uRCxNQUFQLHlDQUFPQSxNQUFQLE9BQWtCLFFBQWxCLElBQThCQSxNQUpoQixDQUFoQjs7QUFPQSxZQUFJLENBQUNtRCxJQUFELElBQVNrRSxRQUFRbkQsTUFBakIsSUFBMkIsWUFBWTFELElBQVosQ0FBaUJSLE1BQWpCLENBQS9CLEVBQXlEO0FBQ3ZEcUgsa0JBQVFuRCxNQUFSLEdBQWlCLEtBQWpCO0FBQ0Q7O0FBRUQsWUFBSSxDQUFDZixJQUFMLEVBQVc7QUFDVEEsaUJBQU8sSUFBSTJILFFBQUosQ0FBYSxJQUFiLEVBQW1CekQsT0FBbkIsQ0FBUDtBQUNBbUcsZ0JBQU1ySyxJQUFOLENBQVdwQyxRQUFYLEVBQXFCb0MsSUFBckI7QUFDRDs7QUFFRCxZQUFJLE9BQU9uRCxNQUFQLEtBQWtCLFFBQXRCLEVBQWdDO0FBQzlCLGNBQUltRCxLQUFLbkQsTUFBTCxNQUFpQnZDLFNBQXJCLEVBQWdDO0FBQzlCLGtCQUFNLElBQUlnRCxLQUFKLHVCQUE4QlQsTUFBOUIsT0FBTjtBQUNEO0FBQ0RtRCxlQUFLbkQsTUFBTDtBQUNEO0FBQ0YsT0F6Qk0sQ0FBUDtBQTBCRCxLQXJWb0I7O0FBQUE7QUFBQTtBQUFBLDBCQTRGQTtBQUNuQixlQUFPYyxPQUFQO0FBQ0Q7QUE5Rm9CO0FBQUE7QUFBQSwwQkFnR0E7QUFDbkIsZUFBT3VFLE9BQVA7QUFDRDtBQWxHb0I7O0FBQUE7QUFBQTs7QUEwVnZCOzs7Ozs7QUFNQXZKLElBQUVnQyxRQUFGLEVBQVl5RixFQUFaLENBQWVqQyxNQUFNRyxjQUFyQixFQUFxQ0wsU0FBUzJDLFdBQTlDLEVBQTJELFVBQVU3RyxLQUFWLEVBQWlCO0FBQzFFLFFBQUksQ0FBQyxrQkFBa0JzRCxJQUFsQixDQUF1QnRELE1BQU1DLE1BQU4sQ0FBYTBMLE9BQXBDLENBQUwsRUFBbUQ7QUFDakQzTCxZQUFNb0csY0FBTjtBQUNEOztBQUVELFFBQU1tSyxXQUFXM1IsRUFBRSxJQUFGLENBQWpCO0FBQ0EsUUFBTXVELFdBQVd4RCxLQUFLc0Qsc0JBQUwsQ0FBNEIsSUFBNUIsQ0FBakI7QUFDQXJELE1BQUV1RCxRQUFGLEVBQVk0RCxJQUFaLENBQWlCLFlBQVk7QUFDM0IsVUFBTXlLLFVBQVU1UixFQUFFLElBQUYsQ0FBaEI7QUFDQSxVQUFNcUgsT0FBVXVLLFFBQVF2SyxJQUFSLENBQWFwQyxRQUFiLENBQWhCO0FBQ0EsVUFBTWYsU0FBVW1ELE9BQU8sUUFBUCxHQUFrQnNLLFNBQVN0SyxJQUFULEVBQWxDO0FBQ0EySCxlQUFTOUgsZ0JBQVQsQ0FBMEJ4RyxJQUExQixDQUErQmtSLE9BQS9CLEVBQXdDMU4sTUFBeEM7QUFDRCxLQUxEO0FBTUQsR0FiRDs7QUFnQkE7Ozs7OztBQU1BbEUsSUFBRTRDLEVBQUYsQ0FBS21DLElBQUwsSUFBeUJpSyxTQUFTOUgsZ0JBQWxDO0FBQ0FsSCxJQUFFNEMsRUFBRixDQUFLbUMsSUFBTCxFQUFXMkMsV0FBWCxHQUF5QnNILFFBQXpCO0FBQ0FoUCxJQUFFNEMsRUFBRixDQUFLbUMsSUFBTCxFQUFXNEMsVUFBWCxHQUF5QixZQUFZO0FBQ25DM0gsTUFBRTRDLEVBQUYsQ0FBS21DLElBQUwsSUFBYUssa0JBQWI7QUFDQSxXQUFPNEosU0FBUzlILGdCQUFoQjtBQUNELEdBSEQ7O0FBS0EsU0FBTzhILFFBQVA7QUFFRCxDQS9YZ0IsQ0ErWGRuSyxNQS9YYyxDQUFqQjs7Ozs7OztBQ0xBOzs7Ozs7O0FBT0EsSUFBTWdOLFdBQVksVUFBQzdSLENBQUQsRUFBTzs7QUFFdkI7Ozs7QUFJQSxNQUFJLE9BQU84UixNQUFQLEtBQWtCLFdBQXRCLEVBQW1DO0FBQ2pDLFVBQU0sSUFBSW5OLEtBQUosQ0FBVSw4REFBVixDQUFOO0FBQ0Q7O0FBRUQ7Ozs7OztBQU1BLE1BQU1JLE9BQTJCLFVBQWpDO0FBQ0EsTUFBTUMsVUFBMkIsWUFBakM7QUFDQSxNQUFNQyxXQUEyQixhQUFqQztBQUNBLE1BQU1DLGtCQUErQkQsUUFBckM7QUFDQSxNQUFNRSxlQUEyQixXQUFqQztBQUNBLE1BQU1DLHFCQUEyQnBGLEVBQUU0QyxFQUFGLENBQUttQyxJQUFMLENBQWpDO0FBQ0EsTUFBTWdOLGlCQUEyQixFQUFqQyxDQXRCdUIsQ0FzQmE7QUFDcEMsTUFBTUMsZ0JBQTJCLEVBQWpDLENBdkJ1QixDQXVCYTtBQUNwQyxNQUFNQyxjQUEyQixDQUFqQyxDQXhCdUIsQ0F3Qlk7QUFDbkMsTUFBTUMsbUJBQTJCLEVBQWpDLENBekJ1QixDQXlCYTtBQUNwQyxNQUFNQyxxQkFBMkIsRUFBakMsQ0ExQnVCLENBMEJhO0FBQ3BDLE1BQU1DLDJCQUEyQixDQUFqQyxDQTNCdUIsQ0EyQlk7QUFDbkMsTUFBTUMsaUJBQTJCLElBQUk1TixNQUFKLENBQWN5TixnQkFBZCxTQUFrQ0Msa0JBQWxDLFNBQXdESixjQUF4RCxDQUFqQzs7QUFFQSxNQUFNdk0sUUFBUTtBQUNaMEosbUJBQTBCaEssU0FEZDtBQUVaaUssdUJBQTRCakssU0FGaEI7QUFHWmEsbUJBQTBCYixTQUhkO0FBSVorSixxQkFBMkIvSixTQUpmO0FBS1pvTixxQkFBMkJwTixTQUxmO0FBTVpTLDhCQUEyQlQsU0FBM0IsR0FBdUNDLFlBTjNCO0FBT1pvTixrQ0FBNkJyTixTQUE3QixHQUF5Q0MsWUFQN0I7QUFRWnFOLDhCQUEyQnROLFNBQTNCLEdBQXVDQztBQVIzQixHQUFkOztBQVdBLE1BQU1TLFlBQVk7QUFDaEI2TSxjQUFZLFVBREk7QUFFaEIxTSxVQUFZLE1BRkk7QUFHaEIyTSxZQUFZLFFBSEk7QUFJaEJDLGVBQVkscUJBSkk7QUFLaEJDLGNBQVk7QUFMSSxHQUFsQjs7QUFRQSxNQUFNdE4sV0FBVztBQUNmMkMsaUJBQWdCLDBCQUREO0FBRWY0SyxnQkFBZ0IsZ0JBRkQ7QUFHZkMsVUFBZ0IsZ0JBSEQ7QUFJZkMsZ0JBQWdCLGFBSkQ7QUFLZkMsbUJBQWdCO0FBTEQsR0FBakI7O0FBUUEsTUFBTUMsZ0JBQWdCO0FBQ3BCQyxTQUFZLFdBRFE7QUFFcEJDLFlBQVksU0FGUTtBQUdwQkMsWUFBWSxjQUhRO0FBSXBCQyxlQUFZO0FBSlEsR0FBdEI7O0FBT0EsTUFBTTlKLFVBQVU7QUFDZCtKLGVBQWNMLGNBQWNHLE1BRGQ7QUFFZEcsWUFBYyxDQUZBO0FBR2RDLFVBQWM7QUFIQSxHQUFoQjs7QUFNQSxNQUFNM0osY0FBYztBQUNsQnlKLGVBQWMsUUFESTtBQUVsQkMsWUFBYyxpQkFGSTtBQUdsQkMsVUFBYzs7QUFJaEI7Ozs7OztBQVBvQixHQUFwQjtBQXRFdUIsTUFtRmpCM0IsUUFuRmlCO0FBcUZyQixzQkFBWXZPLE9BQVosRUFBcUJZLE1BQXJCLEVBQTZCO0FBQUE7O0FBQzNCLFdBQUs4QixRQUFMLEdBQWlCMUMsT0FBakI7QUFDQSxXQUFLbVEsT0FBTCxHQUFpQixJQUFqQjtBQUNBLFdBQUtsSSxPQUFMLEdBQWlCLEtBQUtDLFVBQUwsQ0FBZ0J0SCxNQUFoQixDQUFqQjtBQUNBLFdBQUt3UCxLQUFMLEdBQWlCLEtBQUtDLGVBQUwsRUFBakI7QUFDQSxXQUFLQyxTQUFMLEdBQWlCLEtBQUtDLGFBQUwsRUFBakI7O0FBRUEsV0FBS25JLGtCQUFMO0FBQ0Q7O0FBR0Q7O0FBY0E7O0FBOUdxQix1QkFnSHJCdEQsTUFoSHFCLHFCQWdIWjtBQUNQLFVBQUksS0FBS3BDLFFBQUwsQ0FBYzhOLFFBQWQsSUFBMEI5VCxFQUFFLEtBQUtnRyxRQUFQLEVBQWlCYyxRQUFqQixDQUEwQmxCLFVBQVU2TSxRQUFwQyxDQUE5QixFQUE2RTtBQUMzRTtBQUNEOztBQUVELFVBQU0vTCxTQUFXbUwsU0FBU2tDLHFCQUFULENBQStCLEtBQUsvTixRQUFwQyxDQUFqQjtBQUNBLFVBQU1nTyxXQUFXaFUsRUFBRSxLQUFLMFQsS0FBUCxFQUFjNU0sUUFBZCxDQUF1QmxCLFVBQVVHLElBQWpDLENBQWpCOztBQUVBOEwsZUFBU29DLFdBQVQ7O0FBRUEsVUFBSUQsUUFBSixFQUFjO0FBQ1o7QUFDRDs7QUFFRCxVQUFNckcsZ0JBQWdCO0FBQ3BCQSx1QkFBZ0IsS0FBSzNIO0FBREQsT0FBdEI7QUFHQSxVQUFNa08sWUFBWWxVLEVBQUV3RixLQUFGLENBQVFBLE1BQU1PLElBQWQsRUFBb0I0SCxhQUFwQixDQUFsQjs7QUFFQTNOLFFBQUUwRyxNQUFGLEVBQVU1QyxPQUFWLENBQWtCb1EsU0FBbEI7O0FBRUEsVUFBSUEsVUFBVTVOLGtCQUFWLEVBQUosRUFBb0M7QUFDbEM7QUFDRDs7QUFFRCxVQUFJaEQsVUFBVSxLQUFLMEMsUUFBbkI7QUFDQTtBQUNBLFVBQUloRyxFQUFFMEcsTUFBRixFQUFVSSxRQUFWLENBQW1CbEIsVUFBVThNLE1BQTdCLENBQUosRUFBMEM7QUFDeEMsWUFBSTFTLEVBQUUsS0FBSzBULEtBQVAsRUFBYzVNLFFBQWQsQ0FBdUJsQixVQUFVZ04sUUFBakMsS0FBOEM1UyxFQUFFLEtBQUswVCxLQUFQLEVBQWM1TSxRQUFkLENBQXVCbEIsVUFBVStNLFNBQWpDLENBQWxELEVBQStGO0FBQzdGclAsb0JBQVVvRCxNQUFWO0FBQ0Q7QUFDRjtBQUNELFdBQUsrTSxPQUFMLEdBQWUsSUFBSTNCLE1BQUosQ0FBV3hPLE9BQVgsRUFBb0IsS0FBS29RLEtBQXpCLEVBQWdDLEtBQUtTLGdCQUFMLEVBQWhDLENBQWY7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxVQUFJLGtCQUFrQm5TLFNBQVM2SyxlQUEzQixJQUNELENBQUM3TSxFQUFFMEcsTUFBRixFQUFVQyxPQUFWLENBQWtCckIsU0FBU3lOLFVBQTNCLEVBQXVDclAsTUFEM0MsRUFDbUQ7QUFDakQxRCxVQUFFLE1BQUYsRUFBVW1PLFFBQVYsR0FBcUIxRyxFQUFyQixDQUF3QixXQUF4QixFQUFxQyxJQUFyQyxFQUEyQ3pILEVBQUVvVSxJQUE3QztBQUNEOztBQUVELFdBQUtwTyxRQUFMLENBQWMrQyxLQUFkO0FBQ0EsV0FBSy9DLFFBQUwsQ0FBY2dELFlBQWQsQ0FBMkIsZUFBM0IsRUFBNEMsSUFBNUM7O0FBRUFoSixRQUFFLEtBQUswVCxLQUFQLEVBQWN6SyxXQUFkLENBQTBCckQsVUFBVUcsSUFBcEM7QUFDQS9GLFFBQUUwRyxNQUFGLEVBQ0d1QyxXQURILENBQ2VyRCxVQUFVRyxJQUR6QixFQUVHakMsT0FGSCxDQUVXOUQsRUFBRXdGLEtBQUYsQ0FBUUEsTUFBTXlKLEtBQWQsRUFBcUJ0QixhQUFyQixDQUZYO0FBR0QsS0FsS29COztBQUFBLHVCQW9LckJuSCxPQXBLcUIsc0JBb0tYO0FBQ1J4RyxRQUFFeUcsVUFBRixDQUFhLEtBQUtULFFBQWxCLEVBQTRCZixRQUE1QjtBQUNBakYsUUFBRSxLQUFLZ0csUUFBUCxFQUFpQjBHLEdBQWpCLENBQXFCeEgsU0FBckI7QUFDQSxXQUFLYyxRQUFMLEdBQWdCLElBQWhCO0FBQ0EsV0FBSzBOLEtBQUwsR0FBYSxJQUFiO0FBQ0EsVUFBSSxLQUFLRCxPQUFMLEtBQWlCLElBQXJCLEVBQTJCO0FBQ3pCLGFBQUtBLE9BQUwsQ0FBYVksT0FBYjtBQUNEO0FBQ0QsV0FBS1osT0FBTCxHQUFlLElBQWY7QUFDRCxLQTdLb0I7O0FBQUEsdUJBK0tyQmEsTUEvS3FCLHFCQStLWjtBQUNQLFdBQUtWLFNBQUwsR0FBaUIsS0FBS0MsYUFBTCxFQUFqQjtBQUNBLFVBQUksS0FBS0osT0FBTCxLQUFpQixJQUFyQixFQUEyQjtBQUN6QixhQUFLQSxPQUFMLENBQWFjLGNBQWI7QUFDRDtBQUNGLEtBcExvQjs7QUFzTHJCOztBQXRMcUIsdUJBd0xyQjdJLGtCQXhMcUIsaUNBd0xBO0FBQUE7O0FBQ25CMUwsUUFBRSxLQUFLZ0csUUFBUCxFQUFpQnlCLEVBQWpCLENBQW9CakMsTUFBTThNLEtBQTFCLEVBQWlDLFVBQUNsUixLQUFELEVBQVc7QUFDMUNBLGNBQU1vRyxjQUFOO0FBQ0FwRyxjQUFNb1QsZUFBTjtBQUNBLGNBQUtwTSxNQUFMO0FBQ0QsT0FKRDtBQUtELEtBOUxvQjs7QUFBQSx1QkFnTXJCb0QsVUFoTXFCLHVCQWdNVnRILE1BaE1VLEVBZ01GO0FBQ2pCLFVBQU11USxjQUFjelUsRUFBRSxLQUFLZ0csUUFBUCxFQUFpQnFCLElBQWpCLEVBQXBCO0FBQ0EsVUFBSW9OLFlBQVluQixTQUFaLEtBQTBCM1IsU0FBOUIsRUFBeUM7QUFDdkM4UyxvQkFBWW5CLFNBQVosR0FBd0JMLGNBQWN3QixZQUFZbkIsU0FBWixDQUFzQjFPLFdBQXRCLEVBQWQsQ0FBeEI7QUFDRDs7QUFFRFYsZUFBU2xFLEVBQUUyTSxNQUFGLENBQ1AsRUFETyxFQUVQLEtBQUsrSCxXQUFMLENBQWlCbkwsT0FGVixFQUdQdkosRUFBRSxLQUFLZ0csUUFBUCxFQUFpQnFCLElBQWpCLEVBSE8sRUFJUG5ELE1BSk8sQ0FBVDs7QUFPQW5FLFdBQUtpRSxlQUFMLENBQ0VlLElBREYsRUFFRWIsTUFGRixFQUdFLEtBQUt3USxXQUFMLENBQWlCN0ssV0FIbkI7O0FBTUEsYUFBTzNGLE1BQVA7QUFDRCxLQXBOb0I7O0FBQUEsdUJBc05yQnlQLGVBdE5xQiw4QkFzTkg7QUFDaEIsVUFBSSxDQUFDLEtBQUtELEtBQVYsRUFBaUI7QUFDZixZQUFNaE4sU0FBU21MLFNBQVNrQyxxQkFBVCxDQUErQixLQUFLL04sUUFBcEMsQ0FBZjtBQUNBLGFBQUswTixLQUFMLEdBQWExVCxFQUFFMEcsTUFBRixFQUFVOEIsSUFBVixDQUFlbEQsU0FBU3dOLElBQXhCLEVBQThCLENBQTlCLENBQWI7QUFDRDtBQUNELGFBQU8sS0FBS1ksS0FBWjtBQUNELEtBNU5vQjs7QUFBQSx1QkE4TnJCaUIsYUE5TnFCLDRCQThOTDtBQUNkLFVBQU1DLGtCQUFrQjVVLEVBQUUsS0FBS2dHLFFBQVAsRUFBaUJVLE1BQWpCLEVBQXhCO0FBQ0EsVUFBSTRNLFlBQVksS0FBSy9ILE9BQUwsQ0FBYStILFNBQTdCOztBQUVBO0FBQ0EsVUFBSXNCLGdCQUFnQjlOLFFBQWhCLENBQXlCbEIsVUFBVThNLE1BQW5DLEtBQThDLEtBQUtuSCxPQUFMLENBQWErSCxTQUFiLEtBQTJCTCxjQUFjQyxHQUEzRixFQUFnRztBQUM5Rkksb0JBQVlMLGNBQWNDLEdBQTFCO0FBQ0EsWUFBSWxULEVBQUUsS0FBSzBULEtBQVAsRUFBYzVNLFFBQWQsQ0FBdUJsQixVQUFVK00sU0FBakMsQ0FBSixFQUFpRDtBQUMvQ1csc0JBQVlMLGNBQWNFLE1BQTFCO0FBQ0Q7QUFDRixPQUxELE1BS08sSUFBSW5ULEVBQUUsS0FBSzBULEtBQVAsRUFBYzVNLFFBQWQsQ0FBdUJsQixVQUFVK00sU0FBakMsQ0FBSixFQUFpRDtBQUN0RFcsb0JBQVlMLGNBQWNJLFNBQTFCO0FBQ0Q7QUFDRCxhQUFPQyxTQUFQO0FBQ0QsS0E1T29COztBQUFBLHVCQThPckJPLGFBOU9xQiw0QkE4T0w7QUFDZCxhQUFPN1QsRUFBRSxLQUFLZ0csUUFBUCxFQUFpQlcsT0FBakIsQ0FBeUIsU0FBekIsRUFBb0NqRCxNQUFwQyxHQUE2QyxDQUFwRDtBQUNELEtBaFBvQjs7QUFBQSx1QkFrUHJCeVEsZ0JBbFBxQiwrQkFrUEY7QUFDakIsVUFBTVUsZUFBZTtBQUNuQnZCLG1CQUFZLEtBQUtxQixhQUFMLEVBRE87QUFFbkJHLG1CQUFZO0FBQ1Z2QixrQkFBUztBQUNQQSxvQkFBUyxLQUFLaEksT0FBTCxDQUFhZ0k7QUFEZixXQURDO0FBSVZDLGdCQUFPO0FBQ0x1QixxQkFBVSxLQUFLeEosT0FBTCxDQUFhaUk7QUFEbEI7QUFKRzs7QUFVZDtBQVpxQixPQUFyQixDQWFBLElBQUksS0FBS0ksU0FBVCxFQUFvQjtBQUNsQmlCLHFCQUFhQyxTQUFiLENBQXVCRSxVQUF2QixHQUFvQztBQUNsQ0QsbUJBQVMsQ0FBQyxLQUFLbkI7QUFEbUIsU0FBcEM7QUFHRDtBQUNELGFBQU9pQixZQUFQO0FBQ0QsS0F0UW9COztBQXdRckI7O0FBeFFxQixhQTBRZDNOLGdCQTFRYyw2QkEwUUdoRCxNQTFRSCxFQTBRVztBQUM5QixhQUFPLEtBQUtpRCxJQUFMLENBQVUsWUFBWTtBQUMzQixZQUFJRSxPQUFPckgsRUFBRSxJQUFGLEVBQVFxSCxJQUFSLENBQWFwQyxRQUFiLENBQVg7QUFDQSxZQUFNc0csVUFBVSxRQUFPckgsTUFBUCx5Q0FBT0EsTUFBUCxPQUFrQixRQUFsQixHQUE2QkEsTUFBN0IsR0FBc0MsSUFBdEQ7O0FBRUEsWUFBSSxDQUFDbUQsSUFBTCxFQUFXO0FBQ1RBLGlCQUFPLElBQUl3SyxRQUFKLENBQWEsSUFBYixFQUFtQnRHLE9BQW5CLENBQVA7QUFDQXZMLFlBQUUsSUFBRixFQUFRcUgsSUFBUixDQUFhcEMsUUFBYixFQUF1Qm9DLElBQXZCO0FBQ0Q7O0FBRUQsWUFBSSxPQUFPbkQsTUFBUCxLQUFrQixRQUF0QixFQUFnQztBQUM5QixjQUFJbUQsS0FBS25ELE1BQUwsTUFBaUJ2QyxTQUFyQixFQUFnQztBQUM5QixrQkFBTSxJQUFJZ0QsS0FBSix1QkFBOEJULE1BQTlCLE9BQU47QUFDRDtBQUNEbUQsZUFBS25ELE1BQUw7QUFDRDtBQUNGLE9BZk0sQ0FBUDtBQWdCRCxLQTNSb0I7O0FBQUEsYUE2UmQrUCxXQTdSYyx3QkE2UkY3UyxLQTdSRSxFQTZSSztBQUN4QixVQUFJQSxVQUFVQSxNQUFNNEwsS0FBTixLQUFnQm9GLHdCQUFoQixJQUNaaFIsTUFBTXFILElBQU4sS0FBZSxPQUFmLElBQTBCckgsTUFBTTRMLEtBQU4sS0FBZ0JpRixXQUR4QyxDQUFKLEVBQzBEO0FBQ3hEO0FBQ0Q7O0FBRUQsVUFBTWdELFVBQVVqVixFQUFFaU4sU0FBRixDQUFZak4sRUFBRXNGLFNBQVMyQyxXQUFYLENBQVosQ0FBaEI7QUFDQSxXQUFLLElBQUk4SCxJQUFJLENBQWIsRUFBZ0JBLElBQUlrRixRQUFRdlIsTUFBNUIsRUFBb0NxTSxHQUFwQyxFQUF5QztBQUN2QyxZQUFNckosU0FBZ0JtTCxTQUFTa0MscUJBQVQsQ0FBK0JrQixRQUFRbEYsQ0FBUixDQUEvQixDQUF0QjtBQUNBLFlBQU1tRixVQUFnQmxWLEVBQUVpVixRQUFRbEYsQ0FBUixDQUFGLEVBQWMxSSxJQUFkLENBQW1CcEMsUUFBbkIsQ0FBdEI7QUFDQSxZQUFNMEksZ0JBQWdCO0FBQ3BCQSx5QkFBZ0JzSCxRQUFRbEYsQ0FBUjtBQURJLFNBQXRCOztBQUlBLFlBQUksQ0FBQ21GLE9BQUwsRUFBYztBQUNaO0FBQ0Q7O0FBRUQsWUFBTUMsZUFBZUQsUUFBUXhCLEtBQTdCO0FBQ0EsWUFBSSxDQUFDMVQsRUFBRTBHLE1BQUYsRUFBVUksUUFBVixDQUFtQmxCLFVBQVVHLElBQTdCLENBQUwsRUFBeUM7QUFDdkM7QUFDRDs7QUFFRCxZQUFJM0UsVUFBVUEsTUFBTXFILElBQU4sS0FBZSxPQUFmLElBQ1Ysa0JBQWtCL0QsSUFBbEIsQ0FBdUJ0RCxNQUFNQyxNQUFOLENBQWEwTCxPQUFwQyxDQURVLElBQ3NDM0wsTUFBTXFILElBQU4sS0FBZSxPQUFmLElBQTBCckgsTUFBTTRMLEtBQU4sS0FBZ0JpRixXQUQxRixLQUVHalMsRUFBRThJLFFBQUYsQ0FBV3BDLE1BQVgsRUFBbUJ0RixNQUFNQyxNQUF6QixDQUZQLEVBRXlDO0FBQ3ZDO0FBQ0Q7O0FBRUQsWUFBTStULFlBQVlwVixFQUFFd0YsS0FBRixDQUFRQSxNQUFNMEosSUFBZCxFQUFvQnZCLGFBQXBCLENBQWxCO0FBQ0EzTixVQUFFMEcsTUFBRixFQUFVNUMsT0FBVixDQUFrQnNSLFNBQWxCO0FBQ0EsWUFBSUEsVUFBVTlPLGtCQUFWLEVBQUosRUFBb0M7QUFDbEM7QUFDRDs7QUFFRDtBQUNBO0FBQ0EsWUFBSSxrQkFBa0J0RSxTQUFTNkssZUFBL0IsRUFBZ0Q7QUFDOUM3TSxZQUFFLE1BQUYsRUFBVW1PLFFBQVYsR0FBcUJ6QixHQUFyQixDQUF5QixXQUF6QixFQUFzQyxJQUF0QyxFQUE0QzFNLEVBQUVvVSxJQUE5QztBQUNEOztBQUVEYSxnQkFBUWxGLENBQVIsRUFBVy9HLFlBQVgsQ0FBd0IsZUFBeEIsRUFBeUMsT0FBekM7O0FBRUFoSixVQUFFbVYsWUFBRixFQUFnQnRPLFdBQWhCLENBQTRCakIsVUFBVUcsSUFBdEM7QUFDQS9GLFVBQUUwRyxNQUFGLEVBQ0dHLFdBREgsQ0FDZWpCLFVBQVVHLElBRHpCLEVBRUdqQyxPQUZILENBRVc5RCxFQUFFd0YsS0FBRixDQUFRQSxNQUFNMkosTUFBZCxFQUFzQnhCLGFBQXRCLENBRlg7QUFHRDtBQUNGLEtBN1VvQjs7QUFBQSxhQStVZG9HLHFCQS9VYyxrQ0ErVVF6USxPQS9VUixFQStVaUI7QUFDcEMsVUFBSW9ELGVBQUo7QUFDQSxVQUFNbkQsV0FBV3hELEtBQUtzRCxzQkFBTCxDQUE0QkMsT0FBNUIsQ0FBakI7O0FBRUEsVUFBSUMsUUFBSixFQUFjO0FBQ1ptRCxpQkFBUzFHLEVBQUV1RCxRQUFGLEVBQVksQ0FBWixDQUFUO0FBQ0Q7O0FBRUQsYUFBT21ELFVBQVVwRCxRQUFRK1IsVUFBekI7QUFDRCxLQXhWb0I7O0FBQUEsYUEwVmRDLHNCQTFWYyxtQ0EwVlNsVSxLQTFWVCxFQTBWZ0I7QUFDbkMsVUFBSSxDQUFDaVIsZUFBZTNOLElBQWYsQ0FBb0J0RCxNQUFNNEwsS0FBMUIsQ0FBRCxJQUFxQyxVQUFVdEksSUFBVixDQUFldEQsTUFBTUMsTUFBTixDQUFhMEwsT0FBNUIsS0FBd0MzTCxNQUFNNEwsS0FBTixLQUFnQmdGLGFBQTdGLElBQ0Qsa0JBQWtCdE4sSUFBbEIsQ0FBdUJ0RCxNQUFNQyxNQUFOLENBQWEwTCxPQUFwQyxDQURILEVBQ2lEO0FBQy9DO0FBQ0Q7O0FBRUQzTCxZQUFNb0csY0FBTjtBQUNBcEcsWUFBTW9ULGVBQU47O0FBRUEsVUFBSSxLQUFLVixRQUFMLElBQWlCOVQsRUFBRSxJQUFGLEVBQVE4RyxRQUFSLENBQWlCbEIsVUFBVTZNLFFBQTNCLENBQXJCLEVBQTJEO0FBQ3pEO0FBQ0Q7O0FBRUQsVUFBTS9MLFNBQVdtTCxTQUFTa0MscUJBQVQsQ0FBK0IsSUFBL0IsQ0FBakI7QUFDQSxVQUFNQyxXQUFXaFUsRUFBRTBHLE1BQUYsRUFBVUksUUFBVixDQUFtQmxCLFVBQVVHLElBQTdCLENBQWpCOztBQUVBLFVBQUksQ0FBQ2lPLFFBQUQsS0FBYzVTLE1BQU00TCxLQUFOLEtBQWdCK0UsY0FBaEIsSUFBa0MzUSxNQUFNNEwsS0FBTixLQUFnQmdGLGFBQWhFLEtBQ0NnQyxhQUFhNVMsTUFBTTRMLEtBQU4sS0FBZ0IrRSxjQUFoQixJQUFrQzNRLE1BQU00TCxLQUFOLEtBQWdCZ0YsYUFBL0QsQ0FETCxFQUNvRjs7QUFFbEYsWUFBSTVRLE1BQU00TCxLQUFOLEtBQWdCK0UsY0FBcEIsRUFBb0M7QUFDbEMsY0FBTTNKLFNBQVNwSSxFQUFFMEcsTUFBRixFQUFVOEIsSUFBVixDQUFlbEQsU0FBUzJDLFdBQXhCLEVBQXFDLENBQXJDLENBQWY7QUFDQWpJLFlBQUVvSSxNQUFGLEVBQVV0RSxPQUFWLENBQWtCLE9BQWxCO0FBQ0Q7O0FBRUQ5RCxVQUFFLElBQUYsRUFBUThELE9BQVIsQ0FBZ0IsT0FBaEI7QUFDQTtBQUNEOztBQUVELFVBQU15UixRQUFRdlYsRUFBRTBHLE1BQUYsRUFBVThCLElBQVYsQ0FBZWxELFNBQVMwTixhQUF4QixFQUF1Q3dDLEdBQXZDLEVBQWQ7O0FBRUEsVUFBSSxDQUFDRCxNQUFNN1IsTUFBWCxFQUFtQjtBQUNqQjtBQUNEOztBQUVELFVBQUk0SSxRQUFRaUosTUFBTXJJLE9BQU4sQ0FBYzlMLE1BQU1DLE1BQXBCLENBQVo7O0FBRUEsVUFBSUQsTUFBTTRMLEtBQU4sS0FBZ0JrRixnQkFBaEIsSUFBb0M1RixRQUFRLENBQWhELEVBQW1EO0FBQUU7QUFDbkRBO0FBQ0Q7O0FBRUQsVUFBSWxMLE1BQU00TCxLQUFOLEtBQWdCbUYsa0JBQWhCLElBQXNDN0YsUUFBUWlKLE1BQU03UixNQUFOLEdBQWUsQ0FBakUsRUFBb0U7QUFBRTtBQUNwRTRJO0FBQ0Q7O0FBRUQsVUFBSUEsUUFBUSxDQUFaLEVBQWU7QUFDYkEsZ0JBQVEsQ0FBUjtBQUNEOztBQUVEaUosWUFBTWpKLEtBQU4sRUFBYXZELEtBQWI7QUFDRCxLQTNZb0I7O0FBQUE7QUFBQTtBQUFBLDBCQWtHQTtBQUNuQixlQUFPL0QsT0FBUDtBQUNEO0FBcEdvQjtBQUFBO0FBQUEsMEJBc0dBO0FBQ25CLGVBQU91RSxPQUFQO0FBQ0Q7QUF4R29CO0FBQUE7QUFBQSwwQkEwR0k7QUFDdkIsZUFBT00sV0FBUDtBQUNEO0FBNUdvQjs7QUFBQTtBQUFBOztBQWdadkI7Ozs7OztBQU1BN0osSUFBRWdDLFFBQUYsRUFDR3lGLEVBREgsQ0FDTWpDLE1BQU0rTSxnQkFEWixFQUM4QmpOLFNBQVMyQyxXQUR2QyxFQUNxRDRKLFNBQVN5RCxzQkFEOUQsRUFFRzdOLEVBRkgsQ0FFTWpDLE1BQU0rTSxnQkFGWixFQUU4QmpOLFNBQVN3TixJQUZ2QyxFQUU2Q2pCLFNBQVN5RCxzQkFGdEQsRUFHRzdOLEVBSEgsQ0FHU2pDLE1BQU1HLGNBSGYsU0FHaUNILE1BQU1nTixjQUh2QyxFQUd5RFgsU0FBU29DLFdBSGxFLEVBSUd4TSxFQUpILENBSU1qQyxNQUFNRyxjQUpaLEVBSTRCTCxTQUFTMkMsV0FKckMsRUFJa0QsVUFBVTdHLEtBQVYsRUFBaUI7QUFDL0RBLFVBQU1vRyxjQUFOO0FBQ0FwRyxVQUFNb1QsZUFBTjtBQUNBM0MsYUFBUzNLLGdCQUFULENBQTBCeEcsSUFBMUIsQ0FBK0JWLEVBQUUsSUFBRixDQUEvQixFQUF3QyxRQUF4QztBQUNELEdBUkgsRUFTR3lILEVBVEgsQ0FTTWpDLE1BQU1HLGNBVFosRUFTNEJMLFNBQVN1TixVQVRyQyxFQVNpRCxVQUFDNEMsQ0FBRCxFQUFPO0FBQ3BEQSxNQUFFakIsZUFBRjtBQUNELEdBWEg7O0FBY0E7Ozs7OztBQU1BeFUsSUFBRTRDLEVBQUYsQ0FBS21DLElBQUwsSUFBeUI4TSxTQUFTM0ssZ0JBQWxDO0FBQ0FsSCxJQUFFNEMsRUFBRixDQUFLbUMsSUFBTCxFQUFXMkMsV0FBWCxHQUF5Qm1LLFFBQXpCO0FBQ0E3UixJQUFFNEMsRUFBRixDQUFLbUMsSUFBTCxFQUFXNEMsVUFBWCxHQUF5QixZQUFZO0FBQ25DM0gsTUFBRTRDLEVBQUYsQ0FBS21DLElBQUwsSUFBYUssa0JBQWI7QUFDQSxXQUFPeU0sU0FBUzNLLGdCQUFoQjtBQUNELEdBSEQ7O0FBS0EsU0FBTzJLLFFBQVA7QUFFRCxDQW5iZ0IsQ0FtYmRoTixNQW5iYyxDQUFqQixFQVpBOzs7Ozs7O0FDR0E7Ozs7Ozs7QUFPQSxJQUFNNlEsUUFBUyxVQUFDMVYsQ0FBRCxFQUFPOztBQUdwQjs7Ozs7O0FBTUEsTUFBTStFLE9BQStCLE9BQXJDO0FBQ0EsTUFBTUMsVUFBK0IsWUFBckM7QUFDQSxNQUFNQyxXQUErQixVQUFyQztBQUNBLE1BQU1DLGtCQUFtQ0QsUUFBekM7QUFDQSxNQUFNRSxlQUErQixXQUFyQztBQUNBLE1BQU1DLHFCQUErQnBGLEVBQUU0QyxFQUFGLENBQUttQyxJQUFMLENBQXJDO0FBQ0EsTUFBTU0sc0JBQStCLEdBQXJDO0FBQ0EsTUFBTXNRLCtCQUErQixHQUFyQztBQUNBLE1BQU01RCxpQkFBK0IsRUFBckMsQ0FqQm9CLENBaUJvQjs7QUFFeEMsTUFBTXhJLFVBQVU7QUFDZHFNLGNBQVcsSUFERztBQUVkbk0sY0FBVyxJQUZHO0FBR2RWLFdBQVcsSUFIRztBQUlkd0gsVUFBVztBQUpHLEdBQWhCOztBQU9BLE1BQU0xRyxjQUFjO0FBQ2xCK0wsY0FBVyxrQkFETztBQUVsQm5NLGNBQVcsU0FGTztBQUdsQlYsV0FBVyxTQUhPO0FBSWxCd0gsVUFBVztBQUpPLEdBQXBCOztBQU9BLE1BQU0vSyxRQUFRO0FBQ1owSixtQkFBMkJoSyxTQURmO0FBRVppSyx1QkFBNkJqSyxTQUZqQjtBQUdaYSxtQkFBMkJiLFNBSGY7QUFJWitKLHFCQUE0Qi9KLFNBSmhCO0FBS1oyUSx5QkFBOEIzUSxTQUxsQjtBQU1aNFEsdUJBQTZCNVEsU0FOakI7QUFPWjZRLHFDQUFvQzdRLFNBUHhCO0FBUVo4USx5Q0FBc0M5USxTQVIxQjtBQVNaK1EseUNBQXNDL1EsU0FUMUI7QUFVWmdSLDZDQUF3Q2hSLFNBVjVCO0FBV1pTLDhCQUE0QlQsU0FBNUIsR0FBd0NDO0FBWDVCLEdBQWQ7O0FBY0EsTUFBTVMsWUFBWTtBQUNoQnVRLHdCQUFxQix5QkFETDtBQUVoQkMsY0FBcUIsZ0JBRkw7QUFHaEJDLFVBQXFCLFlBSEw7QUFJaEJ2USxVQUFxQixNQUpMO0FBS2hCQyxVQUFxQjtBQUxMLEdBQWxCOztBQVFBLE1BQU1ULFdBQVc7QUFDZmdSLFlBQXFCLGVBRE47QUFFZnJPLGlCQUFxQix1QkFGTjtBQUdmc08sa0JBQXFCLHdCQUhOO0FBSWZDLG1CQUFxQixtREFKTjtBQUtmQyxvQkFBcUI7O0FBSXZCOzs7Ozs7QUFUaUIsR0FBakI7QUF2RG9CLE1Bc0VkZixLQXRFYztBQXdFbEIsbUJBQVlwUyxPQUFaLEVBQXFCWSxNQUFyQixFQUE2QjtBQUFBOztBQUMzQixXQUFLcUgsT0FBTCxHQUE0QixLQUFLQyxVQUFMLENBQWdCdEgsTUFBaEIsQ0FBNUI7QUFDQSxXQUFLOEIsUUFBTCxHQUE0QjFDLE9BQTVCO0FBQ0EsV0FBS29ULE9BQUwsR0FBNEIxVyxFQUFFc0QsT0FBRixFQUFXa0YsSUFBWCxDQUFnQmxELFNBQVNnUixNQUF6QixFQUFpQyxDQUFqQyxDQUE1QjtBQUNBLFdBQUtLLFNBQUwsR0FBNEIsSUFBNUI7QUFDQSxXQUFLQyxRQUFMLEdBQTRCLEtBQTVCO0FBQ0EsV0FBS0Msa0JBQUwsR0FBNEIsS0FBNUI7QUFDQSxXQUFLQyxvQkFBTCxHQUE0QixLQUE1QjtBQUNBLFdBQUtDLG9CQUFMLEdBQTRCLENBQTVCO0FBQ0EsV0FBS0MsZUFBTCxHQUE0QixDQUE1QjtBQUNEOztBQUdEOztBQVdBOztBQWhHa0Isb0JBa0dsQjVPLE1BbEdrQixtQkFrR1h1RixhQWxHVyxFQWtHSTtBQUNwQixhQUFPLEtBQUtpSixRQUFMLEdBQWdCLEtBQUt0RyxJQUFMLEVBQWhCLEdBQThCLEtBQUtDLElBQUwsQ0FBVTVDLGFBQVYsQ0FBckM7QUFDRCxLQXBHaUI7O0FBQUEsb0JBc0dsQjRDLElBdEdrQixpQkFzR2I1QyxhQXRHYSxFQXNHRTtBQUFBOztBQUNsQixVQUFJLEtBQUtnQyxnQkFBVCxFQUEyQjtBQUN6QjtBQUNEOztBQUVELFVBQUk1UCxLQUFLK0MscUJBQUwsTUFBZ0M5QyxFQUFFLEtBQUtnRyxRQUFQLEVBQWlCYyxRQUFqQixDQUEwQmxCLFVBQVVFLElBQXBDLENBQXBDLEVBQStFO0FBQzdFLGFBQUs2SixnQkFBTCxHQUF3QixJQUF4QjtBQUNEOztBQUVELFVBQU11RSxZQUFZbFUsRUFBRXdGLEtBQUYsQ0FBUUEsTUFBTU8sSUFBZCxFQUFvQjtBQUNwQzRIO0FBRG9DLE9BQXBCLENBQWxCOztBQUlBM04sUUFBRSxLQUFLZ0csUUFBUCxFQUFpQmxDLE9BQWpCLENBQXlCb1EsU0FBekI7O0FBRUEsVUFBSSxLQUFLMEMsUUFBTCxJQUFpQjFDLFVBQVU1TixrQkFBVixFQUFyQixFQUFxRDtBQUNuRDtBQUNEOztBQUVELFdBQUtzUSxRQUFMLEdBQWdCLElBQWhCOztBQUVBLFdBQUtLLGVBQUw7QUFDQSxXQUFLQyxhQUFMOztBQUVBbFgsUUFBRWdDLFNBQVNtVixJQUFYLEVBQWlCL0ksUUFBakIsQ0FBMEJ4SSxVQUFVeVEsSUFBcEM7O0FBRUEsV0FBS2UsZUFBTDtBQUNBLFdBQUtDLGVBQUw7O0FBRUFyWCxRQUFFLEtBQUtnRyxRQUFQLEVBQWlCeUIsRUFBakIsQ0FDRWpDLE1BQU11USxhQURSLEVBRUV6USxTQUFTaVIsWUFGWCxFQUdFLFVBQUNuVixLQUFEO0FBQUEsZUFBVyxNQUFLa1AsSUFBTCxDQUFVbFAsS0FBVixDQUFYO0FBQUEsT0FIRjs7QUFNQXBCLFFBQUUsS0FBSzBXLE9BQVAsRUFBZ0JqUCxFQUFoQixDQUFtQmpDLE1BQU0wUSxpQkFBekIsRUFBNEMsWUFBTTtBQUNoRGxXLFVBQUUsTUFBS2dHLFFBQVAsRUFBaUJ6RCxHQUFqQixDQUFxQmlELE1BQU15USxlQUEzQixFQUE0QyxVQUFDN1UsS0FBRCxFQUFXO0FBQ3JELGNBQUlwQixFQUFFb0IsTUFBTUMsTUFBUixFQUFnQkMsRUFBaEIsQ0FBbUIsTUFBSzBFLFFBQXhCLENBQUosRUFBdUM7QUFDckMsa0JBQUs4USxvQkFBTCxHQUE0QixJQUE1QjtBQUNEO0FBQ0YsU0FKRDtBQUtELE9BTkQ7O0FBUUEsV0FBS1EsYUFBTCxDQUFtQjtBQUFBLGVBQU0sTUFBS0MsWUFBTCxDQUFrQjVKLGFBQWxCLENBQU47QUFBQSxPQUFuQjtBQUNELEtBbEppQjs7QUFBQSxvQkFvSmxCMkMsSUFwSmtCLGlCQW9KYmxQLEtBcEphLEVBb0pOO0FBQUE7O0FBQ1YsVUFBSUEsS0FBSixFQUFXO0FBQ1RBLGNBQU1vRyxjQUFOO0FBQ0Q7O0FBRUQsVUFBSSxLQUFLbUksZ0JBQUwsSUFBeUIsQ0FBQyxLQUFLaUgsUUFBbkMsRUFBNkM7QUFDM0M7QUFDRDs7QUFFRCxVQUFNM1csYUFBYUYsS0FBSytDLHFCQUFMLE1BQWdDOUMsRUFBRSxLQUFLZ0csUUFBUCxFQUFpQmMsUUFBakIsQ0FBMEJsQixVQUFVRSxJQUFwQyxDQUFuRDs7QUFFQSxVQUFJN0YsVUFBSixFQUFnQjtBQUNkLGFBQUswUCxnQkFBTCxHQUF3QixJQUF4QjtBQUNEOztBQUVELFVBQU15RixZQUFZcFYsRUFBRXdGLEtBQUYsQ0FBUUEsTUFBTTBKLElBQWQsQ0FBbEI7O0FBRUFsUCxRQUFFLEtBQUtnRyxRQUFQLEVBQWlCbEMsT0FBakIsQ0FBeUJzUixTQUF6Qjs7QUFFQSxVQUFJLENBQUMsS0FBS3dCLFFBQU4sSUFBa0J4QixVQUFVOU8sa0JBQVYsRUFBdEIsRUFBc0Q7QUFDcEQ7QUFDRDs7QUFFRCxXQUFLc1EsUUFBTCxHQUFnQixLQUFoQjs7QUFFQSxXQUFLUSxlQUFMO0FBQ0EsV0FBS0MsZUFBTDs7QUFFQXJYLFFBQUVnQyxRQUFGLEVBQVkwSyxHQUFaLENBQWdCbEgsTUFBTXFRLE9BQXRCOztBQUVBN1YsUUFBRSxLQUFLZ0csUUFBUCxFQUFpQmEsV0FBakIsQ0FBNkJqQixVQUFVRyxJQUF2Qzs7QUFFQS9GLFFBQUUsS0FBS2dHLFFBQVAsRUFBaUIwRyxHQUFqQixDQUFxQmxILE1BQU11USxhQUEzQjtBQUNBL1YsUUFBRSxLQUFLMFcsT0FBUCxFQUFnQmhLLEdBQWhCLENBQW9CbEgsTUFBTTBRLGlCQUExQjs7QUFFQSxVQUFJalcsVUFBSixFQUFnQjs7QUFFZEQsVUFBRSxLQUFLZ0csUUFBUCxFQUNHekQsR0FESCxDQUNPeEMsS0FBS3lDLGNBRFosRUFDNEIsVUFBQ3BCLEtBQUQ7QUFBQSxpQkFBVyxPQUFLb1csVUFBTCxDQUFnQnBXLEtBQWhCLENBQVg7QUFBQSxTQUQ1QixFQUVHeUIsb0JBRkgsQ0FFd0J3QyxtQkFGeEI7QUFHRCxPQUxELE1BS087QUFDTCxhQUFLbVMsVUFBTDtBQUNEO0FBQ0YsS0EvTGlCOztBQUFBLG9CQWlNbEJoUixPQWpNa0Isc0JBaU1SO0FBQ1J4RyxRQUFFeUcsVUFBRixDQUFhLEtBQUtULFFBQWxCLEVBQTRCZixRQUE1Qjs7QUFFQWpGLFFBQUU2QixNQUFGLEVBQVVHLFFBQVYsRUFBb0IsS0FBS2dFLFFBQXpCLEVBQW1DLEtBQUsyUSxTQUF4QyxFQUFtRGpLLEdBQW5ELENBQXVEeEgsU0FBdkQ7O0FBRUEsV0FBS3FHLE9BQUwsR0FBNEIsSUFBNUI7QUFDQSxXQUFLdkYsUUFBTCxHQUE0QixJQUE1QjtBQUNBLFdBQUswUSxPQUFMLEdBQTRCLElBQTVCO0FBQ0EsV0FBS0MsU0FBTCxHQUE0QixJQUE1QjtBQUNBLFdBQUtDLFFBQUwsR0FBNEIsSUFBNUI7QUFDQSxXQUFLQyxrQkFBTCxHQUE0QixJQUE1QjtBQUNBLFdBQUtDLG9CQUFMLEdBQTRCLElBQTVCO0FBQ0EsV0FBS0UsZUFBTCxHQUE0QixJQUE1QjtBQUNELEtBOU1pQjs7QUFBQSxvQkFnTmxCUyxZQWhOa0IsMkJBZ05IO0FBQ2IsV0FBS0MsYUFBTDtBQUNELEtBbE5pQjs7QUFvTmxCOztBQXBOa0Isb0JBc05sQmxNLFVBdE5rQix1QkFzTlB0SCxNQXROTyxFQXNOQztBQUNqQkEsZUFBU2xFLEVBQUUyTSxNQUFGLENBQVMsRUFBVCxFQUFhcEQsT0FBYixFQUFzQnJGLE1BQXRCLENBQVQ7QUFDQW5FLFdBQUtpRSxlQUFMLENBQXFCZSxJQUFyQixFQUEyQmIsTUFBM0IsRUFBbUMyRixXQUFuQztBQUNBLGFBQU8zRixNQUFQO0FBQ0QsS0ExTmlCOztBQUFBLG9CQTRObEJxVCxZQTVOa0IseUJBNE5MNUosYUE1TkssRUE0TlU7QUFBQTs7QUFDMUIsVUFBTTFOLGFBQWFGLEtBQUsrQyxxQkFBTCxNQUNqQjlDLEVBQUUsS0FBS2dHLFFBQVAsRUFBaUJjLFFBQWpCLENBQTBCbEIsVUFBVUUsSUFBcEMsQ0FERjs7QUFHQSxVQUFJLENBQUMsS0FBS0UsUUFBTCxDQUFjcVAsVUFBZixJQUNELEtBQUtyUCxRQUFMLENBQWNxUCxVQUFkLENBQXlCdlUsUUFBekIsS0FBc0M2VyxLQUFLQyxZQUQ5QyxFQUM0RDtBQUMxRDtBQUNBNVYsaUJBQVNtVixJQUFULENBQWNVLFdBQWQsQ0FBMEIsS0FBSzdSLFFBQS9CO0FBQ0Q7O0FBRUQsV0FBS0EsUUFBTCxDQUFjN0QsS0FBZCxDQUFvQjJWLE9BQXBCLEdBQThCLE9BQTlCO0FBQ0EsV0FBSzlSLFFBQUwsQ0FBYytSLGVBQWQsQ0FBOEIsYUFBOUI7QUFDQSxXQUFLL1IsUUFBTCxDQUFjZ1MsU0FBZCxHQUEwQixDQUExQjs7QUFFQSxVQUFJL1gsVUFBSixFQUFnQjtBQUNkRixhQUFLNkQsTUFBTCxDQUFZLEtBQUtvQyxRQUFqQjtBQUNEOztBQUVEaEcsUUFBRSxLQUFLZ0csUUFBUCxFQUFpQm9JLFFBQWpCLENBQTBCeEksVUFBVUcsSUFBcEM7O0FBRUEsVUFBSSxLQUFLd0YsT0FBTCxDQUFheEMsS0FBakIsRUFBd0I7QUFDdEIsYUFBS2tQLGFBQUw7QUFDRDs7QUFFRCxVQUFNQyxhQUFhbFksRUFBRXdGLEtBQUYsQ0FBUUEsTUFBTXlKLEtBQWQsRUFBcUI7QUFDdEN0QjtBQURzQyxPQUFyQixDQUFuQjs7QUFJQSxVQUFNd0sscUJBQXFCLFNBQXJCQSxrQkFBcUIsR0FBTTtBQUMvQixZQUFJLE9BQUs1TSxPQUFMLENBQWF4QyxLQUFqQixFQUF3QjtBQUN0QixpQkFBSy9DLFFBQUwsQ0FBYytDLEtBQWQ7QUFDRDtBQUNELGVBQUs0RyxnQkFBTCxHQUF3QixLQUF4QjtBQUNBM1AsVUFBRSxPQUFLZ0csUUFBUCxFQUFpQmxDLE9BQWpCLENBQXlCb1UsVUFBekI7QUFDRCxPQU5EOztBQVFBLFVBQUlqWSxVQUFKLEVBQWdCO0FBQ2RELFVBQUUsS0FBSzBXLE9BQVAsRUFDR25VLEdBREgsQ0FDT3hDLEtBQUt5QyxjQURaLEVBQzRCMlYsa0JBRDVCLEVBRUd0VixvQkFGSCxDQUV3QndDLG1CQUZ4QjtBQUdELE9BSkQsTUFJTztBQUNMOFM7QUFDRDtBQUNGLEtBdlFpQjs7QUFBQSxvQkF5UWxCRixhQXpRa0IsNEJBeVFGO0FBQUE7O0FBQ2RqWSxRQUFFZ0MsUUFBRixFQUNHMEssR0FESCxDQUNPbEgsTUFBTXFRLE9BRGIsRUFDc0I7QUFEdEIsT0FFR3BPLEVBRkgsQ0FFTWpDLE1BQU1xUSxPQUZaLEVBRXFCLFVBQUN6VSxLQUFELEVBQVc7QUFDNUIsWUFBSVksYUFBYVosTUFBTUMsTUFBbkIsSUFDQSxPQUFLMkUsUUFBTCxLQUFrQjVFLE1BQU1DLE1BRHhCLElBRUEsQ0FBQ3JCLEVBQUUsT0FBS2dHLFFBQVAsRUFBaUJvUyxHQUFqQixDQUFxQmhYLE1BQU1DLE1BQTNCLEVBQW1DcUMsTUFGeEMsRUFFZ0Q7QUFDOUMsaUJBQUtzQyxRQUFMLENBQWMrQyxLQUFkO0FBQ0Q7QUFDRixPQVJIO0FBU0QsS0FuUmlCOztBQUFBLG9CQXFSbEJxTyxlQXJSa0IsOEJBcVJBO0FBQUE7O0FBQ2hCLFVBQUksS0FBS1IsUUFBTCxJQUFpQixLQUFLckwsT0FBTCxDQUFhOUIsUUFBbEMsRUFBNEM7QUFDMUN6SixVQUFFLEtBQUtnRyxRQUFQLEVBQWlCeUIsRUFBakIsQ0FBb0JqQyxNQUFNd1EsZUFBMUIsRUFBMkMsVUFBQzVVLEtBQUQsRUFBVztBQUNwRCxjQUFJQSxNQUFNNEwsS0FBTixLQUFnQitFLGNBQXBCLEVBQW9DO0FBQ2xDM1Esa0JBQU1vRyxjQUFOO0FBQ0EsbUJBQUs4SSxJQUFMO0FBQ0Q7QUFDRixTQUxEO0FBT0QsT0FSRCxNQVFPLElBQUksQ0FBQyxLQUFLc0csUUFBVixFQUFvQjtBQUN6QjVXLFVBQUUsS0FBS2dHLFFBQVAsRUFBaUIwRyxHQUFqQixDQUFxQmxILE1BQU13USxlQUEzQjtBQUNEO0FBQ0YsS0FqU2lCOztBQUFBLG9CQW1TbEJxQixlQW5Ta0IsOEJBbVNBO0FBQUE7O0FBQ2hCLFVBQUksS0FBS1QsUUFBVCxFQUFtQjtBQUNqQjVXLFVBQUU2QixNQUFGLEVBQVU0RixFQUFWLENBQWFqQyxNQUFNc1EsTUFBbkIsRUFBMkIsVUFBQzFVLEtBQUQ7QUFBQSxpQkFBVyxPQUFLcVcsWUFBTCxDQUFrQnJXLEtBQWxCLENBQVg7QUFBQSxTQUEzQjtBQUNELE9BRkQsTUFFTztBQUNMcEIsVUFBRTZCLE1BQUYsRUFBVTZLLEdBQVYsQ0FBY2xILE1BQU1zUSxNQUFwQjtBQUNEO0FBQ0YsS0F6U2lCOztBQUFBLG9CQTJTbEIwQixVQTNTa0IseUJBMlNMO0FBQUE7O0FBQ1gsV0FBS3hSLFFBQUwsQ0FBYzdELEtBQWQsQ0FBb0IyVixPQUFwQixHQUE4QixNQUE5QjtBQUNBLFdBQUs5UixRQUFMLENBQWNnRCxZQUFkLENBQTJCLGFBQTNCLEVBQTBDLElBQTFDO0FBQ0EsV0FBSzJHLGdCQUFMLEdBQXdCLEtBQXhCO0FBQ0EsV0FBSzJILGFBQUwsQ0FBbUIsWUFBTTtBQUN2QnRYLFVBQUVnQyxTQUFTbVYsSUFBWCxFQUFpQnRRLFdBQWpCLENBQTZCakIsVUFBVXlRLElBQXZDO0FBQ0EsZUFBS2dDLGlCQUFMO0FBQ0EsZUFBS0MsZUFBTDtBQUNBdFksVUFBRSxPQUFLZ0csUUFBUCxFQUFpQmxDLE9BQWpCLENBQXlCMEIsTUFBTTJKLE1BQS9CO0FBQ0QsT0FMRDtBQU1ELEtBclRpQjs7QUFBQSxvQkF1VGxCb0osZUF2VGtCLDhCQXVUQTtBQUNoQixVQUFJLEtBQUs1QixTQUFULEVBQW9CO0FBQ2xCM1csVUFBRSxLQUFLMlcsU0FBUCxFQUFrQjFQLE1BQWxCO0FBQ0EsYUFBSzBQLFNBQUwsR0FBaUIsSUFBakI7QUFDRDtBQUNGLEtBNVRpQjs7QUFBQSxvQkE4VGxCVyxhQTlUa0IsMEJBOFRKa0IsUUE5VEksRUE4VE07QUFBQTs7QUFDdEIsVUFBTUMsVUFBVXpZLEVBQUUsS0FBS2dHLFFBQVAsRUFBaUJjLFFBQWpCLENBQTBCbEIsVUFBVUUsSUFBcEMsSUFDZEYsVUFBVUUsSUFESSxHQUNHLEVBRG5COztBQUdBLFVBQUksS0FBSzhRLFFBQUwsSUFBaUIsS0FBS3JMLE9BQUwsQ0FBYXFLLFFBQWxDLEVBQTRDO0FBQzFDLFlBQU04QyxZQUFZM1ksS0FBSytDLHFCQUFMLE1BQWdDMlYsT0FBbEQ7O0FBRUEsYUFBSzlCLFNBQUwsR0FBaUIzVSxTQUFTQyxhQUFULENBQXVCLEtBQXZCLENBQWpCO0FBQ0EsYUFBSzBVLFNBQUwsQ0FBZWdDLFNBQWYsR0FBMkIvUyxVQUFVd1EsUUFBckM7O0FBRUEsWUFBSXFDLE9BQUosRUFBYTtBQUNYelksWUFBRSxLQUFLMlcsU0FBUCxFQUFrQnZJLFFBQWxCLENBQTJCcUssT0FBM0I7QUFDRDs7QUFFRHpZLFVBQUUsS0FBSzJXLFNBQVAsRUFBa0JpQyxRQUFsQixDQUEyQjVXLFNBQVNtVixJQUFwQzs7QUFFQW5YLFVBQUUsS0FBS2dHLFFBQVAsRUFBaUJ5QixFQUFqQixDQUFvQmpDLE1BQU11USxhQUExQixFQUF5QyxVQUFDM1UsS0FBRCxFQUFXO0FBQ2xELGNBQUksT0FBSzBWLG9CQUFULEVBQStCO0FBQzdCLG1CQUFLQSxvQkFBTCxHQUE0QixLQUE1QjtBQUNBO0FBQ0Q7QUFDRCxjQUFJMVYsTUFBTUMsTUFBTixLQUFpQkQsTUFBTXlYLGFBQTNCLEVBQTBDO0FBQ3hDO0FBQ0Q7QUFDRCxjQUFJLE9BQUt0TixPQUFMLENBQWFxSyxRQUFiLEtBQTBCLFFBQTlCLEVBQXdDO0FBQ3RDLG1CQUFLNVAsUUFBTCxDQUFjK0MsS0FBZDtBQUNELFdBRkQsTUFFTztBQUNMLG1CQUFLdUgsSUFBTDtBQUNEO0FBQ0YsU0FiRDs7QUFlQSxZQUFJb0ksU0FBSixFQUFlO0FBQ2IzWSxlQUFLNkQsTUFBTCxDQUFZLEtBQUsrUyxTQUFqQjtBQUNEOztBQUVEM1csVUFBRSxLQUFLMlcsU0FBUCxFQUFrQnZJLFFBQWxCLENBQTJCeEksVUFBVUcsSUFBckM7O0FBRUEsWUFBSSxDQUFDeVMsUUFBTCxFQUFlO0FBQ2I7QUFDRDs7QUFFRCxZQUFJLENBQUNFLFNBQUwsRUFBZ0I7QUFDZEY7QUFDQTtBQUNEOztBQUVEeFksVUFBRSxLQUFLMlcsU0FBUCxFQUNHcFUsR0FESCxDQUNPeEMsS0FBS3lDLGNBRFosRUFDNEJnVyxRQUQ1QixFQUVHM1Ysb0JBRkgsQ0FFd0I4Uyw0QkFGeEI7QUFJRCxPQTlDRCxNQThDTyxJQUFJLENBQUMsS0FBS2lCLFFBQU4sSUFBa0IsS0FBS0QsU0FBM0IsRUFBc0M7QUFDM0MzVyxVQUFFLEtBQUsyVyxTQUFQLEVBQWtCOVAsV0FBbEIsQ0FBOEJqQixVQUFVRyxJQUF4Qzs7QUFFQSxZQUFNK1MsaUJBQWlCLFNBQWpCQSxjQUFpQixHQUFNO0FBQzNCLGlCQUFLUCxlQUFMO0FBQ0EsY0FBSUMsUUFBSixFQUFjO0FBQ1pBO0FBQ0Q7QUFDRixTQUxEOztBQU9BLFlBQUl6WSxLQUFLK0MscUJBQUwsTUFDRDlDLEVBQUUsS0FBS2dHLFFBQVAsRUFBaUJjLFFBQWpCLENBQTBCbEIsVUFBVUUsSUFBcEMsQ0FESCxFQUM4QztBQUM1QzlGLFlBQUUsS0FBSzJXLFNBQVAsRUFDR3BVLEdBREgsQ0FDT3hDLEtBQUt5QyxjQURaLEVBQzRCc1csY0FENUIsRUFFR2pXLG9CQUZILENBRXdCOFMsNEJBRnhCO0FBR0QsU0FMRCxNQUtPO0FBQ0xtRDtBQUNEO0FBRUYsT0FuQk0sTUFtQkEsSUFBSU4sUUFBSixFQUFjO0FBQ25CQTtBQUNEO0FBQ0YsS0F0WWlCOztBQXlZbEI7QUFDQTtBQUNBO0FBQ0E7O0FBNVlrQixvQkE4WWxCZCxhQTlZa0IsNEJBOFlGO0FBQ2QsVUFBTXFCLHFCQUNKLEtBQUsvUyxRQUFMLENBQWNnVCxZQUFkLEdBQTZCaFgsU0FBUzZLLGVBQVQsQ0FBeUJvTSxZQUR4RDs7QUFHQSxVQUFJLENBQUMsS0FBS3BDLGtCQUFOLElBQTRCa0Msa0JBQWhDLEVBQW9EO0FBQ2xELGFBQUsvUyxRQUFMLENBQWM3RCxLQUFkLENBQW9CK1csV0FBcEIsR0FBcUMsS0FBS2xDLGVBQTFDO0FBQ0Q7O0FBRUQsVUFBSSxLQUFLSCxrQkFBTCxJQUEyQixDQUFDa0Msa0JBQWhDLEVBQW9EO0FBQ2xELGFBQUsvUyxRQUFMLENBQWM3RCxLQUFkLENBQW9CZ1gsWUFBcEIsR0FBc0MsS0FBS25DLGVBQTNDO0FBQ0Q7QUFDRixLQXpaaUI7O0FBQUEsb0JBMlpsQnFCLGlCQTNaa0IsZ0NBMlpFO0FBQ2xCLFdBQUtyUyxRQUFMLENBQWM3RCxLQUFkLENBQW9CK1csV0FBcEIsR0FBa0MsRUFBbEM7QUFDQSxXQUFLbFQsUUFBTCxDQUFjN0QsS0FBZCxDQUFvQmdYLFlBQXBCLEdBQW1DLEVBQW5DO0FBQ0QsS0E5WmlCOztBQUFBLG9CQWdhbEJsQyxlQWhha0IsOEJBZ2FBO0FBQ2hCLFdBQUtKLGtCQUFMLEdBQTBCN1UsU0FBU21WLElBQVQsQ0FBY2lDLFdBQWQsR0FBNEJ2WCxPQUFPd1gsVUFBN0Q7QUFDQSxXQUFLckMsZUFBTCxHQUF1QixLQUFLc0Msa0JBQUwsRUFBdkI7QUFDRCxLQW5haUI7O0FBQUEsb0JBcWFsQnBDLGFBcmFrQiw0QkFxYUY7QUFBQTs7QUFDZCxVQUFJLEtBQUtMLGtCQUFULEVBQTZCO0FBQzNCO0FBQ0E7O0FBRUE7QUFDQTdXLFVBQUVzRixTQUFTa1IsYUFBWCxFQUEwQnJQLElBQTFCLENBQStCLFVBQUNtRixLQUFELEVBQVFoSixPQUFSLEVBQW9CO0FBQ2pELGNBQU1pVyxnQkFBZ0J2WixFQUFFc0QsT0FBRixFQUFXLENBQVgsRUFBY25CLEtBQWQsQ0FBb0JnWCxZQUExQztBQUNBLGNBQU1LLG9CQUFvQnhaLEVBQUVzRCxPQUFGLEVBQVdtVyxHQUFYLENBQWUsZUFBZixDQUExQjtBQUNBelosWUFBRXNELE9BQUYsRUFBVytELElBQVgsQ0FBZ0IsZUFBaEIsRUFBaUNrUyxhQUFqQyxFQUFnREUsR0FBaEQsQ0FBb0QsZUFBcEQsRUFBd0VDLFdBQVdGLGlCQUFYLElBQWdDLE9BQUt4QyxlQUE3RztBQUNELFNBSkQ7O0FBTUE7QUFDQWhYLFVBQUVzRixTQUFTbVIsY0FBWCxFQUEyQnRQLElBQTNCLENBQWdDLFVBQUNtRixLQUFELEVBQVFoSixPQUFSLEVBQW9CO0FBQ2xELGNBQU1xVyxlQUFlM1osRUFBRXNELE9BQUYsRUFBVyxDQUFYLEVBQWNuQixLQUFkLENBQW9CeVgsV0FBekM7QUFDQSxjQUFNQyxtQkFBbUI3WixFQUFFc0QsT0FBRixFQUFXbVcsR0FBWCxDQUFlLGNBQWYsQ0FBekI7QUFDQXpaLFlBQUVzRCxPQUFGLEVBQVcrRCxJQUFYLENBQWdCLGNBQWhCLEVBQWdDc1MsWUFBaEMsRUFBOENGLEdBQTlDLENBQWtELGNBQWxELEVBQXFFQyxXQUFXRyxnQkFBWCxJQUErQixPQUFLN0MsZUFBekc7QUFDRCxTQUpEOztBQU1BO0FBQ0EsWUFBTXVDLGdCQUFnQnZYLFNBQVNtVixJQUFULENBQWNoVixLQUFkLENBQW9CZ1gsWUFBMUM7QUFDQSxZQUFNSyxvQkFBb0J4WixFQUFFLE1BQUYsRUFBVXlaLEdBQVYsQ0FBYyxlQUFkLENBQTFCO0FBQ0F6WixVQUFFLE1BQUYsRUFBVXFILElBQVYsQ0FBZSxlQUFmLEVBQWdDa1MsYUFBaEMsRUFBK0NFLEdBQS9DLENBQW1ELGVBQW5ELEVBQXVFQyxXQUFXRixpQkFBWCxJQUFnQyxLQUFLeEMsZUFBNUc7QUFDRDtBQUNGLEtBN2JpQjs7QUFBQSxvQkErYmxCc0IsZUEvYmtCLDhCQStiQTtBQUNoQjtBQUNBdFksUUFBRXNGLFNBQVNrUixhQUFYLEVBQTBCclAsSUFBMUIsQ0FBK0IsVUFBQ21GLEtBQUQsRUFBUWhKLE9BQVIsRUFBb0I7QUFDakQsWUFBTXdXLFVBQVU5WixFQUFFc0QsT0FBRixFQUFXK0QsSUFBWCxDQUFnQixlQUFoQixDQUFoQjtBQUNBLFlBQUksT0FBT3lTLE9BQVAsS0FBbUIsV0FBdkIsRUFBb0M7QUFDbEM5WixZQUFFc0QsT0FBRixFQUFXbVcsR0FBWCxDQUFlLGVBQWYsRUFBZ0NLLE9BQWhDLEVBQXlDclQsVUFBekMsQ0FBb0QsZUFBcEQ7QUFDRDtBQUNGLE9BTEQ7O0FBT0E7QUFDQXpHLFFBQUVzRixTQUFTbVIsY0FBWCxFQUEyQnRQLElBQTNCLENBQWdDLFVBQUNtRixLQUFELEVBQVFoSixPQUFSLEVBQW9CO0FBQ2xELFlBQU15VyxTQUFTL1osRUFBRXNELE9BQUYsRUFBVytELElBQVgsQ0FBZ0IsY0FBaEIsQ0FBZjtBQUNBLFlBQUksT0FBTzBTLE1BQVAsS0FBa0IsV0FBdEIsRUFBbUM7QUFDakMvWixZQUFFc0QsT0FBRixFQUFXbVcsR0FBWCxDQUFlLGNBQWYsRUFBK0JNLE1BQS9CLEVBQXVDdFQsVUFBdkMsQ0FBa0QsY0FBbEQ7QUFDRDtBQUNGLE9BTEQ7O0FBT0E7QUFDQSxVQUFNcVQsVUFBVTlaLEVBQUUsTUFBRixFQUFVcUgsSUFBVixDQUFlLGVBQWYsQ0FBaEI7QUFDQSxVQUFJLE9BQU95UyxPQUFQLEtBQW1CLFdBQXZCLEVBQW9DO0FBQ2xDOVosVUFBRSxNQUFGLEVBQVV5WixHQUFWLENBQWMsZUFBZCxFQUErQkssT0FBL0IsRUFBd0NyVCxVQUF4QyxDQUFtRCxlQUFuRDtBQUNEO0FBQ0YsS0FyZGlCOztBQUFBLG9CQXVkbEI2UyxrQkF2ZGtCLGlDQXVkRztBQUFFO0FBQ3JCLFVBQU1VLFlBQVloWSxTQUFTQyxhQUFULENBQXVCLEtBQXZCLENBQWxCO0FBQ0ErWCxnQkFBVXJCLFNBQVYsR0FBc0IvUyxVQUFVdVEsa0JBQWhDO0FBQ0FuVSxlQUFTbVYsSUFBVCxDQUFjVSxXQUFkLENBQTBCbUMsU0FBMUI7QUFDQSxVQUFNQyxpQkFBaUJELFVBQVU3SSxxQkFBVixHQUFrQytJLEtBQWxDLEdBQTBDRixVQUFVWixXQUEzRTtBQUNBcFgsZUFBU21WLElBQVQsQ0FBY2dELFdBQWQsQ0FBMEJILFNBQTFCO0FBQ0EsYUFBT0MsY0FBUDtBQUNELEtBOWRpQjs7QUFpZWxCOztBQWpla0IsVUFtZVgvUyxnQkFuZVcsNkJBbWVNaEQsTUFuZU4sRUFtZWN5SixhQW5lZCxFQW1lNkI7QUFDN0MsYUFBTyxLQUFLeEcsSUFBTCxDQUFVLFlBQVk7QUFDM0IsWUFBSUUsT0FBWXJILEVBQUUsSUFBRixFQUFRcUgsSUFBUixDQUFhcEMsUUFBYixDQUFoQjtBQUNBLFlBQU1zRyxVQUFVdkwsRUFBRTJNLE1BQUYsQ0FDZCxFQURjLEVBRWQrSSxNQUFNbk0sT0FGUSxFQUdkdkosRUFBRSxJQUFGLEVBQVFxSCxJQUFSLEVBSGMsRUFJZCxRQUFPbkQsTUFBUCx5Q0FBT0EsTUFBUCxPQUFrQixRQUFsQixJQUE4QkEsTUFKaEIsQ0FBaEI7O0FBT0EsWUFBSSxDQUFDbUQsSUFBTCxFQUFXO0FBQ1RBLGlCQUFPLElBQUlxTyxLQUFKLENBQVUsSUFBVixFQUFnQm5LLE9BQWhCLENBQVA7QUFDQXZMLFlBQUUsSUFBRixFQUFRcUgsSUFBUixDQUFhcEMsUUFBYixFQUF1Qm9DLElBQXZCO0FBQ0Q7O0FBRUQsWUFBSSxPQUFPbkQsTUFBUCxLQUFrQixRQUF0QixFQUFnQztBQUM5QixjQUFJbUQsS0FBS25ELE1BQUwsTUFBaUJ2QyxTQUFyQixFQUFnQztBQUM5QixrQkFBTSxJQUFJZ0QsS0FBSix1QkFBOEJULE1BQTlCLE9BQU47QUFDRDtBQUNEbUQsZUFBS25ELE1BQUwsRUFBYXlKLGFBQWI7QUFDRCxTQUxELE1BS08sSUFBSXBDLFFBQVFnRixJQUFaLEVBQWtCO0FBQ3ZCbEosZUFBS2tKLElBQUwsQ0FBVTVDLGFBQVY7QUFDRDtBQUNGLE9BdEJNLENBQVA7QUF1QkQsS0EzZmlCOztBQUFBO0FBQUE7QUFBQSwwQkF1Rkc7QUFDbkIsZUFBTzNJLE9BQVA7QUFDRDtBQXpGaUI7QUFBQTtBQUFBLDBCQTJGRztBQUNuQixlQUFPdUUsT0FBUDtBQUNEO0FBN0ZpQjs7QUFBQTtBQUFBOztBQWdnQnBCOzs7Ozs7QUFNQXZKLElBQUVnQyxRQUFGLEVBQVl5RixFQUFaLENBQWVqQyxNQUFNRyxjQUFyQixFQUFxQ0wsU0FBUzJDLFdBQTlDLEVBQTJELFVBQVU3RyxLQUFWLEVBQWlCO0FBQUE7O0FBQzFFLFFBQUlDLGVBQUo7QUFDQSxRQUFNa0MsV0FBV3hELEtBQUtzRCxzQkFBTCxDQUE0QixJQUE1QixDQUFqQjs7QUFFQSxRQUFJRSxRQUFKLEVBQWM7QUFDWmxDLGVBQVNyQixFQUFFdUQsUUFBRixFQUFZLENBQVosQ0FBVDtBQUNEOztBQUVELFFBQU1XLFNBQVNsRSxFQUFFcUIsTUFBRixFQUFVZ0csSUFBVixDQUFlcEMsUUFBZixJQUNiLFFBRGEsR0FDRmpGLEVBQUUyTSxNQUFGLENBQVMsRUFBVCxFQUFhM00sRUFBRXFCLE1BQUYsRUFBVWdHLElBQVYsRUFBYixFQUErQnJILEVBQUUsSUFBRixFQUFRcUgsSUFBUixFQUEvQixDQURiOztBQUdBLFFBQUksS0FBSzBGLE9BQUwsS0FBaUIsR0FBakIsSUFBd0IsS0FBS0EsT0FBTCxLQUFpQixNQUE3QyxFQUFxRDtBQUNuRDNMLFlBQU1vRyxjQUFOO0FBQ0Q7O0FBRUQsUUFBTW9LLFVBQVU1UixFQUFFcUIsTUFBRixFQUFVa0IsR0FBVixDQUFjaUQsTUFBTU8sSUFBcEIsRUFBMEIsVUFBQ21PLFNBQUQsRUFBZTtBQUN2RCxVQUFJQSxVQUFVNU4sa0JBQVYsRUFBSixFQUFvQztBQUNsQztBQUNBO0FBQ0Q7O0FBRURzTCxjQUFRclAsR0FBUixDQUFZaUQsTUFBTTJKLE1BQWxCLEVBQTBCLFlBQU07QUFDOUIsWUFBSW5QLFdBQVFzQixFQUFSLENBQVcsVUFBWCxDQUFKLEVBQTRCO0FBQzFCLGtCQUFLeUgsS0FBTDtBQUNEO0FBQ0YsT0FKRDtBQUtELEtBWGUsQ0FBaEI7O0FBYUEyTSxVQUFNeE8sZ0JBQU4sQ0FBdUJ4RyxJQUF2QixDQUE0QlYsRUFBRXFCLE1BQUYsQ0FBNUIsRUFBdUM2QyxNQUF2QyxFQUErQyxJQUEvQztBQUNELEdBN0JEOztBQWdDQTs7Ozs7O0FBTUFsRSxJQUFFNEMsRUFBRixDQUFLbUMsSUFBTCxJQUF5QjJRLE1BQU14TyxnQkFBL0I7QUFDQWxILElBQUU0QyxFQUFGLENBQUttQyxJQUFMLEVBQVcyQyxXQUFYLEdBQXlCZ08sS0FBekI7QUFDQTFWLElBQUU0QyxFQUFGLENBQUttQyxJQUFMLEVBQVc0QyxVQUFYLEdBQXlCLFlBQVk7QUFDbkMzSCxNQUFFNEMsRUFBRixDQUFLbUMsSUFBTCxJQUFhSyxrQkFBYjtBQUNBLFdBQU9zUSxNQUFNeE8sZ0JBQWI7QUFDRCxHQUhEOztBQUtBLFNBQU93TyxLQUFQO0FBRUQsQ0FyakJhLENBcWpCWDdRLE1BcmpCVyxDQUFkOzs7Ozs7O0FDUEE7Ozs7Ozs7QUFPQSxJQUFNdVYsWUFBYSxVQUFDcGEsQ0FBRCxFQUFPOztBQUd4Qjs7Ozs7O0FBTUEsTUFBTStFLE9BQXFCLFdBQTNCO0FBQ0EsTUFBTUMsVUFBcUIsWUFBM0I7QUFDQSxNQUFNQyxXQUFxQixjQUEzQjtBQUNBLE1BQU1DLGtCQUF5QkQsUUFBL0I7QUFDQSxNQUFNRSxlQUFxQixXQUEzQjtBQUNBLE1BQU1DLHFCQUFxQnBGLEVBQUU0QyxFQUFGLENBQUttQyxJQUFMLENBQTNCOztBQUVBLE1BQU13RSxVQUFVO0FBQ2RnSyxZQUFTLEVBREs7QUFFZDhHLFlBQVMsTUFGSztBQUdkaFosWUFBUztBQUhLLEdBQWhCOztBQU1BLE1BQU13SSxjQUFjO0FBQ2xCMEosWUFBUyxRQURTO0FBRWxCOEcsWUFBUyxRQUZTO0FBR2xCaFosWUFBUztBQUhTLEdBQXBCOztBQU1BLE1BQU1tRSxRQUFRO0FBQ1o4VSwyQkFBMkJwVixTQURmO0FBRVpxVix1QkFBeUJyVixTQUZiO0FBR1p1Riw0QkFBdUJ2RixTQUF2QixHQUFtQ0M7QUFIdkIsR0FBZDs7QUFNQSxNQUFNUyxZQUFZO0FBQ2hCNFUsbUJBQWdCLGVBREE7QUFFaEJDLG1CQUFnQixlQUZBO0FBR2hCNVMsWUFBZ0I7QUFIQSxHQUFsQjs7QUFNQSxNQUFNdkMsV0FBVztBQUNmb1YsY0FBa0IscUJBREg7QUFFZjdTLFlBQWtCLFNBRkg7QUFHZjhTLG9CQUFrQixtQkFISDtBQUlmQyxlQUFrQixXQUpIO0FBS2ZDLGdCQUFrQixrQkFMSDtBQU1mQyxjQUFrQixXQU5IO0FBT2ZDLG9CQUFrQixnQkFQSDtBQVFmQyxxQkFBa0I7QUFSSCxHQUFqQjs7QUFXQSxNQUFNQyxlQUFlO0FBQ25CQyxZQUFXLFFBRFE7QUFFbkJDLGNBQVc7O0FBSWI7Ozs7OztBQU5xQixHQUFyQjtBQW5Ed0IsTUErRGxCZixTQS9Ea0I7QUFpRXRCLHVCQUFZOVcsT0FBWixFQUFxQlksTUFBckIsRUFBNkI7QUFBQTs7QUFBQTs7QUFDM0IsV0FBSzhCLFFBQUwsR0FBc0IxQyxPQUF0QjtBQUNBLFdBQUs4WCxjQUFMLEdBQXNCOVgsUUFBUXlKLE9BQVIsS0FBb0IsTUFBcEIsR0FBNkJsTCxNQUE3QixHQUFzQ3lCLE9BQTVEO0FBQ0EsV0FBS2lJLE9BQUwsR0FBc0IsS0FBS0MsVUFBTCxDQUFnQnRILE1BQWhCLENBQXRCO0FBQ0EsV0FBS21YLFNBQUwsR0FBeUIsS0FBSzlQLE9BQUwsQ0FBYWxLLE1BQWhCLFNBQTBCaUUsU0FBU3NWLFNBQW5DLFVBQ0csS0FBS3JQLE9BQUwsQ0FBYWxLLE1BRGhCLFNBQzBCaUUsU0FBU3VWLFVBRG5DLFdBRUcsS0FBS3RQLE9BQUwsQ0FBYWxLLE1BRmhCLFNBRTBCaUUsU0FBU3lWLGNBRm5DLENBQXRCO0FBR0EsV0FBS08sUUFBTCxHQUFzQixFQUF0QjtBQUNBLFdBQUtDLFFBQUwsR0FBc0IsRUFBdEI7QUFDQSxXQUFLQyxhQUFMLEdBQXNCLElBQXRCO0FBQ0EsV0FBS0MsYUFBTCxHQUFzQixDQUF0Qjs7QUFFQXpiLFFBQUUsS0FBS29iLGNBQVAsRUFBdUIzVCxFQUF2QixDQUEwQmpDLE1BQU0rVSxNQUFoQyxFQUF3QyxVQUFDblosS0FBRDtBQUFBLGVBQVcsTUFBS3NhLFFBQUwsQ0FBY3RhLEtBQWQsQ0FBWDtBQUFBLE9BQXhDOztBQUVBLFdBQUt1YSxPQUFMO0FBQ0EsV0FBS0QsUUFBTDtBQUNEOztBQUdEOztBQVdBOztBQS9Gc0Isd0JBaUd0QkMsT0FqR3NCLHNCQWlHWjtBQUFBOztBQUNSLFVBQU1DLGFBQWEsS0FBS1IsY0FBTCxLQUF3QixLQUFLQSxjQUFMLENBQW9CdlosTUFBNUMsR0FDakJvWixhQUFhRSxRQURJLEdBQ09GLGFBQWFDLE1BRHZDOztBQUdBLFVBQU1XLGVBQWUsS0FBS3RRLE9BQUwsQ0FBYThPLE1BQWIsS0FBd0IsTUFBeEIsR0FDbkJ1QixVQURtQixHQUNOLEtBQUtyUSxPQUFMLENBQWE4TyxNQUQ1Qjs7QUFHQSxVQUFNeUIsYUFBYUQsaUJBQWlCWixhQUFhRSxRQUE5QixHQUNqQixLQUFLWSxhQUFMLEVBRGlCLEdBQ00sQ0FEekI7O0FBR0EsV0FBS1QsUUFBTCxHQUFnQixFQUFoQjtBQUNBLFdBQUtDLFFBQUwsR0FBZ0IsRUFBaEI7O0FBRUEsV0FBS0UsYUFBTCxHQUFxQixLQUFLTyxnQkFBTCxFQUFyQjs7QUFFQSxVQUFNQyxVQUFVamMsRUFBRWlOLFNBQUYsQ0FBWWpOLEVBQUUsS0FBS3FiLFNBQVAsQ0FBWixDQUFoQjs7QUFFQVksY0FDR0MsR0FESCxDQUNPLFVBQUM1WSxPQUFELEVBQWE7QUFDaEIsWUFBSWpDLGVBQUo7QUFDQSxZQUFNOGEsaUJBQWlCcGMsS0FBS3NELHNCQUFMLENBQTRCQyxPQUE1QixDQUF2Qjs7QUFFQSxZQUFJNlksY0FBSixFQUFvQjtBQUNsQjlhLG1CQUFTckIsRUFBRW1jLGNBQUYsRUFBa0IsQ0FBbEIsQ0FBVDtBQUNEOztBQUVELFlBQUk5YSxNQUFKLEVBQVk7QUFDVixjQUFNK2EsWUFBWS9hLE9BQU84UCxxQkFBUCxFQUFsQjtBQUNBLGNBQUlpTCxVQUFVbEMsS0FBVixJQUFtQmtDLFVBQVVDLE1BQWpDLEVBQXlDO0FBQ3ZDO0FBQ0EsbUJBQU8sQ0FDTHJjLEVBQUVxQixNQUFGLEVBQVV3YSxZQUFWLElBQTBCUyxHQUExQixHQUFnQ1IsVUFEM0IsRUFFTEssY0FGSyxDQUFQO0FBSUQ7QUFDRjtBQUNELGVBQU8sSUFBUDtBQUNELE9BcEJILEVBcUJHbE0sTUFyQkgsQ0FxQlUsVUFBQ3NNLElBQUQ7QUFBQSxlQUFXQSxJQUFYO0FBQUEsT0FyQlYsRUFzQkdDLElBdEJILENBc0JRLFVBQUNDLENBQUQsRUFBSUMsQ0FBSjtBQUFBLGVBQWFELEVBQUUsQ0FBRixJQUFPQyxFQUFFLENBQUYsQ0FBcEI7QUFBQSxPQXRCUixFQXVCR0MsT0F2QkgsQ0F1QlcsVUFBQ0osSUFBRCxFQUFVO0FBQ2pCLGVBQUtqQixRQUFMLENBQWNwTCxJQUFkLENBQW1CcU0sS0FBSyxDQUFMLENBQW5CO0FBQ0EsZUFBS2hCLFFBQUwsQ0FBY3JMLElBQWQsQ0FBbUJxTSxLQUFLLENBQUwsQ0FBbkI7QUFDRCxPQTFCSDtBQTJCRCxLQTdJcUI7O0FBQUEsd0JBK0l0Qi9WLE9BL0lzQixzQkErSVo7QUFDUnhHLFFBQUV5RyxVQUFGLENBQWEsS0FBS1QsUUFBbEIsRUFBNEJmLFFBQTVCO0FBQ0FqRixRQUFFLEtBQUtvYixjQUFQLEVBQXVCMU8sR0FBdkIsQ0FBMkJ4SCxTQUEzQjs7QUFFQSxXQUFLYyxRQUFMLEdBQXNCLElBQXRCO0FBQ0EsV0FBS29WLGNBQUwsR0FBc0IsSUFBdEI7QUFDQSxXQUFLN1AsT0FBTCxHQUFzQixJQUF0QjtBQUNBLFdBQUs4UCxTQUFMLEdBQXNCLElBQXRCO0FBQ0EsV0FBS0MsUUFBTCxHQUFzQixJQUF0QjtBQUNBLFdBQUtDLFFBQUwsR0FBc0IsSUFBdEI7QUFDQSxXQUFLQyxhQUFMLEdBQXNCLElBQXRCO0FBQ0EsV0FBS0MsYUFBTCxHQUFzQixJQUF0QjtBQUNELEtBM0pxQjs7QUE4SnRCOztBQTlKc0Isd0JBZ0t0QmpRLFVBaEtzQix1QkFnS1h0SCxNQWhLVyxFQWdLSDtBQUNqQkEsZUFBU2xFLEVBQUUyTSxNQUFGLENBQVMsRUFBVCxFQUFhcEQsT0FBYixFQUFzQnJGLE1BQXRCLENBQVQ7O0FBRUEsVUFBSSxPQUFPQSxPQUFPN0MsTUFBZCxLQUF5QixRQUE3QixFQUF1QztBQUNyQyxZQUFJd08sS0FBSzdQLEVBQUVrRSxPQUFPN0MsTUFBVCxFQUFpQndQLElBQWpCLENBQXNCLElBQXRCLENBQVQ7QUFDQSxZQUFJLENBQUNoQixFQUFMLEVBQVM7QUFDUEEsZUFBSzlQLEtBQUtpRCxNQUFMLENBQVkrQixJQUFaLENBQUw7QUFDQS9FLFlBQUVrRSxPQUFPN0MsTUFBVCxFQUFpQndQLElBQWpCLENBQXNCLElBQXRCLEVBQTRCaEIsRUFBNUI7QUFDRDtBQUNEM0wsZUFBTzdDLE1BQVAsU0FBb0J3TyxFQUFwQjtBQUNEOztBQUVEOVAsV0FBS2lFLGVBQUwsQ0FBcUJlLElBQXJCLEVBQTJCYixNQUEzQixFQUFtQzJGLFdBQW5DOztBQUVBLGFBQU8zRixNQUFQO0FBQ0QsS0EvS3FCOztBQUFBLHdCQWlMdEI2WCxhQWpMc0IsNEJBaUxOO0FBQ2QsYUFBTyxLQUFLWCxjQUFMLEtBQXdCdlosTUFBeEIsR0FDSCxLQUFLdVosY0FBTCxDQUFvQndCLFdBRGpCLEdBQytCLEtBQUt4QixjQUFMLENBQW9CcEQsU0FEMUQ7QUFFRCxLQXBMcUI7O0FBQUEsd0JBc0x0QmdFLGdCQXRMc0IsK0JBc0xIO0FBQ2pCLGFBQU8sS0FBS1osY0FBTCxDQUFvQnBDLFlBQXBCLElBQW9DOVYsS0FBSzJaLEdBQUwsQ0FDekM3YSxTQUFTbVYsSUFBVCxDQUFjNkIsWUFEMkIsRUFFekNoWCxTQUFTNkssZUFBVCxDQUF5Qm1NLFlBRmdCLENBQTNDO0FBSUQsS0EzTHFCOztBQUFBLHdCQTZMdEI4RCxnQkE3THNCLCtCQTZMSDtBQUNqQixhQUFPLEtBQUsxQixjQUFMLEtBQXdCdlosTUFBeEIsR0FDSEEsT0FBT2tiLFdBREosR0FDa0IsS0FBSzNCLGNBQUwsQ0FBb0JqSyxxQkFBcEIsR0FBNENrTCxNQURyRTtBQUVELEtBaE1xQjs7QUFBQSx3QkFrTXRCWCxRQWxNc0IsdUJBa01YO0FBQ1QsVUFBTTFELFlBQWUsS0FBSytELGFBQUwsS0FBdUIsS0FBS3hRLE9BQUwsQ0FBYWdJLE1BQXpEO0FBQ0EsVUFBTXlGLGVBQWUsS0FBS2dELGdCQUFMLEVBQXJCO0FBQ0EsVUFBTWdCLFlBQWUsS0FBS3pSLE9BQUwsQ0FBYWdJLE1BQWIsR0FDakJ5RixZQURpQixHQUVqQixLQUFLOEQsZ0JBQUwsRUFGSjs7QUFJQSxVQUFJLEtBQUtyQixhQUFMLEtBQXVCekMsWUFBM0IsRUFBeUM7QUFDdkMsYUFBSzJDLE9BQUw7QUFDRDs7QUFFRCxVQUFJM0QsYUFBYWdGLFNBQWpCLEVBQTRCO0FBQzFCLFlBQU0zYixTQUFTLEtBQUtrYSxRQUFMLENBQWMsS0FBS0EsUUFBTCxDQUFjN1gsTUFBZCxHQUF1QixDQUFyQyxDQUFmOztBQUVBLFlBQUksS0FBSzhYLGFBQUwsS0FBdUJuYSxNQUEzQixFQUFtQztBQUNqQyxlQUFLNGIsU0FBTCxDQUFlNWIsTUFBZjtBQUNEO0FBQ0Q7QUFDRDs7QUFFRCxVQUFJLEtBQUttYSxhQUFMLElBQXNCeEQsWUFBWSxLQUFLc0QsUUFBTCxDQUFjLENBQWQsQ0FBbEMsSUFBc0QsS0FBS0EsUUFBTCxDQUFjLENBQWQsSUFBbUIsQ0FBN0UsRUFBZ0Y7QUFDOUUsYUFBS0UsYUFBTCxHQUFxQixJQUFyQjtBQUNBLGFBQUswQixNQUFMO0FBQ0E7QUFDRDs7QUFFRCxXQUFLLElBQUluTixJQUFJLEtBQUt1TCxRQUFMLENBQWM1WCxNQUEzQixFQUFtQ3FNLEdBQW5DLEdBQXlDO0FBQ3ZDLFlBQU1vTixpQkFBaUIsS0FBSzNCLGFBQUwsS0FBdUIsS0FBS0QsUUFBTCxDQUFjeEwsQ0FBZCxDQUF2QixJQUNoQmlJLGFBQWEsS0FBS3NELFFBQUwsQ0FBY3ZMLENBQWQsQ0FERyxLQUVmLEtBQUt1TCxRQUFMLENBQWN2TCxJQUFJLENBQWxCLE1BQXlCcE8sU0FBekIsSUFDQXFXLFlBQVksS0FBS3NELFFBQUwsQ0FBY3ZMLElBQUksQ0FBbEIsQ0FIRyxDQUF2Qjs7QUFLQSxZQUFJb04sY0FBSixFQUFvQjtBQUNsQixlQUFLRixTQUFMLENBQWUsS0FBSzFCLFFBQUwsQ0FBY3hMLENBQWQsQ0FBZjtBQUNEO0FBQ0Y7QUFDRixLQXRPcUI7O0FBQUEsd0JBd090QmtOLFNBeE9zQixzQkF3T1o1YixNQXhPWSxFQXdPSjtBQUNoQixXQUFLbWEsYUFBTCxHQUFxQm5hLE1BQXJCOztBQUVBLFdBQUs2YixNQUFMOztBQUVBLFVBQUlFLFVBQVUsS0FBSy9CLFNBQUwsQ0FBZWdDLEtBQWYsQ0FBcUIsR0FBckIsQ0FBZDtBQUNBRCxnQkFBY0EsUUFBUWxCLEdBQVIsQ0FBWSxVQUFDM1ksUUFBRCxFQUFjO0FBQ3RDLGVBQVVBLFFBQUgsc0JBQTRCbEMsTUFBNUIsWUFDR2tDLFFBREgsZUFDcUJsQyxNQURyQixRQUFQO0FBRUQsT0FIYSxDQUFkOztBQUtBLFVBQU1pYyxRQUFRdGQsRUFBRW9kLFFBQVFHLElBQVIsQ0FBYSxHQUFiLENBQUYsQ0FBZDs7QUFFQSxVQUFJRCxNQUFNeFcsUUFBTixDQUFlbEIsVUFBVTRVLGFBQXpCLENBQUosRUFBNkM7QUFDM0M4QyxjQUFNM1csT0FBTixDQUFjckIsU0FBU3dWLFFBQXZCLEVBQWlDdFMsSUFBakMsQ0FBc0NsRCxTQUFTMFYsZUFBL0MsRUFBZ0U1TSxRQUFoRSxDQUF5RXhJLFVBQVVpQyxNQUFuRjtBQUNBeVYsY0FBTWxQLFFBQU4sQ0FBZXhJLFVBQVVpQyxNQUF6QjtBQUNELE9BSEQsTUFHTztBQUNMO0FBQ0F5VixjQUFNbFAsUUFBTixDQUFleEksVUFBVWlDLE1BQXpCO0FBQ0E7QUFDQTtBQUNBeVYsY0FBTUUsT0FBTixDQUFjbFksU0FBU3FWLGNBQXZCLEVBQXVDNU8sSUFBdkMsQ0FBK0N6RyxTQUFTc1YsU0FBeEQsVUFBc0V0VixTQUFTdVYsVUFBL0UsRUFBNkZ6TSxRQUE3RixDQUFzR3hJLFVBQVVpQyxNQUFoSDtBQUNEOztBQUVEN0gsUUFBRSxLQUFLb2IsY0FBUCxFQUF1QnRYLE9BQXZCLENBQStCMEIsTUFBTThVLFFBQXJDLEVBQStDO0FBQzdDM00sdUJBQWV0TTtBQUQ4QixPQUEvQztBQUdELEtBblFxQjs7QUFBQSx3QkFxUXRCNmIsTUFyUXNCLHFCQXFRYjtBQUNQbGQsUUFBRSxLQUFLcWIsU0FBUCxFQUFrQnBMLE1BQWxCLENBQXlCM0ssU0FBU3VDLE1BQWxDLEVBQTBDaEIsV0FBMUMsQ0FBc0RqQixVQUFVaUMsTUFBaEU7QUFDRCxLQXZRcUI7O0FBMFF0Qjs7QUExUXNCLGNBNFFmWCxnQkE1UWUsNkJBNFFFaEQsTUE1UUYsRUE0UVU7QUFDOUIsYUFBTyxLQUFLaUQsSUFBTCxDQUFVLFlBQVk7QUFDM0IsWUFBSUUsT0FBWXJILEVBQUUsSUFBRixFQUFRcUgsSUFBUixDQUFhcEMsUUFBYixDQUFoQjtBQUNBLFlBQU1zRyxVQUFVLFFBQU9ySCxNQUFQLHlDQUFPQSxNQUFQLE9BQWtCLFFBQWxCLElBQThCQSxNQUE5Qzs7QUFFQSxZQUFJLENBQUNtRCxJQUFMLEVBQVc7QUFDVEEsaUJBQU8sSUFBSStTLFNBQUosQ0FBYyxJQUFkLEVBQW9CN08sT0FBcEIsQ0FBUDtBQUNBdkwsWUFBRSxJQUFGLEVBQVFxSCxJQUFSLENBQWFwQyxRQUFiLEVBQXVCb0MsSUFBdkI7QUFDRDs7QUFFRCxZQUFJLE9BQU9uRCxNQUFQLEtBQWtCLFFBQXRCLEVBQWdDO0FBQzlCLGNBQUltRCxLQUFLbkQsTUFBTCxNQUFpQnZDLFNBQXJCLEVBQWdDO0FBQzlCLGtCQUFNLElBQUlnRCxLQUFKLHVCQUE4QlQsTUFBOUIsT0FBTjtBQUNEO0FBQ0RtRCxlQUFLbkQsTUFBTDtBQUNEO0FBQ0YsT0FmTSxDQUFQO0FBZ0JELEtBN1JxQjs7QUFBQTtBQUFBO0FBQUEsMEJBc0ZEO0FBQ25CLGVBQU9jLE9BQVA7QUFDRDtBQXhGcUI7QUFBQTtBQUFBLDBCQTBGRDtBQUNuQixlQUFPdUUsT0FBUDtBQUNEO0FBNUZxQjs7QUFBQTtBQUFBOztBQW1TeEI7Ozs7OztBQU1BdkosSUFBRTZCLE1BQUYsRUFBVTRGLEVBQVYsQ0FBYWpDLE1BQU1pRixhQUFuQixFQUFrQyxZQUFNO0FBQ3RDLFFBQU1nVCxhQUFhemQsRUFBRWlOLFNBQUYsQ0FBWWpOLEVBQUVzRixTQUFTb1YsUUFBWCxDQUFaLENBQW5COztBQUVBLFNBQUssSUFBSTNLLElBQUkwTixXQUFXL1osTUFBeEIsRUFBZ0NxTSxHQUFoQyxHQUFzQztBQUNwQyxVQUFNMk4sT0FBTzFkLEVBQUV5ZCxXQUFXMU4sQ0FBWCxDQUFGLENBQWI7QUFDQXFLLGdCQUFVbFQsZ0JBQVYsQ0FBMkJ4RyxJQUEzQixDQUFnQ2dkLElBQWhDLEVBQXNDQSxLQUFLclcsSUFBTCxFQUF0QztBQUNEO0FBQ0YsR0FQRDs7QUFVQTs7Ozs7O0FBTUFySCxJQUFFNEMsRUFBRixDQUFLbUMsSUFBTCxJQUF5QnFWLFVBQVVsVCxnQkFBbkM7QUFDQWxILElBQUU0QyxFQUFGLENBQUttQyxJQUFMLEVBQVcyQyxXQUFYLEdBQXlCMFMsU0FBekI7QUFDQXBhLElBQUU0QyxFQUFGLENBQUttQyxJQUFMLEVBQVc0QyxVQUFYLEdBQXlCLFlBQVk7QUFDbkMzSCxNQUFFNEMsRUFBRixDQUFLbUMsSUFBTCxJQUFhSyxrQkFBYjtBQUNBLFdBQU9nVixVQUFVbFQsZ0JBQWpCO0FBQ0QsR0FIRDs7QUFLQSxTQUFPa1QsU0FBUDtBQUVELENBbFVpQixDQWtVZnZWLE1BbFVlLENBQWxCOzs7OztBQ1BBOzs7Ozs7O0FBT0EsSUFBTThZLE1BQU8sVUFBQzNkLENBQUQsRUFBTzs7QUFHbEI7Ozs7OztBQU1BLE1BQU0rRSxPQUFzQixLQUE1QjtBQUNBLE1BQU1DLFVBQXNCLFlBQTVCO0FBQ0EsTUFBTUMsV0FBc0IsUUFBNUI7QUFDQSxNQUFNQyxrQkFBMEJELFFBQWhDO0FBQ0EsTUFBTUUsZUFBc0IsV0FBNUI7QUFDQSxNQUFNQyxxQkFBc0JwRixFQUFFNEMsRUFBRixDQUFLbUMsSUFBTCxDQUE1QjtBQUNBLE1BQU1NLHNCQUFzQixHQUE1Qjs7QUFFQSxNQUFNRyxRQUFRO0FBQ1owSixtQkFBd0JoSyxTQURaO0FBRVppSyx1QkFBMEJqSyxTQUZkO0FBR1phLG1CQUF3QmIsU0FIWjtBQUlaK0oscUJBQXlCL0osU0FKYjtBQUtaUyw4QkFBeUJULFNBQXpCLEdBQXFDQztBQUx6QixHQUFkOztBQVFBLE1BQU1TLFlBQVk7QUFDaEI2VSxtQkFBZ0IsZUFEQTtBQUVoQjVTLFlBQWdCLFFBRkE7QUFHaEI0SyxjQUFnQixVQUhBO0FBSWhCM00sVUFBZ0IsTUFKQTtBQUtoQkMsVUFBZ0I7QUFMQSxHQUFsQjs7QUFRQSxNQUFNVCxXQUFXO0FBQ2Z3VixjQUF3QixXQURUO0FBRWZILG9CQUF3QixtQkFGVDtBQUdmOVMsWUFBd0IsU0FIVDtBQUlmSSxpQkFBd0IsaUVBSlQ7QUFLZitTLHFCQUF3QixrQkFMVDtBQU1mNEMsMkJBQXdCOztBQUkxQjs7Ozs7O0FBVmlCLEdBQWpCO0FBakNrQixNQWlEWkQsR0FqRFk7QUFtRGhCLGlCQUFZcmEsT0FBWixFQUFxQjtBQUFBOztBQUNuQixXQUFLMEMsUUFBTCxHQUFnQjFDLE9BQWhCO0FBQ0Q7O0FBR0Q7O0FBT0E7O0FBL0RnQixrQkFpRWhCaU4sSUFqRWdCLG1CQWlFVDtBQUFBOztBQUNMLFVBQUksS0FBS3ZLLFFBQUwsQ0FBY3FQLFVBQWQsSUFDQSxLQUFLclAsUUFBTCxDQUFjcVAsVUFBZCxDQUF5QnZVLFFBQXpCLEtBQXNDNlcsS0FBS0MsWUFEM0MsSUFFQTVYLEVBQUUsS0FBS2dHLFFBQVAsRUFBaUJjLFFBQWpCLENBQTBCbEIsVUFBVWlDLE1BQXBDLENBRkEsSUFHQTdILEVBQUUsS0FBS2dHLFFBQVAsRUFBaUJjLFFBQWpCLENBQTBCbEIsVUFBVTZNLFFBQXBDLENBSEosRUFHbUQ7QUFDakQ7QUFDRDs7QUFFRCxVQUFJcFIsZUFBSjtBQUNBLFVBQUl3YyxpQkFBSjtBQUNBLFVBQU1DLGNBQWM5ZCxFQUFFLEtBQUtnRyxRQUFQLEVBQWlCVyxPQUFqQixDQUF5QnJCLFNBQVNxVixjQUFsQyxFQUFrRCxDQUFsRCxDQUFwQjtBQUNBLFVBQU1wWCxXQUFjeEQsS0FBS3NELHNCQUFMLENBQTRCLEtBQUsyQyxRQUFqQyxDQUFwQjs7QUFFQSxVQUFJOFgsV0FBSixFQUFpQjtBQUNmRCxtQkFBVzdkLEVBQUVpTixTQUFGLENBQVlqTixFQUFFOGQsV0FBRixFQUFldFYsSUFBZixDQUFvQmxELFNBQVN1QyxNQUE3QixDQUFaLENBQVg7QUFDQWdXLG1CQUFXQSxTQUFTQSxTQUFTbmEsTUFBVCxHQUFrQixDQUEzQixDQUFYO0FBQ0Q7O0FBRUQsVUFBTTBSLFlBQVlwVixFQUFFd0YsS0FBRixDQUFRQSxNQUFNMEosSUFBZCxFQUFvQjtBQUNwQ3ZCLHVCQUFlLEtBQUszSDtBQURnQixPQUFwQixDQUFsQjs7QUFJQSxVQUFNa08sWUFBWWxVLEVBQUV3RixLQUFGLENBQVFBLE1BQU1PLElBQWQsRUFBb0I7QUFDcEM0SCx1QkFBZWtRO0FBRHFCLE9BQXBCLENBQWxCOztBQUlBLFVBQUlBLFFBQUosRUFBYztBQUNaN2QsVUFBRTZkLFFBQUYsRUFBWS9aLE9BQVosQ0FBb0JzUixTQUFwQjtBQUNEOztBQUVEcFYsUUFBRSxLQUFLZ0csUUFBUCxFQUFpQmxDLE9BQWpCLENBQXlCb1EsU0FBekI7O0FBRUEsVUFBSUEsVUFBVTVOLGtCQUFWLE1BQ0Q4TyxVQUFVOU8sa0JBQVYsRUFESCxFQUNtQztBQUNqQztBQUNEOztBQUVELFVBQUkvQyxRQUFKLEVBQWM7QUFDWmxDLGlCQUFTckIsRUFBRXVELFFBQUYsRUFBWSxDQUFaLENBQVQ7QUFDRDs7QUFFRCxXQUFLMFosU0FBTCxDQUNFLEtBQUtqWCxRQURQLEVBRUU4WCxXQUZGOztBQUtBLFVBQU0vTSxXQUFXLFNBQVhBLFFBQVcsR0FBTTtBQUNyQixZQUFNZ04sY0FBYy9kLEVBQUV3RixLQUFGLENBQVFBLE1BQU0ySixNQUFkLEVBQXNCO0FBQ3hDeEIseUJBQWUsTUFBSzNIO0FBRG9CLFNBQXRCLENBQXBCOztBQUlBLFlBQU1rUyxhQUFhbFksRUFBRXdGLEtBQUYsQ0FBUUEsTUFBTXlKLEtBQWQsRUFBcUI7QUFDdEN0Qix5QkFBZWtRO0FBRHVCLFNBQXJCLENBQW5COztBQUlBN2QsVUFBRTZkLFFBQUYsRUFBWS9aLE9BQVosQ0FBb0JpYSxXQUFwQjtBQUNBL2QsVUFBRSxNQUFLZ0csUUFBUCxFQUFpQmxDLE9BQWpCLENBQXlCb1UsVUFBekI7QUFDRCxPQVhEOztBQWFBLFVBQUk3VyxNQUFKLEVBQVk7QUFDVixhQUFLNGIsU0FBTCxDQUFlNWIsTUFBZixFQUF1QkEsT0FBT2dVLFVBQTlCLEVBQTBDdEUsUUFBMUM7QUFDRCxPQUZELE1BRU87QUFDTEE7QUFDRDtBQUNGLEtBakllOztBQUFBLGtCQW1JaEJ2SyxPQW5JZ0Isc0JBbUlOO0FBQ1J4RyxRQUFFeUcsVUFBRixDQUFhLEtBQUtULFFBQWxCLEVBQTRCZixRQUE1QjtBQUNBLFdBQUtlLFFBQUwsR0FBZ0IsSUFBaEI7QUFDRCxLQXRJZTs7QUF5SWhCOztBQXpJZ0Isa0JBMkloQmlYLFNBM0lnQixzQkEySU4zWixPQTNJTSxFQTJJRzBhLFNBM0lILEVBMkljeEYsUUEzSWQsRUEySXdCO0FBQUE7O0FBQ3RDLFVBQU15RixTQUFrQmplLEVBQUVnZSxTQUFGLEVBQWF4VixJQUFiLENBQWtCbEQsU0FBU3VDLE1BQTNCLEVBQW1DLENBQW5DLENBQXhCO0FBQ0EsVUFBTXdKLGtCQUFrQm1ILFlBQ25CelksS0FBSytDLHFCQUFMLEVBRG1CLElBRWxCbWIsVUFBVWplLEVBQUVpZSxNQUFGLEVBQVVuWCxRQUFWLENBQW1CbEIsVUFBVUUsSUFBN0IsQ0FGaEI7O0FBSUEsVUFBTWlMLFdBQVcsU0FBWEEsUUFBVztBQUFBLGVBQU0sT0FBS21OLG1CQUFMLENBQ3JCNWEsT0FEcUIsRUFFckIyYSxNQUZxQixFQUdyQjVNLGVBSHFCLEVBSXJCbUgsUUFKcUIsQ0FBTjtBQUFBLE9BQWpCOztBQU9BLFVBQUl5RixVQUFVNU0sZUFBZCxFQUErQjtBQUM3QnJSLFVBQUVpZSxNQUFGLEVBQ0cxYixHQURILENBQ094QyxLQUFLeUMsY0FEWixFQUM0QnVPLFFBRDVCLEVBRUdsTyxvQkFGSCxDQUV3QndDLG1CQUZ4QjtBQUlELE9BTEQsTUFLTztBQUNMMEw7QUFDRDs7QUFFRCxVQUFJa04sTUFBSixFQUFZO0FBQ1ZqZSxVQUFFaWUsTUFBRixFQUFVcFgsV0FBVixDQUFzQmpCLFVBQVVHLElBQWhDO0FBQ0Q7QUFDRixLQXBLZTs7QUFBQSxrQkFzS2hCbVksbUJBdEtnQixnQ0FzS0k1YSxPQXRLSixFQXNLYTJhLE1BdEtiLEVBc0txQjVNLGVBdEtyQixFQXNLc0NtSCxRQXRLdEMsRUFzS2dEO0FBQzlELFVBQUl5RixNQUFKLEVBQVk7QUFDVmplLFVBQUVpZSxNQUFGLEVBQVVwWCxXQUFWLENBQXNCakIsVUFBVWlDLE1BQWhDOztBQUVBLFlBQU1zVyxnQkFBZ0JuZSxFQUFFaWUsT0FBTzVJLFVBQVQsRUFBcUI3TSxJQUFyQixDQUNwQmxELFNBQVNzWSxxQkFEVyxFQUVwQixDQUZvQixDQUF0Qjs7QUFJQSxZQUFJTyxhQUFKLEVBQW1CO0FBQ2pCbmUsWUFBRW1lLGFBQUYsRUFBaUJ0WCxXQUFqQixDQUE2QmpCLFVBQVVpQyxNQUF2QztBQUNEOztBQUVEb1csZUFBT2pWLFlBQVAsQ0FBb0IsZUFBcEIsRUFBcUMsS0FBckM7QUFDRDs7QUFFRGhKLFFBQUVzRCxPQUFGLEVBQVc4SyxRQUFYLENBQW9CeEksVUFBVWlDLE1BQTlCO0FBQ0F2RSxjQUFRMEYsWUFBUixDQUFxQixlQUFyQixFQUFzQyxJQUF0Qzs7QUFFQSxVQUFJcUksZUFBSixFQUFxQjtBQUNuQnRSLGFBQUs2RCxNQUFMLENBQVlOLE9BQVo7QUFDQXRELFVBQUVzRCxPQUFGLEVBQVc4SyxRQUFYLENBQW9CeEksVUFBVUcsSUFBOUI7QUFDRCxPQUhELE1BR087QUFDTC9GLFVBQUVzRCxPQUFGLEVBQVd1RCxXQUFYLENBQXVCakIsVUFBVUUsSUFBakM7QUFDRDs7QUFFRCxVQUFJeEMsUUFBUStSLFVBQVIsSUFDQXJWLEVBQUVzRCxRQUFRK1IsVUFBVixFQUFzQnZPLFFBQXRCLENBQStCbEIsVUFBVTZVLGFBQXpDLENBREosRUFDNkQ7O0FBRTNELFlBQU0yRCxrQkFBa0JwZSxFQUFFc0QsT0FBRixFQUFXcUQsT0FBWCxDQUFtQnJCLFNBQVN3VixRQUE1QixFQUFzQyxDQUF0QyxDQUF4QjtBQUNBLFlBQUlzRCxlQUFKLEVBQXFCO0FBQ25CcGUsWUFBRW9lLGVBQUYsRUFBbUI1VixJQUFuQixDQUF3QmxELFNBQVMwVixlQUFqQyxFQUFrRDVNLFFBQWxELENBQTJEeEksVUFBVWlDLE1BQXJFO0FBQ0Q7O0FBRUR2RSxnQkFBUTBGLFlBQVIsQ0FBcUIsZUFBckIsRUFBc0MsSUFBdEM7QUFDRDs7QUFFRCxVQUFJd1AsUUFBSixFQUFjO0FBQ1pBO0FBQ0Q7QUFDRixLQTdNZTs7QUFnTmhCOztBQWhOZ0IsUUFrTlR0UixnQkFsTlMsNkJBa05RaEQsTUFsTlIsRUFrTmdCO0FBQzlCLGFBQU8sS0FBS2lELElBQUwsQ0FBVSxZQUFZO0FBQzNCLFlBQU11SyxRQUFRMVIsRUFBRSxJQUFGLENBQWQ7QUFDQSxZQUFJcUgsT0FBVXFLLE1BQU1ySyxJQUFOLENBQVdwQyxRQUFYLENBQWQ7O0FBRUEsWUFBSSxDQUFDb0MsSUFBTCxFQUFXO0FBQ1RBLGlCQUFPLElBQUlzVyxHQUFKLENBQVEsSUFBUixDQUFQO0FBQ0FqTSxnQkFBTXJLLElBQU4sQ0FBV3BDLFFBQVgsRUFBcUJvQyxJQUFyQjtBQUNEOztBQUVELFlBQUksT0FBT25ELE1BQVAsS0FBa0IsUUFBdEIsRUFBZ0M7QUFDOUIsY0FBSW1ELEtBQUtuRCxNQUFMLE1BQWlCdkMsU0FBckIsRUFBZ0M7QUFDOUIsa0JBQU0sSUFBSWdELEtBQUosdUJBQThCVCxNQUE5QixPQUFOO0FBQ0Q7QUFDRG1ELGVBQUtuRCxNQUFMO0FBQ0Q7QUFDRixPQWZNLENBQVA7QUFnQkQsS0FuT2U7O0FBQUE7QUFBQTtBQUFBLDBCQTBESztBQUNuQixlQUFPYyxPQUFQO0FBQ0Q7QUE1RGU7O0FBQUE7QUFBQTs7QUF3T2xCOzs7Ozs7QUFNQWhGLElBQUVnQyxRQUFGLEVBQ0d5RixFQURILENBQ01qQyxNQUFNRyxjQURaLEVBQzRCTCxTQUFTMkMsV0FEckMsRUFDa0QsVUFBVTdHLEtBQVYsRUFBaUI7QUFDL0RBLFVBQU1vRyxjQUFOO0FBQ0FtVyxRQUFJelcsZ0JBQUosQ0FBcUJ4RyxJQUFyQixDQUEwQlYsRUFBRSxJQUFGLENBQTFCLEVBQW1DLE1BQW5DO0FBQ0QsR0FKSDs7QUFPQTs7Ozs7O0FBTUFBLElBQUU0QyxFQUFGLENBQUttQyxJQUFMLElBQXlCNFksSUFBSXpXLGdCQUE3QjtBQUNBbEgsSUFBRTRDLEVBQUYsQ0FBS21DLElBQUwsRUFBVzJDLFdBQVgsR0FBeUJpVyxHQUF6QjtBQUNBM2QsSUFBRTRDLEVBQUYsQ0FBS21DLElBQUwsRUFBVzRDLFVBQVgsR0FBeUIsWUFBWTtBQUNuQzNILE1BQUU0QyxFQUFGLENBQUttQyxJQUFMLElBQWFLLGtCQUFiO0FBQ0EsV0FBT3VZLElBQUl6VyxnQkFBWDtBQUNELEdBSEQ7O0FBS0EsU0FBT3lXLEdBQVA7QUFFRCxDQXBRVyxDQW9RVDlZLE1BcFFTLENBQVo7Ozs7Ozs7QUNMQTs7Ozs7OztBQU9BLElBQU13WixVQUFXLFVBQUNyZSxDQUFELEVBQU87O0FBRXRCOzs7O0FBSUEsTUFBSSxPQUFPOFIsTUFBUCxLQUFrQixXQUF0QixFQUFtQztBQUNqQyxVQUFNLElBQUluTixLQUFKLENBQVUsOERBQVYsQ0FBTjtBQUNEOztBQUdEOzs7Ozs7QUFNQSxNQUFNSSxPQUFzQixTQUE1QjtBQUNBLE1BQU1DLFVBQXNCLFlBQTVCO0FBQ0EsTUFBTUMsV0FBc0IsWUFBNUI7QUFDQSxNQUFNQyxrQkFBMEJELFFBQWhDO0FBQ0EsTUFBTUcscUJBQXNCcEYsRUFBRTRDLEVBQUYsQ0FBS21DLElBQUwsQ0FBNUI7QUFDQSxNQUFNTSxzQkFBc0IsR0FBNUI7QUFDQSxNQUFNaVosZUFBc0IsWUFBNUI7QUFDQSxNQUFNQyxxQkFBcUIsSUFBSTlaLE1BQUosYUFBcUI2WixZQUFyQixXQUF5QyxHQUF6QyxDQUEzQjs7QUFFQSxNQUFNelUsY0FBYztBQUNsQjJVLGVBQXNCLFNBREo7QUFFbEJDLGNBQXNCLFFBRko7QUFHbEJDLFdBQXNCLDJCQUhKO0FBSWxCNWEsYUFBc0IsUUFKSjtBQUtsQjZhLFdBQXNCLGlCQUxKO0FBTWxCQyxVQUFzQixTQU5KO0FBT2xCcmIsY0FBc0Isa0JBUEo7QUFRbEIrUCxlQUFzQixtQkFSSjtBQVNsQkMsWUFBc0IsaUJBVEo7QUFVbEJ5SyxlQUFzQiwwQkFWSjtBQVdsQmEsdUJBQXNCO0FBWEosR0FBcEI7O0FBY0EsTUFBTTVMLGdCQUFnQjtBQUNwQjZMLFVBQVMsTUFEVztBQUVwQjVMLFNBQVMsS0FGVztBQUdwQmhKLFdBQVMsT0FIVztBQUlwQmtKLFlBQVMsUUFKVztBQUtwQm5KLFVBQVM7QUFMVyxHQUF0Qjs7QUFRQSxNQUFNVixVQUFVO0FBQ2RpVixlQUFzQixJQURSO0FBRWRDLGNBQXNCLHlDQUNBLDJCQURBLEdBRUEseUNBSlI7QUFLZDNhLGFBQXNCLGFBTFI7QUFNZDRhLFdBQXNCLEVBTlI7QUFPZEMsV0FBc0IsQ0FQUjtBQVFkQyxVQUFzQixLQVJSO0FBU2RyYixjQUFzQixLQVRSO0FBVWQrUCxlQUFzQixLQVZSO0FBV2RDLFlBQXNCLENBWFI7QUFZZHlLLGVBQXNCLEtBWlI7QUFhZGEsdUJBQXNCO0FBYlIsR0FBaEI7O0FBZ0JBLE1BQU1FLGFBQWE7QUFDakJoWixVQUFPLE1BRFU7QUFFakJpWixTQUFPO0FBRlUsR0FBbkI7O0FBS0EsTUFBTXhaLFFBQVE7QUFDWjBKLG1CQUFvQmhLLFNBRFI7QUFFWmlLLHVCQUFzQmpLLFNBRlY7QUFHWmEsbUJBQW9CYixTQUhSO0FBSVorSixxQkFBcUIvSixTQUpUO0FBS1orWiwyQkFBd0IvWixTQUxaO0FBTVpvTixxQkFBcUJwTixTQU5UO0FBT1oyUSx5QkFBdUIzUSxTQVBYO0FBUVpnYSwyQkFBd0JoYSxTQVJaO0FBU1pvRiwrQkFBMEJwRixTQVRkO0FBVVpxRiwrQkFBMEJyRjtBQVZkLEdBQWQ7O0FBYUEsTUFBTVUsWUFBWTtBQUNoQkUsVUFBTyxNQURTO0FBRWhCQyxVQUFPO0FBRlMsR0FBbEI7O0FBS0EsTUFBTVQsV0FBVztBQUNmNlosYUFBZ0IsVUFERDtBQUVmQyxtQkFBZ0IsZ0JBRkQ7QUFHZkMsV0FBZ0I7QUFIRCxHQUFqQjs7QUFNQSxNQUFNQyxVQUFVO0FBQ2RDLFdBQVMsT0FESztBQUVkeFgsV0FBUyxPQUZLO0FBR2R1SyxXQUFTLE9BSEs7QUFJZGtOLFlBQVM7O0FBSVg7Ozs7OztBQVJnQixHQUFoQjtBQTdGc0IsTUEyR2hCbkIsT0EzR2dCO0FBNkdwQixxQkFBWS9hLE9BQVosRUFBcUJZLE1BQXJCLEVBQTZCO0FBQUE7O0FBRTNCO0FBQ0EsV0FBS3ViLFVBQUwsR0FBc0IsSUFBdEI7QUFDQSxXQUFLQyxRQUFMLEdBQXNCLENBQXRCO0FBQ0EsV0FBS0MsV0FBTCxHQUFzQixFQUF0QjtBQUNBLFdBQUtDLGNBQUwsR0FBc0IsRUFBdEI7QUFDQSxXQUFLbk0sT0FBTCxHQUFzQixJQUF0Qjs7QUFFQTtBQUNBLFdBQUtuUSxPQUFMLEdBQWVBLE9BQWY7QUFDQSxXQUFLWSxNQUFMLEdBQWUsS0FBS3NILFVBQUwsQ0FBZ0J0SCxNQUFoQixDQUFmO0FBQ0EsV0FBSzJiLEdBQUwsR0FBZSxJQUFmOztBQUVBLFdBQUtDLGFBQUw7QUFFRDs7QUFHRDs7QUErQkE7O0FBL0pvQixzQkFpS3BCQyxNQWpLb0IscUJBaUtYO0FBQ1AsV0FBS04sVUFBTCxHQUFrQixJQUFsQjtBQUNELEtBbkttQjs7QUFBQSxzQkFxS3BCTyxPQXJLb0Isc0JBcUtWO0FBQ1IsV0FBS1AsVUFBTCxHQUFrQixLQUFsQjtBQUNELEtBdkttQjs7QUFBQSxzQkF5S3BCUSxhQXpLb0IsNEJBeUtKO0FBQ2QsV0FBS1IsVUFBTCxHQUFrQixDQUFDLEtBQUtBLFVBQXhCO0FBQ0QsS0EzS21COztBQUFBLHNCQTZLcEJyWCxNQTdLb0IsbUJBNktiaEgsS0E3S2EsRUE2S047QUFDWixVQUFJQSxLQUFKLEVBQVc7QUFDVCxZQUFNOGUsVUFBVSxLQUFLeEwsV0FBTCxDQUFpQnpQLFFBQWpDO0FBQ0EsWUFBSWlRLFVBQVVsVixFQUFFb0IsTUFBTXlYLGFBQVIsRUFBdUJ4UixJQUF2QixDQUE0QjZZLE9BQTVCLENBQWQ7O0FBRUEsWUFBSSxDQUFDaEwsT0FBTCxFQUFjO0FBQ1pBLG9CQUFVLElBQUksS0FBS1IsV0FBVCxDQUNSdFQsTUFBTXlYLGFBREUsRUFFUixLQUFLc0gsa0JBQUwsRUFGUSxDQUFWO0FBSUFuZ0IsWUFBRW9CLE1BQU15WCxhQUFSLEVBQXVCeFIsSUFBdkIsQ0FBNEI2WSxPQUE1QixFQUFxQ2hMLE9BQXJDO0FBQ0Q7O0FBRURBLGdCQUFRMEssY0FBUixDQUF1QlEsS0FBdkIsR0FBK0IsQ0FBQ2xMLFFBQVEwSyxjQUFSLENBQXVCUSxLQUF2RDs7QUFFQSxZQUFJbEwsUUFBUW1MLG9CQUFSLEVBQUosRUFBb0M7QUFDbENuTCxrQkFBUW9MLE1BQVIsQ0FBZSxJQUFmLEVBQXFCcEwsT0FBckI7QUFDRCxTQUZELE1BRU87QUFDTEEsa0JBQVFxTCxNQUFSLENBQWUsSUFBZixFQUFxQnJMLE9BQXJCO0FBQ0Q7QUFFRixPQXBCRCxNQW9CTzs7QUFFTCxZQUFJbFYsRUFBRSxLQUFLd2dCLGFBQUwsRUFBRixFQUF3QjFaLFFBQXhCLENBQWlDbEIsVUFBVUcsSUFBM0MsQ0FBSixFQUFzRDtBQUNwRCxlQUFLd2EsTUFBTCxDQUFZLElBQVosRUFBa0IsSUFBbEI7QUFDQTtBQUNEOztBQUVELGFBQUtELE1BQUwsQ0FBWSxJQUFaLEVBQWtCLElBQWxCO0FBQ0Q7QUFDRixLQTNNbUI7O0FBQUEsc0JBNk1wQjlaLE9BN01vQixzQkE2TVY7QUFDUnNHLG1CQUFhLEtBQUs0UyxRQUFsQjs7QUFFQTFmLFFBQUV5RyxVQUFGLENBQWEsS0FBS25ELE9BQWxCLEVBQTJCLEtBQUtvUixXQUFMLENBQWlCelAsUUFBNUM7O0FBRUFqRixRQUFFLEtBQUtzRCxPQUFQLEVBQWdCb0osR0FBaEIsQ0FBb0IsS0FBS2dJLFdBQUwsQ0FBaUJ4UCxTQUFyQztBQUNBbEYsUUFBRSxLQUFLc0QsT0FBUCxFQUFnQnFELE9BQWhCLENBQXdCLFFBQXhCLEVBQWtDK0YsR0FBbEMsQ0FBc0MsZUFBdEM7O0FBRUEsVUFBSSxLQUFLbVQsR0FBVCxFQUFjO0FBQ1o3ZixVQUFFLEtBQUs2ZixHQUFQLEVBQVk1WSxNQUFaO0FBQ0Q7O0FBRUQsV0FBS3dZLFVBQUwsR0FBc0IsSUFBdEI7QUFDQSxXQUFLQyxRQUFMLEdBQXNCLElBQXRCO0FBQ0EsV0FBS0MsV0FBTCxHQUFzQixJQUF0QjtBQUNBLFdBQUtDLGNBQUwsR0FBc0IsSUFBdEI7QUFDQSxVQUFJLEtBQUtuTSxPQUFMLEtBQWlCLElBQXJCLEVBQTJCO0FBQ3pCLGFBQUtBLE9BQUwsQ0FBYVksT0FBYjtBQUNEO0FBQ0QsV0FBS1osT0FBTCxHQUFzQixJQUF0Qjs7QUFFQSxXQUFLblEsT0FBTCxHQUFlLElBQWY7QUFDQSxXQUFLWSxNQUFMLEdBQWUsSUFBZjtBQUNBLFdBQUsyYixHQUFMLEdBQWUsSUFBZjtBQUNELEtBck9tQjs7QUFBQSxzQkF1T3BCdFAsSUF2T29CLG1CQXVPYjtBQUFBOztBQUNMLFVBQUl2USxFQUFFLEtBQUtzRCxPQUFQLEVBQWdCbVcsR0FBaEIsQ0FBb0IsU0FBcEIsTUFBbUMsTUFBdkMsRUFBK0M7QUFDN0MsY0FBTSxJQUFJOVUsS0FBSixDQUFVLHFDQUFWLENBQU47QUFDRDs7QUFFRCxVQUFNdVAsWUFBWWxVLEVBQUV3RixLQUFGLENBQVEsS0FBS2tQLFdBQUwsQ0FBaUJsUCxLQUFqQixDQUF1Qk8sSUFBL0IsQ0FBbEI7QUFDQSxVQUFJLEtBQUswYSxhQUFMLE1BQXdCLEtBQUtoQixVQUFqQyxFQUE2QztBQUMzQ3pmLFVBQUUsS0FBS3NELE9BQVAsRUFBZ0JRLE9BQWhCLENBQXdCb1EsU0FBeEI7O0FBRUEsWUFBTXdNLGFBQWExZ0IsRUFBRThJLFFBQUYsQ0FDakIsS0FBS3hGLE9BQUwsQ0FBYXFkLGFBQWIsQ0FBMkI5VCxlQURWLEVBRWpCLEtBQUt2SixPQUZZLENBQW5COztBQUtBLFlBQUk0USxVQUFVNU4sa0JBQVYsTUFBa0MsQ0FBQ29hLFVBQXZDLEVBQW1EO0FBQ2pEO0FBQ0Q7O0FBRUQsWUFBTWIsTUFBUSxLQUFLVyxhQUFMLEVBQWQ7QUFDQSxZQUFNSSxRQUFRN2dCLEtBQUtpRCxNQUFMLENBQVksS0FBSzBSLFdBQUwsQ0FBaUIzUCxJQUE3QixDQUFkOztBQUVBOGEsWUFBSTdXLFlBQUosQ0FBaUIsSUFBakIsRUFBdUI0WCxLQUF2QjtBQUNBLGFBQUt0ZCxPQUFMLENBQWEwRixZQUFiLENBQTBCLGtCQUExQixFQUE4QzRYLEtBQTlDOztBQUVBLGFBQUtDLFVBQUw7O0FBRUEsWUFBSSxLQUFLM2MsTUFBTCxDQUFZc2EsU0FBaEIsRUFBMkI7QUFDekJ4ZSxZQUFFNmYsR0FBRixFQUFPelIsUUFBUCxDQUFnQnhJLFVBQVVFLElBQTFCO0FBQ0Q7O0FBRUQsWUFBTXdOLFlBQWEsT0FBTyxLQUFLcFAsTUFBTCxDQUFZb1AsU0FBbkIsS0FBaUMsVUFBakMsR0FDakIsS0FBS3BQLE1BQUwsQ0FBWW9QLFNBQVosQ0FBc0I1UyxJQUF0QixDQUEyQixJQUEzQixFQUFpQ21mLEdBQWpDLEVBQXNDLEtBQUt2YyxPQUEzQyxDQURpQixHQUVqQixLQUFLWSxNQUFMLENBQVlvUCxTQUZkOztBQUlBLFlBQU13TixhQUFhLEtBQUtDLGNBQUwsQ0FBb0J6TixTQUFwQixDQUFuQjtBQUNBLGFBQUswTixrQkFBTCxDQUF3QkYsVUFBeEI7O0FBRUEsWUFBTTlDLFlBQVksS0FBSzlaLE1BQUwsQ0FBWThaLFNBQVosS0FBMEIsS0FBMUIsR0FBa0NoYyxTQUFTbVYsSUFBM0MsR0FBa0RuWCxFQUFFLEtBQUtrRSxNQUFMLENBQVk4WixTQUFkLENBQXBFOztBQUVBaGUsVUFBRTZmLEdBQUYsRUFBT3hZLElBQVAsQ0FBWSxLQUFLcU4sV0FBTCxDQUFpQnpQLFFBQTdCLEVBQXVDLElBQXZDOztBQUVBLFlBQUksQ0FBQ2pGLEVBQUU4SSxRQUFGLENBQVcsS0FBS3hGLE9BQUwsQ0FBYXFkLGFBQWIsQ0FBMkI5VCxlQUF0QyxFQUF1RCxLQUFLZ1QsR0FBNUQsQ0FBTCxFQUF1RTtBQUNyRTdmLFlBQUU2ZixHQUFGLEVBQU9qSCxRQUFQLENBQWdCb0YsU0FBaEI7QUFDRDs7QUFFRGhlLFVBQUUsS0FBS3NELE9BQVAsRUFBZ0JRLE9BQWhCLENBQXdCLEtBQUs0USxXQUFMLENBQWlCbFAsS0FBakIsQ0FBdUJ5WixRQUEvQzs7QUFFQSxhQUFLeEwsT0FBTCxHQUFlLElBQUkzQixNQUFKLENBQVcsS0FBS3hPLE9BQWhCLEVBQXlCdWMsR0FBekIsRUFBOEI7QUFDM0N2TSxxQkFBV3dOLFVBRGdDO0FBRTNDaE0scUJBQVc7QUFDVHZCLG9CQUFRO0FBQ05BLHNCQUFRLEtBQUtyUCxNQUFMLENBQVlxUDtBQURkLGFBREM7QUFJVEMsa0JBQU07QUFDSnlOLHdCQUFVLEtBQUsvYyxNQUFMLENBQVkyYTtBQURsQixhQUpHO0FBT1RxQyxtQkFBTztBQUNMNWQsdUJBQVNnQyxTQUFTK1o7QUFEYjtBQVBFLFdBRmdDO0FBYTNDOEIsb0JBQVUsa0JBQUM5WixJQUFELEVBQVU7QUFDbEIsZ0JBQUlBLEtBQUsrWixpQkFBTCxLQUEyQi9aLEtBQUtpTSxTQUFwQyxFQUErQztBQUM3QyxvQkFBSytOLDRCQUFMLENBQWtDaGEsSUFBbEM7QUFDRDtBQUNGLFdBakIwQztBQWtCM0NpYSxvQkFBVyxrQkFBQ2phLElBQUQsRUFBVTtBQUNuQixrQkFBS2dhLDRCQUFMLENBQWtDaGEsSUFBbEM7QUFDRDtBQXBCMEMsU0FBOUIsQ0FBZjs7QUF1QkFySCxVQUFFNmYsR0FBRixFQUFPelIsUUFBUCxDQUFnQnhJLFVBQVVHLElBQTFCOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsWUFBSSxrQkFBa0IvRCxTQUFTNkssZUFBL0IsRUFBZ0Q7QUFDOUM3TSxZQUFFLE1BQUYsRUFBVW1PLFFBQVYsR0FBcUIxRyxFQUFyQixDQUF3QixXQUF4QixFQUFxQyxJQUFyQyxFQUEyQ3pILEVBQUVvVSxJQUE3QztBQUNEOztBQUVELFlBQU1yRCxXQUFXLFNBQVhBLFFBQVcsR0FBTTtBQUNyQixjQUFJLE1BQUs3TSxNQUFMLENBQVlzYSxTQUFoQixFQUEyQjtBQUN6QixrQkFBSytDLGNBQUw7QUFDRDtBQUNELGNBQU1DLGlCQUFpQixNQUFLN0IsV0FBNUI7QUFDQSxnQkFBS0EsV0FBTCxHQUF1QixJQUF2Qjs7QUFFQTNmLFlBQUUsTUFBS3NELE9BQVAsRUFBZ0JRLE9BQWhCLENBQXdCLE1BQUs0USxXQUFMLENBQWlCbFAsS0FBakIsQ0FBdUJ5SixLQUEvQzs7QUFFQSxjQUFJdVMsbUJBQW1CekMsV0FBV0MsR0FBbEMsRUFBdUM7QUFDckMsa0JBQUt1QixNQUFMLENBQVksSUFBWjtBQUNEO0FBQ0YsU0FaRDs7QUFjQSxZQUFJeGdCLEtBQUsrQyxxQkFBTCxNQUFnQzlDLEVBQUUsS0FBSzZmLEdBQVAsRUFBWS9ZLFFBQVosQ0FBcUJsQixVQUFVRSxJQUEvQixDQUFwQyxFQUEwRTtBQUN4RTlGLFlBQUUsS0FBSzZmLEdBQVAsRUFDR3RkLEdBREgsQ0FDT3hDLEtBQUt5QyxjQURaLEVBQzRCdU8sUUFENUIsRUFFR2xPLG9CQUZILENBRXdCd2IsUUFBUW9ELG9CQUZoQztBQUdELFNBSkQsTUFJTztBQUNMMVE7QUFDRDtBQUNGO0FBQ0YsS0E3VW1COztBQUFBLHNCQStVcEJULElBL1VvQixpQkErVWZrSSxRQS9VZSxFQStVTDtBQUFBOztBQUNiLFVBQU1xSCxNQUFZLEtBQUtXLGFBQUwsRUFBbEI7QUFDQSxVQUFNcEwsWUFBWXBWLEVBQUV3RixLQUFGLENBQVEsS0FBS2tQLFdBQUwsQ0FBaUJsUCxLQUFqQixDQUF1QjBKLElBQS9CLENBQWxCO0FBQ0EsVUFBTTZCLFdBQVksU0FBWkEsUUFBWSxHQUFNO0FBQ3RCLFlBQUksT0FBSzRPLFdBQUwsS0FBcUJaLFdBQVdoWixJQUFoQyxJQUF3QzhaLElBQUl4SyxVQUFoRCxFQUE0RDtBQUMxRHdLLGNBQUl4SyxVQUFKLENBQWU4RSxXQUFmLENBQTJCMEYsR0FBM0I7QUFDRDs7QUFFRCxlQUFLNkIsY0FBTDtBQUNBLGVBQUtwZSxPQUFMLENBQWF5VSxlQUFiLENBQTZCLGtCQUE3QjtBQUNBL1gsVUFBRSxPQUFLc0QsT0FBUCxFQUFnQlEsT0FBaEIsQ0FBd0IsT0FBSzRRLFdBQUwsQ0FBaUJsUCxLQUFqQixDQUF1QjJKLE1BQS9DO0FBQ0EsWUFBSSxPQUFLc0UsT0FBTCxLQUFpQixJQUFyQixFQUEyQjtBQUN6QixpQkFBS0EsT0FBTCxDQUFhWSxPQUFiO0FBQ0Q7O0FBRUQsWUFBSW1FLFFBQUosRUFBYztBQUNaQTtBQUNEO0FBQ0YsT0FmRDs7QUFpQkF4WSxRQUFFLEtBQUtzRCxPQUFQLEVBQWdCUSxPQUFoQixDQUF3QnNSLFNBQXhCOztBQUVBLFVBQUlBLFVBQVU5TyxrQkFBVixFQUFKLEVBQW9DO0FBQ2xDO0FBQ0Q7O0FBRUR0RyxRQUFFNmYsR0FBRixFQUFPaFosV0FBUCxDQUFtQmpCLFVBQVVHLElBQTdCOztBQUVBO0FBQ0E7QUFDQSxVQUFJLGtCQUFrQi9ELFNBQVM2SyxlQUEvQixFQUFnRDtBQUM5QzdNLFVBQUUsTUFBRixFQUFVbU8sUUFBVixHQUFxQnpCLEdBQXJCLENBQXlCLFdBQXpCLEVBQXNDLElBQXRDLEVBQTRDMU0sRUFBRW9VLElBQTlDO0FBQ0Q7O0FBRUQsV0FBS3dMLGNBQUwsQ0FBb0JOLFFBQVFoTixLQUE1QixJQUFxQyxLQUFyQztBQUNBLFdBQUtzTixjQUFMLENBQW9CTixRQUFRdlgsS0FBNUIsSUFBcUMsS0FBckM7QUFDQSxXQUFLNlgsY0FBTCxDQUFvQk4sUUFBUUMsS0FBNUIsSUFBcUMsS0FBckM7O0FBRUEsVUFBSXhmLEtBQUsrQyxxQkFBTCxNQUNBOUMsRUFBRSxLQUFLNmYsR0FBUCxFQUFZL1ksUUFBWixDQUFxQmxCLFVBQVVFLElBQS9CLENBREosRUFDMEM7O0FBRXhDOUYsVUFBRTZmLEdBQUYsRUFDR3RkLEdBREgsQ0FDT3hDLEtBQUt5QyxjQURaLEVBQzRCdU8sUUFENUIsRUFFR2xPLG9CQUZILENBRXdCd0MsbUJBRnhCO0FBSUQsT0FQRCxNQU9PO0FBQ0wwTDtBQUNEOztBQUVELFdBQUs0TyxXQUFMLEdBQW1CLEVBQW5CO0FBRUQsS0FsWW1COztBQUFBLHNCQW9ZcEJyTCxNQXBZb0IscUJBb1lYO0FBQ1AsVUFBSSxLQUFLYixPQUFMLEtBQWlCLElBQXJCLEVBQTJCO0FBQ3pCLGFBQUtBLE9BQUwsQ0FBYWMsY0FBYjtBQUNEO0FBQ0YsS0F4WW1COztBQTBZcEI7O0FBMVlvQixzQkE0WXBCa00sYUE1WW9CLDRCQTRZSjtBQUNkLGFBQU8xYyxRQUFRLEtBQUs0ZCxRQUFMLEVBQVIsQ0FBUDtBQUNELEtBOVltQjs7QUFBQSxzQkFnWnBCWCxrQkFoWm9CLCtCQWdaREYsVUFoWkMsRUFnWlc7QUFDN0I5Z0IsUUFBRSxLQUFLd2dCLGFBQUwsRUFBRixFQUF3QnBTLFFBQXhCLENBQW9Da1EsWUFBcEMsU0FBb0R3QyxVQUFwRDtBQUNELEtBbFptQjs7QUFBQSxzQkFvWnBCTixhQXBab0IsNEJBb1pKO0FBQ2QsYUFBTyxLQUFLWCxHQUFMLEdBQVcsS0FBS0EsR0FBTCxJQUFZN2YsRUFBRSxLQUFLa0UsTUFBTCxDQUFZdWEsUUFBZCxFQUF3QixDQUF4QixDQUE5QjtBQUNELEtBdFptQjs7QUFBQSxzQkF3WnBCb0MsVUF4Wm9CLHlCQXdaUDtBQUNYLFVBQU1lLE9BQU81aEIsRUFBRSxLQUFLd2dCLGFBQUwsRUFBRixDQUFiO0FBQ0EsV0FBS3FCLGlCQUFMLENBQXVCRCxLQUFLcFosSUFBTCxDQUFVbEQsU0FBUzhaLGFBQW5CLENBQXZCLEVBQTBELEtBQUt1QyxRQUFMLEVBQTFEO0FBQ0FDLFdBQUsvYSxXQUFMLENBQW9CakIsVUFBVUUsSUFBOUIsU0FBc0NGLFVBQVVHLElBQWhEO0FBQ0QsS0E1Wm1COztBQUFBLHNCQThacEI4YixpQkE5Wm9CLDhCQThaRnphLFFBOVpFLEVBOFpRMGEsT0E5WlIsRUE4WmlCO0FBQ25DLFVBQU1sRCxPQUFPLEtBQUsxYSxNQUFMLENBQVkwYSxJQUF6QjtBQUNBLFVBQUksUUFBT2tELE9BQVAseUNBQU9BLE9BQVAsT0FBbUIsUUFBbkIsS0FBZ0NBLFFBQVFoaEIsUUFBUixJQUFvQmdoQixRQUFRQyxNQUE1RCxDQUFKLEVBQXlFO0FBQ3ZFO0FBQ0EsWUFBSW5ELElBQUosRUFBVTtBQUNSLGNBQUksQ0FBQzVlLEVBQUU4aEIsT0FBRixFQUFXcGIsTUFBWCxHQUFvQnBGLEVBQXBCLENBQXVCOEYsUUFBdkIsQ0FBTCxFQUF1QztBQUNyQ0EscUJBQVM0YSxLQUFULEdBQWlCQyxNQUFqQixDQUF3QkgsT0FBeEI7QUFDRDtBQUNGLFNBSkQsTUFJTztBQUNMMWEsbUJBQVM4YSxJQUFULENBQWNsaUIsRUFBRThoQixPQUFGLEVBQVdJLElBQVgsRUFBZDtBQUNEO0FBQ0YsT0FURCxNQVNPO0FBQ0w5YSxpQkFBU3dYLE9BQU8sTUFBUCxHQUFnQixNQUF6QixFQUFpQ2tELE9BQWpDO0FBQ0Q7QUFDRixLQTVhbUI7O0FBQUEsc0JBOGFwQkgsUUE5YW9CLHVCQThhVDtBQUNULFVBQUlqRCxRQUFRLEtBQUtwYixPQUFMLENBQWFFLFlBQWIsQ0FBMEIscUJBQTFCLENBQVo7O0FBRUEsVUFBSSxDQUFDa2IsS0FBTCxFQUFZO0FBQ1ZBLGdCQUFRLE9BQU8sS0FBS3hhLE1BQUwsQ0FBWXdhLEtBQW5CLEtBQTZCLFVBQTdCLEdBQ04sS0FBS3hhLE1BQUwsQ0FBWXdhLEtBQVosQ0FBa0JoZSxJQUFsQixDQUF1QixLQUFLNEMsT0FBNUIsQ0FETSxHQUVOLEtBQUtZLE1BQUwsQ0FBWXdhLEtBRmQ7QUFHRDs7QUFFRCxhQUFPQSxLQUFQO0FBQ0QsS0F4Ym1COztBQTJicEI7O0FBM2JvQixzQkE2YnBCcUMsY0E3Ym9CLDJCQTZiTHpOLFNBN2JLLEVBNmJNO0FBQ3hCLGFBQU9MLGNBQWNLLFVBQVUxTyxXQUFWLEVBQWQsQ0FBUDtBQUNELEtBL2JtQjs7QUFBQSxzQkFpY3BCa2IsYUFqY29CLDRCQWljSjtBQUFBOztBQUNkLFVBQU1xQyxXQUFXLEtBQUtqZSxNQUFMLENBQVlKLE9BQVosQ0FBb0J1WixLQUFwQixDQUEwQixHQUExQixDQUFqQjs7QUFFQThFLGVBQVN4RixPQUFULENBQWlCLFVBQUM3WSxPQUFELEVBQWE7QUFDNUIsWUFBSUEsWUFBWSxPQUFoQixFQUF5QjtBQUN2QjlELFlBQUUsT0FBS3NELE9BQVAsRUFBZ0JtRSxFQUFoQixDQUNFLE9BQUtpTixXQUFMLENBQWlCbFAsS0FBakIsQ0FBdUI4TSxLQUR6QixFQUVFLE9BQUtwTyxNQUFMLENBQVlYLFFBRmQsRUFHRSxVQUFDbkMsS0FBRDtBQUFBLG1CQUFXLE9BQUtnSCxNQUFMLENBQVloSCxLQUFaLENBQVg7QUFBQSxXQUhGO0FBTUQsU0FQRCxNQU9PLElBQUkwQyxZQUFZd2IsUUFBUUUsTUFBeEIsRUFBZ0M7QUFDckMsY0FBTTRDLFVBQVd0ZSxZQUFZd2IsUUFBUUMsS0FBcEIsR0FDZixPQUFLN0ssV0FBTCxDQUFpQmxQLEtBQWpCLENBQXVCOEUsVUFEUixHQUVmLE9BQUtvSyxXQUFMLENBQWlCbFAsS0FBakIsQ0FBdUJxUSxPQUZ6QjtBQUdBLGNBQU13TSxXQUFXdmUsWUFBWXdiLFFBQVFDLEtBQXBCLEdBQ2YsT0FBSzdLLFdBQUwsQ0FBaUJsUCxLQUFqQixDQUF1QitFLFVBRFIsR0FFZixPQUFLbUssV0FBTCxDQUFpQmxQLEtBQWpCLENBQXVCMFosUUFGekI7O0FBSUFsZixZQUFFLE9BQUtzRCxPQUFQLEVBQ0dtRSxFQURILENBRUkyYSxPQUZKLEVBR0ksT0FBS2xlLE1BQUwsQ0FBWVgsUUFIaEIsRUFJSSxVQUFDbkMsS0FBRDtBQUFBLG1CQUFXLE9BQUtrZixNQUFMLENBQVlsZixLQUFaLENBQVg7QUFBQSxXQUpKLEVBTUdxRyxFQU5ILENBT0k0YSxRQVBKLEVBUUksT0FBS25lLE1BQUwsQ0FBWVgsUUFSaEIsRUFTSSxVQUFDbkMsS0FBRDtBQUFBLG1CQUFXLE9BQUttZixNQUFMLENBQVluZixLQUFaLENBQVg7QUFBQSxXQVRKO0FBV0Q7O0FBRURwQixVQUFFLE9BQUtzRCxPQUFQLEVBQWdCcUQsT0FBaEIsQ0FBd0IsUUFBeEIsRUFBa0NjLEVBQWxDLENBQ0UsZUFERixFQUVFO0FBQUEsaUJBQU0sT0FBSzZJLElBQUwsRUFBTjtBQUFBLFNBRkY7QUFJRCxPQWpDRDs7QUFtQ0EsVUFBSSxLQUFLcE0sTUFBTCxDQUFZWCxRQUFoQixFQUEwQjtBQUN4QixhQUFLVyxNQUFMLEdBQWNsRSxFQUFFMk0sTUFBRixDQUFTLEVBQVQsRUFBYSxLQUFLekksTUFBbEIsRUFBMEI7QUFDdENKLG1CQUFXLFFBRDJCO0FBRXRDUCxvQkFBVztBQUYyQixTQUExQixDQUFkO0FBSUQsT0FMRCxNQUtPO0FBQ0wsYUFBSytlLFNBQUw7QUFDRDtBQUNGLEtBL2VtQjs7QUFBQSxzQkFpZnBCQSxTQWpmb0Isd0JBaWZSO0FBQ1YsVUFBTUMsb0JBQW1CLEtBQUtqZixPQUFMLENBQWFFLFlBQWIsQ0FBMEIscUJBQTFCLENBQW5CLENBQU47QUFDQSxVQUFJLEtBQUtGLE9BQUwsQ0FBYUUsWUFBYixDQUEwQixPQUExQixLQUNEK2UsY0FBYyxRQURqQixFQUMyQjtBQUN6QixhQUFLamYsT0FBTCxDQUFhMEYsWUFBYixDQUNFLHFCQURGLEVBRUUsS0FBSzFGLE9BQUwsQ0FBYUUsWUFBYixDQUEwQixPQUExQixLQUFzQyxFQUZ4QztBQUlBLGFBQUtGLE9BQUwsQ0FBYTBGLFlBQWIsQ0FBMEIsT0FBMUIsRUFBbUMsRUFBbkM7QUFDRDtBQUNGLEtBM2ZtQjs7QUFBQSxzQkE2ZnBCc1gsTUE3Zm9CLG1CQTZmYmxmLEtBN2ZhLEVBNmZOOFQsT0E3Zk0sRUE2Zkc7QUFDckIsVUFBTWdMLFVBQVUsS0FBS3hMLFdBQUwsQ0FBaUJ6UCxRQUFqQzs7QUFFQWlRLGdCQUFVQSxXQUFXbFYsRUFBRW9CLE1BQU15WCxhQUFSLEVBQXVCeFIsSUFBdkIsQ0FBNEI2WSxPQUE1QixDQUFyQjs7QUFFQSxVQUFJLENBQUNoTCxPQUFMLEVBQWM7QUFDWkEsa0JBQVUsSUFBSSxLQUFLUixXQUFULENBQ1J0VCxNQUFNeVgsYUFERSxFQUVSLEtBQUtzSCxrQkFBTCxFQUZRLENBQVY7QUFJQW5nQixVQUFFb0IsTUFBTXlYLGFBQVIsRUFBdUJ4UixJQUF2QixDQUE0QjZZLE9BQTVCLEVBQXFDaEwsT0FBckM7QUFDRDs7QUFFRCxVQUFJOVQsS0FBSixFQUFXO0FBQ1Q4VCxnQkFBUTBLLGNBQVIsQ0FDRXhlLE1BQU1xSCxJQUFOLEtBQWUsU0FBZixHQUEyQjZXLFFBQVF2WCxLQUFuQyxHQUEyQ3VYLFFBQVFDLEtBRHJELElBRUksSUFGSjtBQUdEOztBQUVELFVBQUl2ZixFQUFFa1YsUUFBUXNMLGFBQVIsRUFBRixFQUEyQjFaLFFBQTNCLENBQW9DbEIsVUFBVUcsSUFBOUMsS0FDRG1QLFFBQVF5SyxXQUFSLEtBQXdCWixXQUFXaFosSUFEdEMsRUFDNEM7QUFDMUNtUCxnQkFBUXlLLFdBQVIsR0FBc0JaLFdBQVdoWixJQUFqQztBQUNBO0FBQ0Q7O0FBRUQrRyxtQkFBYW9JLFFBQVF3SyxRQUFyQjs7QUFFQXhLLGNBQVF5SyxXQUFSLEdBQXNCWixXQUFXaFosSUFBakM7O0FBRUEsVUFBSSxDQUFDbVAsUUFBUWhSLE1BQVIsQ0FBZXlhLEtBQWhCLElBQXlCLENBQUN6SixRQUFRaFIsTUFBUixDQUFleWEsS0FBZixDQUFxQnBPLElBQW5ELEVBQXlEO0FBQ3ZEMkUsZ0JBQVEzRSxJQUFSO0FBQ0E7QUFDRDs7QUFFRDJFLGNBQVF3SyxRQUFSLEdBQW1CamQsV0FBVyxZQUFNO0FBQ2xDLFlBQUl5UyxRQUFReUssV0FBUixLQUF3QlosV0FBV2haLElBQXZDLEVBQTZDO0FBQzNDbVAsa0JBQVEzRSxJQUFSO0FBQ0Q7QUFDRixPQUprQixFQUloQjJFLFFBQVFoUixNQUFSLENBQWV5YSxLQUFmLENBQXFCcE8sSUFKTCxDQUFuQjtBQUtELEtBcGlCbUI7O0FBQUEsc0JBc2lCcEJnUSxNQXRpQm9CLG1CQXNpQmJuZixLQXRpQmEsRUFzaUJOOFQsT0F0aUJNLEVBc2lCRztBQUNyQixVQUFNZ0wsVUFBVSxLQUFLeEwsV0FBTCxDQUFpQnpQLFFBQWpDOztBQUVBaVEsZ0JBQVVBLFdBQVdsVixFQUFFb0IsTUFBTXlYLGFBQVIsRUFBdUJ4UixJQUF2QixDQUE0QjZZLE9BQTVCLENBQXJCOztBQUVBLFVBQUksQ0FBQ2hMLE9BQUwsRUFBYztBQUNaQSxrQkFBVSxJQUFJLEtBQUtSLFdBQVQsQ0FDUnRULE1BQU15WCxhQURFLEVBRVIsS0FBS3NILGtCQUFMLEVBRlEsQ0FBVjtBQUlBbmdCLFVBQUVvQixNQUFNeVgsYUFBUixFQUF1QnhSLElBQXZCLENBQTRCNlksT0FBNUIsRUFBcUNoTCxPQUFyQztBQUNEOztBQUVELFVBQUk5VCxLQUFKLEVBQVc7QUFDVDhULGdCQUFRMEssY0FBUixDQUNFeGUsTUFBTXFILElBQU4sS0FBZSxVQUFmLEdBQTRCNlcsUUFBUXZYLEtBQXBDLEdBQTRDdVgsUUFBUUMsS0FEdEQsSUFFSSxLQUZKO0FBR0Q7O0FBRUQsVUFBSXJLLFFBQVFtTCxvQkFBUixFQUFKLEVBQW9DO0FBQ2xDO0FBQ0Q7O0FBRUR2VCxtQkFBYW9JLFFBQVF3SyxRQUFyQjs7QUFFQXhLLGNBQVF5SyxXQUFSLEdBQXNCWixXQUFXQyxHQUFqQzs7QUFFQSxVQUFJLENBQUM5SixRQUFRaFIsTUFBUixDQUFleWEsS0FBaEIsSUFBeUIsQ0FBQ3pKLFFBQVFoUixNQUFSLENBQWV5YSxLQUFmLENBQXFCck8sSUFBbkQsRUFBeUQ7QUFDdkQ0RSxnQkFBUTVFLElBQVI7QUFDQTtBQUNEOztBQUVENEUsY0FBUXdLLFFBQVIsR0FBbUJqZCxXQUFXLFlBQU07QUFDbEMsWUFBSXlTLFFBQVF5SyxXQUFSLEtBQXdCWixXQUFXQyxHQUF2QyxFQUE0QztBQUMxQzlKLGtCQUFRNUUsSUFBUjtBQUNEO0FBQ0YsT0FKa0IsRUFJaEI0RSxRQUFRaFIsTUFBUixDQUFleWEsS0FBZixDQUFxQnJPLElBSkwsQ0FBbkI7QUFLRCxLQTNrQm1COztBQUFBLHNCQTZrQnBCK1Asb0JBN2tCb0IsbUNBNmtCRztBQUNyQixXQUFLLElBQU12YyxPQUFYLElBQXNCLEtBQUs4YixjQUEzQixFQUEyQztBQUN6QyxZQUFJLEtBQUtBLGNBQUwsQ0FBb0I5YixPQUFwQixDQUFKLEVBQWtDO0FBQ2hDLGlCQUFPLElBQVA7QUFDRDtBQUNGOztBQUVELGFBQU8sS0FBUDtBQUNELEtBcmxCbUI7O0FBQUEsc0JBdWxCcEIwSCxVQXZsQm9CLHVCQXVsQlR0SCxNQXZsQlMsRUF1bEJEO0FBQ2pCQSxlQUFTbEUsRUFBRTJNLE1BQUYsQ0FDUCxFQURPLEVBRVAsS0FBSytILFdBQUwsQ0FBaUJuTCxPQUZWLEVBR1B2SixFQUFFLEtBQUtzRCxPQUFQLEVBQWdCK0QsSUFBaEIsRUFITyxFQUlQbkQsTUFKTyxDQUFUOztBQU9BLFVBQUlBLE9BQU95YSxLQUFQLElBQWdCLE9BQU96YSxPQUFPeWEsS0FBZCxLQUF3QixRQUE1QyxFQUFzRDtBQUNwRHphLGVBQU95YSxLQUFQLEdBQWU7QUFDYnBPLGdCQUFPck0sT0FBT3lhLEtBREQ7QUFFYnJPLGdCQUFPcE0sT0FBT3lhO0FBRkQsU0FBZjtBQUlEOztBQUVELFVBQUl6YSxPQUFPd2EsS0FBUCxJQUFnQixPQUFPeGEsT0FBT3dhLEtBQWQsS0FBd0IsUUFBNUMsRUFBc0Q7QUFDcER4YSxlQUFPd2EsS0FBUCxHQUFleGEsT0FBT3dhLEtBQVAsQ0FBYWplLFFBQWIsRUFBZjtBQUNEOztBQUVELFVBQUl5RCxPQUFPNGQsT0FBUCxJQUFrQixPQUFPNWQsT0FBTzRkLE9BQWQsS0FBMEIsUUFBaEQsRUFBMEQ7QUFDeEQ1ZCxlQUFPNGQsT0FBUCxHQUFpQjVkLE9BQU80ZCxPQUFQLENBQWVyaEIsUUFBZixFQUFqQjtBQUNEOztBQUVEVixXQUFLaUUsZUFBTCxDQUNFZSxJQURGLEVBRUViLE1BRkYsRUFHRSxLQUFLd1EsV0FBTCxDQUFpQjdLLFdBSG5COztBQU1BLGFBQU8zRixNQUFQO0FBQ0QsS0FybkJtQjs7QUFBQSxzQkF1bkJwQmljLGtCQXZuQm9CLGlDQXVuQkM7QUFDbkIsVUFBTWpjLFNBQVMsRUFBZjs7QUFFQSxVQUFJLEtBQUtBLE1BQVQsRUFBaUI7QUFDZixhQUFLLElBQU1zZSxHQUFYLElBQWtCLEtBQUt0ZSxNQUF2QixFQUErQjtBQUM3QixjQUFJLEtBQUt3USxXQUFMLENBQWlCbkwsT0FBakIsQ0FBeUJpWixHQUF6QixNQUFrQyxLQUFLdGUsTUFBTCxDQUFZc2UsR0FBWixDQUF0QyxFQUF3RDtBQUN0RHRlLG1CQUFPc2UsR0FBUCxJQUFjLEtBQUt0ZSxNQUFMLENBQVlzZSxHQUFaLENBQWQ7QUFDRDtBQUNGO0FBQ0Y7O0FBRUQsYUFBT3RlLE1BQVA7QUFDRCxLQW5vQm1COztBQUFBLHNCQXFvQnBCd2QsY0Fyb0JvQiw2QkFxb0JIO0FBQ2YsVUFBTUUsT0FBTzVoQixFQUFFLEtBQUt3Z0IsYUFBTCxFQUFGLENBQWI7QUFDQSxVQUFNaUMsV0FBV2IsS0FBSy9RLElBQUwsQ0FBVSxPQUFWLEVBQW1CbFEsS0FBbkIsQ0FBeUI0ZCxrQkFBekIsQ0FBakI7QUFDQSxVQUFJa0UsYUFBYSxJQUFiLElBQXFCQSxTQUFTL2UsTUFBVCxHQUFrQixDQUEzQyxFQUE4QztBQUM1Q2tlLGFBQUsvYSxXQUFMLENBQWlCNGIsU0FBU2xGLElBQVQsQ0FBYyxFQUFkLENBQWpCO0FBQ0Q7QUFDRixLQTNvQm1COztBQUFBLHNCQTZvQnBCOEQsNEJBN29Cb0IseUNBNm9CU2hhLElBN29CVCxFQTZvQmU7QUFDakMsV0FBS3FhLGNBQUw7QUFDQSxXQUFLVixrQkFBTCxDQUF3QixLQUFLRCxjQUFMLENBQW9CMVosS0FBS2lNLFNBQXpCLENBQXhCO0FBQ0QsS0FocEJtQjs7QUFBQSxzQkFrcEJwQmlPLGNBbHBCb0IsNkJBa3BCSDtBQUNmLFVBQU0xQixNQUFzQixLQUFLVyxhQUFMLEVBQTVCO0FBQ0EsVUFBTWtDLHNCQUFzQixLQUFLeGUsTUFBTCxDQUFZc2EsU0FBeEM7QUFDQSxVQUFJcUIsSUFBSXJjLFlBQUosQ0FBaUIsYUFBakIsTUFBb0MsSUFBeEMsRUFBOEM7QUFDNUM7QUFDRDtBQUNEeEQsUUFBRTZmLEdBQUYsRUFBT2haLFdBQVAsQ0FBbUJqQixVQUFVRSxJQUE3QjtBQUNBLFdBQUs1QixNQUFMLENBQVlzYSxTQUFaLEdBQXdCLEtBQXhCO0FBQ0EsV0FBS2xPLElBQUw7QUFDQSxXQUFLQyxJQUFMO0FBQ0EsV0FBS3JNLE1BQUwsQ0FBWXNhLFNBQVosR0FBd0JrRSxtQkFBeEI7QUFDRCxLQTdwQm1COztBQStwQnBCOztBQS9wQm9CLFlBaXFCYnhiLGdCQWpxQmEsNkJBaXFCSWhELE1BanFCSixFQWlxQlk7QUFDOUIsYUFBTyxLQUFLaUQsSUFBTCxDQUFVLFlBQVk7QUFDM0IsWUFBSUUsT0FBWXJILEVBQUUsSUFBRixFQUFRcUgsSUFBUixDQUFhcEMsUUFBYixDQUFoQjtBQUNBLFlBQU1zRyxVQUFVLFFBQU9ySCxNQUFQLHlDQUFPQSxNQUFQLE9BQWtCLFFBQWxCLElBQThCQSxNQUE5Qzs7QUFFQSxZQUFJLENBQUNtRCxJQUFELElBQVMsZUFBZTNDLElBQWYsQ0FBb0JSLE1BQXBCLENBQWIsRUFBMEM7QUFDeEM7QUFDRDs7QUFFRCxZQUFJLENBQUNtRCxJQUFMLEVBQVc7QUFDVEEsaUJBQU8sSUFBSWdYLE9BQUosQ0FBWSxJQUFaLEVBQWtCOVMsT0FBbEIsQ0FBUDtBQUNBdkwsWUFBRSxJQUFGLEVBQVFxSCxJQUFSLENBQWFwQyxRQUFiLEVBQXVCb0MsSUFBdkI7QUFDRDs7QUFFRCxZQUFJLE9BQU9uRCxNQUFQLEtBQWtCLFFBQXRCLEVBQWdDO0FBQzlCLGNBQUltRCxLQUFLbkQsTUFBTCxNQUFpQnZDLFNBQXJCLEVBQWdDO0FBQzlCLGtCQUFNLElBQUlnRCxLQUFKLHVCQUE4QlQsTUFBOUIsT0FBTjtBQUNEO0FBQ0RtRCxlQUFLbkQsTUFBTDtBQUNEO0FBQ0YsT0FuQk0sQ0FBUDtBQW9CRCxLQXRyQm1COztBQUFBO0FBQUE7QUFBQSwwQkFrSUM7QUFDbkIsZUFBT2MsT0FBUDtBQUNEO0FBcEltQjtBQUFBO0FBQUEsMEJBc0lDO0FBQ25CLGVBQU91RSxPQUFQO0FBQ0Q7QUF4SW1CO0FBQUE7QUFBQSwwQkEwSUY7QUFDaEIsZUFBT3hFLElBQVA7QUFDRDtBQTVJbUI7QUFBQTtBQUFBLDBCQThJRTtBQUNwQixlQUFPRSxRQUFQO0FBQ0Q7QUFoSm1CO0FBQUE7QUFBQSwwQkFrSkQ7QUFDakIsZUFBT08sS0FBUDtBQUNEO0FBcEptQjtBQUFBO0FBQUEsMEJBc0pHO0FBQ3JCLGVBQU9OLFNBQVA7QUFDRDtBQXhKbUI7QUFBQTtBQUFBLDBCQTBKSztBQUN2QixlQUFPMkUsV0FBUDtBQUNEO0FBNUptQjs7QUFBQTtBQUFBOztBQTJyQnRCOzs7Ozs7QUFNQTdKLElBQUU0QyxFQUFGLENBQUttQyxJQUFMLElBQXlCc1osUUFBUW5YLGdCQUFqQztBQUNBbEgsSUFBRTRDLEVBQUYsQ0FBS21DLElBQUwsRUFBVzJDLFdBQVgsR0FBeUIyVyxPQUF6QjtBQUNBcmUsSUFBRTRDLEVBQUYsQ0FBS21DLElBQUwsRUFBVzRDLFVBQVgsR0FBeUIsWUFBWTtBQUNuQzNILE1BQUU0QyxFQUFGLENBQUttQyxJQUFMLElBQWFLLGtCQUFiO0FBQ0EsV0FBT2laLFFBQVFuWCxnQkFBZjtBQUNELEdBSEQ7O0FBS0EsU0FBT21YLE9BQVA7QUFFRCxDQTFzQmUsQ0Ewc0JieFosTUExc0JhLENBQWhCLEVBWkE7Ozs7Ozs7Ozs7O0FDR0E7Ozs7Ozs7QUFPQSxJQUFNOGQsVUFBVyxVQUFDM2lCLENBQUQsRUFBTzs7QUFHdEI7Ozs7OztBQU1BLE1BQU0rRSxPQUFzQixTQUE1QjtBQUNBLE1BQU1DLFVBQXNCLFlBQTVCO0FBQ0EsTUFBTUMsV0FBc0IsWUFBNUI7QUFDQSxNQUFNQyxrQkFBMEJELFFBQWhDO0FBQ0EsTUFBTUcscUJBQXNCcEYsRUFBRTRDLEVBQUYsQ0FBS21DLElBQUwsQ0FBNUI7QUFDQSxNQUFNdVosZUFBc0IsWUFBNUI7QUFDQSxNQUFNQyxxQkFBc0IsSUFBSTlaLE1BQUosYUFBcUI2WixZQUFyQixXQUF5QyxHQUF6QyxDQUE1Qjs7QUFFQSxNQUFNL1UsVUFBVXZKLEVBQUUyTSxNQUFGLENBQVMsRUFBVCxFQUFhMFIsUUFBUTlVLE9BQXJCLEVBQThCO0FBQzVDK0osZUFBWSxPQURnQztBQUU1Q3hQLGFBQVksT0FGZ0M7QUFHNUNnZSxhQUFZLEVBSGdDO0FBSTVDckQsY0FBWSx5Q0FDQSwyQkFEQSxHQUVBLGtDQUZBLEdBR0E7QUFQZ0MsR0FBOUIsQ0FBaEI7O0FBVUEsTUFBTTVVLGNBQWM3SixFQUFFMk0sTUFBRixDQUFTLEVBQVQsRUFBYTBSLFFBQVF4VSxXQUFyQixFQUFrQztBQUNwRGlZLGFBQVU7QUFEMEMsR0FBbEMsQ0FBcEI7O0FBSUEsTUFBTWxjLFlBQVk7QUFDaEJFLFVBQU8sTUFEUztBQUVoQkMsVUFBTztBQUZTLEdBQWxCOztBQUtBLE1BQU1ULFdBQVc7QUFDZnNkLFdBQVUsaUJBREs7QUFFZkMsYUFBVTtBQUZLLEdBQWpCOztBQUtBLE1BQU1yZCxRQUFRO0FBQ1owSixtQkFBb0JoSyxTQURSO0FBRVppSyx1QkFBc0JqSyxTQUZWO0FBR1phLG1CQUFvQmIsU0FIUjtBQUlaK0oscUJBQXFCL0osU0FKVDtBQUtaK1osMkJBQXdCL1osU0FMWjtBQU1ab04scUJBQXFCcE4sU0FOVDtBQU9aMlEseUJBQXVCM1EsU0FQWDtBQVFaZ2EsMkJBQXdCaGEsU0FSWjtBQVNab0YsK0JBQTBCcEYsU0FUZDtBQVVacUYsK0JBQTBCckY7O0FBSTVCOzs7Ozs7QUFkYyxHQUFkO0FBekNzQixNQTZEaEJ5ZCxPQTdEZ0I7QUFBQTs7QUFBQTtBQUFBOztBQUFBO0FBQUE7O0FBK0ZwQjs7QUEvRm9CLHNCQWlHcEJsQyxhQWpHb0IsNEJBaUdKO0FBQ2QsYUFBTyxLQUFLa0IsUUFBTCxNQUFtQixLQUFLbUIsV0FBTCxFQUExQjtBQUNELEtBbkdtQjs7QUFBQSxzQkFxR3BCOUIsa0JBckdvQiwrQkFxR0RGLFVBckdDLEVBcUdXO0FBQzdCOWdCLFFBQUUsS0FBS3dnQixhQUFMLEVBQUYsRUFBd0JwUyxRQUF4QixDQUFvQ2tRLFlBQXBDLFNBQW9Ed0MsVUFBcEQ7QUFDRCxLQXZHbUI7O0FBQUEsc0JBeUdwQk4sYUF6R29CLDRCQXlHSjtBQUNkLGFBQU8sS0FBS1gsR0FBTCxHQUFXLEtBQUtBLEdBQUwsSUFBWTdmLEVBQUUsS0FBS2tFLE1BQUwsQ0FBWXVhLFFBQWQsRUFBd0IsQ0FBeEIsQ0FBOUI7QUFDRCxLQTNHbUI7O0FBQUEsc0JBNkdwQm9DLFVBN0dvQix5QkE2R1A7QUFDWCxVQUFNZSxPQUFPNWhCLEVBQUUsS0FBS3dnQixhQUFMLEVBQUYsQ0FBYjs7QUFFQTtBQUNBLFdBQUtxQixpQkFBTCxDQUF1QkQsS0FBS3BaLElBQUwsQ0FBVWxELFNBQVNzZCxLQUFuQixDQUF2QixFQUFrRCxLQUFLakIsUUFBTCxFQUFsRDtBQUNBLFdBQUtFLGlCQUFMLENBQXVCRCxLQUFLcFosSUFBTCxDQUFVbEQsU0FBU3VkLE9BQW5CLENBQXZCLEVBQW9ELEtBQUtDLFdBQUwsRUFBcEQ7O0FBRUFsQixXQUFLL2EsV0FBTCxDQUFvQmpCLFVBQVVFLElBQTlCLFNBQXNDRixVQUFVRyxJQUFoRDtBQUNELEtBckhtQjs7QUF1SHBCOztBQXZIb0Isc0JBeUhwQitjLFdBekhvQiwwQkF5SE47QUFDWixhQUFPLEtBQUt4ZixPQUFMLENBQWFFLFlBQWIsQ0FBMEIsY0FBMUIsTUFDRCxPQUFPLEtBQUtVLE1BQUwsQ0FBWTRkLE9BQW5CLEtBQStCLFVBQS9CLEdBQ0UsS0FBSzVkLE1BQUwsQ0FBWTRkLE9BQVosQ0FBb0JwaEIsSUFBcEIsQ0FBeUIsS0FBSzRDLE9BQTlCLENBREYsR0FFRSxLQUFLWSxNQUFMLENBQVk0ZCxPQUhiLENBQVA7QUFJRCxLQTlIbUI7O0FBQUEsc0JBZ0lwQkosY0FoSW9CLDZCQWdJSDtBQUNmLFVBQU1FLE9BQU81aEIsRUFBRSxLQUFLd2dCLGFBQUwsRUFBRixDQUFiO0FBQ0EsVUFBTWlDLFdBQVdiLEtBQUsvUSxJQUFMLENBQVUsT0FBVixFQUFtQmxRLEtBQW5CLENBQXlCNGQsa0JBQXpCLENBQWpCO0FBQ0EsVUFBSWtFLGFBQWEsSUFBYixJQUFxQkEsU0FBUy9lLE1BQVQsR0FBa0IsQ0FBM0MsRUFBOEM7QUFDNUNrZSxhQUFLL2EsV0FBTCxDQUFpQjRiLFNBQVNsRixJQUFULENBQWMsRUFBZCxDQUFqQjtBQUNEO0FBQ0YsS0F0SW1COztBQXlJcEI7O0FBeklvQixZQTJJYnJXLGdCQTNJYSw2QkEySUloRCxNQTNJSixFQTJJWTtBQUM5QixhQUFPLEtBQUtpRCxJQUFMLENBQVUsWUFBWTtBQUMzQixZQUFJRSxPQUFZckgsRUFBRSxJQUFGLEVBQVFxSCxJQUFSLENBQWFwQyxRQUFiLENBQWhCO0FBQ0EsWUFBTXNHLFVBQVUsUUFBT3JILE1BQVAseUNBQU9BLE1BQVAsT0FBa0IsUUFBbEIsR0FBNkJBLE1BQTdCLEdBQXNDLElBQXREOztBQUVBLFlBQUksQ0FBQ21ELElBQUQsSUFBUyxlQUFlM0MsSUFBZixDQUFvQlIsTUFBcEIsQ0FBYixFQUEwQztBQUN4QztBQUNEOztBQUVELFlBQUksQ0FBQ21ELElBQUwsRUFBVztBQUNUQSxpQkFBTyxJQUFJc2IsT0FBSixDQUFZLElBQVosRUFBa0JwWCxPQUFsQixDQUFQO0FBQ0F2TCxZQUFFLElBQUYsRUFBUXFILElBQVIsQ0FBYXBDLFFBQWIsRUFBdUJvQyxJQUF2QjtBQUNEOztBQUVELFlBQUksT0FBT25ELE1BQVAsS0FBa0IsUUFBdEIsRUFBZ0M7QUFDOUIsY0FBSW1ELEtBQUtuRCxNQUFMLE1BQWlCdkMsU0FBckIsRUFBZ0M7QUFDOUIsa0JBQU0sSUFBSWdELEtBQUosdUJBQThCVCxNQUE5QixPQUFOO0FBQ0Q7QUFDRG1ELGVBQUtuRCxNQUFMO0FBQ0Q7QUFDRixPQW5CTSxDQUFQO0FBb0JELEtBaEttQjs7QUFBQTtBQUFBOzs7QUFnRXBCOztBQWhFb0IsMEJBa0VDO0FBQ25CLGVBQU9jLE9BQVA7QUFDRDtBQXBFbUI7QUFBQTtBQUFBLDBCQXNFQztBQUNuQixlQUFPdUUsT0FBUDtBQUNEO0FBeEVtQjtBQUFBO0FBQUEsMEJBMEVGO0FBQ2hCLGVBQU94RSxJQUFQO0FBQ0Q7QUE1RW1CO0FBQUE7QUFBQSwwQkE4RUU7QUFDcEIsZUFBT0UsUUFBUDtBQUNEO0FBaEZtQjtBQUFBO0FBQUEsMEJBa0ZEO0FBQ2pCLGVBQU9PLEtBQVA7QUFDRDtBQXBGbUI7QUFBQTtBQUFBLDBCQXNGRztBQUNyQixlQUFPTixTQUFQO0FBQ0Q7QUF4Rm1CO0FBQUE7QUFBQSwwQkEwRks7QUFDdkIsZUFBTzJFLFdBQVA7QUFDRDtBQTVGbUI7O0FBQUE7QUFBQSxJQTZEQXdVLE9BN0RBOztBQW9LdEI7Ozs7OztBQU1BcmUsSUFBRTRDLEVBQUYsQ0FBS21DLElBQUwsSUFBeUI0ZCxRQUFRemIsZ0JBQWpDO0FBQ0FsSCxJQUFFNEMsRUFBRixDQUFLbUMsSUFBTCxFQUFXMkMsV0FBWCxHQUF5QmliLE9BQXpCO0FBQ0EzaUIsSUFBRTRDLEVBQUYsQ0FBS21DLElBQUwsRUFBVzRDLFVBQVgsR0FBeUIsWUFBWTtBQUNuQzNILE1BQUU0QyxFQUFGLENBQUttQyxJQUFMLElBQWFLLGtCQUFiO0FBQ0EsV0FBT3VkLFFBQVF6YixnQkFBZjtBQUNELEdBSEQ7O0FBS0EsU0FBT3liLE9BQVA7QUFFRCxDQW5MZSxDQW1MYjlkLE1BbkxhLENBQWhCIiwiZmlsZSI6ImJvb3RzdHJhcC5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8qKlxuICogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiAqIEJvb3RzdHJhcCAodjQuMC4wLWJldGEpOiB1dGlsLmpzXG4gKiBMaWNlbnNlZCB1bmRlciBNSVQgKGh0dHBzOi8vZ2l0aHViLmNvbS90d2JzL2Jvb3RzdHJhcC9ibG9iL21hc3Rlci9MSUNFTlNFKVxuICogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiAqL1xuXG5jb25zdCBVdGlsID0gKCgkKSA9PiB7XG5cblxuICAvKipcbiAgICogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gICAqIFByaXZhdGUgVHJhbnNpdGlvbkVuZCBIZWxwZXJzXG4gICAqIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuICAgKi9cblxuICBsZXQgdHJhbnNpdGlvbiA9IGZhbHNlXG5cbiAgY29uc3QgTUFYX1VJRCA9IDEwMDAwMDBcblxuICBjb25zdCBUcmFuc2l0aW9uRW5kRXZlbnQgPSB7XG4gICAgV2Via2l0VHJhbnNpdGlvbiA6ICd3ZWJraXRUcmFuc2l0aW9uRW5kJyxcbiAgICBNb3pUcmFuc2l0aW9uICAgIDogJ3RyYW5zaXRpb25lbmQnLFxuICAgIE9UcmFuc2l0aW9uICAgICAgOiAnb1RyYW5zaXRpb25FbmQgb3RyYW5zaXRpb25lbmQnLFxuICAgIHRyYW5zaXRpb24gICAgICAgOiAndHJhbnNpdGlvbmVuZCdcbiAgfVxuXG4gIC8vIHNob3V0b3V0IEFuZ3VzQ3JvbGwgKGh0dHBzOi8vZ29vLmdsL3B4d1FHcClcbiAgZnVuY3Rpb24gdG9UeXBlKG9iaikge1xuICAgIHJldHVybiB7fS50b1N0cmluZy5jYWxsKG9iaikubWF0Y2goL1xccyhbYS16QS1aXSspLylbMV0udG9Mb3dlckNhc2UoKVxuICB9XG5cbiAgZnVuY3Rpb24gaXNFbGVtZW50KG9iaikge1xuICAgIHJldHVybiAob2JqWzBdIHx8IG9iaikubm9kZVR5cGVcbiAgfVxuXG4gIGZ1bmN0aW9uIGdldFNwZWNpYWxUcmFuc2l0aW9uRW5kRXZlbnQoKSB7XG4gICAgcmV0dXJuIHtcbiAgICAgIGJpbmRUeXBlOiB0cmFuc2l0aW9uLmVuZCxcbiAgICAgIGRlbGVnYXRlVHlwZTogdHJhbnNpdGlvbi5lbmQsXG4gICAgICBoYW5kbGUoZXZlbnQpIHtcbiAgICAgICAgaWYgKCQoZXZlbnQudGFyZ2V0KS5pcyh0aGlzKSkge1xuICAgICAgICAgIHJldHVybiBldmVudC5oYW5kbGVPYmouaGFuZGxlci5hcHBseSh0aGlzLCBhcmd1bWVudHMpIC8vIGVzbGludC1kaXNhYmxlLWxpbmUgcHJlZmVyLXJlc3QtcGFyYW1zXG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIHVuZGVmaW5lZFxuICAgICAgfVxuICAgIH1cbiAgfVxuXG4gIGZ1bmN0aW9uIHRyYW5zaXRpb25FbmRUZXN0KCkge1xuICAgIGlmICh3aW5kb3cuUVVuaXQpIHtcbiAgICAgIHJldHVybiBmYWxzZVxuICAgIH1cblxuICAgIGNvbnN0IGVsID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnYm9vdHN0cmFwJylcblxuICAgIGZvciAoY29uc3QgbmFtZSBpbiBUcmFuc2l0aW9uRW5kRXZlbnQpIHtcbiAgICAgIGlmIChlbC5zdHlsZVtuYW1lXSAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICAgIHJldHVybiB7XG4gICAgICAgICAgZW5kOiBUcmFuc2l0aW9uRW5kRXZlbnRbbmFtZV1cbiAgICAgICAgfVxuICAgICAgfVxuICAgIH1cblxuICAgIHJldHVybiBmYWxzZVxuICB9XG5cbiAgZnVuY3Rpb24gdHJhbnNpdGlvbkVuZEVtdWxhdG9yKGR1cmF0aW9uKSB7XG4gICAgbGV0IGNhbGxlZCA9IGZhbHNlXG5cbiAgICAkKHRoaXMpLm9uZShVdGlsLlRSQU5TSVRJT05fRU5ELCAoKSA9PiB7XG4gICAgICBjYWxsZWQgPSB0cnVlXG4gICAgfSlcblxuICAgIHNldFRpbWVvdXQoKCkgPT4ge1xuICAgICAgaWYgKCFjYWxsZWQpIHtcbiAgICAgICAgVXRpbC50cmlnZ2VyVHJhbnNpdGlvbkVuZCh0aGlzKVxuICAgICAgfVxuICAgIH0sIGR1cmF0aW9uKVxuXG4gICAgcmV0dXJuIHRoaXNcbiAgfVxuXG4gIGZ1bmN0aW9uIHNldFRyYW5zaXRpb25FbmRTdXBwb3J0KCkge1xuICAgIHRyYW5zaXRpb24gPSB0cmFuc2l0aW9uRW5kVGVzdCgpXG5cbiAgICAkLmZuLmVtdWxhdGVUcmFuc2l0aW9uRW5kID0gdHJhbnNpdGlvbkVuZEVtdWxhdG9yXG5cbiAgICBpZiAoVXRpbC5zdXBwb3J0c1RyYW5zaXRpb25FbmQoKSkge1xuICAgICAgJC5ldmVudC5zcGVjaWFsW1V0aWwuVFJBTlNJVElPTl9FTkRdID0gZ2V0U3BlY2lhbFRyYW5zaXRpb25FbmRFdmVudCgpXG4gICAgfVxuICB9XG5cblxuICAvKipcbiAgICogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiAgICogUHVibGljIFV0aWwgQXBpXG4gICAqIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gICAqL1xuXG4gIGNvbnN0IFV0aWwgPSB7XG5cbiAgICBUUkFOU0lUSU9OX0VORDogJ2JzVHJhbnNpdGlvbkVuZCcsXG5cbiAgICBnZXRVSUQocHJlZml4KSB7XG4gICAgICBkbyB7XG4gICAgICAgIC8vIGVzbGludC1kaXNhYmxlLW5leHQtbGluZSBuby1iaXR3aXNlXG4gICAgICAgIHByZWZpeCArPSB+fihNYXRoLnJhbmRvbSgpICogTUFYX1VJRCkgLy8gXCJ+flwiIGFjdHMgbGlrZSBhIGZhc3RlciBNYXRoLmZsb29yKCkgaGVyZVxuICAgICAgfSB3aGlsZSAoZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQocHJlZml4KSlcbiAgICAgIHJldHVybiBwcmVmaXhcbiAgICB9LFxuXG4gICAgZ2V0U2VsZWN0b3JGcm9tRWxlbWVudChlbGVtZW50KSB7XG4gICAgICBsZXQgc2VsZWN0b3IgPSBlbGVtZW50LmdldEF0dHJpYnV0ZSgnZGF0YS10YXJnZXQnKVxuICAgICAgaWYgKCFzZWxlY3RvciB8fCBzZWxlY3RvciA9PT0gJyMnKSB7XG4gICAgICAgIHNlbGVjdG9yID0gZWxlbWVudC5nZXRBdHRyaWJ1dGUoJ2hyZWYnKSB8fCAnJ1xuICAgICAgfVxuXG4gICAgICB0cnkge1xuICAgICAgICBjb25zdCAkc2VsZWN0b3IgPSAkKHNlbGVjdG9yKVxuICAgICAgICByZXR1cm4gJHNlbGVjdG9yLmxlbmd0aCA+IDAgPyBzZWxlY3RvciA6IG51bGxcbiAgICAgIH0gY2F0Y2ggKGVycm9yKSB7XG4gICAgICAgIHJldHVybiBudWxsXG4gICAgICB9XG4gICAgfSxcblxuICAgIHJlZmxvdyhlbGVtZW50KSB7XG4gICAgICByZXR1cm4gZWxlbWVudC5vZmZzZXRIZWlnaHRcbiAgICB9LFxuXG4gICAgdHJpZ2dlclRyYW5zaXRpb25FbmQoZWxlbWVudCkge1xuICAgICAgJChlbGVtZW50KS50cmlnZ2VyKHRyYW5zaXRpb24uZW5kKVxuICAgIH0sXG5cbiAgICBzdXBwb3J0c1RyYW5zaXRpb25FbmQoKSB7XG4gICAgICByZXR1cm4gQm9vbGVhbih0cmFuc2l0aW9uKVxuICAgIH0sXG5cbiAgICB0eXBlQ2hlY2tDb25maWcoY29tcG9uZW50TmFtZSwgY29uZmlnLCBjb25maWdUeXBlcykge1xuICAgICAgZm9yIChjb25zdCBwcm9wZXJ0eSBpbiBjb25maWdUeXBlcykge1xuICAgICAgICBpZiAoY29uZmlnVHlwZXMuaGFzT3duUHJvcGVydHkocHJvcGVydHkpKSB7XG4gICAgICAgICAgY29uc3QgZXhwZWN0ZWRUeXBlcyA9IGNvbmZpZ1R5cGVzW3Byb3BlcnR5XVxuICAgICAgICAgIGNvbnN0IHZhbHVlICAgICAgICAgPSBjb25maWdbcHJvcGVydHldXG4gICAgICAgICAgY29uc3QgdmFsdWVUeXBlICAgICA9IHZhbHVlICYmIGlzRWxlbWVudCh2YWx1ZSkgP1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAnZWxlbWVudCcgOiB0b1R5cGUodmFsdWUpXG5cbiAgICAgICAgICBpZiAoIW5ldyBSZWdFeHAoZXhwZWN0ZWRUeXBlcykudGVzdCh2YWx1ZVR5cGUpKSB7XG4gICAgICAgICAgICB0aHJvdyBuZXcgRXJyb3IoXG4gICAgICAgICAgICAgIGAke2NvbXBvbmVudE5hbWUudG9VcHBlckNhc2UoKX06IGAgK1xuICAgICAgICAgICAgICBgT3B0aW9uIFwiJHtwcm9wZXJ0eX1cIiBwcm92aWRlZCB0eXBlIFwiJHt2YWx1ZVR5cGV9XCIgYCArXG4gICAgICAgICAgICAgIGBidXQgZXhwZWN0ZWQgdHlwZSBcIiR7ZXhwZWN0ZWRUeXBlc31cIi5gKVxuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgfVxuICAgIH1cbiAgfVxuXG4gIHNldFRyYW5zaXRpb25FbmRTdXBwb3J0KClcblxuICByZXR1cm4gVXRpbFxuXG59KShqUXVlcnkpXG5cbmV4cG9ydCBkZWZhdWx0IFV0aWxcbiIsImltcG9ydCBVdGlsIGZyb20gJy4vdXRpbCdcblxuXG4vKipcbiAqIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gKiBCb290c3RyYXAgKHY0LjAuMC1iZXRhKTogYWxlcnQuanNcbiAqIExpY2Vuc2VkIHVuZGVyIE1JVCAoaHR0cHM6Ly9naXRodWIuY29tL3R3YnMvYm9vdHN0cmFwL2Jsb2IvbWFzdGVyL0xJQ0VOU0UpXG4gKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuICovXG5cbmNvbnN0IEFsZXJ0ID0gKCgkKSA9PiB7XG5cblxuICAvKipcbiAgICogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gICAqIENvbnN0YW50c1xuICAgKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiAgICovXG5cbiAgY29uc3QgTkFNRSAgICAgICAgICAgICAgICA9ICdhbGVydCdcbiAgY29uc3QgVkVSU0lPTiAgICAgICAgICAgICA9ICc0LjAuMC1iZXRhJ1xuICBjb25zdCBEQVRBX0tFWSAgICAgICAgICAgID0gJ2JzLmFsZXJ0J1xuICBjb25zdCBFVkVOVF9LRVkgICAgICAgICAgID0gYC4ke0RBVEFfS0VZfWBcbiAgY29uc3QgREFUQV9BUElfS0VZICAgICAgICA9ICcuZGF0YS1hcGknXG4gIGNvbnN0IEpRVUVSWV9OT19DT05GTElDVCAgPSAkLmZuW05BTUVdXG4gIGNvbnN0IFRSQU5TSVRJT05fRFVSQVRJT04gPSAxNTBcblxuICBjb25zdCBTZWxlY3RvciA9IHtcbiAgICBESVNNSVNTIDogJ1tkYXRhLWRpc21pc3M9XCJhbGVydFwiXSdcbiAgfVxuXG4gIGNvbnN0IEV2ZW50ID0ge1xuICAgIENMT1NFICAgICAgICAgIDogYGNsb3NlJHtFVkVOVF9LRVl9YCxcbiAgICBDTE9TRUQgICAgICAgICA6IGBjbG9zZWQke0VWRU5UX0tFWX1gLFxuICAgIENMSUNLX0RBVEFfQVBJIDogYGNsaWNrJHtFVkVOVF9LRVl9JHtEQVRBX0FQSV9LRVl9YFxuICB9XG5cbiAgY29uc3QgQ2xhc3NOYW1lID0ge1xuICAgIEFMRVJUIDogJ2FsZXJ0JyxcbiAgICBGQURFICA6ICdmYWRlJyxcbiAgICBTSE9XICA6ICdzaG93J1xuICB9XG5cblxuICAvKipcbiAgICogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gICAqIENsYXNzIERlZmluaXRpb25cbiAgICogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gICAqL1xuXG4gIGNsYXNzIEFsZXJ0IHtcblxuICAgIGNvbnN0cnVjdG9yKGVsZW1lbnQpIHtcbiAgICAgIHRoaXMuX2VsZW1lbnQgPSBlbGVtZW50XG4gICAgfVxuXG5cbiAgICAvLyBnZXR0ZXJzXG5cbiAgICBzdGF0aWMgZ2V0IFZFUlNJT04oKSB7XG4gICAgICByZXR1cm4gVkVSU0lPTlxuICAgIH1cblxuXG4gICAgLy8gcHVibGljXG5cbiAgICBjbG9zZShlbGVtZW50KSB7XG4gICAgICBlbGVtZW50ID0gZWxlbWVudCB8fCB0aGlzLl9lbGVtZW50XG5cbiAgICAgIGNvbnN0IHJvb3RFbGVtZW50ID0gdGhpcy5fZ2V0Um9vdEVsZW1lbnQoZWxlbWVudClcbiAgICAgIGNvbnN0IGN1c3RvbUV2ZW50ID0gdGhpcy5fdHJpZ2dlckNsb3NlRXZlbnQocm9vdEVsZW1lbnQpXG5cbiAgICAgIGlmIChjdXN0b21FdmVudC5pc0RlZmF1bHRQcmV2ZW50ZWQoKSkge1xuICAgICAgICByZXR1cm5cbiAgICAgIH1cblxuICAgICAgdGhpcy5fcmVtb3ZlRWxlbWVudChyb290RWxlbWVudClcbiAgICB9XG5cbiAgICBkaXNwb3NlKCkge1xuICAgICAgJC5yZW1vdmVEYXRhKHRoaXMuX2VsZW1lbnQsIERBVEFfS0VZKVxuICAgICAgdGhpcy5fZWxlbWVudCA9IG51bGxcbiAgICB9XG5cblxuICAgIC8vIHByaXZhdGVcblxuICAgIF9nZXRSb290RWxlbWVudChlbGVtZW50KSB7XG4gICAgICBjb25zdCBzZWxlY3RvciA9IFV0aWwuZ2V0U2VsZWN0b3JGcm9tRWxlbWVudChlbGVtZW50KVxuICAgICAgbGV0IHBhcmVudCAgICAgPSBmYWxzZVxuXG4gICAgICBpZiAoc2VsZWN0b3IpIHtcbiAgICAgICAgcGFyZW50ID0gJChzZWxlY3RvcilbMF1cbiAgICAgIH1cblxuICAgICAgaWYgKCFwYXJlbnQpIHtcbiAgICAgICAgcGFyZW50ID0gJChlbGVtZW50KS5jbG9zZXN0KGAuJHtDbGFzc05hbWUuQUxFUlR9YClbMF1cbiAgICAgIH1cblxuICAgICAgcmV0dXJuIHBhcmVudFxuICAgIH1cblxuICAgIF90cmlnZ2VyQ2xvc2VFdmVudChlbGVtZW50KSB7XG4gICAgICBjb25zdCBjbG9zZUV2ZW50ID0gJC5FdmVudChFdmVudC5DTE9TRSlcblxuICAgICAgJChlbGVtZW50KS50cmlnZ2VyKGNsb3NlRXZlbnQpXG4gICAgICByZXR1cm4gY2xvc2VFdmVudFxuICAgIH1cblxuICAgIF9yZW1vdmVFbGVtZW50KGVsZW1lbnQpIHtcbiAgICAgICQoZWxlbWVudCkucmVtb3ZlQ2xhc3MoQ2xhc3NOYW1lLlNIT1cpXG5cbiAgICAgIGlmICghVXRpbC5zdXBwb3J0c1RyYW5zaXRpb25FbmQoKSB8fFxuICAgICAgICAgICEkKGVsZW1lbnQpLmhhc0NsYXNzKENsYXNzTmFtZS5GQURFKSkge1xuICAgICAgICB0aGlzLl9kZXN0cm95RWxlbWVudChlbGVtZW50KVxuICAgICAgICByZXR1cm5cbiAgICAgIH1cblxuICAgICAgJChlbGVtZW50KVxuICAgICAgICAub25lKFV0aWwuVFJBTlNJVElPTl9FTkQsIChldmVudCkgPT4gdGhpcy5fZGVzdHJveUVsZW1lbnQoZWxlbWVudCwgZXZlbnQpKVxuICAgICAgICAuZW11bGF0ZVRyYW5zaXRpb25FbmQoVFJBTlNJVElPTl9EVVJBVElPTilcbiAgICB9XG5cbiAgICBfZGVzdHJveUVsZW1lbnQoZWxlbWVudCkge1xuICAgICAgJChlbGVtZW50KVxuICAgICAgICAuZGV0YWNoKClcbiAgICAgICAgLnRyaWdnZXIoRXZlbnQuQ0xPU0VEKVxuICAgICAgICAucmVtb3ZlKClcbiAgICB9XG5cblxuICAgIC8vIHN0YXRpY1xuXG4gICAgc3RhdGljIF9qUXVlcnlJbnRlcmZhY2UoY29uZmlnKSB7XG4gICAgICByZXR1cm4gdGhpcy5lYWNoKGZ1bmN0aW9uICgpIHtcbiAgICAgICAgY29uc3QgJGVsZW1lbnQgPSAkKHRoaXMpXG4gICAgICAgIGxldCBkYXRhICAgICAgID0gJGVsZW1lbnQuZGF0YShEQVRBX0tFWSlcblxuICAgICAgICBpZiAoIWRhdGEpIHtcbiAgICAgICAgICBkYXRhID0gbmV3IEFsZXJ0KHRoaXMpXG4gICAgICAgICAgJGVsZW1lbnQuZGF0YShEQVRBX0tFWSwgZGF0YSlcbiAgICAgICAgfVxuXG4gICAgICAgIGlmIChjb25maWcgPT09ICdjbG9zZScpIHtcbiAgICAgICAgICBkYXRhW2NvbmZpZ10odGhpcylcbiAgICAgICAgfVxuICAgICAgfSlcbiAgICB9XG5cbiAgICBzdGF0aWMgX2hhbmRsZURpc21pc3MoYWxlcnRJbnN0YW5jZSkge1xuICAgICAgcmV0dXJuIGZ1bmN0aW9uIChldmVudCkge1xuICAgICAgICBpZiAoZXZlbnQpIHtcbiAgICAgICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpXG4gICAgICAgIH1cblxuICAgICAgICBhbGVydEluc3RhbmNlLmNsb3NlKHRoaXMpXG4gICAgICB9XG4gICAgfVxuXG4gIH1cblxuXG4gIC8qKlxuICAgKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiAgICogRGF0YSBBcGkgaW1wbGVtZW50YXRpb25cbiAgICogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gICAqL1xuXG4gICQoZG9jdW1lbnQpLm9uKFxuICAgIEV2ZW50LkNMSUNLX0RBVEFfQVBJLFxuICAgIFNlbGVjdG9yLkRJU01JU1MsXG4gICAgQWxlcnQuX2hhbmRsZURpc21pc3MobmV3IEFsZXJ0KCkpXG4gIClcblxuXG4gIC8qKlxuICAgKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiAgICogalF1ZXJ5XG4gICAqIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuICAgKi9cblxuICAkLmZuW05BTUVdICAgICAgICAgICAgID0gQWxlcnQuX2pRdWVyeUludGVyZmFjZVxuICAkLmZuW05BTUVdLkNvbnN0cnVjdG9yID0gQWxlcnRcbiAgJC5mbltOQU1FXS5ub0NvbmZsaWN0ICA9IGZ1bmN0aW9uICgpIHtcbiAgICAkLmZuW05BTUVdID0gSlFVRVJZX05PX0NPTkZMSUNUXG4gICAgcmV0dXJuIEFsZXJ0Ll9qUXVlcnlJbnRlcmZhY2VcbiAgfVxuXG4gIHJldHVybiBBbGVydFxuXG59KShqUXVlcnkpXG5cbmV4cG9ydCBkZWZhdWx0IEFsZXJ0XG4iLCIvKipcbiAqIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gKiBCb290c3RyYXAgKHY0LjAuMC1iZXRhKTogYnV0dG9uLmpzXG4gKiBMaWNlbnNlZCB1bmRlciBNSVQgKGh0dHBzOi8vZ2l0aHViLmNvbS90d2JzL2Jvb3RzdHJhcC9ibG9iL21hc3Rlci9MSUNFTlNFKVxuICogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiAqL1xuXG5jb25zdCBCdXR0b24gPSAoKCQpID0+IHtcblxuXG4gIC8qKlxuICAgKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiAgICogQ29uc3RhbnRzXG4gICAqIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuICAgKi9cblxuICBjb25zdCBOQU1FICAgICAgICAgICAgICAgID0gJ2J1dHRvbidcbiAgY29uc3QgVkVSU0lPTiAgICAgICAgICAgICA9ICc0LjAuMC1iZXRhJ1xuICBjb25zdCBEQVRBX0tFWSAgICAgICAgICAgID0gJ2JzLmJ1dHRvbidcbiAgY29uc3QgRVZFTlRfS0VZICAgICAgICAgICA9IGAuJHtEQVRBX0tFWX1gXG4gIGNvbnN0IERBVEFfQVBJX0tFWSAgICAgICAgPSAnLmRhdGEtYXBpJ1xuICBjb25zdCBKUVVFUllfTk9fQ09ORkxJQ1QgID0gJC5mbltOQU1FXVxuXG4gIGNvbnN0IENsYXNzTmFtZSA9IHtcbiAgICBBQ1RJVkUgOiAnYWN0aXZlJyxcbiAgICBCVVRUT04gOiAnYnRuJyxcbiAgICBGT0NVUyAgOiAnZm9jdXMnXG4gIH1cblxuICBjb25zdCBTZWxlY3RvciA9IHtcbiAgICBEQVRBX1RPR0dMRV9DQVJST1QgOiAnW2RhdGEtdG9nZ2xlXj1cImJ1dHRvblwiXScsXG4gICAgREFUQV9UT0dHTEUgICAgICAgIDogJ1tkYXRhLXRvZ2dsZT1cImJ1dHRvbnNcIl0nLFxuICAgIElOUFVUICAgICAgICAgICAgICA6ICdpbnB1dCcsXG4gICAgQUNUSVZFICAgICAgICAgICAgIDogJy5hY3RpdmUnLFxuICAgIEJVVFRPTiAgICAgICAgICAgICA6ICcuYnRuJ1xuICB9XG5cbiAgY29uc3QgRXZlbnQgPSB7XG4gICAgQ0xJQ0tfREFUQV9BUEkgICAgICA6IGBjbGljayR7RVZFTlRfS0VZfSR7REFUQV9BUElfS0VZfWAsXG4gICAgRk9DVVNfQkxVUl9EQVRBX0FQSSA6IGBmb2N1cyR7RVZFTlRfS0VZfSR7REFUQV9BUElfS0VZfSBgXG4gICAgICAgICAgICAgICAgICAgICAgICArIGBibHVyJHtFVkVOVF9LRVl9JHtEQVRBX0FQSV9LRVl9YFxuICB9XG5cblxuICAvKipcbiAgICogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gICAqIENsYXNzIERlZmluaXRpb25cbiAgICogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gICAqL1xuXG4gIGNsYXNzIEJ1dHRvbiB7XG5cbiAgICBjb25zdHJ1Y3RvcihlbGVtZW50KSB7XG4gICAgICB0aGlzLl9lbGVtZW50ID0gZWxlbWVudFxuICAgIH1cblxuXG4gICAgLy8gZ2V0dGVyc1xuXG4gICAgc3RhdGljIGdldCBWRVJTSU9OKCkge1xuICAgICAgcmV0dXJuIFZFUlNJT05cbiAgICB9XG5cblxuICAgIC8vIHB1YmxpY1xuXG4gICAgdG9nZ2xlKCkge1xuICAgICAgbGV0IHRyaWdnZXJDaGFuZ2VFdmVudCA9IHRydWVcbiAgICAgIGxldCBhZGRBcmlhUHJlc3NlZCA9IHRydWVcbiAgICAgIGNvbnN0IHJvb3RFbGVtZW50ICAgICAgPSAkKHRoaXMuX2VsZW1lbnQpLmNsb3Nlc3QoXG4gICAgICAgIFNlbGVjdG9yLkRBVEFfVE9HR0xFXG4gICAgICApWzBdXG5cbiAgICAgIGlmIChyb290RWxlbWVudCkge1xuICAgICAgICBjb25zdCBpbnB1dCA9ICQodGhpcy5fZWxlbWVudCkuZmluZChTZWxlY3Rvci5JTlBVVClbMF1cblxuICAgICAgICBpZiAoaW5wdXQpIHtcbiAgICAgICAgICBpZiAoaW5wdXQudHlwZSA9PT0gJ3JhZGlvJykge1xuICAgICAgICAgICAgaWYgKGlucHV0LmNoZWNrZWQgJiZcbiAgICAgICAgICAgICAgJCh0aGlzLl9lbGVtZW50KS5oYXNDbGFzcyhDbGFzc05hbWUuQUNUSVZFKSkge1xuICAgICAgICAgICAgICB0cmlnZ2VyQ2hhbmdlRXZlbnQgPSBmYWxzZVxuXG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICBjb25zdCBhY3RpdmVFbGVtZW50ID0gJChyb290RWxlbWVudCkuZmluZChTZWxlY3Rvci5BQ1RJVkUpWzBdXG5cbiAgICAgICAgICAgICAgaWYgKGFjdGl2ZUVsZW1lbnQpIHtcbiAgICAgICAgICAgICAgICAkKGFjdGl2ZUVsZW1lbnQpLnJlbW92ZUNsYXNzKENsYXNzTmFtZS5BQ1RJVkUpXG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9XG5cbiAgICAgICAgICBpZiAodHJpZ2dlckNoYW5nZUV2ZW50KSB7XG4gICAgICAgICAgICBpZiAoaW5wdXQuaGFzQXR0cmlidXRlKCdkaXNhYmxlZCcpIHx8XG4gICAgICAgICAgICAgIHJvb3RFbGVtZW50Lmhhc0F0dHJpYnV0ZSgnZGlzYWJsZWQnKSB8fFxuICAgICAgICAgICAgICBpbnB1dC5jbGFzc0xpc3QuY29udGFpbnMoJ2Rpc2FibGVkJykgfHxcbiAgICAgICAgICAgICAgcm9vdEVsZW1lbnQuY2xhc3NMaXN0LmNvbnRhaW5zKCdkaXNhYmxlZCcpKSB7XG4gICAgICAgICAgICAgIHJldHVyblxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgaW5wdXQuY2hlY2tlZCA9ICEkKHRoaXMuX2VsZW1lbnQpLmhhc0NsYXNzKENsYXNzTmFtZS5BQ1RJVkUpXG4gICAgICAgICAgICAkKGlucHV0KS50cmlnZ2VyKCdjaGFuZ2UnKVxuICAgICAgICAgIH1cblxuICAgICAgICAgIGlucHV0LmZvY3VzKClcbiAgICAgICAgICBhZGRBcmlhUHJlc3NlZCA9IGZhbHNlXG4gICAgICAgIH1cblxuICAgICAgfVxuXG4gICAgICBpZiAoYWRkQXJpYVByZXNzZWQpIHtcbiAgICAgICAgdGhpcy5fZWxlbWVudC5zZXRBdHRyaWJ1dGUoJ2FyaWEtcHJlc3NlZCcsXG4gICAgICAgICAgISQodGhpcy5fZWxlbWVudCkuaGFzQ2xhc3MoQ2xhc3NOYW1lLkFDVElWRSkpXG4gICAgICB9XG5cbiAgICAgIGlmICh0cmlnZ2VyQ2hhbmdlRXZlbnQpIHtcbiAgICAgICAgJCh0aGlzLl9lbGVtZW50KS50b2dnbGVDbGFzcyhDbGFzc05hbWUuQUNUSVZFKVxuICAgICAgfVxuICAgIH1cblxuICAgIGRpc3Bvc2UoKSB7XG4gICAgICAkLnJlbW92ZURhdGEodGhpcy5fZWxlbWVudCwgREFUQV9LRVkpXG4gICAgICB0aGlzLl9lbGVtZW50ID0gbnVsbFxuICAgIH1cblxuXG4gICAgLy8gc3RhdGljXG5cbiAgICBzdGF0aWMgX2pRdWVyeUludGVyZmFjZShjb25maWcpIHtcbiAgICAgIHJldHVybiB0aGlzLmVhY2goZnVuY3Rpb24gKCkge1xuICAgICAgICBsZXQgZGF0YSA9ICQodGhpcykuZGF0YShEQVRBX0tFWSlcblxuICAgICAgICBpZiAoIWRhdGEpIHtcbiAgICAgICAgICBkYXRhID0gbmV3IEJ1dHRvbih0aGlzKVxuICAgICAgICAgICQodGhpcykuZGF0YShEQVRBX0tFWSwgZGF0YSlcbiAgICAgICAgfVxuXG4gICAgICAgIGlmIChjb25maWcgPT09ICd0b2dnbGUnKSB7XG4gICAgICAgICAgZGF0YVtjb25maWddKClcbiAgICAgICAgfVxuICAgICAgfSlcbiAgICB9XG5cbiAgfVxuXG5cbiAgLyoqXG4gICAqIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuICAgKiBEYXRhIEFwaSBpbXBsZW1lbnRhdGlvblxuICAgKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiAgICovXG5cbiAgJChkb2N1bWVudClcbiAgICAub24oRXZlbnQuQ0xJQ0tfREFUQV9BUEksIFNlbGVjdG9yLkRBVEFfVE9HR0xFX0NBUlJPVCwgKGV2ZW50KSA9PiB7XG4gICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpXG5cbiAgICAgIGxldCBidXR0b24gPSBldmVudC50YXJnZXRcblxuICAgICAgaWYgKCEkKGJ1dHRvbikuaGFzQ2xhc3MoQ2xhc3NOYW1lLkJVVFRPTikpIHtcbiAgICAgICAgYnV0dG9uID0gJChidXR0b24pLmNsb3Nlc3QoU2VsZWN0b3IuQlVUVE9OKVxuICAgICAgfVxuXG4gICAgICBCdXR0b24uX2pRdWVyeUludGVyZmFjZS5jYWxsKCQoYnV0dG9uKSwgJ3RvZ2dsZScpXG4gICAgfSlcbiAgICAub24oRXZlbnQuRk9DVVNfQkxVUl9EQVRBX0FQSSwgU2VsZWN0b3IuREFUQV9UT0dHTEVfQ0FSUk9ULCAoZXZlbnQpID0+IHtcbiAgICAgIGNvbnN0IGJ1dHRvbiA9ICQoZXZlbnQudGFyZ2V0KS5jbG9zZXN0KFNlbGVjdG9yLkJVVFRPTilbMF1cbiAgICAgICQoYnV0dG9uKS50b2dnbGVDbGFzcyhDbGFzc05hbWUuRk9DVVMsIC9eZm9jdXMoaW4pPyQvLnRlc3QoZXZlbnQudHlwZSkpXG4gICAgfSlcblxuXG4gIC8qKlxuICAgKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiAgICogalF1ZXJ5XG4gICAqIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuICAgKi9cblxuICAkLmZuW05BTUVdICAgICAgICAgICAgID0gQnV0dG9uLl9qUXVlcnlJbnRlcmZhY2VcbiAgJC5mbltOQU1FXS5Db25zdHJ1Y3RvciA9IEJ1dHRvblxuICAkLmZuW05BTUVdLm5vQ29uZmxpY3QgID0gZnVuY3Rpb24gKCkge1xuICAgICQuZm5bTkFNRV0gPSBKUVVFUllfTk9fQ09ORkxJQ1RcbiAgICByZXR1cm4gQnV0dG9uLl9qUXVlcnlJbnRlcmZhY2VcbiAgfVxuXG4gIHJldHVybiBCdXR0b25cblxufSkoalF1ZXJ5KVxuXG5leHBvcnQgZGVmYXVsdCBCdXR0b25cbiIsImltcG9ydCBVdGlsIGZyb20gJy4vdXRpbCdcblxuXG4vKipcbiAqIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gKiBCb290c3RyYXAgKHY0LjAuMC1iZXRhKTogY2Fyb3VzZWwuanNcbiAqIExpY2Vuc2VkIHVuZGVyIE1JVCAoaHR0cHM6Ly9naXRodWIuY29tL3R3YnMvYm9vdHN0cmFwL2Jsb2IvbWFzdGVyL0xJQ0VOU0UpXG4gKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuICovXG5cbmNvbnN0IENhcm91c2VsID0gKCgkKSA9PiB7XG5cblxuICAvKipcbiAgICogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gICAqIENvbnN0YW50c1xuICAgKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiAgICovXG5cbiAgY29uc3QgTkFNRSAgICAgICAgICAgICAgICAgICA9ICdjYXJvdXNlbCdcbiAgY29uc3QgVkVSU0lPTiAgICAgICAgICAgICAgICA9ICc0LjAuMC1iZXRhJ1xuICBjb25zdCBEQVRBX0tFWSAgICAgICAgICAgICAgID0gJ2JzLmNhcm91c2VsJ1xuICBjb25zdCBFVkVOVF9LRVkgICAgICAgICAgICAgID0gYC4ke0RBVEFfS0VZfWBcbiAgY29uc3QgREFUQV9BUElfS0VZICAgICAgICAgICA9ICcuZGF0YS1hcGknXG4gIGNvbnN0IEpRVUVSWV9OT19DT05GTElDVCAgICAgPSAkLmZuW05BTUVdXG4gIGNvbnN0IFRSQU5TSVRJT05fRFVSQVRJT04gICAgPSA2MDBcbiAgY29uc3QgQVJST1dfTEVGVF9LRVlDT0RFICAgICA9IDM3IC8vIEtleWJvYXJkRXZlbnQud2hpY2ggdmFsdWUgZm9yIGxlZnQgYXJyb3cga2V5XG4gIGNvbnN0IEFSUk9XX1JJR0hUX0tFWUNPREUgICAgPSAzOSAvLyBLZXlib2FyZEV2ZW50LndoaWNoIHZhbHVlIGZvciByaWdodCBhcnJvdyBrZXlcbiAgY29uc3QgVE9VQ0hFVkVOVF9DT01QQVRfV0FJVCA9IDUwMCAvLyBUaW1lIGZvciBtb3VzZSBjb21wYXQgZXZlbnRzIHRvIGZpcmUgYWZ0ZXIgdG91Y2hcblxuICBjb25zdCBEZWZhdWx0ID0ge1xuICAgIGludGVydmFsIDogNTAwMCxcbiAgICBrZXlib2FyZCA6IHRydWUsXG4gICAgc2xpZGUgICAgOiBmYWxzZSxcbiAgICBwYXVzZSAgICA6ICdob3ZlcicsXG4gICAgd3JhcCAgICAgOiB0cnVlXG4gIH1cblxuICBjb25zdCBEZWZhdWx0VHlwZSA9IHtcbiAgICBpbnRlcnZhbCA6ICcobnVtYmVyfGJvb2xlYW4pJyxcbiAgICBrZXlib2FyZCA6ICdib29sZWFuJyxcbiAgICBzbGlkZSAgICA6ICcoYm9vbGVhbnxzdHJpbmcpJyxcbiAgICBwYXVzZSAgICA6ICcoc3RyaW5nfGJvb2xlYW4pJyxcbiAgICB3cmFwICAgICA6ICdib29sZWFuJ1xuICB9XG5cbiAgY29uc3QgRGlyZWN0aW9uID0ge1xuICAgIE5FWFQgICAgIDogJ25leHQnLFxuICAgIFBSRVYgICAgIDogJ3ByZXYnLFxuICAgIExFRlQgICAgIDogJ2xlZnQnLFxuICAgIFJJR0hUICAgIDogJ3JpZ2h0J1xuICB9XG5cbiAgY29uc3QgRXZlbnQgPSB7XG4gICAgU0xJREUgICAgICAgICAgOiBgc2xpZGUke0VWRU5UX0tFWX1gLFxuICAgIFNMSUQgICAgICAgICAgIDogYHNsaWQke0VWRU5UX0tFWX1gLFxuICAgIEtFWURPV04gICAgICAgIDogYGtleWRvd24ke0VWRU5UX0tFWX1gLFxuICAgIE1PVVNFRU5URVIgICAgIDogYG1vdXNlZW50ZXIke0VWRU5UX0tFWX1gLFxuICAgIE1PVVNFTEVBVkUgICAgIDogYG1vdXNlbGVhdmUke0VWRU5UX0tFWX1gLFxuICAgIFRPVUNIRU5EICAgICAgIDogYHRvdWNoZW5kJHtFVkVOVF9LRVl9YCxcbiAgICBMT0FEX0RBVEFfQVBJICA6IGBsb2FkJHtFVkVOVF9LRVl9JHtEQVRBX0FQSV9LRVl9YCxcbiAgICBDTElDS19EQVRBX0FQSSA6IGBjbGljayR7RVZFTlRfS0VZfSR7REFUQV9BUElfS0VZfWBcbiAgfVxuXG4gIGNvbnN0IENsYXNzTmFtZSA9IHtcbiAgICBDQVJPVVNFTCA6ICdjYXJvdXNlbCcsXG4gICAgQUNUSVZFICAgOiAnYWN0aXZlJyxcbiAgICBTTElERSAgICA6ICdzbGlkZScsXG4gICAgUklHSFQgICAgOiAnY2Fyb3VzZWwtaXRlbS1yaWdodCcsXG4gICAgTEVGVCAgICAgOiAnY2Fyb3VzZWwtaXRlbS1sZWZ0JyxcbiAgICBORVhUICAgICA6ICdjYXJvdXNlbC1pdGVtLW5leHQnLFxuICAgIFBSRVYgICAgIDogJ2Nhcm91c2VsLWl0ZW0tcHJldicsXG4gICAgSVRFTSAgICAgOiAnY2Fyb3VzZWwtaXRlbSdcbiAgfVxuXG4gIGNvbnN0IFNlbGVjdG9yID0ge1xuICAgIEFDVElWRSAgICAgIDogJy5hY3RpdmUnLFxuICAgIEFDVElWRV9JVEVNIDogJy5hY3RpdmUuY2Fyb3VzZWwtaXRlbScsXG4gICAgSVRFTSAgICAgICAgOiAnLmNhcm91c2VsLWl0ZW0nLFxuICAgIE5FWFRfUFJFViAgIDogJy5jYXJvdXNlbC1pdGVtLW5leHQsIC5jYXJvdXNlbC1pdGVtLXByZXYnLFxuICAgIElORElDQVRPUlMgIDogJy5jYXJvdXNlbC1pbmRpY2F0b3JzJyxcbiAgICBEQVRBX1NMSURFICA6ICdbZGF0YS1zbGlkZV0sIFtkYXRhLXNsaWRlLXRvXScsXG4gICAgREFUQV9SSURFICAgOiAnW2RhdGEtcmlkZT1cImNhcm91c2VsXCJdJ1xuICB9XG5cblxuICAvKipcbiAgICogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gICAqIENsYXNzIERlZmluaXRpb25cbiAgICogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gICAqL1xuXG4gIGNsYXNzIENhcm91c2VsIHtcblxuICAgIGNvbnN0cnVjdG9yKGVsZW1lbnQsIGNvbmZpZykge1xuICAgICAgdGhpcy5faXRlbXMgICAgICAgICAgICAgPSBudWxsXG4gICAgICB0aGlzLl9pbnRlcnZhbCAgICAgICAgICA9IG51bGxcbiAgICAgIHRoaXMuX2FjdGl2ZUVsZW1lbnQgICAgID0gbnVsbFxuXG4gICAgICB0aGlzLl9pc1BhdXNlZCAgICAgICAgICA9IGZhbHNlXG4gICAgICB0aGlzLl9pc1NsaWRpbmcgICAgICAgICA9IGZhbHNlXG5cbiAgICAgIHRoaXMudG91Y2hUaW1lb3V0ICAgICAgID0gbnVsbFxuXG4gICAgICB0aGlzLl9jb25maWcgICAgICAgICAgICA9IHRoaXMuX2dldENvbmZpZyhjb25maWcpXG4gICAgICB0aGlzLl9lbGVtZW50ICAgICAgICAgICA9ICQoZWxlbWVudClbMF1cbiAgICAgIHRoaXMuX2luZGljYXRvcnNFbGVtZW50ID0gJCh0aGlzLl9lbGVtZW50KS5maW5kKFNlbGVjdG9yLklORElDQVRPUlMpWzBdXG5cbiAgICAgIHRoaXMuX2FkZEV2ZW50TGlzdGVuZXJzKClcbiAgICB9XG5cblxuICAgIC8vIGdldHRlcnNcblxuICAgIHN0YXRpYyBnZXQgVkVSU0lPTigpIHtcbiAgICAgIHJldHVybiBWRVJTSU9OXG4gICAgfVxuXG4gICAgc3RhdGljIGdldCBEZWZhdWx0KCkge1xuICAgICAgcmV0dXJuIERlZmF1bHRcbiAgICB9XG5cblxuICAgIC8vIHB1YmxpY1xuXG4gICAgbmV4dCgpIHtcbiAgICAgIGlmICghdGhpcy5faXNTbGlkaW5nKSB7XG4gICAgICAgIHRoaXMuX3NsaWRlKERpcmVjdGlvbi5ORVhUKVxuICAgICAgfVxuICAgIH1cblxuICAgIG5leHRXaGVuVmlzaWJsZSgpIHtcbiAgICAgIC8vIERvbid0IGNhbGwgbmV4dCB3aGVuIHRoZSBwYWdlIGlzbid0IHZpc2libGVcbiAgICAgIGlmICghZG9jdW1lbnQuaGlkZGVuKSB7XG4gICAgICAgIHRoaXMubmV4dCgpXG4gICAgICB9XG4gICAgfVxuXG4gICAgcHJldigpIHtcbiAgICAgIGlmICghdGhpcy5faXNTbGlkaW5nKSB7XG4gICAgICAgIHRoaXMuX3NsaWRlKERpcmVjdGlvbi5QUkVWKVxuICAgICAgfVxuICAgIH1cblxuICAgIHBhdXNlKGV2ZW50KSB7XG4gICAgICBpZiAoIWV2ZW50KSB7XG4gICAgICAgIHRoaXMuX2lzUGF1c2VkID0gdHJ1ZVxuICAgICAgfVxuXG4gICAgICBpZiAoJCh0aGlzLl9lbGVtZW50KS5maW5kKFNlbGVjdG9yLk5FWFRfUFJFVilbMF0gJiZcbiAgICAgICAgVXRpbC5zdXBwb3J0c1RyYW5zaXRpb25FbmQoKSkge1xuICAgICAgICBVdGlsLnRyaWdnZXJUcmFuc2l0aW9uRW5kKHRoaXMuX2VsZW1lbnQpXG4gICAgICAgIHRoaXMuY3ljbGUodHJ1ZSlcbiAgICAgIH1cblxuICAgICAgY2xlYXJJbnRlcnZhbCh0aGlzLl9pbnRlcnZhbClcbiAgICAgIHRoaXMuX2ludGVydmFsID0gbnVsbFxuICAgIH1cblxuICAgIGN5Y2xlKGV2ZW50KSB7XG4gICAgICBpZiAoIWV2ZW50KSB7XG4gICAgICAgIHRoaXMuX2lzUGF1c2VkID0gZmFsc2VcbiAgICAgIH1cblxuICAgICAgaWYgKHRoaXMuX2ludGVydmFsKSB7XG4gICAgICAgIGNsZWFySW50ZXJ2YWwodGhpcy5faW50ZXJ2YWwpXG4gICAgICAgIHRoaXMuX2ludGVydmFsID0gbnVsbFxuICAgICAgfVxuXG4gICAgICBpZiAodGhpcy5fY29uZmlnLmludGVydmFsICYmICF0aGlzLl9pc1BhdXNlZCkge1xuICAgICAgICB0aGlzLl9pbnRlcnZhbCA9IHNldEludGVydmFsKFxuICAgICAgICAgIChkb2N1bWVudC52aXNpYmlsaXR5U3RhdGUgPyB0aGlzLm5leHRXaGVuVmlzaWJsZSA6IHRoaXMubmV4dCkuYmluZCh0aGlzKSxcbiAgICAgICAgICB0aGlzLl9jb25maWcuaW50ZXJ2YWxcbiAgICAgICAgKVxuICAgICAgfVxuICAgIH1cblxuICAgIHRvKGluZGV4KSB7XG4gICAgICB0aGlzLl9hY3RpdmVFbGVtZW50ID0gJCh0aGlzLl9lbGVtZW50KS5maW5kKFNlbGVjdG9yLkFDVElWRV9JVEVNKVswXVxuXG4gICAgICBjb25zdCBhY3RpdmVJbmRleCA9IHRoaXMuX2dldEl0ZW1JbmRleCh0aGlzLl9hY3RpdmVFbGVtZW50KVxuXG4gICAgICBpZiAoaW5kZXggPiB0aGlzLl9pdGVtcy5sZW5ndGggLSAxIHx8IGluZGV4IDwgMCkge1xuICAgICAgICByZXR1cm5cbiAgICAgIH1cblxuICAgICAgaWYgKHRoaXMuX2lzU2xpZGluZykge1xuICAgICAgICAkKHRoaXMuX2VsZW1lbnQpLm9uZShFdmVudC5TTElELCAoKSA9PiB0aGlzLnRvKGluZGV4KSlcbiAgICAgICAgcmV0dXJuXG4gICAgICB9XG5cbiAgICAgIGlmIChhY3RpdmVJbmRleCA9PT0gaW5kZXgpIHtcbiAgICAgICAgdGhpcy5wYXVzZSgpXG4gICAgICAgIHRoaXMuY3ljbGUoKVxuICAgICAgICByZXR1cm5cbiAgICAgIH1cblxuICAgICAgY29uc3QgZGlyZWN0aW9uID0gaW5kZXggPiBhY3RpdmVJbmRleCA/XG4gICAgICAgIERpcmVjdGlvbi5ORVhUIDpcbiAgICAgICAgRGlyZWN0aW9uLlBSRVZcblxuICAgICAgdGhpcy5fc2xpZGUoZGlyZWN0aW9uLCB0aGlzLl9pdGVtc1tpbmRleF0pXG4gICAgfVxuXG4gICAgZGlzcG9zZSgpIHtcbiAgICAgICQodGhpcy5fZWxlbWVudCkub2ZmKEVWRU5UX0tFWSlcbiAgICAgICQucmVtb3ZlRGF0YSh0aGlzLl9lbGVtZW50LCBEQVRBX0tFWSlcblxuICAgICAgdGhpcy5faXRlbXMgICAgICAgICAgICAgPSBudWxsXG4gICAgICB0aGlzLl9jb25maWcgICAgICAgICAgICA9IG51bGxcbiAgICAgIHRoaXMuX2VsZW1lbnQgICAgICAgICAgID0gbnVsbFxuICAgICAgdGhpcy5faW50ZXJ2YWwgICAgICAgICAgPSBudWxsXG4gICAgICB0aGlzLl9pc1BhdXNlZCAgICAgICAgICA9IG51bGxcbiAgICAgIHRoaXMuX2lzU2xpZGluZyAgICAgICAgID0gbnVsbFxuICAgICAgdGhpcy5fYWN0aXZlRWxlbWVudCAgICAgPSBudWxsXG4gICAgICB0aGlzLl9pbmRpY2F0b3JzRWxlbWVudCA9IG51bGxcbiAgICB9XG5cblxuICAgIC8vIHByaXZhdGVcblxuICAgIF9nZXRDb25maWcoY29uZmlnKSB7XG4gICAgICBjb25maWcgPSAkLmV4dGVuZCh7fSwgRGVmYXVsdCwgY29uZmlnKVxuICAgICAgVXRpbC50eXBlQ2hlY2tDb25maWcoTkFNRSwgY29uZmlnLCBEZWZhdWx0VHlwZSlcbiAgICAgIHJldHVybiBjb25maWdcbiAgICB9XG5cbiAgICBfYWRkRXZlbnRMaXN0ZW5lcnMoKSB7XG4gICAgICBpZiAodGhpcy5fY29uZmlnLmtleWJvYXJkKSB7XG4gICAgICAgICQodGhpcy5fZWxlbWVudClcbiAgICAgICAgICAub24oRXZlbnQuS0VZRE9XTiwgKGV2ZW50KSA9PiB0aGlzLl9rZXlkb3duKGV2ZW50KSlcbiAgICAgIH1cblxuICAgICAgaWYgKHRoaXMuX2NvbmZpZy5wYXVzZSA9PT0gJ2hvdmVyJykge1xuICAgICAgICAkKHRoaXMuX2VsZW1lbnQpXG4gICAgICAgICAgLm9uKEV2ZW50Lk1PVVNFRU5URVIsIChldmVudCkgPT4gdGhpcy5wYXVzZShldmVudCkpXG4gICAgICAgICAgLm9uKEV2ZW50Lk1PVVNFTEVBVkUsIChldmVudCkgPT4gdGhpcy5jeWNsZShldmVudCkpXG4gICAgICAgIGlmICgnb250b3VjaHN0YXJ0JyBpbiBkb2N1bWVudC5kb2N1bWVudEVsZW1lbnQpIHtcbiAgICAgICAgICAvLyBpZiBpdCdzIGEgdG91Y2gtZW5hYmxlZCBkZXZpY2UsIG1vdXNlZW50ZXIvbGVhdmUgYXJlIGZpcmVkIGFzXG4gICAgICAgICAgLy8gcGFydCBvZiB0aGUgbW91c2UgY29tcGF0aWJpbGl0eSBldmVudHMgb24gZmlyc3QgdGFwIC0gdGhlIGNhcm91c2VsXG4gICAgICAgICAgLy8gd291bGQgc3RvcCBjeWNsaW5nIHVudGlsIHVzZXIgdGFwcGVkIG91dCBvZiBpdDtcbiAgICAgICAgICAvLyBoZXJlLCB3ZSBsaXN0ZW4gZm9yIHRvdWNoZW5kLCBleHBsaWNpdGx5IHBhdXNlIHRoZSBjYXJvdXNlbFxuICAgICAgICAgIC8vIChhcyBpZiBpdCdzIHRoZSBzZWNvbmQgdGltZSB3ZSB0YXAgb24gaXQsIG1vdXNlZW50ZXIgY29tcGF0IGV2ZW50XG4gICAgICAgICAgLy8gaXMgTk9UIGZpcmVkKSBhbmQgYWZ0ZXIgYSB0aW1lb3V0ICh0byBhbGxvdyBmb3IgbW91c2UgY29tcGF0aWJpbGl0eVxuICAgICAgICAgIC8vIGV2ZW50cyB0byBmaXJlKSB3ZSBleHBsaWNpdGx5IHJlc3RhcnQgY3ljbGluZ1xuICAgICAgICAgICQodGhpcy5fZWxlbWVudCkub24oRXZlbnQuVE9VQ0hFTkQsICgpID0+IHtcbiAgICAgICAgICAgIHRoaXMucGF1c2UoKVxuICAgICAgICAgICAgaWYgKHRoaXMudG91Y2hUaW1lb3V0KSB7XG4gICAgICAgICAgICAgIGNsZWFyVGltZW91dCh0aGlzLnRvdWNoVGltZW91dClcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHRoaXMudG91Y2hUaW1lb3V0ID0gc2V0VGltZW91dCgoZXZlbnQpID0+IHRoaXMuY3ljbGUoZXZlbnQpLCBUT1VDSEVWRU5UX0NPTVBBVF9XQUlUICsgdGhpcy5fY29uZmlnLmludGVydmFsKVxuICAgICAgICAgIH0pXG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG5cbiAgICBfa2V5ZG93bihldmVudCkge1xuICAgICAgaWYgKC9pbnB1dHx0ZXh0YXJlYS9pLnRlc3QoZXZlbnQudGFyZ2V0LnRhZ05hbWUpKSB7XG4gICAgICAgIHJldHVyblxuICAgICAgfVxuXG4gICAgICBzd2l0Y2ggKGV2ZW50LndoaWNoKSB7XG4gICAgICAgIGNhc2UgQVJST1dfTEVGVF9LRVlDT0RFOlxuICAgICAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KClcbiAgICAgICAgICB0aGlzLnByZXYoKVxuICAgICAgICAgIGJyZWFrXG4gICAgICAgIGNhc2UgQVJST1dfUklHSFRfS0VZQ09ERTpcbiAgICAgICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpXG4gICAgICAgICAgdGhpcy5uZXh0KClcbiAgICAgICAgICBicmVha1xuICAgICAgICBkZWZhdWx0OlxuICAgICAgICAgIHJldHVyblxuICAgICAgfVxuICAgIH1cblxuICAgIF9nZXRJdGVtSW5kZXgoZWxlbWVudCkge1xuICAgICAgdGhpcy5faXRlbXMgPSAkLm1ha2VBcnJheSgkKGVsZW1lbnQpLnBhcmVudCgpLmZpbmQoU2VsZWN0b3IuSVRFTSkpXG4gICAgICByZXR1cm4gdGhpcy5faXRlbXMuaW5kZXhPZihlbGVtZW50KVxuICAgIH1cblxuICAgIF9nZXRJdGVtQnlEaXJlY3Rpb24oZGlyZWN0aW9uLCBhY3RpdmVFbGVtZW50KSB7XG4gICAgICBjb25zdCBpc05leHREaXJlY3Rpb24gPSBkaXJlY3Rpb24gPT09IERpcmVjdGlvbi5ORVhUXG4gICAgICBjb25zdCBpc1ByZXZEaXJlY3Rpb24gPSBkaXJlY3Rpb24gPT09IERpcmVjdGlvbi5QUkVWXG4gICAgICBjb25zdCBhY3RpdmVJbmRleCAgICAgPSB0aGlzLl9nZXRJdGVtSW5kZXgoYWN0aXZlRWxlbWVudClcbiAgICAgIGNvbnN0IGxhc3RJdGVtSW5kZXggICA9IHRoaXMuX2l0ZW1zLmxlbmd0aCAtIDFcbiAgICAgIGNvbnN0IGlzR29pbmdUb1dyYXAgICA9IGlzUHJldkRpcmVjdGlvbiAmJiBhY3RpdmVJbmRleCA9PT0gMCB8fFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaXNOZXh0RGlyZWN0aW9uICYmIGFjdGl2ZUluZGV4ID09PSBsYXN0SXRlbUluZGV4XG5cbiAgICAgIGlmIChpc0dvaW5nVG9XcmFwICYmICF0aGlzLl9jb25maWcud3JhcCkge1xuICAgICAgICByZXR1cm4gYWN0aXZlRWxlbWVudFxuICAgICAgfVxuXG4gICAgICBjb25zdCBkZWx0YSAgICAgPSBkaXJlY3Rpb24gPT09IERpcmVjdGlvbi5QUkVWID8gLTEgOiAxXG4gICAgICBjb25zdCBpdGVtSW5kZXggPSAoYWN0aXZlSW5kZXggKyBkZWx0YSkgJSB0aGlzLl9pdGVtcy5sZW5ndGhcblxuICAgICAgcmV0dXJuIGl0ZW1JbmRleCA9PT0gLTEgP1xuICAgICAgICB0aGlzLl9pdGVtc1t0aGlzLl9pdGVtcy5sZW5ndGggLSAxXSA6IHRoaXMuX2l0ZW1zW2l0ZW1JbmRleF1cbiAgICB9XG5cblxuICAgIF90cmlnZ2VyU2xpZGVFdmVudChyZWxhdGVkVGFyZ2V0LCBldmVudERpcmVjdGlvbk5hbWUpIHtcbiAgICAgIGNvbnN0IHRhcmdldEluZGV4ID0gdGhpcy5fZ2V0SXRlbUluZGV4KHJlbGF0ZWRUYXJnZXQpXG4gICAgICBjb25zdCBmcm9tSW5kZXggPSB0aGlzLl9nZXRJdGVtSW5kZXgoJCh0aGlzLl9lbGVtZW50KS5maW5kKFNlbGVjdG9yLkFDVElWRV9JVEVNKVswXSlcbiAgICAgIGNvbnN0IHNsaWRlRXZlbnQgPSAkLkV2ZW50KEV2ZW50LlNMSURFLCB7XG4gICAgICAgIHJlbGF0ZWRUYXJnZXQsXG4gICAgICAgIGRpcmVjdGlvbjogZXZlbnREaXJlY3Rpb25OYW1lLFxuICAgICAgICBmcm9tOiBmcm9tSW5kZXgsXG4gICAgICAgIHRvOiB0YXJnZXRJbmRleFxuICAgICAgfSlcblxuICAgICAgJCh0aGlzLl9lbGVtZW50KS50cmlnZ2VyKHNsaWRlRXZlbnQpXG5cbiAgICAgIHJldHVybiBzbGlkZUV2ZW50XG4gICAgfVxuXG4gICAgX3NldEFjdGl2ZUluZGljYXRvckVsZW1lbnQoZWxlbWVudCkge1xuICAgICAgaWYgKHRoaXMuX2luZGljYXRvcnNFbGVtZW50KSB7XG4gICAgICAgICQodGhpcy5faW5kaWNhdG9yc0VsZW1lbnQpXG4gICAgICAgICAgLmZpbmQoU2VsZWN0b3IuQUNUSVZFKVxuICAgICAgICAgIC5yZW1vdmVDbGFzcyhDbGFzc05hbWUuQUNUSVZFKVxuXG4gICAgICAgIGNvbnN0IG5leHRJbmRpY2F0b3IgPSB0aGlzLl9pbmRpY2F0b3JzRWxlbWVudC5jaGlsZHJlbltcbiAgICAgICAgICB0aGlzLl9nZXRJdGVtSW5kZXgoZWxlbWVudClcbiAgICAgICAgXVxuXG4gICAgICAgIGlmIChuZXh0SW5kaWNhdG9yKSB7XG4gICAgICAgICAgJChuZXh0SW5kaWNhdG9yKS5hZGRDbGFzcyhDbGFzc05hbWUuQUNUSVZFKVxuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuXG4gICAgX3NsaWRlKGRpcmVjdGlvbiwgZWxlbWVudCkge1xuICAgICAgY29uc3QgYWN0aXZlRWxlbWVudCA9ICQodGhpcy5fZWxlbWVudCkuZmluZChTZWxlY3Rvci5BQ1RJVkVfSVRFTSlbMF1cbiAgICAgIGNvbnN0IGFjdGl2ZUVsZW1lbnRJbmRleCA9IHRoaXMuX2dldEl0ZW1JbmRleChhY3RpdmVFbGVtZW50KVxuICAgICAgY29uc3QgbmV4dEVsZW1lbnQgICA9IGVsZW1lbnQgfHwgYWN0aXZlRWxlbWVudCAmJlxuICAgICAgICB0aGlzLl9nZXRJdGVtQnlEaXJlY3Rpb24oZGlyZWN0aW9uLCBhY3RpdmVFbGVtZW50KVxuICAgICAgY29uc3QgbmV4dEVsZW1lbnRJbmRleCA9IHRoaXMuX2dldEl0ZW1JbmRleChuZXh0RWxlbWVudClcbiAgICAgIGNvbnN0IGlzQ3ljbGluZyA9IEJvb2xlYW4odGhpcy5faW50ZXJ2YWwpXG5cbiAgICAgIGxldCBkaXJlY3Rpb25hbENsYXNzTmFtZVxuICAgICAgbGV0IG9yZGVyQ2xhc3NOYW1lXG4gICAgICBsZXQgZXZlbnREaXJlY3Rpb25OYW1lXG5cbiAgICAgIGlmIChkaXJlY3Rpb24gPT09IERpcmVjdGlvbi5ORVhUKSB7XG4gICAgICAgIGRpcmVjdGlvbmFsQ2xhc3NOYW1lID0gQ2xhc3NOYW1lLkxFRlRcbiAgICAgICAgb3JkZXJDbGFzc05hbWUgPSBDbGFzc05hbWUuTkVYVFxuICAgICAgICBldmVudERpcmVjdGlvbk5hbWUgPSBEaXJlY3Rpb24uTEVGVFxuICAgICAgfSBlbHNlIHtcbiAgICAgICAgZGlyZWN0aW9uYWxDbGFzc05hbWUgPSBDbGFzc05hbWUuUklHSFRcbiAgICAgICAgb3JkZXJDbGFzc05hbWUgPSBDbGFzc05hbWUuUFJFVlxuICAgICAgICBldmVudERpcmVjdGlvbk5hbWUgPSBEaXJlY3Rpb24uUklHSFRcbiAgICAgIH1cblxuICAgICAgaWYgKG5leHRFbGVtZW50ICYmICQobmV4dEVsZW1lbnQpLmhhc0NsYXNzKENsYXNzTmFtZS5BQ1RJVkUpKSB7XG4gICAgICAgIHRoaXMuX2lzU2xpZGluZyA9IGZhbHNlXG4gICAgICAgIHJldHVyblxuICAgICAgfVxuXG4gICAgICBjb25zdCBzbGlkZUV2ZW50ID0gdGhpcy5fdHJpZ2dlclNsaWRlRXZlbnQobmV4dEVsZW1lbnQsIGV2ZW50RGlyZWN0aW9uTmFtZSlcbiAgICAgIGlmIChzbGlkZUV2ZW50LmlzRGVmYXVsdFByZXZlbnRlZCgpKSB7XG4gICAgICAgIHJldHVyblxuICAgICAgfVxuXG4gICAgICBpZiAoIWFjdGl2ZUVsZW1lbnQgfHwgIW5leHRFbGVtZW50KSB7XG4gICAgICAgIC8vIHNvbWUgd2VpcmRuZXNzIGlzIGhhcHBlbmluZywgc28gd2UgYmFpbFxuICAgICAgICByZXR1cm5cbiAgICAgIH1cblxuICAgICAgdGhpcy5faXNTbGlkaW5nID0gdHJ1ZVxuXG4gICAgICBpZiAoaXNDeWNsaW5nKSB7XG4gICAgICAgIHRoaXMucGF1c2UoKVxuICAgICAgfVxuXG4gICAgICB0aGlzLl9zZXRBY3RpdmVJbmRpY2F0b3JFbGVtZW50KG5leHRFbGVtZW50KVxuXG4gICAgICBjb25zdCBzbGlkRXZlbnQgPSAkLkV2ZW50KEV2ZW50LlNMSUQsIHtcbiAgICAgICAgcmVsYXRlZFRhcmdldDogbmV4dEVsZW1lbnQsXG4gICAgICAgIGRpcmVjdGlvbjogZXZlbnREaXJlY3Rpb25OYW1lLFxuICAgICAgICBmcm9tOiBhY3RpdmVFbGVtZW50SW5kZXgsXG4gICAgICAgIHRvOiBuZXh0RWxlbWVudEluZGV4XG4gICAgICB9KVxuXG4gICAgICBpZiAoVXRpbC5zdXBwb3J0c1RyYW5zaXRpb25FbmQoKSAmJlxuICAgICAgICAkKHRoaXMuX2VsZW1lbnQpLmhhc0NsYXNzKENsYXNzTmFtZS5TTElERSkpIHtcblxuICAgICAgICAkKG5leHRFbGVtZW50KS5hZGRDbGFzcyhvcmRlckNsYXNzTmFtZSlcblxuICAgICAgICBVdGlsLnJlZmxvdyhuZXh0RWxlbWVudClcblxuICAgICAgICAkKGFjdGl2ZUVsZW1lbnQpLmFkZENsYXNzKGRpcmVjdGlvbmFsQ2xhc3NOYW1lKVxuICAgICAgICAkKG5leHRFbGVtZW50KS5hZGRDbGFzcyhkaXJlY3Rpb25hbENsYXNzTmFtZSlcblxuICAgICAgICAkKGFjdGl2ZUVsZW1lbnQpXG4gICAgICAgICAgLm9uZShVdGlsLlRSQU5TSVRJT05fRU5ELCAoKSA9PiB7XG4gICAgICAgICAgICAkKG5leHRFbGVtZW50KVxuICAgICAgICAgICAgICAucmVtb3ZlQ2xhc3MoYCR7ZGlyZWN0aW9uYWxDbGFzc05hbWV9ICR7b3JkZXJDbGFzc05hbWV9YClcbiAgICAgICAgICAgICAgLmFkZENsYXNzKENsYXNzTmFtZS5BQ1RJVkUpXG5cbiAgICAgICAgICAgICQoYWN0aXZlRWxlbWVudCkucmVtb3ZlQ2xhc3MoYCR7Q2xhc3NOYW1lLkFDVElWRX0gJHtvcmRlckNsYXNzTmFtZX0gJHtkaXJlY3Rpb25hbENsYXNzTmFtZX1gKVxuXG4gICAgICAgICAgICB0aGlzLl9pc1NsaWRpbmcgPSBmYWxzZVxuXG4gICAgICAgICAgICBzZXRUaW1lb3V0KCgpID0+ICQodGhpcy5fZWxlbWVudCkudHJpZ2dlcihzbGlkRXZlbnQpLCAwKVxuXG4gICAgICAgICAgfSlcbiAgICAgICAgICAuZW11bGF0ZVRyYW5zaXRpb25FbmQoVFJBTlNJVElPTl9EVVJBVElPTilcblxuICAgICAgfSBlbHNlIHtcbiAgICAgICAgJChhY3RpdmVFbGVtZW50KS5yZW1vdmVDbGFzcyhDbGFzc05hbWUuQUNUSVZFKVxuICAgICAgICAkKG5leHRFbGVtZW50KS5hZGRDbGFzcyhDbGFzc05hbWUuQUNUSVZFKVxuXG4gICAgICAgIHRoaXMuX2lzU2xpZGluZyA9IGZhbHNlXG4gICAgICAgICQodGhpcy5fZWxlbWVudCkudHJpZ2dlcihzbGlkRXZlbnQpXG4gICAgICB9XG5cbiAgICAgIGlmIChpc0N5Y2xpbmcpIHtcbiAgICAgICAgdGhpcy5jeWNsZSgpXG4gICAgICB9XG4gICAgfVxuXG5cbiAgICAvLyBzdGF0aWNcblxuICAgIHN0YXRpYyBfalF1ZXJ5SW50ZXJmYWNlKGNvbmZpZykge1xuICAgICAgcmV0dXJuIHRoaXMuZWFjaChmdW5jdGlvbiAoKSB7XG4gICAgICAgIGxldCBkYXRhICAgICAgPSAkKHRoaXMpLmRhdGEoREFUQV9LRVkpXG4gICAgICAgIGNvbnN0IF9jb25maWcgPSAkLmV4dGVuZCh7fSwgRGVmYXVsdCwgJCh0aGlzKS5kYXRhKCkpXG5cbiAgICAgICAgaWYgKHR5cGVvZiBjb25maWcgPT09ICdvYmplY3QnKSB7XG4gICAgICAgICAgJC5leHRlbmQoX2NvbmZpZywgY29uZmlnKVxuICAgICAgICB9XG5cbiAgICAgICAgY29uc3QgYWN0aW9uID0gdHlwZW9mIGNvbmZpZyA9PT0gJ3N0cmluZycgPyBjb25maWcgOiBfY29uZmlnLnNsaWRlXG5cbiAgICAgICAgaWYgKCFkYXRhKSB7XG4gICAgICAgICAgZGF0YSA9IG5ldyBDYXJvdXNlbCh0aGlzLCBfY29uZmlnKVxuICAgICAgICAgICQodGhpcykuZGF0YShEQVRBX0tFWSwgZGF0YSlcbiAgICAgICAgfVxuXG4gICAgICAgIGlmICh0eXBlb2YgY29uZmlnID09PSAnbnVtYmVyJykge1xuICAgICAgICAgIGRhdGEudG8oY29uZmlnKVxuICAgICAgICB9IGVsc2UgaWYgKHR5cGVvZiBhY3Rpb24gPT09ICdzdHJpbmcnKSB7XG4gICAgICAgICAgaWYgKGRhdGFbYWN0aW9uXSA9PT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICB0aHJvdyBuZXcgRXJyb3IoYE5vIG1ldGhvZCBuYW1lZCBcIiR7YWN0aW9ufVwiYClcbiAgICAgICAgICB9XG4gICAgICAgICAgZGF0YVthY3Rpb25dKClcbiAgICAgICAgfSBlbHNlIGlmIChfY29uZmlnLmludGVydmFsKSB7XG4gICAgICAgICAgZGF0YS5wYXVzZSgpXG4gICAgICAgICAgZGF0YS5jeWNsZSgpXG4gICAgICAgIH1cbiAgICAgIH0pXG4gICAgfVxuXG4gICAgc3RhdGljIF9kYXRhQXBpQ2xpY2tIYW5kbGVyKGV2ZW50KSB7XG4gICAgICBjb25zdCBzZWxlY3RvciA9IFV0aWwuZ2V0U2VsZWN0b3JGcm9tRWxlbWVudCh0aGlzKVxuXG4gICAgICBpZiAoIXNlbGVjdG9yKSB7XG4gICAgICAgIHJldHVyblxuICAgICAgfVxuXG4gICAgICBjb25zdCB0YXJnZXQgPSAkKHNlbGVjdG9yKVswXVxuXG4gICAgICBpZiAoIXRhcmdldCB8fCAhJCh0YXJnZXQpLmhhc0NsYXNzKENsYXNzTmFtZS5DQVJPVVNFTCkpIHtcbiAgICAgICAgcmV0dXJuXG4gICAgICB9XG5cbiAgICAgIGNvbnN0IGNvbmZpZyAgICAgPSAkLmV4dGVuZCh7fSwgJCh0YXJnZXQpLmRhdGEoKSwgJCh0aGlzKS5kYXRhKCkpXG4gICAgICBjb25zdCBzbGlkZUluZGV4ID0gdGhpcy5nZXRBdHRyaWJ1dGUoJ2RhdGEtc2xpZGUtdG8nKVxuXG4gICAgICBpZiAoc2xpZGVJbmRleCkge1xuICAgICAgICBjb25maWcuaW50ZXJ2YWwgPSBmYWxzZVxuICAgICAgfVxuXG4gICAgICBDYXJvdXNlbC5falF1ZXJ5SW50ZXJmYWNlLmNhbGwoJCh0YXJnZXQpLCBjb25maWcpXG5cbiAgICAgIGlmIChzbGlkZUluZGV4KSB7XG4gICAgICAgICQodGFyZ2V0KS5kYXRhKERBVEFfS0VZKS50byhzbGlkZUluZGV4KVxuICAgICAgfVxuXG4gICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpXG4gICAgfVxuXG4gIH1cblxuXG4gIC8qKlxuICAgKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiAgICogRGF0YSBBcGkgaW1wbGVtZW50YXRpb25cbiAgICogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gICAqL1xuXG4gICQoZG9jdW1lbnQpXG4gICAgLm9uKEV2ZW50LkNMSUNLX0RBVEFfQVBJLCBTZWxlY3Rvci5EQVRBX1NMSURFLCBDYXJvdXNlbC5fZGF0YUFwaUNsaWNrSGFuZGxlcilcblxuICAkKHdpbmRvdykub24oRXZlbnQuTE9BRF9EQVRBX0FQSSwgKCkgPT4ge1xuICAgICQoU2VsZWN0b3IuREFUQV9SSURFKS5lYWNoKGZ1bmN0aW9uICgpIHtcbiAgICAgIGNvbnN0ICRjYXJvdXNlbCA9ICQodGhpcylcbiAgICAgIENhcm91c2VsLl9qUXVlcnlJbnRlcmZhY2UuY2FsbCgkY2Fyb3VzZWwsICRjYXJvdXNlbC5kYXRhKCkpXG4gICAgfSlcbiAgfSlcblxuXG4gIC8qKlxuICAgKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiAgICogalF1ZXJ5XG4gICAqIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuICAgKi9cblxuICAkLmZuW05BTUVdICAgICAgICAgICAgID0gQ2Fyb3VzZWwuX2pRdWVyeUludGVyZmFjZVxuICAkLmZuW05BTUVdLkNvbnN0cnVjdG9yID0gQ2Fyb3VzZWxcbiAgJC5mbltOQU1FXS5ub0NvbmZsaWN0ICA9IGZ1bmN0aW9uICgpIHtcbiAgICAkLmZuW05BTUVdID0gSlFVRVJZX05PX0NPTkZMSUNUXG4gICAgcmV0dXJuIENhcm91c2VsLl9qUXVlcnlJbnRlcmZhY2VcbiAgfVxuXG4gIHJldHVybiBDYXJvdXNlbFxuXG59KShqUXVlcnkpXG5cbmV4cG9ydCBkZWZhdWx0IENhcm91c2VsXG4iLCJpbXBvcnQgVXRpbCBmcm9tICcuL3V0aWwnXG5cblxuLyoqXG4gKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuICogQm9vdHN0cmFwICh2NC4wLjAtYmV0YSk6IGNvbGxhcHNlLmpzXG4gKiBMaWNlbnNlZCB1bmRlciBNSVQgKGh0dHBzOi8vZ2l0aHViLmNvbS90d2JzL2Jvb3RzdHJhcC9ibG9iL21hc3Rlci9MSUNFTlNFKVxuICogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiAqL1xuXG5jb25zdCBDb2xsYXBzZSA9ICgoJCkgPT4ge1xuXG5cbiAgLyoqXG4gICAqIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuICAgKiBDb25zdGFudHNcbiAgICogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gICAqL1xuXG4gIGNvbnN0IE5BTUUgICAgICAgICAgICAgICAgPSAnY29sbGFwc2UnXG4gIGNvbnN0IFZFUlNJT04gICAgICAgICAgICAgPSAnNC4wLjAtYmV0YSdcbiAgY29uc3QgREFUQV9LRVkgICAgICAgICAgICA9ICdicy5jb2xsYXBzZSdcbiAgY29uc3QgRVZFTlRfS0VZICAgICAgICAgICA9IGAuJHtEQVRBX0tFWX1gXG4gIGNvbnN0IERBVEFfQVBJX0tFWSAgICAgICAgPSAnLmRhdGEtYXBpJ1xuICBjb25zdCBKUVVFUllfTk9fQ09ORkxJQ1QgID0gJC5mbltOQU1FXVxuICBjb25zdCBUUkFOU0lUSU9OX0RVUkFUSU9OID0gNjAwXG5cbiAgY29uc3QgRGVmYXVsdCA9IHtcbiAgICB0b2dnbGUgOiB0cnVlLFxuICAgIHBhcmVudCA6ICcnXG4gIH1cblxuICBjb25zdCBEZWZhdWx0VHlwZSA9IHtcbiAgICB0b2dnbGUgOiAnYm9vbGVhbicsXG4gICAgcGFyZW50IDogJ3N0cmluZydcbiAgfVxuXG4gIGNvbnN0IEV2ZW50ID0ge1xuICAgIFNIT1cgICAgICAgICAgIDogYHNob3cke0VWRU5UX0tFWX1gLFxuICAgIFNIT1dOICAgICAgICAgIDogYHNob3duJHtFVkVOVF9LRVl9YCxcbiAgICBISURFICAgICAgICAgICA6IGBoaWRlJHtFVkVOVF9LRVl9YCxcbiAgICBISURERU4gICAgICAgICA6IGBoaWRkZW4ke0VWRU5UX0tFWX1gLFxuICAgIENMSUNLX0RBVEFfQVBJIDogYGNsaWNrJHtFVkVOVF9LRVl9JHtEQVRBX0FQSV9LRVl9YFxuICB9XG5cbiAgY29uc3QgQ2xhc3NOYW1lID0ge1xuICAgIFNIT1cgICAgICAgOiAnc2hvdycsXG4gICAgQ09MTEFQU0UgICA6ICdjb2xsYXBzZScsXG4gICAgQ09MTEFQU0lORyA6ICdjb2xsYXBzaW5nJyxcbiAgICBDT0xMQVBTRUQgIDogJ2NvbGxhcHNlZCdcbiAgfVxuXG4gIGNvbnN0IERpbWVuc2lvbiA9IHtcbiAgICBXSURUSCAgOiAnd2lkdGgnLFxuICAgIEhFSUdIVCA6ICdoZWlnaHQnXG4gIH1cblxuICBjb25zdCBTZWxlY3RvciA9IHtcbiAgICBBQ1RJVkVTICAgICA6ICcuc2hvdywgLmNvbGxhcHNpbmcnLFxuICAgIERBVEFfVE9HR0xFIDogJ1tkYXRhLXRvZ2dsZT1cImNvbGxhcHNlXCJdJ1xuICB9XG5cblxuICAvKipcbiAgICogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gICAqIENsYXNzIERlZmluaXRpb25cbiAgICogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gICAqL1xuXG4gIGNsYXNzIENvbGxhcHNlIHtcblxuICAgIGNvbnN0cnVjdG9yKGVsZW1lbnQsIGNvbmZpZykge1xuICAgICAgdGhpcy5faXNUcmFuc2l0aW9uaW5nID0gZmFsc2VcbiAgICAgIHRoaXMuX2VsZW1lbnQgICAgICAgICA9IGVsZW1lbnRcbiAgICAgIHRoaXMuX2NvbmZpZyAgICAgICAgICA9IHRoaXMuX2dldENvbmZpZyhjb25maWcpXG4gICAgICB0aGlzLl90cmlnZ2VyQXJyYXkgICAgPSAkLm1ha2VBcnJheSgkKFxuICAgICAgICBgW2RhdGEtdG9nZ2xlPVwiY29sbGFwc2VcIl1baHJlZj1cIiMke2VsZW1lbnQuaWR9XCJdLGAgK1xuICAgICAgICBgW2RhdGEtdG9nZ2xlPVwiY29sbGFwc2VcIl1bZGF0YS10YXJnZXQ9XCIjJHtlbGVtZW50LmlkfVwiXWBcbiAgICAgICkpXG4gICAgICBjb25zdCB0YWJUb2dnbGVzID0gJChTZWxlY3Rvci5EQVRBX1RPR0dMRSlcbiAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgdGFiVG9nZ2xlcy5sZW5ndGg7IGkrKykge1xuICAgICAgICBjb25zdCBlbGVtID0gdGFiVG9nZ2xlc1tpXVxuICAgICAgICBjb25zdCBzZWxlY3RvciA9IFV0aWwuZ2V0U2VsZWN0b3JGcm9tRWxlbWVudChlbGVtKVxuICAgICAgICBpZiAoc2VsZWN0b3IgIT09IG51bGwgJiYgJChzZWxlY3RvcikuZmlsdGVyKGVsZW1lbnQpLmxlbmd0aCA+IDApIHtcbiAgICAgICAgICB0aGlzLl90cmlnZ2VyQXJyYXkucHVzaChlbGVtKVxuICAgICAgICB9XG4gICAgICB9XG5cbiAgICAgIHRoaXMuX3BhcmVudCA9IHRoaXMuX2NvbmZpZy5wYXJlbnQgPyB0aGlzLl9nZXRQYXJlbnQoKSA6IG51bGxcblxuICAgICAgaWYgKCF0aGlzLl9jb25maWcucGFyZW50KSB7XG4gICAgICAgIHRoaXMuX2FkZEFyaWFBbmRDb2xsYXBzZWRDbGFzcyh0aGlzLl9lbGVtZW50LCB0aGlzLl90cmlnZ2VyQXJyYXkpXG4gICAgICB9XG5cbiAgICAgIGlmICh0aGlzLl9jb25maWcudG9nZ2xlKSB7XG4gICAgICAgIHRoaXMudG9nZ2xlKClcbiAgICAgIH1cbiAgICB9XG5cblxuICAgIC8vIGdldHRlcnNcblxuICAgIHN0YXRpYyBnZXQgVkVSU0lPTigpIHtcbiAgICAgIHJldHVybiBWRVJTSU9OXG4gICAgfVxuXG4gICAgc3RhdGljIGdldCBEZWZhdWx0KCkge1xuICAgICAgcmV0dXJuIERlZmF1bHRcbiAgICB9XG5cblxuICAgIC8vIHB1YmxpY1xuXG4gICAgdG9nZ2xlKCkge1xuICAgICAgaWYgKCQodGhpcy5fZWxlbWVudCkuaGFzQ2xhc3MoQ2xhc3NOYW1lLlNIT1cpKSB7XG4gICAgICAgIHRoaXMuaGlkZSgpXG4gICAgICB9IGVsc2Uge1xuICAgICAgICB0aGlzLnNob3coKVxuICAgICAgfVxuICAgIH1cblxuICAgIHNob3coKSB7XG4gICAgICBpZiAodGhpcy5faXNUcmFuc2l0aW9uaW5nIHx8XG4gICAgICAgICQodGhpcy5fZWxlbWVudCkuaGFzQ2xhc3MoQ2xhc3NOYW1lLlNIT1cpKSB7XG4gICAgICAgIHJldHVyblxuICAgICAgfVxuXG4gICAgICBsZXQgYWN0aXZlc1xuICAgICAgbGV0IGFjdGl2ZXNEYXRhXG5cbiAgICAgIGlmICh0aGlzLl9wYXJlbnQpIHtcbiAgICAgICAgYWN0aXZlcyA9ICQubWFrZUFycmF5KCQodGhpcy5fcGFyZW50KS5jaGlsZHJlbigpLmNoaWxkcmVuKFNlbGVjdG9yLkFDVElWRVMpKVxuICAgICAgICBpZiAoIWFjdGl2ZXMubGVuZ3RoKSB7XG4gICAgICAgICAgYWN0aXZlcyA9IG51bGxcbiAgICAgICAgfVxuICAgICAgfVxuXG4gICAgICBpZiAoYWN0aXZlcykge1xuICAgICAgICBhY3RpdmVzRGF0YSA9ICQoYWN0aXZlcykuZGF0YShEQVRBX0tFWSlcbiAgICAgICAgaWYgKGFjdGl2ZXNEYXRhICYmIGFjdGl2ZXNEYXRhLl9pc1RyYW5zaXRpb25pbmcpIHtcbiAgICAgICAgICByZXR1cm5cbiAgICAgICAgfVxuICAgICAgfVxuXG4gICAgICBjb25zdCBzdGFydEV2ZW50ID0gJC5FdmVudChFdmVudC5TSE9XKVxuICAgICAgJCh0aGlzLl9lbGVtZW50KS50cmlnZ2VyKHN0YXJ0RXZlbnQpXG4gICAgICBpZiAoc3RhcnRFdmVudC5pc0RlZmF1bHRQcmV2ZW50ZWQoKSkge1xuICAgICAgICByZXR1cm5cbiAgICAgIH1cblxuICAgICAgaWYgKGFjdGl2ZXMpIHtcbiAgICAgICAgQ29sbGFwc2UuX2pRdWVyeUludGVyZmFjZS5jYWxsKCQoYWN0aXZlcyksICdoaWRlJylcbiAgICAgICAgaWYgKCFhY3RpdmVzRGF0YSkge1xuICAgICAgICAgICQoYWN0aXZlcykuZGF0YShEQVRBX0tFWSwgbnVsbClcbiAgICAgICAgfVxuICAgICAgfVxuXG4gICAgICBjb25zdCBkaW1lbnNpb24gPSB0aGlzLl9nZXREaW1lbnNpb24oKVxuXG4gICAgICAkKHRoaXMuX2VsZW1lbnQpXG4gICAgICAgIC5yZW1vdmVDbGFzcyhDbGFzc05hbWUuQ09MTEFQU0UpXG4gICAgICAgIC5hZGRDbGFzcyhDbGFzc05hbWUuQ09MTEFQU0lORylcblxuICAgICAgdGhpcy5fZWxlbWVudC5zdHlsZVtkaW1lbnNpb25dID0gMFxuXG4gICAgICBpZiAodGhpcy5fdHJpZ2dlckFycmF5Lmxlbmd0aCkge1xuICAgICAgICAkKHRoaXMuX3RyaWdnZXJBcnJheSlcbiAgICAgICAgICAucmVtb3ZlQ2xhc3MoQ2xhc3NOYW1lLkNPTExBUFNFRClcbiAgICAgICAgICAuYXR0cignYXJpYS1leHBhbmRlZCcsIHRydWUpXG4gICAgICB9XG5cbiAgICAgIHRoaXMuc2V0VHJhbnNpdGlvbmluZyh0cnVlKVxuXG4gICAgICBjb25zdCBjb21wbGV0ZSA9ICgpID0+IHtcbiAgICAgICAgJCh0aGlzLl9lbGVtZW50KVxuICAgICAgICAgIC5yZW1vdmVDbGFzcyhDbGFzc05hbWUuQ09MTEFQU0lORylcbiAgICAgICAgICAuYWRkQ2xhc3MoQ2xhc3NOYW1lLkNPTExBUFNFKVxuICAgICAgICAgIC5hZGRDbGFzcyhDbGFzc05hbWUuU0hPVylcblxuICAgICAgICB0aGlzLl9lbGVtZW50LnN0eWxlW2RpbWVuc2lvbl0gPSAnJ1xuXG4gICAgICAgIHRoaXMuc2V0VHJhbnNpdGlvbmluZyhmYWxzZSlcblxuICAgICAgICAkKHRoaXMuX2VsZW1lbnQpLnRyaWdnZXIoRXZlbnQuU0hPV04pXG4gICAgICB9XG5cbiAgICAgIGlmICghVXRpbC5zdXBwb3J0c1RyYW5zaXRpb25FbmQoKSkge1xuICAgICAgICBjb21wbGV0ZSgpXG4gICAgICAgIHJldHVyblxuICAgICAgfVxuXG4gICAgICBjb25zdCBjYXBpdGFsaXplZERpbWVuc2lvbiA9IGRpbWVuc2lvblswXS50b1VwcGVyQ2FzZSgpICsgZGltZW5zaW9uLnNsaWNlKDEpXG4gICAgICBjb25zdCBzY3JvbGxTaXplICAgICAgICAgICA9IGBzY3JvbGwke2NhcGl0YWxpemVkRGltZW5zaW9ufWBcblxuICAgICAgJCh0aGlzLl9lbGVtZW50KVxuICAgICAgICAub25lKFV0aWwuVFJBTlNJVElPTl9FTkQsIGNvbXBsZXRlKVxuICAgICAgICAuZW11bGF0ZVRyYW5zaXRpb25FbmQoVFJBTlNJVElPTl9EVVJBVElPTilcblxuICAgICAgdGhpcy5fZWxlbWVudC5zdHlsZVtkaW1lbnNpb25dID0gYCR7dGhpcy5fZWxlbWVudFtzY3JvbGxTaXplXX1weGBcbiAgICB9XG5cbiAgICBoaWRlKCkge1xuICAgICAgaWYgKHRoaXMuX2lzVHJhbnNpdGlvbmluZyB8fFxuICAgICAgICAhJCh0aGlzLl9lbGVtZW50KS5oYXNDbGFzcyhDbGFzc05hbWUuU0hPVykpIHtcbiAgICAgICAgcmV0dXJuXG4gICAgICB9XG5cbiAgICAgIGNvbnN0IHN0YXJ0RXZlbnQgPSAkLkV2ZW50KEV2ZW50LkhJREUpXG4gICAgICAkKHRoaXMuX2VsZW1lbnQpLnRyaWdnZXIoc3RhcnRFdmVudClcbiAgICAgIGlmIChzdGFydEV2ZW50LmlzRGVmYXVsdFByZXZlbnRlZCgpKSB7XG4gICAgICAgIHJldHVyblxuICAgICAgfVxuXG4gICAgICBjb25zdCBkaW1lbnNpb24gICAgICAgPSB0aGlzLl9nZXREaW1lbnNpb24oKVxuXG4gICAgICB0aGlzLl9lbGVtZW50LnN0eWxlW2RpbWVuc2lvbl0gPSBgJHt0aGlzLl9lbGVtZW50LmdldEJvdW5kaW5nQ2xpZW50UmVjdCgpW2RpbWVuc2lvbl19cHhgXG5cbiAgICAgIFV0aWwucmVmbG93KHRoaXMuX2VsZW1lbnQpXG5cbiAgICAgICQodGhpcy5fZWxlbWVudClcbiAgICAgICAgLmFkZENsYXNzKENsYXNzTmFtZS5DT0xMQVBTSU5HKVxuICAgICAgICAucmVtb3ZlQ2xhc3MoQ2xhc3NOYW1lLkNPTExBUFNFKVxuICAgICAgICAucmVtb3ZlQ2xhc3MoQ2xhc3NOYW1lLlNIT1cpXG5cbiAgICAgIGlmICh0aGlzLl90cmlnZ2VyQXJyYXkubGVuZ3RoKSB7XG4gICAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgdGhpcy5fdHJpZ2dlckFycmF5Lmxlbmd0aDsgaSsrKSB7XG4gICAgICAgICAgY29uc3QgdHJpZ2dlciA9IHRoaXMuX3RyaWdnZXJBcnJheVtpXVxuICAgICAgICAgIGNvbnN0IHNlbGVjdG9yID0gVXRpbC5nZXRTZWxlY3RvckZyb21FbGVtZW50KHRyaWdnZXIpXG4gICAgICAgICAgaWYgKHNlbGVjdG9yICE9PSBudWxsKSB7XG4gICAgICAgICAgICBjb25zdCAkZWxlbSA9ICQoc2VsZWN0b3IpXG4gICAgICAgICAgICBpZiAoISRlbGVtLmhhc0NsYXNzKENsYXNzTmFtZS5TSE9XKSkge1xuICAgICAgICAgICAgICAkKHRyaWdnZXIpLmFkZENsYXNzKENsYXNzTmFtZS5DT0xMQVBTRUQpXG4gICAgICAgICAgICAgICAgICAgLmF0dHIoJ2FyaWEtZXhwYW5kZWQnLCBmYWxzZSlcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgIH1cblxuICAgICAgdGhpcy5zZXRUcmFuc2l0aW9uaW5nKHRydWUpXG5cbiAgICAgIGNvbnN0IGNvbXBsZXRlID0gKCkgPT4ge1xuICAgICAgICB0aGlzLnNldFRyYW5zaXRpb25pbmcoZmFsc2UpXG4gICAgICAgICQodGhpcy5fZWxlbWVudClcbiAgICAgICAgICAucmVtb3ZlQ2xhc3MoQ2xhc3NOYW1lLkNPTExBUFNJTkcpXG4gICAgICAgICAgLmFkZENsYXNzKENsYXNzTmFtZS5DT0xMQVBTRSlcbiAgICAgICAgICAudHJpZ2dlcihFdmVudC5ISURERU4pXG4gICAgICB9XG5cbiAgICAgIHRoaXMuX2VsZW1lbnQuc3R5bGVbZGltZW5zaW9uXSA9ICcnXG5cbiAgICAgIGlmICghVXRpbC5zdXBwb3J0c1RyYW5zaXRpb25FbmQoKSkge1xuICAgICAgICBjb21wbGV0ZSgpXG4gICAgICAgIHJldHVyblxuICAgICAgfVxuXG4gICAgICAkKHRoaXMuX2VsZW1lbnQpXG4gICAgICAgIC5vbmUoVXRpbC5UUkFOU0lUSU9OX0VORCwgY29tcGxldGUpXG4gICAgICAgIC5lbXVsYXRlVHJhbnNpdGlvbkVuZChUUkFOU0lUSU9OX0RVUkFUSU9OKVxuICAgIH1cblxuICAgIHNldFRyYW5zaXRpb25pbmcoaXNUcmFuc2l0aW9uaW5nKSB7XG4gICAgICB0aGlzLl9pc1RyYW5zaXRpb25pbmcgPSBpc1RyYW5zaXRpb25pbmdcbiAgICB9XG5cbiAgICBkaXNwb3NlKCkge1xuICAgICAgJC5yZW1vdmVEYXRhKHRoaXMuX2VsZW1lbnQsIERBVEFfS0VZKVxuXG4gICAgICB0aGlzLl9jb25maWcgICAgICAgICAgPSBudWxsXG4gICAgICB0aGlzLl9wYXJlbnQgICAgICAgICAgPSBudWxsXG4gICAgICB0aGlzLl9lbGVtZW50ICAgICAgICAgPSBudWxsXG4gICAgICB0aGlzLl90cmlnZ2VyQXJyYXkgICAgPSBudWxsXG4gICAgICB0aGlzLl9pc1RyYW5zaXRpb25pbmcgPSBudWxsXG4gICAgfVxuXG5cbiAgICAvLyBwcml2YXRlXG5cbiAgICBfZ2V0Q29uZmlnKGNvbmZpZykge1xuICAgICAgY29uZmlnID0gJC5leHRlbmQoe30sIERlZmF1bHQsIGNvbmZpZylcbiAgICAgIGNvbmZpZy50b2dnbGUgPSBCb29sZWFuKGNvbmZpZy50b2dnbGUpIC8vIGNvZXJjZSBzdHJpbmcgdmFsdWVzXG4gICAgICBVdGlsLnR5cGVDaGVja0NvbmZpZyhOQU1FLCBjb25maWcsIERlZmF1bHRUeXBlKVxuICAgICAgcmV0dXJuIGNvbmZpZ1xuICAgIH1cblxuICAgIF9nZXREaW1lbnNpb24oKSB7XG4gICAgICBjb25zdCBoYXNXaWR0aCA9ICQodGhpcy5fZWxlbWVudCkuaGFzQ2xhc3MoRGltZW5zaW9uLldJRFRIKVxuICAgICAgcmV0dXJuIGhhc1dpZHRoID8gRGltZW5zaW9uLldJRFRIIDogRGltZW5zaW9uLkhFSUdIVFxuICAgIH1cblxuICAgIF9nZXRQYXJlbnQoKSB7XG4gICAgICBjb25zdCBwYXJlbnQgICA9ICQodGhpcy5fY29uZmlnLnBhcmVudClbMF1cbiAgICAgIGNvbnN0IHNlbGVjdG9yID1cbiAgICAgICAgYFtkYXRhLXRvZ2dsZT1cImNvbGxhcHNlXCJdW2RhdGEtcGFyZW50PVwiJHt0aGlzLl9jb25maWcucGFyZW50fVwiXWBcblxuICAgICAgJChwYXJlbnQpLmZpbmQoc2VsZWN0b3IpLmVhY2goKGksIGVsZW1lbnQpID0+IHtcbiAgICAgICAgdGhpcy5fYWRkQXJpYUFuZENvbGxhcHNlZENsYXNzKFxuICAgICAgICAgIENvbGxhcHNlLl9nZXRUYXJnZXRGcm9tRWxlbWVudChlbGVtZW50KSxcbiAgICAgICAgICBbZWxlbWVudF1cbiAgICAgICAgKVxuICAgICAgfSlcblxuICAgICAgcmV0dXJuIHBhcmVudFxuICAgIH1cblxuICAgIF9hZGRBcmlhQW5kQ29sbGFwc2VkQ2xhc3MoZWxlbWVudCwgdHJpZ2dlckFycmF5KSB7XG4gICAgICBpZiAoZWxlbWVudCkge1xuICAgICAgICBjb25zdCBpc09wZW4gPSAkKGVsZW1lbnQpLmhhc0NsYXNzKENsYXNzTmFtZS5TSE9XKVxuXG4gICAgICAgIGlmICh0cmlnZ2VyQXJyYXkubGVuZ3RoKSB7XG4gICAgICAgICAgJCh0cmlnZ2VyQXJyYXkpXG4gICAgICAgICAgICAudG9nZ2xlQ2xhc3MoQ2xhc3NOYW1lLkNPTExBUFNFRCwgIWlzT3BlbilcbiAgICAgICAgICAgIC5hdHRyKCdhcmlhLWV4cGFuZGVkJywgaXNPcGVuKVxuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuXG5cbiAgICAvLyBzdGF0aWNcblxuICAgIHN0YXRpYyBfZ2V0VGFyZ2V0RnJvbUVsZW1lbnQoZWxlbWVudCkge1xuICAgICAgY29uc3Qgc2VsZWN0b3IgPSBVdGlsLmdldFNlbGVjdG9yRnJvbUVsZW1lbnQoZWxlbWVudClcbiAgICAgIHJldHVybiBzZWxlY3RvciA/ICQoc2VsZWN0b3IpWzBdIDogbnVsbFxuICAgIH1cblxuICAgIHN0YXRpYyBfalF1ZXJ5SW50ZXJmYWNlKGNvbmZpZykge1xuICAgICAgcmV0dXJuIHRoaXMuZWFjaChmdW5jdGlvbiAoKSB7XG4gICAgICAgIGNvbnN0ICR0aGlzICAgPSAkKHRoaXMpXG4gICAgICAgIGxldCBkYXRhICAgICAgPSAkdGhpcy5kYXRhKERBVEFfS0VZKVxuICAgICAgICBjb25zdCBfY29uZmlnID0gJC5leHRlbmQoXG4gICAgICAgICAge30sXG4gICAgICAgICAgRGVmYXVsdCxcbiAgICAgICAgICAkdGhpcy5kYXRhKCksXG4gICAgICAgICAgdHlwZW9mIGNvbmZpZyA9PT0gJ29iamVjdCcgJiYgY29uZmlnXG4gICAgICAgIClcblxuICAgICAgICBpZiAoIWRhdGEgJiYgX2NvbmZpZy50b2dnbGUgJiYgL3Nob3d8aGlkZS8udGVzdChjb25maWcpKSB7XG4gICAgICAgICAgX2NvbmZpZy50b2dnbGUgPSBmYWxzZVxuICAgICAgICB9XG5cbiAgICAgICAgaWYgKCFkYXRhKSB7XG4gICAgICAgICAgZGF0YSA9IG5ldyBDb2xsYXBzZSh0aGlzLCBfY29uZmlnKVxuICAgICAgICAgICR0aGlzLmRhdGEoREFUQV9LRVksIGRhdGEpXG4gICAgICAgIH1cblxuICAgICAgICBpZiAodHlwZW9mIGNvbmZpZyA9PT0gJ3N0cmluZycpIHtcbiAgICAgICAgICBpZiAoZGF0YVtjb25maWddID09PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgIHRocm93IG5ldyBFcnJvcihgTm8gbWV0aG9kIG5hbWVkIFwiJHtjb25maWd9XCJgKVxuICAgICAgICAgIH1cbiAgICAgICAgICBkYXRhW2NvbmZpZ10oKVxuICAgICAgICB9XG4gICAgICB9KVxuICAgIH1cblxuICB9XG5cblxuICAvKipcbiAgICogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gICAqIERhdGEgQXBpIGltcGxlbWVudGF0aW9uXG4gICAqIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuICAgKi9cblxuICAkKGRvY3VtZW50KS5vbihFdmVudC5DTElDS19EQVRBX0FQSSwgU2VsZWN0b3IuREFUQV9UT0dHTEUsIGZ1bmN0aW9uIChldmVudCkge1xuICAgIGlmICghL2lucHV0fHRleHRhcmVhL2kudGVzdChldmVudC50YXJnZXQudGFnTmFtZSkpIHtcbiAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KClcbiAgICB9XG5cbiAgICBjb25zdCAkdHJpZ2dlciA9ICQodGhpcylcbiAgICBjb25zdCBzZWxlY3RvciA9IFV0aWwuZ2V0U2VsZWN0b3JGcm9tRWxlbWVudCh0aGlzKVxuICAgICQoc2VsZWN0b3IpLmVhY2goZnVuY3Rpb24gKCkge1xuICAgICAgY29uc3QgJHRhcmdldCA9ICQodGhpcylcbiAgICAgIGNvbnN0IGRhdGEgICAgPSAkdGFyZ2V0LmRhdGEoREFUQV9LRVkpXG4gICAgICBjb25zdCBjb25maWcgID0gZGF0YSA/ICd0b2dnbGUnIDogJHRyaWdnZXIuZGF0YSgpXG4gICAgICBDb2xsYXBzZS5falF1ZXJ5SW50ZXJmYWNlLmNhbGwoJHRhcmdldCwgY29uZmlnKVxuICAgIH0pXG4gIH0pXG5cblxuICAvKipcbiAgICogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gICAqIGpRdWVyeVxuICAgKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiAgICovXG5cbiAgJC5mbltOQU1FXSAgICAgICAgICAgICA9IENvbGxhcHNlLl9qUXVlcnlJbnRlcmZhY2VcbiAgJC5mbltOQU1FXS5Db25zdHJ1Y3RvciA9IENvbGxhcHNlXG4gICQuZm5bTkFNRV0ubm9Db25mbGljdCAgPSBmdW5jdGlvbiAoKSB7XG4gICAgJC5mbltOQU1FXSA9IEpRVUVSWV9OT19DT05GTElDVFxuICAgIHJldHVybiBDb2xsYXBzZS5falF1ZXJ5SW50ZXJmYWNlXG4gIH1cblxuICByZXR1cm4gQ29sbGFwc2VcblxufSkoalF1ZXJ5KVxuXG5leHBvcnQgZGVmYXVsdCBDb2xsYXBzZVxuIiwiLyogZ2xvYmFsIFBvcHBlciAqL1xuXG5pbXBvcnQgVXRpbCBmcm9tICcuL3V0aWwnXG5cblxuLyoqXG4gKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuICogQm9vdHN0cmFwICh2NC4wLjAtYmV0YSk6IGRyb3Bkb3duLmpzXG4gKiBMaWNlbnNlZCB1bmRlciBNSVQgKGh0dHBzOi8vZ2l0aHViLmNvbS90d2JzL2Jvb3RzdHJhcC9ibG9iL21hc3Rlci9MSUNFTlNFKVxuICogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiAqL1xuXG5jb25zdCBEcm9wZG93biA9ICgoJCkgPT4ge1xuXG4gIC8qKlxuICAgKiBDaGVjayBmb3IgUG9wcGVyIGRlcGVuZGVuY3lcbiAgICogUG9wcGVyIC0gaHR0cHM6Ly9wb3BwZXIuanMub3JnXG4gICAqL1xuICBpZiAodHlwZW9mIFBvcHBlciA9PT0gJ3VuZGVmaW5lZCcpIHtcbiAgICB0aHJvdyBuZXcgRXJyb3IoJ0Jvb3RzdHJhcCBkcm9wZG93biByZXF1aXJlIFBvcHBlci5qcyAoaHR0cHM6Ly9wb3BwZXIuanMub3JnKScpXG4gIH1cblxuICAvKipcbiAgICogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gICAqIENvbnN0YW50c1xuICAgKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiAgICovXG5cbiAgY29uc3QgTkFNRSAgICAgICAgICAgICAgICAgICAgID0gJ2Ryb3Bkb3duJ1xuICBjb25zdCBWRVJTSU9OICAgICAgICAgICAgICAgICAgPSAnNC4wLjAtYmV0YSdcbiAgY29uc3QgREFUQV9LRVkgICAgICAgICAgICAgICAgID0gJ2JzLmRyb3Bkb3duJ1xuICBjb25zdCBFVkVOVF9LRVkgICAgICAgICAgICAgICAgPSBgLiR7REFUQV9LRVl9YFxuICBjb25zdCBEQVRBX0FQSV9LRVkgICAgICAgICAgICAgPSAnLmRhdGEtYXBpJ1xuICBjb25zdCBKUVVFUllfTk9fQ09ORkxJQ1QgICAgICAgPSAkLmZuW05BTUVdXG4gIGNvbnN0IEVTQ0FQRV9LRVlDT0RFICAgICAgICAgICA9IDI3IC8vIEtleWJvYXJkRXZlbnQud2hpY2ggdmFsdWUgZm9yIEVzY2FwZSAoRXNjKSBrZXlcbiAgY29uc3QgU1BBQ0VfS0VZQ09ERSAgICAgICAgICAgID0gMzIgLy8gS2V5Ym9hcmRFdmVudC53aGljaCB2YWx1ZSBmb3Igc3BhY2Uga2V5XG4gIGNvbnN0IFRBQl9LRVlDT0RFICAgICAgICAgICAgICA9IDkgLy8gS2V5Ym9hcmRFdmVudC53aGljaCB2YWx1ZSBmb3IgdGFiIGtleVxuICBjb25zdCBBUlJPV19VUF9LRVlDT0RFICAgICAgICAgPSAzOCAvLyBLZXlib2FyZEV2ZW50LndoaWNoIHZhbHVlIGZvciB1cCBhcnJvdyBrZXlcbiAgY29uc3QgQVJST1dfRE9XTl9LRVlDT0RFICAgICAgID0gNDAgLy8gS2V5Ym9hcmRFdmVudC53aGljaCB2YWx1ZSBmb3IgZG93biBhcnJvdyBrZXlcbiAgY29uc3QgUklHSFRfTU9VU0VfQlVUVE9OX1dISUNIID0gMyAvLyBNb3VzZUV2ZW50LndoaWNoIHZhbHVlIGZvciB0aGUgcmlnaHQgYnV0dG9uIChhc3N1bWluZyBhIHJpZ2h0LWhhbmRlZCBtb3VzZSlcbiAgY29uc3QgUkVHRVhQX0tFWURPV04gICAgICAgICAgID0gbmV3IFJlZ0V4cChgJHtBUlJPV19VUF9LRVlDT0RFfXwke0FSUk9XX0RPV05fS0VZQ09ERX18JHtFU0NBUEVfS0VZQ09ERX1gKVxuXG4gIGNvbnN0IEV2ZW50ID0ge1xuICAgIEhJREUgICAgICAgICAgICAgOiBgaGlkZSR7RVZFTlRfS0VZfWAsXG4gICAgSElEREVOICAgICAgICAgICA6IGBoaWRkZW4ke0VWRU5UX0tFWX1gLFxuICAgIFNIT1cgICAgICAgICAgICAgOiBgc2hvdyR7RVZFTlRfS0VZfWAsXG4gICAgU0hPV04gICAgICAgICAgICA6IGBzaG93biR7RVZFTlRfS0VZfWAsXG4gICAgQ0xJQ0sgICAgICAgICAgICA6IGBjbGljayR7RVZFTlRfS0VZfWAsXG4gICAgQ0xJQ0tfREFUQV9BUEkgICA6IGBjbGljayR7RVZFTlRfS0VZfSR7REFUQV9BUElfS0VZfWAsXG4gICAgS0VZRE9XTl9EQVRBX0FQSSA6IGBrZXlkb3duJHtFVkVOVF9LRVl9JHtEQVRBX0FQSV9LRVl9YCxcbiAgICBLRVlVUF9EQVRBX0FQSSAgIDogYGtleXVwJHtFVkVOVF9LRVl9JHtEQVRBX0FQSV9LRVl9YFxuICB9XG5cbiAgY29uc3QgQ2xhc3NOYW1lID0ge1xuICAgIERJU0FCTEVEICA6ICdkaXNhYmxlZCcsXG4gICAgU0hPVyAgICAgIDogJ3Nob3cnLFxuICAgIERST1BVUCAgICA6ICdkcm9wdXAnLFxuICAgIE1FTlVSSUdIVCA6ICdkcm9wZG93bi1tZW51LXJpZ2h0JyxcbiAgICBNRU5VTEVGVCAgOiAnZHJvcGRvd24tbWVudS1sZWZ0J1xuICB9XG5cbiAgY29uc3QgU2VsZWN0b3IgPSB7XG4gICAgREFUQV9UT0dHTEUgICA6ICdbZGF0YS10b2dnbGU9XCJkcm9wZG93blwiXScsXG4gICAgRk9STV9DSElMRCAgICA6ICcuZHJvcGRvd24gZm9ybScsXG4gICAgTUVOVSAgICAgICAgICA6ICcuZHJvcGRvd24tbWVudScsXG4gICAgTkFWQkFSX05BViAgICA6ICcubmF2YmFyLW5hdicsXG4gICAgVklTSUJMRV9JVEVNUyA6ICcuZHJvcGRvd24tbWVudSAuZHJvcGRvd24taXRlbTpub3QoLmRpc2FibGVkKSdcbiAgfVxuXG4gIGNvbnN0IEF0dGFjaG1lbnRNYXAgPSB7XG4gICAgVE9QICAgICAgIDogJ3RvcC1zdGFydCcsXG4gICAgVE9QRU5EICAgIDogJ3RvcC1lbmQnLFxuICAgIEJPVFRPTSAgICA6ICdib3R0b20tc3RhcnQnLFxuICAgIEJPVFRPTUVORCA6ICdib3R0b20tZW5kJ1xuICB9XG5cbiAgY29uc3QgRGVmYXVsdCA9IHtcbiAgICBwbGFjZW1lbnQgICA6IEF0dGFjaG1lbnRNYXAuQk9UVE9NLFxuICAgIG9mZnNldCAgICAgIDogMCxcbiAgICBmbGlwICAgICAgICA6IHRydWVcbiAgfVxuXG4gIGNvbnN0IERlZmF1bHRUeXBlID0ge1xuICAgIHBsYWNlbWVudCAgIDogJ3N0cmluZycsXG4gICAgb2Zmc2V0ICAgICAgOiAnKG51bWJlcnxzdHJpbmcpJyxcbiAgICBmbGlwICAgICAgICA6ICdib29sZWFuJ1xuICB9XG5cblxuICAvKipcbiAgICogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gICAqIENsYXNzIERlZmluaXRpb25cbiAgICogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gICAqL1xuXG4gIGNsYXNzIERyb3Bkb3duIHtcblxuICAgIGNvbnN0cnVjdG9yKGVsZW1lbnQsIGNvbmZpZykge1xuICAgICAgdGhpcy5fZWxlbWVudCAgPSBlbGVtZW50XG4gICAgICB0aGlzLl9wb3BwZXIgICA9IG51bGxcbiAgICAgIHRoaXMuX2NvbmZpZyAgID0gdGhpcy5fZ2V0Q29uZmlnKGNvbmZpZylcbiAgICAgIHRoaXMuX21lbnUgICAgID0gdGhpcy5fZ2V0TWVudUVsZW1lbnQoKVxuICAgICAgdGhpcy5faW5OYXZiYXIgPSB0aGlzLl9kZXRlY3ROYXZiYXIoKVxuXG4gICAgICB0aGlzLl9hZGRFdmVudExpc3RlbmVycygpXG4gICAgfVxuXG5cbiAgICAvLyBnZXR0ZXJzXG5cbiAgICBzdGF0aWMgZ2V0IFZFUlNJT04oKSB7XG4gICAgICByZXR1cm4gVkVSU0lPTlxuICAgIH1cblxuICAgIHN0YXRpYyBnZXQgRGVmYXVsdCgpIHtcbiAgICAgIHJldHVybiBEZWZhdWx0XG4gICAgfVxuXG4gICAgc3RhdGljIGdldCBEZWZhdWx0VHlwZSgpIHtcbiAgICAgIHJldHVybiBEZWZhdWx0VHlwZVxuICAgIH1cblxuICAgIC8vIHB1YmxpY1xuXG4gICAgdG9nZ2xlKCkge1xuICAgICAgaWYgKHRoaXMuX2VsZW1lbnQuZGlzYWJsZWQgfHwgJCh0aGlzLl9lbGVtZW50KS5oYXNDbGFzcyhDbGFzc05hbWUuRElTQUJMRUQpKSB7XG4gICAgICAgIHJldHVyblxuICAgICAgfVxuXG4gICAgICBjb25zdCBwYXJlbnQgICA9IERyb3Bkb3duLl9nZXRQYXJlbnRGcm9tRWxlbWVudCh0aGlzLl9lbGVtZW50KVxuICAgICAgY29uc3QgaXNBY3RpdmUgPSAkKHRoaXMuX21lbnUpLmhhc0NsYXNzKENsYXNzTmFtZS5TSE9XKVxuXG4gICAgICBEcm9wZG93bi5fY2xlYXJNZW51cygpXG5cbiAgICAgIGlmIChpc0FjdGl2ZSkge1xuICAgICAgICByZXR1cm5cbiAgICAgIH1cblxuICAgICAgY29uc3QgcmVsYXRlZFRhcmdldCA9IHtcbiAgICAgICAgcmVsYXRlZFRhcmdldCA6IHRoaXMuX2VsZW1lbnRcbiAgICAgIH1cbiAgICAgIGNvbnN0IHNob3dFdmVudCA9ICQuRXZlbnQoRXZlbnQuU0hPVywgcmVsYXRlZFRhcmdldClcblxuICAgICAgJChwYXJlbnQpLnRyaWdnZXIoc2hvd0V2ZW50KVxuXG4gICAgICBpZiAoc2hvd0V2ZW50LmlzRGVmYXVsdFByZXZlbnRlZCgpKSB7XG4gICAgICAgIHJldHVyblxuICAgICAgfVxuXG4gICAgICBsZXQgZWxlbWVudCA9IHRoaXMuX2VsZW1lbnRcbiAgICAgIC8vIGZvciBkcm9wdXAgd2l0aCBhbGlnbm1lbnQgd2UgdXNlIHRoZSBwYXJlbnQgYXMgcG9wcGVyIGNvbnRhaW5lclxuICAgICAgaWYgKCQocGFyZW50KS5oYXNDbGFzcyhDbGFzc05hbWUuRFJPUFVQKSkge1xuICAgICAgICBpZiAoJCh0aGlzLl9tZW51KS5oYXNDbGFzcyhDbGFzc05hbWUuTUVOVUxFRlQpIHx8ICQodGhpcy5fbWVudSkuaGFzQ2xhc3MoQ2xhc3NOYW1lLk1FTlVSSUdIVCkpIHtcbiAgICAgICAgICBlbGVtZW50ID0gcGFyZW50XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICAgIHRoaXMuX3BvcHBlciA9IG5ldyBQb3BwZXIoZWxlbWVudCwgdGhpcy5fbWVudSwgdGhpcy5fZ2V0UG9wcGVyQ29uZmlnKCkpXG5cbiAgICAgIC8vIGlmIHRoaXMgaXMgYSB0b3VjaC1lbmFibGVkIGRldmljZSB3ZSBhZGQgZXh0cmFcbiAgICAgIC8vIGVtcHR5IG1vdXNlb3ZlciBsaXN0ZW5lcnMgdG8gdGhlIGJvZHkncyBpbW1lZGlhdGUgY2hpbGRyZW47XG4gICAgICAvLyBvbmx5IG5lZWRlZCBiZWNhdXNlIG9mIGJyb2tlbiBldmVudCBkZWxlZ2F0aW9uIG9uIGlPU1xuICAgICAgLy8gaHR0cHM6Ly93d3cucXVpcmtzbW9kZS5vcmcvYmxvZy9hcmNoaXZlcy8yMDE0LzAyL21vdXNlX2V2ZW50X2J1Yi5odG1sXG4gICAgICBpZiAoJ29udG91Y2hzdGFydCcgaW4gZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50ICYmXG4gICAgICAgICAhJChwYXJlbnQpLmNsb3Nlc3QoU2VsZWN0b3IuTkFWQkFSX05BVikubGVuZ3RoKSB7XG4gICAgICAgICQoJ2JvZHknKS5jaGlsZHJlbigpLm9uKCdtb3VzZW92ZXInLCBudWxsLCAkLm5vb3ApXG4gICAgICB9XG5cbiAgICAgIHRoaXMuX2VsZW1lbnQuZm9jdXMoKVxuICAgICAgdGhpcy5fZWxlbWVudC5zZXRBdHRyaWJ1dGUoJ2FyaWEtZXhwYW5kZWQnLCB0cnVlKVxuXG4gICAgICAkKHRoaXMuX21lbnUpLnRvZ2dsZUNsYXNzKENsYXNzTmFtZS5TSE9XKVxuICAgICAgJChwYXJlbnQpXG4gICAgICAgIC50b2dnbGVDbGFzcyhDbGFzc05hbWUuU0hPVylcbiAgICAgICAgLnRyaWdnZXIoJC5FdmVudChFdmVudC5TSE9XTiwgcmVsYXRlZFRhcmdldCkpXG4gICAgfVxuXG4gICAgZGlzcG9zZSgpIHtcbiAgICAgICQucmVtb3ZlRGF0YSh0aGlzLl9lbGVtZW50LCBEQVRBX0tFWSlcbiAgICAgICQodGhpcy5fZWxlbWVudCkub2ZmKEVWRU5UX0tFWSlcbiAgICAgIHRoaXMuX2VsZW1lbnQgPSBudWxsXG4gICAgICB0aGlzLl9tZW51ID0gbnVsbFxuICAgICAgaWYgKHRoaXMuX3BvcHBlciAhPT0gbnVsbCkge1xuICAgICAgICB0aGlzLl9wb3BwZXIuZGVzdHJveSgpXG4gICAgICB9XG4gICAgICB0aGlzLl9wb3BwZXIgPSBudWxsXG4gICAgfVxuXG4gICAgdXBkYXRlKCkge1xuICAgICAgdGhpcy5faW5OYXZiYXIgPSB0aGlzLl9kZXRlY3ROYXZiYXIoKVxuICAgICAgaWYgKHRoaXMuX3BvcHBlciAhPT0gbnVsbCkge1xuICAgICAgICB0aGlzLl9wb3BwZXIuc2NoZWR1bGVVcGRhdGUoKVxuICAgICAgfVxuICAgIH1cblxuICAgIC8vIHByaXZhdGVcblxuICAgIF9hZGRFdmVudExpc3RlbmVycygpIHtcbiAgICAgICQodGhpcy5fZWxlbWVudCkub24oRXZlbnQuQ0xJQ0ssIChldmVudCkgPT4ge1xuICAgICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpXG4gICAgICAgIGV2ZW50LnN0b3BQcm9wYWdhdGlvbigpXG4gICAgICAgIHRoaXMudG9nZ2xlKClcbiAgICAgIH0pXG4gICAgfVxuXG4gICAgX2dldENvbmZpZyhjb25maWcpIHtcbiAgICAgIGNvbnN0IGVsZW1lbnREYXRhID0gJCh0aGlzLl9lbGVtZW50KS5kYXRhKClcbiAgICAgIGlmIChlbGVtZW50RGF0YS5wbGFjZW1lbnQgIT09IHVuZGVmaW5lZCkge1xuICAgICAgICBlbGVtZW50RGF0YS5wbGFjZW1lbnQgPSBBdHRhY2htZW50TWFwW2VsZW1lbnREYXRhLnBsYWNlbWVudC50b1VwcGVyQ2FzZSgpXVxuICAgICAgfVxuXG4gICAgICBjb25maWcgPSAkLmV4dGVuZChcbiAgICAgICAge30sXG4gICAgICAgIHRoaXMuY29uc3RydWN0b3IuRGVmYXVsdCxcbiAgICAgICAgJCh0aGlzLl9lbGVtZW50KS5kYXRhKCksXG4gICAgICAgIGNvbmZpZ1xuICAgICAgKVxuXG4gICAgICBVdGlsLnR5cGVDaGVja0NvbmZpZyhcbiAgICAgICAgTkFNRSxcbiAgICAgICAgY29uZmlnLFxuICAgICAgICB0aGlzLmNvbnN0cnVjdG9yLkRlZmF1bHRUeXBlXG4gICAgICApXG5cbiAgICAgIHJldHVybiBjb25maWdcbiAgICB9XG5cbiAgICBfZ2V0TWVudUVsZW1lbnQoKSB7XG4gICAgICBpZiAoIXRoaXMuX21lbnUpIHtcbiAgICAgICAgY29uc3QgcGFyZW50ID0gRHJvcGRvd24uX2dldFBhcmVudEZyb21FbGVtZW50KHRoaXMuX2VsZW1lbnQpXG4gICAgICAgIHRoaXMuX21lbnUgPSAkKHBhcmVudCkuZmluZChTZWxlY3Rvci5NRU5VKVswXVxuICAgICAgfVxuICAgICAgcmV0dXJuIHRoaXMuX21lbnVcbiAgICB9XG5cbiAgICBfZ2V0UGxhY2VtZW50KCkge1xuICAgICAgY29uc3QgJHBhcmVudERyb3Bkb3duID0gJCh0aGlzLl9lbGVtZW50KS5wYXJlbnQoKVxuICAgICAgbGV0IHBsYWNlbWVudCA9IHRoaXMuX2NvbmZpZy5wbGFjZW1lbnRcblxuICAgICAgLy8gSGFuZGxlIGRyb3B1cFxuICAgICAgaWYgKCRwYXJlbnREcm9wZG93bi5oYXNDbGFzcyhDbGFzc05hbWUuRFJPUFVQKSB8fCB0aGlzLl9jb25maWcucGxhY2VtZW50ID09PSBBdHRhY2htZW50TWFwLlRPUCkge1xuICAgICAgICBwbGFjZW1lbnQgPSBBdHRhY2htZW50TWFwLlRPUFxuICAgICAgICBpZiAoJCh0aGlzLl9tZW51KS5oYXNDbGFzcyhDbGFzc05hbWUuTUVOVVJJR0hUKSkge1xuICAgICAgICAgIHBsYWNlbWVudCA9IEF0dGFjaG1lbnRNYXAuVE9QRU5EXG4gICAgICAgIH1cbiAgICAgIH0gZWxzZSBpZiAoJCh0aGlzLl9tZW51KS5oYXNDbGFzcyhDbGFzc05hbWUuTUVOVVJJR0hUKSkge1xuICAgICAgICBwbGFjZW1lbnQgPSBBdHRhY2htZW50TWFwLkJPVFRPTUVORFxuICAgICAgfVxuICAgICAgcmV0dXJuIHBsYWNlbWVudFxuICAgIH1cblxuICAgIF9kZXRlY3ROYXZiYXIoKSB7XG4gICAgICByZXR1cm4gJCh0aGlzLl9lbGVtZW50KS5jbG9zZXN0KCcubmF2YmFyJykubGVuZ3RoID4gMFxuICAgIH1cblxuICAgIF9nZXRQb3BwZXJDb25maWcoKSB7XG4gICAgICBjb25zdCBwb3BwZXJDb25maWcgPSB7XG4gICAgICAgIHBsYWNlbWVudCA6IHRoaXMuX2dldFBsYWNlbWVudCgpLFxuICAgICAgICBtb2RpZmllcnMgOiB7XG4gICAgICAgICAgb2Zmc2V0IDoge1xuICAgICAgICAgICAgb2Zmc2V0IDogdGhpcy5fY29uZmlnLm9mZnNldFxuICAgICAgICAgIH0sXG4gICAgICAgICAgZmxpcCA6IHtcbiAgICAgICAgICAgIGVuYWJsZWQgOiB0aGlzLl9jb25maWcuZmxpcFxuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgfVxuXG4gICAgICAvLyBEaXNhYmxlIFBvcHBlci5qcyBmb3IgRHJvcGRvd24gaW4gTmF2YmFyXG4gICAgICBpZiAodGhpcy5faW5OYXZiYXIpIHtcbiAgICAgICAgcG9wcGVyQ29uZmlnLm1vZGlmaWVycy5hcHBseVN0eWxlID0ge1xuICAgICAgICAgIGVuYWJsZWQ6ICF0aGlzLl9pbk5hdmJhclxuICAgICAgICB9XG4gICAgICB9XG4gICAgICByZXR1cm4gcG9wcGVyQ29uZmlnXG4gICAgfVxuXG4gICAgLy8gc3RhdGljXG5cbiAgICBzdGF0aWMgX2pRdWVyeUludGVyZmFjZShjb25maWcpIHtcbiAgICAgIHJldHVybiB0aGlzLmVhY2goZnVuY3Rpb24gKCkge1xuICAgICAgICBsZXQgZGF0YSA9ICQodGhpcykuZGF0YShEQVRBX0tFWSlcbiAgICAgICAgY29uc3QgX2NvbmZpZyA9IHR5cGVvZiBjb25maWcgPT09ICdvYmplY3QnID8gY29uZmlnIDogbnVsbFxuXG4gICAgICAgIGlmICghZGF0YSkge1xuICAgICAgICAgIGRhdGEgPSBuZXcgRHJvcGRvd24odGhpcywgX2NvbmZpZylcbiAgICAgICAgICAkKHRoaXMpLmRhdGEoREFUQV9LRVksIGRhdGEpXG4gICAgICAgIH1cblxuICAgICAgICBpZiAodHlwZW9mIGNvbmZpZyA9PT0gJ3N0cmluZycpIHtcbiAgICAgICAgICBpZiAoZGF0YVtjb25maWddID09PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgIHRocm93IG5ldyBFcnJvcihgTm8gbWV0aG9kIG5hbWVkIFwiJHtjb25maWd9XCJgKVxuICAgICAgICAgIH1cbiAgICAgICAgICBkYXRhW2NvbmZpZ10oKVxuICAgICAgICB9XG4gICAgICB9KVxuICAgIH1cblxuICAgIHN0YXRpYyBfY2xlYXJNZW51cyhldmVudCkge1xuICAgICAgaWYgKGV2ZW50ICYmIChldmVudC53aGljaCA9PT0gUklHSFRfTU9VU0VfQlVUVE9OX1dISUNIIHx8XG4gICAgICAgIGV2ZW50LnR5cGUgPT09ICdrZXl1cCcgJiYgZXZlbnQud2hpY2ggIT09IFRBQl9LRVlDT0RFKSkge1xuICAgICAgICByZXR1cm5cbiAgICAgIH1cblxuICAgICAgY29uc3QgdG9nZ2xlcyA9ICQubWFrZUFycmF5KCQoU2VsZWN0b3IuREFUQV9UT0dHTEUpKVxuICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCB0b2dnbGVzLmxlbmd0aDsgaSsrKSB7XG4gICAgICAgIGNvbnN0IHBhcmVudCAgICAgICAgPSBEcm9wZG93bi5fZ2V0UGFyZW50RnJvbUVsZW1lbnQodG9nZ2xlc1tpXSlcbiAgICAgICAgY29uc3QgY29udGV4dCAgICAgICA9ICQodG9nZ2xlc1tpXSkuZGF0YShEQVRBX0tFWSlcbiAgICAgICAgY29uc3QgcmVsYXRlZFRhcmdldCA9IHtcbiAgICAgICAgICByZWxhdGVkVGFyZ2V0IDogdG9nZ2xlc1tpXVxuICAgICAgICB9XG5cbiAgICAgICAgaWYgKCFjb250ZXh0KSB7XG4gICAgICAgICAgY29udGludWVcbiAgICAgICAgfVxuXG4gICAgICAgIGNvbnN0IGRyb3Bkb3duTWVudSA9IGNvbnRleHQuX21lbnVcbiAgICAgICAgaWYgKCEkKHBhcmVudCkuaGFzQ2xhc3MoQ2xhc3NOYW1lLlNIT1cpKSB7XG4gICAgICAgICAgY29udGludWVcbiAgICAgICAgfVxuXG4gICAgICAgIGlmIChldmVudCAmJiAoZXZlbnQudHlwZSA9PT0gJ2NsaWNrJyAmJlxuICAgICAgICAgICAgL2lucHV0fHRleHRhcmVhL2kudGVzdChldmVudC50YXJnZXQudGFnTmFtZSkgfHwgZXZlbnQudHlwZSA9PT0gJ2tleXVwJyAmJiBldmVudC53aGljaCA9PT0gVEFCX0tFWUNPREUpXG4gICAgICAgICAgICAmJiAkLmNvbnRhaW5zKHBhcmVudCwgZXZlbnQudGFyZ2V0KSkge1xuICAgICAgICAgIGNvbnRpbnVlXG4gICAgICAgIH1cblxuICAgICAgICBjb25zdCBoaWRlRXZlbnQgPSAkLkV2ZW50KEV2ZW50LkhJREUsIHJlbGF0ZWRUYXJnZXQpXG4gICAgICAgICQocGFyZW50KS50cmlnZ2VyKGhpZGVFdmVudClcbiAgICAgICAgaWYgKGhpZGVFdmVudC5pc0RlZmF1bHRQcmV2ZW50ZWQoKSkge1xuICAgICAgICAgIGNvbnRpbnVlXG4gICAgICAgIH1cblxuICAgICAgICAvLyBpZiB0aGlzIGlzIGEgdG91Y2gtZW5hYmxlZCBkZXZpY2Ugd2UgcmVtb3ZlIHRoZSBleHRyYVxuICAgICAgICAvLyBlbXB0eSBtb3VzZW92ZXIgbGlzdGVuZXJzIHdlIGFkZGVkIGZvciBpT1Mgc3VwcG9ydFxuICAgICAgICBpZiAoJ29udG91Y2hzdGFydCcgaW4gZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50KSB7XG4gICAgICAgICAgJCgnYm9keScpLmNoaWxkcmVuKCkub2ZmKCdtb3VzZW92ZXInLCBudWxsLCAkLm5vb3ApXG4gICAgICAgIH1cblxuICAgICAgICB0b2dnbGVzW2ldLnNldEF0dHJpYnV0ZSgnYXJpYS1leHBhbmRlZCcsICdmYWxzZScpXG5cbiAgICAgICAgJChkcm9wZG93bk1lbnUpLnJlbW92ZUNsYXNzKENsYXNzTmFtZS5TSE9XKVxuICAgICAgICAkKHBhcmVudClcbiAgICAgICAgICAucmVtb3ZlQ2xhc3MoQ2xhc3NOYW1lLlNIT1cpXG4gICAgICAgICAgLnRyaWdnZXIoJC5FdmVudChFdmVudC5ISURERU4sIHJlbGF0ZWRUYXJnZXQpKVxuICAgICAgfVxuICAgIH1cblxuICAgIHN0YXRpYyBfZ2V0UGFyZW50RnJvbUVsZW1lbnQoZWxlbWVudCkge1xuICAgICAgbGV0IHBhcmVudFxuICAgICAgY29uc3Qgc2VsZWN0b3IgPSBVdGlsLmdldFNlbGVjdG9yRnJvbUVsZW1lbnQoZWxlbWVudClcblxuICAgICAgaWYgKHNlbGVjdG9yKSB7XG4gICAgICAgIHBhcmVudCA9ICQoc2VsZWN0b3IpWzBdXG4gICAgICB9XG5cbiAgICAgIHJldHVybiBwYXJlbnQgfHwgZWxlbWVudC5wYXJlbnROb2RlXG4gICAgfVxuXG4gICAgc3RhdGljIF9kYXRhQXBpS2V5ZG93bkhhbmRsZXIoZXZlbnQpIHtcbiAgICAgIGlmICghUkVHRVhQX0tFWURPV04udGVzdChldmVudC53aGljaCkgfHwgL2J1dHRvbi9pLnRlc3QoZXZlbnQudGFyZ2V0LnRhZ05hbWUpICYmIGV2ZW50LndoaWNoID09PSBTUEFDRV9LRVlDT0RFIHx8XG4gICAgICAgICAvaW5wdXR8dGV4dGFyZWEvaS50ZXN0KGV2ZW50LnRhcmdldC50YWdOYW1lKSkge1xuICAgICAgICByZXR1cm5cbiAgICAgIH1cblxuICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKVxuICAgICAgZXZlbnQuc3RvcFByb3BhZ2F0aW9uKClcblxuICAgICAgaWYgKHRoaXMuZGlzYWJsZWQgfHwgJCh0aGlzKS5oYXNDbGFzcyhDbGFzc05hbWUuRElTQUJMRUQpKSB7XG4gICAgICAgIHJldHVyblxuICAgICAgfVxuXG4gICAgICBjb25zdCBwYXJlbnQgICA9IERyb3Bkb3duLl9nZXRQYXJlbnRGcm9tRWxlbWVudCh0aGlzKVxuICAgICAgY29uc3QgaXNBY3RpdmUgPSAkKHBhcmVudCkuaGFzQ2xhc3MoQ2xhc3NOYW1lLlNIT1cpXG5cbiAgICAgIGlmICghaXNBY3RpdmUgJiYgKGV2ZW50LndoaWNoICE9PSBFU0NBUEVfS0VZQ09ERSB8fCBldmVudC53aGljaCAhPT0gU1BBQ0VfS0VZQ09ERSkgfHxcbiAgICAgICAgICAgaXNBY3RpdmUgJiYgKGV2ZW50LndoaWNoID09PSBFU0NBUEVfS0VZQ09ERSB8fCBldmVudC53aGljaCA9PT0gU1BBQ0VfS0VZQ09ERSkpIHtcblxuICAgICAgICBpZiAoZXZlbnQud2hpY2ggPT09IEVTQ0FQRV9LRVlDT0RFKSB7XG4gICAgICAgICAgY29uc3QgdG9nZ2xlID0gJChwYXJlbnQpLmZpbmQoU2VsZWN0b3IuREFUQV9UT0dHTEUpWzBdXG4gICAgICAgICAgJCh0b2dnbGUpLnRyaWdnZXIoJ2ZvY3VzJylcbiAgICAgICAgfVxuXG4gICAgICAgICQodGhpcykudHJpZ2dlcignY2xpY2snKVxuICAgICAgICByZXR1cm5cbiAgICAgIH1cblxuICAgICAgY29uc3QgaXRlbXMgPSAkKHBhcmVudCkuZmluZChTZWxlY3Rvci5WSVNJQkxFX0lURU1TKS5nZXQoKVxuXG4gICAgICBpZiAoIWl0ZW1zLmxlbmd0aCkge1xuICAgICAgICByZXR1cm5cbiAgICAgIH1cblxuICAgICAgbGV0IGluZGV4ID0gaXRlbXMuaW5kZXhPZihldmVudC50YXJnZXQpXG5cbiAgICAgIGlmIChldmVudC53aGljaCA9PT0gQVJST1dfVVBfS0VZQ09ERSAmJiBpbmRleCA+IDApIHsgLy8gdXBcbiAgICAgICAgaW5kZXgtLVxuICAgICAgfVxuXG4gICAgICBpZiAoZXZlbnQud2hpY2ggPT09IEFSUk9XX0RPV05fS0VZQ09ERSAmJiBpbmRleCA8IGl0ZW1zLmxlbmd0aCAtIDEpIHsgLy8gZG93blxuICAgICAgICBpbmRleCsrXG4gICAgICB9XG5cbiAgICAgIGlmIChpbmRleCA8IDApIHtcbiAgICAgICAgaW5kZXggPSAwXG4gICAgICB9XG5cbiAgICAgIGl0ZW1zW2luZGV4XS5mb2N1cygpXG4gICAgfVxuXG4gIH1cblxuXG4gIC8qKlxuICAgKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiAgICogRGF0YSBBcGkgaW1wbGVtZW50YXRpb25cbiAgICogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gICAqL1xuXG4gICQoZG9jdW1lbnQpXG4gICAgLm9uKEV2ZW50LktFWURPV05fREFUQV9BUEksIFNlbGVjdG9yLkRBVEFfVE9HR0xFLCAgRHJvcGRvd24uX2RhdGFBcGlLZXlkb3duSGFuZGxlcilcbiAgICAub24oRXZlbnQuS0VZRE9XTl9EQVRBX0FQSSwgU2VsZWN0b3IuTUVOVSwgRHJvcGRvd24uX2RhdGFBcGlLZXlkb3duSGFuZGxlcilcbiAgICAub24oYCR7RXZlbnQuQ0xJQ0tfREFUQV9BUEl9ICR7RXZlbnQuS0VZVVBfREFUQV9BUEl9YCwgRHJvcGRvd24uX2NsZWFyTWVudXMpXG4gICAgLm9uKEV2ZW50LkNMSUNLX0RBVEFfQVBJLCBTZWxlY3Rvci5EQVRBX1RPR0dMRSwgZnVuY3Rpb24gKGV2ZW50KSB7XG4gICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpXG4gICAgICBldmVudC5zdG9wUHJvcGFnYXRpb24oKVxuICAgICAgRHJvcGRvd24uX2pRdWVyeUludGVyZmFjZS5jYWxsKCQodGhpcyksICd0b2dnbGUnKVxuICAgIH0pXG4gICAgLm9uKEV2ZW50LkNMSUNLX0RBVEFfQVBJLCBTZWxlY3Rvci5GT1JNX0NISUxELCAoZSkgPT4ge1xuICAgICAgZS5zdG9wUHJvcGFnYXRpb24oKVxuICAgIH0pXG5cblxuICAvKipcbiAgICogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gICAqIGpRdWVyeVxuICAgKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiAgICovXG5cbiAgJC5mbltOQU1FXSAgICAgICAgICAgICA9IERyb3Bkb3duLl9qUXVlcnlJbnRlcmZhY2VcbiAgJC5mbltOQU1FXS5Db25zdHJ1Y3RvciA9IERyb3Bkb3duXG4gICQuZm5bTkFNRV0ubm9Db25mbGljdCAgPSBmdW5jdGlvbiAoKSB7XG4gICAgJC5mbltOQU1FXSA9IEpRVUVSWV9OT19DT05GTElDVFxuICAgIHJldHVybiBEcm9wZG93bi5falF1ZXJ5SW50ZXJmYWNlXG4gIH1cblxuICByZXR1cm4gRHJvcGRvd25cblxufSkoalF1ZXJ5KVxuXG5leHBvcnQgZGVmYXVsdCBEcm9wZG93blxuIiwiaW1wb3J0IFV0aWwgZnJvbSAnLi91dGlsJ1xuXG5cbi8qKlxuICogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiAqIEJvb3RzdHJhcCAodjQuMC4wLWJldGEpOiBtb2RhbC5qc1xuICogTGljZW5zZWQgdW5kZXIgTUlUIChodHRwczovL2dpdGh1Yi5jb20vdHdicy9ib290c3RyYXAvYmxvYi9tYXN0ZXIvTElDRU5TRSlcbiAqIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gKi9cblxuY29uc3QgTW9kYWwgPSAoKCQpID0+IHtcblxuXG4gIC8qKlxuICAgKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiAgICogQ29uc3RhbnRzXG4gICAqIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuICAgKi9cblxuICBjb25zdCBOQU1FICAgICAgICAgICAgICAgICAgICAgICAgID0gJ21vZGFsJ1xuICBjb25zdCBWRVJTSU9OICAgICAgICAgICAgICAgICAgICAgID0gJzQuMC4wLWJldGEnXG4gIGNvbnN0IERBVEFfS0VZICAgICAgICAgICAgICAgICAgICAgPSAnYnMubW9kYWwnXG4gIGNvbnN0IEVWRU5UX0tFWSAgICAgICAgICAgICAgICAgICAgPSBgLiR7REFUQV9LRVl9YFxuICBjb25zdCBEQVRBX0FQSV9LRVkgICAgICAgICAgICAgICAgID0gJy5kYXRhLWFwaSdcbiAgY29uc3QgSlFVRVJZX05PX0NPTkZMSUNUICAgICAgICAgICA9ICQuZm5bTkFNRV1cbiAgY29uc3QgVFJBTlNJVElPTl9EVVJBVElPTiAgICAgICAgICA9IDMwMFxuICBjb25zdCBCQUNLRFJPUF9UUkFOU0lUSU9OX0RVUkFUSU9OID0gMTUwXG4gIGNvbnN0IEVTQ0FQRV9LRVlDT0RFICAgICAgICAgICAgICAgPSAyNyAvLyBLZXlib2FyZEV2ZW50LndoaWNoIHZhbHVlIGZvciBFc2NhcGUgKEVzYykga2V5XG5cbiAgY29uc3QgRGVmYXVsdCA9IHtcbiAgICBiYWNrZHJvcCA6IHRydWUsXG4gICAga2V5Ym9hcmQgOiB0cnVlLFxuICAgIGZvY3VzICAgIDogdHJ1ZSxcbiAgICBzaG93ICAgICA6IHRydWVcbiAgfVxuXG4gIGNvbnN0IERlZmF1bHRUeXBlID0ge1xuICAgIGJhY2tkcm9wIDogJyhib29sZWFufHN0cmluZyknLFxuICAgIGtleWJvYXJkIDogJ2Jvb2xlYW4nLFxuICAgIGZvY3VzICAgIDogJ2Jvb2xlYW4nLFxuICAgIHNob3cgICAgIDogJ2Jvb2xlYW4nXG4gIH1cblxuICBjb25zdCBFdmVudCA9IHtcbiAgICBISURFICAgICAgICAgICAgICA6IGBoaWRlJHtFVkVOVF9LRVl9YCxcbiAgICBISURERU4gICAgICAgICAgICA6IGBoaWRkZW4ke0VWRU5UX0tFWX1gLFxuICAgIFNIT1cgICAgICAgICAgICAgIDogYHNob3cke0VWRU5UX0tFWX1gLFxuICAgIFNIT1dOICAgICAgICAgICAgIDogYHNob3duJHtFVkVOVF9LRVl9YCxcbiAgICBGT0NVU0lOICAgICAgICAgICA6IGBmb2N1c2luJHtFVkVOVF9LRVl9YCxcbiAgICBSRVNJWkUgICAgICAgICAgICA6IGByZXNpemUke0VWRU5UX0tFWX1gLFxuICAgIENMSUNLX0RJU01JU1MgICAgIDogYGNsaWNrLmRpc21pc3Mke0VWRU5UX0tFWX1gLFxuICAgIEtFWURPV05fRElTTUlTUyAgIDogYGtleWRvd24uZGlzbWlzcyR7RVZFTlRfS0VZfWAsXG4gICAgTU9VU0VVUF9ESVNNSVNTICAgOiBgbW91c2V1cC5kaXNtaXNzJHtFVkVOVF9LRVl9YCxcbiAgICBNT1VTRURPV05fRElTTUlTUyA6IGBtb3VzZWRvd24uZGlzbWlzcyR7RVZFTlRfS0VZfWAsXG4gICAgQ0xJQ0tfREFUQV9BUEkgICAgOiBgY2xpY2ske0VWRU5UX0tFWX0ke0RBVEFfQVBJX0tFWX1gXG4gIH1cblxuICBjb25zdCBDbGFzc05hbWUgPSB7XG4gICAgU0NST0xMQkFSX01FQVNVUkVSIDogJ21vZGFsLXNjcm9sbGJhci1tZWFzdXJlJyxcbiAgICBCQUNLRFJPUCAgICAgICAgICAgOiAnbW9kYWwtYmFja2Ryb3AnLFxuICAgIE9QRU4gICAgICAgICAgICAgICA6ICdtb2RhbC1vcGVuJyxcbiAgICBGQURFICAgICAgICAgICAgICAgOiAnZmFkZScsXG4gICAgU0hPVyAgICAgICAgICAgICAgIDogJ3Nob3cnXG4gIH1cblxuICBjb25zdCBTZWxlY3RvciA9IHtcbiAgICBESUFMT0cgICAgICAgICAgICAgOiAnLm1vZGFsLWRpYWxvZycsXG4gICAgREFUQV9UT0dHTEUgICAgICAgIDogJ1tkYXRhLXRvZ2dsZT1cIm1vZGFsXCJdJyxcbiAgICBEQVRBX0RJU01JU1MgICAgICAgOiAnW2RhdGEtZGlzbWlzcz1cIm1vZGFsXCJdJyxcbiAgICBGSVhFRF9DT05URU5UICAgICAgOiAnLmZpeGVkLXRvcCwgLmZpeGVkLWJvdHRvbSwgLmlzLWZpeGVkLCAuc3RpY2t5LXRvcCcsXG4gICAgTkFWQkFSX1RPR0dMRVIgICAgIDogJy5uYXZiYXItdG9nZ2xlcidcbiAgfVxuXG5cbiAgLyoqXG4gICAqIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuICAgKiBDbGFzcyBEZWZpbml0aW9uXG4gICAqIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuICAgKi9cblxuICBjbGFzcyBNb2RhbCB7XG5cbiAgICBjb25zdHJ1Y3RvcihlbGVtZW50LCBjb25maWcpIHtcbiAgICAgIHRoaXMuX2NvbmZpZyAgICAgICAgICAgICAgPSB0aGlzLl9nZXRDb25maWcoY29uZmlnKVxuICAgICAgdGhpcy5fZWxlbWVudCAgICAgICAgICAgICA9IGVsZW1lbnRcbiAgICAgIHRoaXMuX2RpYWxvZyAgICAgICAgICAgICAgPSAkKGVsZW1lbnQpLmZpbmQoU2VsZWN0b3IuRElBTE9HKVswXVxuICAgICAgdGhpcy5fYmFja2Ryb3AgICAgICAgICAgICA9IG51bGxcbiAgICAgIHRoaXMuX2lzU2hvd24gICAgICAgICAgICAgPSBmYWxzZVxuICAgICAgdGhpcy5faXNCb2R5T3ZlcmZsb3dpbmcgICA9IGZhbHNlXG4gICAgICB0aGlzLl9pZ25vcmVCYWNrZHJvcENsaWNrID0gZmFsc2VcbiAgICAgIHRoaXMuX29yaWdpbmFsQm9keVBhZGRpbmcgPSAwXG4gICAgICB0aGlzLl9zY3JvbGxiYXJXaWR0aCAgICAgID0gMFxuICAgIH1cblxuXG4gICAgLy8gZ2V0dGVyc1xuXG4gICAgc3RhdGljIGdldCBWRVJTSU9OKCkge1xuICAgICAgcmV0dXJuIFZFUlNJT05cbiAgICB9XG5cbiAgICBzdGF0aWMgZ2V0IERlZmF1bHQoKSB7XG4gICAgICByZXR1cm4gRGVmYXVsdFxuICAgIH1cblxuXG4gICAgLy8gcHVibGljXG5cbiAgICB0b2dnbGUocmVsYXRlZFRhcmdldCkge1xuICAgICAgcmV0dXJuIHRoaXMuX2lzU2hvd24gPyB0aGlzLmhpZGUoKSA6IHRoaXMuc2hvdyhyZWxhdGVkVGFyZ2V0KVxuICAgIH1cblxuICAgIHNob3cocmVsYXRlZFRhcmdldCkge1xuICAgICAgaWYgKHRoaXMuX2lzVHJhbnNpdGlvbmluZykge1xuICAgICAgICByZXR1cm5cbiAgICAgIH1cblxuICAgICAgaWYgKFV0aWwuc3VwcG9ydHNUcmFuc2l0aW9uRW5kKCkgJiYgJCh0aGlzLl9lbGVtZW50KS5oYXNDbGFzcyhDbGFzc05hbWUuRkFERSkpIHtcbiAgICAgICAgdGhpcy5faXNUcmFuc2l0aW9uaW5nID0gdHJ1ZVxuICAgICAgfVxuXG4gICAgICBjb25zdCBzaG93RXZlbnQgPSAkLkV2ZW50KEV2ZW50LlNIT1csIHtcbiAgICAgICAgcmVsYXRlZFRhcmdldFxuICAgICAgfSlcblxuICAgICAgJCh0aGlzLl9lbGVtZW50KS50cmlnZ2VyKHNob3dFdmVudClcblxuICAgICAgaWYgKHRoaXMuX2lzU2hvd24gfHwgc2hvd0V2ZW50LmlzRGVmYXVsdFByZXZlbnRlZCgpKSB7XG4gICAgICAgIHJldHVyblxuICAgICAgfVxuXG4gICAgICB0aGlzLl9pc1Nob3duID0gdHJ1ZVxuXG4gICAgICB0aGlzLl9jaGVja1Njcm9sbGJhcigpXG4gICAgICB0aGlzLl9zZXRTY3JvbGxiYXIoKVxuXG4gICAgICAkKGRvY3VtZW50LmJvZHkpLmFkZENsYXNzKENsYXNzTmFtZS5PUEVOKVxuXG4gICAgICB0aGlzLl9zZXRFc2NhcGVFdmVudCgpXG4gICAgICB0aGlzLl9zZXRSZXNpemVFdmVudCgpXG5cbiAgICAgICQodGhpcy5fZWxlbWVudCkub24oXG4gICAgICAgIEV2ZW50LkNMSUNLX0RJU01JU1MsXG4gICAgICAgIFNlbGVjdG9yLkRBVEFfRElTTUlTUyxcbiAgICAgICAgKGV2ZW50KSA9PiB0aGlzLmhpZGUoZXZlbnQpXG4gICAgICApXG5cbiAgICAgICQodGhpcy5fZGlhbG9nKS5vbihFdmVudC5NT1VTRURPV05fRElTTUlTUywgKCkgPT4ge1xuICAgICAgICAkKHRoaXMuX2VsZW1lbnQpLm9uZShFdmVudC5NT1VTRVVQX0RJU01JU1MsIChldmVudCkgPT4ge1xuICAgICAgICAgIGlmICgkKGV2ZW50LnRhcmdldCkuaXModGhpcy5fZWxlbWVudCkpIHtcbiAgICAgICAgICAgIHRoaXMuX2lnbm9yZUJhY2tkcm9wQ2xpY2sgPSB0cnVlXG4gICAgICAgICAgfVxuICAgICAgICB9KVxuICAgICAgfSlcblxuICAgICAgdGhpcy5fc2hvd0JhY2tkcm9wKCgpID0+IHRoaXMuX3Nob3dFbGVtZW50KHJlbGF0ZWRUYXJnZXQpKVxuICAgIH1cblxuICAgIGhpZGUoZXZlbnQpIHtcbiAgICAgIGlmIChldmVudCkge1xuICAgICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpXG4gICAgICB9XG5cbiAgICAgIGlmICh0aGlzLl9pc1RyYW5zaXRpb25pbmcgfHwgIXRoaXMuX2lzU2hvd24pIHtcbiAgICAgICAgcmV0dXJuXG4gICAgICB9XG5cbiAgICAgIGNvbnN0IHRyYW5zaXRpb24gPSBVdGlsLnN1cHBvcnRzVHJhbnNpdGlvbkVuZCgpICYmICQodGhpcy5fZWxlbWVudCkuaGFzQ2xhc3MoQ2xhc3NOYW1lLkZBREUpXG5cbiAgICAgIGlmICh0cmFuc2l0aW9uKSB7XG4gICAgICAgIHRoaXMuX2lzVHJhbnNpdGlvbmluZyA9IHRydWVcbiAgICAgIH1cblxuICAgICAgY29uc3QgaGlkZUV2ZW50ID0gJC5FdmVudChFdmVudC5ISURFKVxuXG4gICAgICAkKHRoaXMuX2VsZW1lbnQpLnRyaWdnZXIoaGlkZUV2ZW50KVxuXG4gICAgICBpZiAoIXRoaXMuX2lzU2hvd24gfHwgaGlkZUV2ZW50LmlzRGVmYXVsdFByZXZlbnRlZCgpKSB7XG4gICAgICAgIHJldHVyblxuICAgICAgfVxuXG4gICAgICB0aGlzLl9pc1Nob3duID0gZmFsc2VcblxuICAgICAgdGhpcy5fc2V0RXNjYXBlRXZlbnQoKVxuICAgICAgdGhpcy5fc2V0UmVzaXplRXZlbnQoKVxuXG4gICAgICAkKGRvY3VtZW50KS5vZmYoRXZlbnQuRk9DVVNJTilcblxuICAgICAgJCh0aGlzLl9lbGVtZW50KS5yZW1vdmVDbGFzcyhDbGFzc05hbWUuU0hPVylcblxuICAgICAgJCh0aGlzLl9lbGVtZW50KS5vZmYoRXZlbnQuQ0xJQ0tfRElTTUlTUylcbiAgICAgICQodGhpcy5fZGlhbG9nKS5vZmYoRXZlbnQuTU9VU0VET1dOX0RJU01JU1MpXG5cbiAgICAgIGlmICh0cmFuc2l0aW9uKSB7XG5cbiAgICAgICAgJCh0aGlzLl9lbGVtZW50KVxuICAgICAgICAgIC5vbmUoVXRpbC5UUkFOU0lUSU9OX0VORCwgKGV2ZW50KSA9PiB0aGlzLl9oaWRlTW9kYWwoZXZlbnQpKVxuICAgICAgICAgIC5lbXVsYXRlVHJhbnNpdGlvbkVuZChUUkFOU0lUSU9OX0RVUkFUSU9OKVxuICAgICAgfSBlbHNlIHtcbiAgICAgICAgdGhpcy5faGlkZU1vZGFsKClcbiAgICAgIH1cbiAgICB9XG5cbiAgICBkaXNwb3NlKCkge1xuICAgICAgJC5yZW1vdmVEYXRhKHRoaXMuX2VsZW1lbnQsIERBVEFfS0VZKVxuXG4gICAgICAkKHdpbmRvdywgZG9jdW1lbnQsIHRoaXMuX2VsZW1lbnQsIHRoaXMuX2JhY2tkcm9wKS5vZmYoRVZFTlRfS0VZKVxuXG4gICAgICB0aGlzLl9jb25maWcgICAgICAgICAgICAgID0gbnVsbFxuICAgICAgdGhpcy5fZWxlbWVudCAgICAgICAgICAgICA9IG51bGxcbiAgICAgIHRoaXMuX2RpYWxvZyAgICAgICAgICAgICAgPSBudWxsXG4gICAgICB0aGlzLl9iYWNrZHJvcCAgICAgICAgICAgID0gbnVsbFxuICAgICAgdGhpcy5faXNTaG93biAgICAgICAgICAgICA9IG51bGxcbiAgICAgIHRoaXMuX2lzQm9keU92ZXJmbG93aW5nICAgPSBudWxsXG4gICAgICB0aGlzLl9pZ25vcmVCYWNrZHJvcENsaWNrID0gbnVsbFxuICAgICAgdGhpcy5fc2Nyb2xsYmFyV2lkdGggICAgICA9IG51bGxcbiAgICB9XG5cbiAgICBoYW5kbGVVcGRhdGUoKSB7XG4gICAgICB0aGlzLl9hZGp1c3REaWFsb2coKVxuICAgIH1cblxuICAgIC8vIHByaXZhdGVcblxuICAgIF9nZXRDb25maWcoY29uZmlnKSB7XG4gICAgICBjb25maWcgPSAkLmV4dGVuZCh7fSwgRGVmYXVsdCwgY29uZmlnKVxuICAgICAgVXRpbC50eXBlQ2hlY2tDb25maWcoTkFNRSwgY29uZmlnLCBEZWZhdWx0VHlwZSlcbiAgICAgIHJldHVybiBjb25maWdcbiAgICB9XG5cbiAgICBfc2hvd0VsZW1lbnQocmVsYXRlZFRhcmdldCkge1xuICAgICAgY29uc3QgdHJhbnNpdGlvbiA9IFV0aWwuc3VwcG9ydHNUcmFuc2l0aW9uRW5kKCkgJiZcbiAgICAgICAgJCh0aGlzLl9lbGVtZW50KS5oYXNDbGFzcyhDbGFzc05hbWUuRkFERSlcblxuICAgICAgaWYgKCF0aGlzLl9lbGVtZW50LnBhcmVudE5vZGUgfHxcbiAgICAgICAgIHRoaXMuX2VsZW1lbnQucGFyZW50Tm9kZS5ub2RlVHlwZSAhPT0gTm9kZS5FTEVNRU5UX05PREUpIHtcbiAgICAgICAgLy8gZG9uJ3QgbW92ZSBtb2RhbHMgZG9tIHBvc2l0aW9uXG4gICAgICAgIGRvY3VtZW50LmJvZHkuYXBwZW5kQ2hpbGQodGhpcy5fZWxlbWVudClcbiAgICAgIH1cblxuICAgICAgdGhpcy5fZWxlbWVudC5zdHlsZS5kaXNwbGF5ID0gJ2Jsb2NrJ1xuICAgICAgdGhpcy5fZWxlbWVudC5yZW1vdmVBdHRyaWJ1dGUoJ2FyaWEtaGlkZGVuJylcbiAgICAgIHRoaXMuX2VsZW1lbnQuc2Nyb2xsVG9wID0gMFxuXG4gICAgICBpZiAodHJhbnNpdGlvbikge1xuICAgICAgICBVdGlsLnJlZmxvdyh0aGlzLl9lbGVtZW50KVxuICAgICAgfVxuXG4gICAgICAkKHRoaXMuX2VsZW1lbnQpLmFkZENsYXNzKENsYXNzTmFtZS5TSE9XKVxuXG4gICAgICBpZiAodGhpcy5fY29uZmlnLmZvY3VzKSB7XG4gICAgICAgIHRoaXMuX2VuZm9yY2VGb2N1cygpXG4gICAgICB9XG5cbiAgICAgIGNvbnN0IHNob3duRXZlbnQgPSAkLkV2ZW50KEV2ZW50LlNIT1dOLCB7XG4gICAgICAgIHJlbGF0ZWRUYXJnZXRcbiAgICAgIH0pXG5cbiAgICAgIGNvbnN0IHRyYW5zaXRpb25Db21wbGV0ZSA9ICgpID0+IHtcbiAgICAgICAgaWYgKHRoaXMuX2NvbmZpZy5mb2N1cykge1xuICAgICAgICAgIHRoaXMuX2VsZW1lbnQuZm9jdXMoKVxuICAgICAgICB9XG4gICAgICAgIHRoaXMuX2lzVHJhbnNpdGlvbmluZyA9IGZhbHNlXG4gICAgICAgICQodGhpcy5fZWxlbWVudCkudHJpZ2dlcihzaG93bkV2ZW50KVxuICAgICAgfVxuXG4gICAgICBpZiAodHJhbnNpdGlvbikge1xuICAgICAgICAkKHRoaXMuX2RpYWxvZylcbiAgICAgICAgICAub25lKFV0aWwuVFJBTlNJVElPTl9FTkQsIHRyYW5zaXRpb25Db21wbGV0ZSlcbiAgICAgICAgICAuZW11bGF0ZVRyYW5zaXRpb25FbmQoVFJBTlNJVElPTl9EVVJBVElPTilcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHRyYW5zaXRpb25Db21wbGV0ZSgpXG4gICAgICB9XG4gICAgfVxuXG4gICAgX2VuZm9yY2VGb2N1cygpIHtcbiAgICAgICQoZG9jdW1lbnQpXG4gICAgICAgIC5vZmYoRXZlbnQuRk9DVVNJTikgLy8gZ3VhcmQgYWdhaW5zdCBpbmZpbml0ZSBmb2N1cyBsb29wXG4gICAgICAgIC5vbihFdmVudC5GT0NVU0lOLCAoZXZlbnQpID0+IHtcbiAgICAgICAgICBpZiAoZG9jdW1lbnQgIT09IGV2ZW50LnRhcmdldCAmJlxuICAgICAgICAgICAgICB0aGlzLl9lbGVtZW50ICE9PSBldmVudC50YXJnZXQgJiZcbiAgICAgICAgICAgICAgISQodGhpcy5fZWxlbWVudCkuaGFzKGV2ZW50LnRhcmdldCkubGVuZ3RoKSB7XG4gICAgICAgICAgICB0aGlzLl9lbGVtZW50LmZvY3VzKClcbiAgICAgICAgICB9XG4gICAgICAgIH0pXG4gICAgfVxuXG4gICAgX3NldEVzY2FwZUV2ZW50KCkge1xuICAgICAgaWYgKHRoaXMuX2lzU2hvd24gJiYgdGhpcy5fY29uZmlnLmtleWJvYXJkKSB7XG4gICAgICAgICQodGhpcy5fZWxlbWVudCkub24oRXZlbnQuS0VZRE9XTl9ESVNNSVNTLCAoZXZlbnQpID0+IHtcbiAgICAgICAgICBpZiAoZXZlbnQud2hpY2ggPT09IEVTQ0FQRV9LRVlDT0RFKSB7XG4gICAgICAgICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpXG4gICAgICAgICAgICB0aGlzLmhpZGUoKVxuICAgICAgICAgIH1cbiAgICAgICAgfSlcblxuICAgICAgfSBlbHNlIGlmICghdGhpcy5faXNTaG93bikge1xuICAgICAgICAkKHRoaXMuX2VsZW1lbnQpLm9mZihFdmVudC5LRVlET1dOX0RJU01JU1MpXG4gICAgICB9XG4gICAgfVxuXG4gICAgX3NldFJlc2l6ZUV2ZW50KCkge1xuICAgICAgaWYgKHRoaXMuX2lzU2hvd24pIHtcbiAgICAgICAgJCh3aW5kb3cpLm9uKEV2ZW50LlJFU0laRSwgKGV2ZW50KSA9PiB0aGlzLmhhbmRsZVVwZGF0ZShldmVudCkpXG4gICAgICB9IGVsc2Uge1xuICAgICAgICAkKHdpbmRvdykub2ZmKEV2ZW50LlJFU0laRSlcbiAgICAgIH1cbiAgICB9XG5cbiAgICBfaGlkZU1vZGFsKCkge1xuICAgICAgdGhpcy5fZWxlbWVudC5zdHlsZS5kaXNwbGF5ID0gJ25vbmUnXG4gICAgICB0aGlzLl9lbGVtZW50LnNldEF0dHJpYnV0ZSgnYXJpYS1oaWRkZW4nLCB0cnVlKVxuICAgICAgdGhpcy5faXNUcmFuc2l0aW9uaW5nID0gZmFsc2VcbiAgICAgIHRoaXMuX3Nob3dCYWNrZHJvcCgoKSA9PiB7XG4gICAgICAgICQoZG9jdW1lbnQuYm9keSkucmVtb3ZlQ2xhc3MoQ2xhc3NOYW1lLk9QRU4pXG4gICAgICAgIHRoaXMuX3Jlc2V0QWRqdXN0bWVudHMoKVxuICAgICAgICB0aGlzLl9yZXNldFNjcm9sbGJhcigpXG4gICAgICAgICQodGhpcy5fZWxlbWVudCkudHJpZ2dlcihFdmVudC5ISURERU4pXG4gICAgICB9KVxuICAgIH1cblxuICAgIF9yZW1vdmVCYWNrZHJvcCgpIHtcbiAgICAgIGlmICh0aGlzLl9iYWNrZHJvcCkge1xuICAgICAgICAkKHRoaXMuX2JhY2tkcm9wKS5yZW1vdmUoKVxuICAgICAgICB0aGlzLl9iYWNrZHJvcCA9IG51bGxcbiAgICAgIH1cbiAgICB9XG5cbiAgICBfc2hvd0JhY2tkcm9wKGNhbGxiYWNrKSB7XG4gICAgICBjb25zdCBhbmltYXRlID0gJCh0aGlzLl9lbGVtZW50KS5oYXNDbGFzcyhDbGFzc05hbWUuRkFERSkgP1xuICAgICAgICBDbGFzc05hbWUuRkFERSA6ICcnXG5cbiAgICAgIGlmICh0aGlzLl9pc1Nob3duICYmIHRoaXMuX2NvbmZpZy5iYWNrZHJvcCkge1xuICAgICAgICBjb25zdCBkb0FuaW1hdGUgPSBVdGlsLnN1cHBvcnRzVHJhbnNpdGlvbkVuZCgpICYmIGFuaW1hdGVcblxuICAgICAgICB0aGlzLl9iYWNrZHJvcCA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2RpdicpXG4gICAgICAgIHRoaXMuX2JhY2tkcm9wLmNsYXNzTmFtZSA9IENsYXNzTmFtZS5CQUNLRFJPUFxuXG4gICAgICAgIGlmIChhbmltYXRlKSB7XG4gICAgICAgICAgJCh0aGlzLl9iYWNrZHJvcCkuYWRkQ2xhc3MoYW5pbWF0ZSlcbiAgICAgICAgfVxuXG4gICAgICAgICQodGhpcy5fYmFja2Ryb3ApLmFwcGVuZFRvKGRvY3VtZW50LmJvZHkpXG5cbiAgICAgICAgJCh0aGlzLl9lbGVtZW50KS5vbihFdmVudC5DTElDS19ESVNNSVNTLCAoZXZlbnQpID0+IHtcbiAgICAgICAgICBpZiAodGhpcy5faWdub3JlQmFja2Ryb3BDbGljaykge1xuICAgICAgICAgICAgdGhpcy5faWdub3JlQmFja2Ryb3BDbGljayA9IGZhbHNlXG4gICAgICAgICAgICByZXR1cm5cbiAgICAgICAgICB9XG4gICAgICAgICAgaWYgKGV2ZW50LnRhcmdldCAhPT0gZXZlbnQuY3VycmVudFRhcmdldCkge1xuICAgICAgICAgICAgcmV0dXJuXG4gICAgICAgICAgfVxuICAgICAgICAgIGlmICh0aGlzLl9jb25maWcuYmFja2Ryb3AgPT09ICdzdGF0aWMnKSB7XG4gICAgICAgICAgICB0aGlzLl9lbGVtZW50LmZvY3VzKClcbiAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgdGhpcy5oaWRlKClcbiAgICAgICAgICB9XG4gICAgICAgIH0pXG5cbiAgICAgICAgaWYgKGRvQW5pbWF0ZSkge1xuICAgICAgICAgIFV0aWwucmVmbG93KHRoaXMuX2JhY2tkcm9wKVxuICAgICAgICB9XG5cbiAgICAgICAgJCh0aGlzLl9iYWNrZHJvcCkuYWRkQ2xhc3MoQ2xhc3NOYW1lLlNIT1cpXG5cbiAgICAgICAgaWYgKCFjYWxsYmFjaykge1xuICAgICAgICAgIHJldHVyblxuICAgICAgICB9XG5cbiAgICAgICAgaWYgKCFkb0FuaW1hdGUpIHtcbiAgICAgICAgICBjYWxsYmFjaygpXG4gICAgICAgICAgcmV0dXJuXG4gICAgICAgIH1cblxuICAgICAgICAkKHRoaXMuX2JhY2tkcm9wKVxuICAgICAgICAgIC5vbmUoVXRpbC5UUkFOU0lUSU9OX0VORCwgY2FsbGJhY2spXG4gICAgICAgICAgLmVtdWxhdGVUcmFuc2l0aW9uRW5kKEJBQ0tEUk9QX1RSQU5TSVRJT05fRFVSQVRJT04pXG5cbiAgICAgIH0gZWxzZSBpZiAoIXRoaXMuX2lzU2hvd24gJiYgdGhpcy5fYmFja2Ryb3ApIHtcbiAgICAgICAgJCh0aGlzLl9iYWNrZHJvcCkucmVtb3ZlQ2xhc3MoQ2xhc3NOYW1lLlNIT1cpXG5cbiAgICAgICAgY29uc3QgY2FsbGJhY2tSZW1vdmUgPSAoKSA9PiB7XG4gICAgICAgICAgdGhpcy5fcmVtb3ZlQmFja2Ryb3AoKVxuICAgICAgICAgIGlmIChjYWxsYmFjaykge1xuICAgICAgICAgICAgY2FsbGJhY2soKVxuICAgICAgICAgIH1cbiAgICAgICAgfVxuXG4gICAgICAgIGlmIChVdGlsLnN1cHBvcnRzVHJhbnNpdGlvbkVuZCgpICYmXG4gICAgICAgICAgICQodGhpcy5fZWxlbWVudCkuaGFzQ2xhc3MoQ2xhc3NOYW1lLkZBREUpKSB7XG4gICAgICAgICAgJCh0aGlzLl9iYWNrZHJvcClcbiAgICAgICAgICAgIC5vbmUoVXRpbC5UUkFOU0lUSU9OX0VORCwgY2FsbGJhY2tSZW1vdmUpXG4gICAgICAgICAgICAuZW11bGF0ZVRyYW5zaXRpb25FbmQoQkFDS0RST1BfVFJBTlNJVElPTl9EVVJBVElPTilcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICBjYWxsYmFja1JlbW92ZSgpXG4gICAgICAgIH1cblxuICAgICAgfSBlbHNlIGlmIChjYWxsYmFjaykge1xuICAgICAgICBjYWxsYmFjaygpXG4gICAgICB9XG4gICAgfVxuXG5cbiAgICAvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gICAgLy8gdGhlIGZvbGxvd2luZyBtZXRob2RzIGFyZSB1c2VkIHRvIGhhbmRsZSBvdmVyZmxvd2luZyBtb2RhbHNcbiAgICAvLyB0b2RvIChmYXQpOiB0aGVzZSBzaG91bGQgcHJvYmFibHkgYmUgcmVmYWN0b3JlZCBvdXQgb2YgbW9kYWwuanNcbiAgICAvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cbiAgICBfYWRqdXN0RGlhbG9nKCkge1xuICAgICAgY29uc3QgaXNNb2RhbE92ZXJmbG93aW5nID1cbiAgICAgICAgdGhpcy5fZWxlbWVudC5zY3JvbGxIZWlnaHQgPiBkb2N1bWVudC5kb2N1bWVudEVsZW1lbnQuY2xpZW50SGVpZ2h0XG5cbiAgICAgIGlmICghdGhpcy5faXNCb2R5T3ZlcmZsb3dpbmcgJiYgaXNNb2RhbE92ZXJmbG93aW5nKSB7XG4gICAgICAgIHRoaXMuX2VsZW1lbnQuc3R5bGUucGFkZGluZ0xlZnQgPSBgJHt0aGlzLl9zY3JvbGxiYXJXaWR0aH1weGBcbiAgICAgIH1cblxuICAgICAgaWYgKHRoaXMuX2lzQm9keU92ZXJmbG93aW5nICYmICFpc01vZGFsT3ZlcmZsb3dpbmcpIHtcbiAgICAgICAgdGhpcy5fZWxlbWVudC5zdHlsZS5wYWRkaW5nUmlnaHQgPSBgJHt0aGlzLl9zY3JvbGxiYXJXaWR0aH1weGBcbiAgICAgIH1cbiAgICB9XG5cbiAgICBfcmVzZXRBZGp1c3RtZW50cygpIHtcbiAgICAgIHRoaXMuX2VsZW1lbnQuc3R5bGUucGFkZGluZ0xlZnQgPSAnJ1xuICAgICAgdGhpcy5fZWxlbWVudC5zdHlsZS5wYWRkaW5nUmlnaHQgPSAnJ1xuICAgIH1cblxuICAgIF9jaGVja1Njcm9sbGJhcigpIHtcbiAgICAgIHRoaXMuX2lzQm9keU92ZXJmbG93aW5nID0gZG9jdW1lbnQuYm9keS5jbGllbnRXaWR0aCA8IHdpbmRvdy5pbm5lcldpZHRoXG4gICAgICB0aGlzLl9zY3JvbGxiYXJXaWR0aCA9IHRoaXMuX2dldFNjcm9sbGJhcldpZHRoKClcbiAgICB9XG5cbiAgICBfc2V0U2Nyb2xsYmFyKCkge1xuICAgICAgaWYgKHRoaXMuX2lzQm9keU92ZXJmbG93aW5nKSB7XG4gICAgICAgIC8vIE5vdGU6IERPTU5vZGUuc3R5bGUucGFkZGluZ1JpZ2h0IHJldHVybnMgdGhlIGFjdHVhbCB2YWx1ZSBvciAnJyBpZiBub3Qgc2V0XG4gICAgICAgIC8vICAgd2hpbGUgJChET01Ob2RlKS5jc3MoJ3BhZGRpbmctcmlnaHQnKSByZXR1cm5zIHRoZSBjYWxjdWxhdGVkIHZhbHVlIG9yIDAgaWYgbm90IHNldFxuXG4gICAgICAgIC8vIEFkanVzdCBmaXhlZCBjb250ZW50IHBhZGRpbmdcbiAgICAgICAgJChTZWxlY3Rvci5GSVhFRF9DT05URU5UKS5lYWNoKChpbmRleCwgZWxlbWVudCkgPT4ge1xuICAgICAgICAgIGNvbnN0IGFjdHVhbFBhZGRpbmcgPSAkKGVsZW1lbnQpWzBdLnN0eWxlLnBhZGRpbmdSaWdodFxuICAgICAgICAgIGNvbnN0IGNhbGN1bGF0ZWRQYWRkaW5nID0gJChlbGVtZW50KS5jc3MoJ3BhZGRpbmctcmlnaHQnKVxuICAgICAgICAgICQoZWxlbWVudCkuZGF0YSgncGFkZGluZy1yaWdodCcsIGFjdHVhbFBhZGRpbmcpLmNzcygncGFkZGluZy1yaWdodCcsIGAke3BhcnNlRmxvYXQoY2FsY3VsYXRlZFBhZGRpbmcpICsgdGhpcy5fc2Nyb2xsYmFyV2lkdGh9cHhgKVxuICAgICAgICB9KVxuXG4gICAgICAgIC8vIEFkanVzdCBuYXZiYXItdG9nZ2xlciBtYXJnaW5cbiAgICAgICAgJChTZWxlY3Rvci5OQVZCQVJfVE9HR0xFUikuZWFjaCgoaW5kZXgsIGVsZW1lbnQpID0+IHtcbiAgICAgICAgICBjb25zdCBhY3R1YWxNYXJnaW4gPSAkKGVsZW1lbnQpWzBdLnN0eWxlLm1hcmdpblJpZ2h0XG4gICAgICAgICAgY29uc3QgY2FsY3VsYXRlZE1hcmdpbiA9ICQoZWxlbWVudCkuY3NzKCdtYXJnaW4tcmlnaHQnKVxuICAgICAgICAgICQoZWxlbWVudCkuZGF0YSgnbWFyZ2luLXJpZ2h0JywgYWN0dWFsTWFyZ2luKS5jc3MoJ21hcmdpbi1yaWdodCcsIGAke3BhcnNlRmxvYXQoY2FsY3VsYXRlZE1hcmdpbikgKyB0aGlzLl9zY3JvbGxiYXJXaWR0aH1weGApXG4gICAgICAgIH0pXG5cbiAgICAgICAgLy8gQWRqdXN0IGJvZHkgcGFkZGluZ1xuICAgICAgICBjb25zdCBhY3R1YWxQYWRkaW5nID0gZG9jdW1lbnQuYm9keS5zdHlsZS5wYWRkaW5nUmlnaHRcbiAgICAgICAgY29uc3QgY2FsY3VsYXRlZFBhZGRpbmcgPSAkKCdib2R5JykuY3NzKCdwYWRkaW5nLXJpZ2h0JylcbiAgICAgICAgJCgnYm9keScpLmRhdGEoJ3BhZGRpbmctcmlnaHQnLCBhY3R1YWxQYWRkaW5nKS5jc3MoJ3BhZGRpbmctcmlnaHQnLCBgJHtwYXJzZUZsb2F0KGNhbGN1bGF0ZWRQYWRkaW5nKSArIHRoaXMuX3Njcm9sbGJhcldpZHRofXB4YClcbiAgICAgIH1cbiAgICB9XG5cbiAgICBfcmVzZXRTY3JvbGxiYXIoKSB7XG4gICAgICAvLyBSZXN0b3JlIGZpeGVkIGNvbnRlbnQgcGFkZGluZ1xuICAgICAgJChTZWxlY3Rvci5GSVhFRF9DT05URU5UKS5lYWNoKChpbmRleCwgZWxlbWVudCkgPT4ge1xuICAgICAgICBjb25zdCBwYWRkaW5nID0gJChlbGVtZW50KS5kYXRhKCdwYWRkaW5nLXJpZ2h0JylcbiAgICAgICAgaWYgKHR5cGVvZiBwYWRkaW5nICE9PSAndW5kZWZpbmVkJykge1xuICAgICAgICAgICQoZWxlbWVudCkuY3NzKCdwYWRkaW5nLXJpZ2h0JywgcGFkZGluZykucmVtb3ZlRGF0YSgncGFkZGluZy1yaWdodCcpXG4gICAgICAgIH1cbiAgICAgIH0pXG5cbiAgICAgIC8vIFJlc3RvcmUgbmF2YmFyLXRvZ2dsZXIgbWFyZ2luXG4gICAgICAkKFNlbGVjdG9yLk5BVkJBUl9UT0dHTEVSKS5lYWNoKChpbmRleCwgZWxlbWVudCkgPT4ge1xuICAgICAgICBjb25zdCBtYXJnaW4gPSAkKGVsZW1lbnQpLmRhdGEoJ21hcmdpbi1yaWdodCcpXG4gICAgICAgIGlmICh0eXBlb2YgbWFyZ2luICE9PSAndW5kZWZpbmVkJykge1xuICAgICAgICAgICQoZWxlbWVudCkuY3NzKCdtYXJnaW4tcmlnaHQnLCBtYXJnaW4pLnJlbW92ZURhdGEoJ21hcmdpbi1yaWdodCcpXG4gICAgICAgIH1cbiAgICAgIH0pXG5cbiAgICAgIC8vIFJlc3RvcmUgYm9keSBwYWRkaW5nXG4gICAgICBjb25zdCBwYWRkaW5nID0gJCgnYm9keScpLmRhdGEoJ3BhZGRpbmctcmlnaHQnKVxuICAgICAgaWYgKHR5cGVvZiBwYWRkaW5nICE9PSAndW5kZWZpbmVkJykge1xuICAgICAgICAkKCdib2R5JykuY3NzKCdwYWRkaW5nLXJpZ2h0JywgcGFkZGluZykucmVtb3ZlRGF0YSgncGFkZGluZy1yaWdodCcpXG4gICAgICB9XG4gICAgfVxuXG4gICAgX2dldFNjcm9sbGJhcldpZHRoKCkgeyAvLyB0aHggZC53YWxzaFxuICAgICAgY29uc3Qgc2Nyb2xsRGl2ID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnZGl2JylcbiAgICAgIHNjcm9sbERpdi5jbGFzc05hbWUgPSBDbGFzc05hbWUuU0NST0xMQkFSX01FQVNVUkVSXG4gICAgICBkb2N1bWVudC5ib2R5LmFwcGVuZENoaWxkKHNjcm9sbERpdilcbiAgICAgIGNvbnN0IHNjcm9sbGJhcldpZHRoID0gc2Nyb2xsRGl2LmdldEJvdW5kaW5nQ2xpZW50UmVjdCgpLndpZHRoIC0gc2Nyb2xsRGl2LmNsaWVudFdpZHRoXG4gICAgICBkb2N1bWVudC5ib2R5LnJlbW92ZUNoaWxkKHNjcm9sbERpdilcbiAgICAgIHJldHVybiBzY3JvbGxiYXJXaWR0aFxuICAgIH1cblxuXG4gICAgLy8gc3RhdGljXG5cbiAgICBzdGF0aWMgX2pRdWVyeUludGVyZmFjZShjb25maWcsIHJlbGF0ZWRUYXJnZXQpIHtcbiAgICAgIHJldHVybiB0aGlzLmVhY2goZnVuY3Rpb24gKCkge1xuICAgICAgICBsZXQgZGF0YSAgICAgID0gJCh0aGlzKS5kYXRhKERBVEFfS0VZKVxuICAgICAgICBjb25zdCBfY29uZmlnID0gJC5leHRlbmQoXG4gICAgICAgICAge30sXG4gICAgICAgICAgTW9kYWwuRGVmYXVsdCxcbiAgICAgICAgICAkKHRoaXMpLmRhdGEoKSxcbiAgICAgICAgICB0eXBlb2YgY29uZmlnID09PSAnb2JqZWN0JyAmJiBjb25maWdcbiAgICAgICAgKVxuXG4gICAgICAgIGlmICghZGF0YSkge1xuICAgICAgICAgIGRhdGEgPSBuZXcgTW9kYWwodGhpcywgX2NvbmZpZylcbiAgICAgICAgICAkKHRoaXMpLmRhdGEoREFUQV9LRVksIGRhdGEpXG4gICAgICAgIH1cblxuICAgICAgICBpZiAodHlwZW9mIGNvbmZpZyA9PT0gJ3N0cmluZycpIHtcbiAgICAgICAgICBpZiAoZGF0YVtjb25maWddID09PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgIHRocm93IG5ldyBFcnJvcihgTm8gbWV0aG9kIG5hbWVkIFwiJHtjb25maWd9XCJgKVxuICAgICAgICAgIH1cbiAgICAgICAgICBkYXRhW2NvbmZpZ10ocmVsYXRlZFRhcmdldClcbiAgICAgICAgfSBlbHNlIGlmIChfY29uZmlnLnNob3cpIHtcbiAgICAgICAgICBkYXRhLnNob3cocmVsYXRlZFRhcmdldClcbiAgICAgICAgfVxuICAgICAgfSlcbiAgICB9XG5cbiAgfVxuXG5cbiAgLyoqXG4gICAqIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuICAgKiBEYXRhIEFwaSBpbXBsZW1lbnRhdGlvblxuICAgKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiAgICovXG5cbiAgJChkb2N1bWVudCkub24oRXZlbnQuQ0xJQ0tfREFUQV9BUEksIFNlbGVjdG9yLkRBVEFfVE9HR0xFLCBmdW5jdGlvbiAoZXZlbnQpIHtcbiAgICBsZXQgdGFyZ2V0XG4gICAgY29uc3Qgc2VsZWN0b3IgPSBVdGlsLmdldFNlbGVjdG9yRnJvbUVsZW1lbnQodGhpcylcblxuICAgIGlmIChzZWxlY3Rvcikge1xuICAgICAgdGFyZ2V0ID0gJChzZWxlY3RvcilbMF1cbiAgICB9XG5cbiAgICBjb25zdCBjb25maWcgPSAkKHRhcmdldCkuZGF0YShEQVRBX0tFWSkgP1xuICAgICAgJ3RvZ2dsZScgOiAkLmV4dGVuZCh7fSwgJCh0YXJnZXQpLmRhdGEoKSwgJCh0aGlzKS5kYXRhKCkpXG5cbiAgICBpZiAodGhpcy50YWdOYW1lID09PSAnQScgfHwgdGhpcy50YWdOYW1lID09PSAnQVJFQScpIHtcbiAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KClcbiAgICB9XG5cbiAgICBjb25zdCAkdGFyZ2V0ID0gJCh0YXJnZXQpLm9uZShFdmVudC5TSE9XLCAoc2hvd0V2ZW50KSA9PiB7XG4gICAgICBpZiAoc2hvd0V2ZW50LmlzRGVmYXVsdFByZXZlbnRlZCgpKSB7XG4gICAgICAgIC8vIG9ubHkgcmVnaXN0ZXIgZm9jdXMgcmVzdG9yZXIgaWYgbW9kYWwgd2lsbCBhY3R1YWxseSBnZXQgc2hvd25cbiAgICAgICAgcmV0dXJuXG4gICAgICB9XG5cbiAgICAgICR0YXJnZXQub25lKEV2ZW50LkhJRERFTiwgKCkgPT4ge1xuICAgICAgICBpZiAoJCh0aGlzKS5pcygnOnZpc2libGUnKSkge1xuICAgICAgICAgIHRoaXMuZm9jdXMoKVxuICAgICAgICB9XG4gICAgICB9KVxuICAgIH0pXG5cbiAgICBNb2RhbC5falF1ZXJ5SW50ZXJmYWNlLmNhbGwoJCh0YXJnZXQpLCBjb25maWcsIHRoaXMpXG4gIH0pXG5cblxuICAvKipcbiAgICogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gICAqIGpRdWVyeVxuICAgKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiAgICovXG5cbiAgJC5mbltOQU1FXSAgICAgICAgICAgICA9IE1vZGFsLl9qUXVlcnlJbnRlcmZhY2VcbiAgJC5mbltOQU1FXS5Db25zdHJ1Y3RvciA9IE1vZGFsXG4gICQuZm5bTkFNRV0ubm9Db25mbGljdCAgPSBmdW5jdGlvbiAoKSB7XG4gICAgJC5mbltOQU1FXSA9IEpRVUVSWV9OT19DT05GTElDVFxuICAgIHJldHVybiBNb2RhbC5falF1ZXJ5SW50ZXJmYWNlXG4gIH1cblxuICByZXR1cm4gTW9kYWxcblxufSkoalF1ZXJ5KVxuXG5leHBvcnQgZGVmYXVsdCBNb2RhbFxuIiwiaW1wb3J0IFV0aWwgZnJvbSAnLi91dGlsJ1xuXG5cbi8qKlxuICogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiAqIEJvb3RzdHJhcCAodjQuMC4wLWJldGEpOiBzY3JvbGxzcHkuanNcbiAqIExpY2Vuc2VkIHVuZGVyIE1JVCAoaHR0cHM6Ly9naXRodWIuY29tL3R3YnMvYm9vdHN0cmFwL2Jsb2IvbWFzdGVyL0xJQ0VOU0UpXG4gKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuICovXG5cbmNvbnN0IFNjcm9sbFNweSA9ICgoJCkgPT4ge1xuXG5cbiAgLyoqXG4gICAqIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuICAgKiBDb25zdGFudHNcbiAgICogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gICAqL1xuXG4gIGNvbnN0IE5BTUUgICAgICAgICAgICAgICA9ICdzY3JvbGxzcHknXG4gIGNvbnN0IFZFUlNJT04gICAgICAgICAgICA9ICc0LjAuMC1iZXRhJ1xuICBjb25zdCBEQVRBX0tFWSAgICAgICAgICAgPSAnYnMuc2Nyb2xsc3B5J1xuICBjb25zdCBFVkVOVF9LRVkgICAgICAgICAgPSBgLiR7REFUQV9LRVl9YFxuICBjb25zdCBEQVRBX0FQSV9LRVkgICAgICAgPSAnLmRhdGEtYXBpJ1xuICBjb25zdCBKUVVFUllfTk9fQ09ORkxJQ1QgPSAkLmZuW05BTUVdXG5cbiAgY29uc3QgRGVmYXVsdCA9IHtcbiAgICBvZmZzZXQgOiAxMCxcbiAgICBtZXRob2QgOiAnYXV0bycsXG4gICAgdGFyZ2V0IDogJydcbiAgfVxuXG4gIGNvbnN0IERlZmF1bHRUeXBlID0ge1xuICAgIG9mZnNldCA6ICdudW1iZXInLFxuICAgIG1ldGhvZCA6ICdzdHJpbmcnLFxuICAgIHRhcmdldCA6ICcoc3RyaW5nfGVsZW1lbnQpJ1xuICB9XG5cbiAgY29uc3QgRXZlbnQgPSB7XG4gICAgQUNUSVZBVEUgICAgICA6IGBhY3RpdmF0ZSR7RVZFTlRfS0VZfWAsXG4gICAgU0NST0xMICAgICAgICA6IGBzY3JvbGwke0VWRU5UX0tFWX1gLFxuICAgIExPQURfREFUQV9BUEkgOiBgbG9hZCR7RVZFTlRfS0VZfSR7REFUQV9BUElfS0VZfWBcbiAgfVxuXG4gIGNvbnN0IENsYXNzTmFtZSA9IHtcbiAgICBEUk9QRE9XTl9JVEVNIDogJ2Ryb3Bkb3duLWl0ZW0nLFxuICAgIERST1BET1dOX01FTlUgOiAnZHJvcGRvd24tbWVudScsXG4gICAgQUNUSVZFICAgICAgICA6ICdhY3RpdmUnXG4gIH1cblxuICBjb25zdCBTZWxlY3RvciA9IHtcbiAgICBEQVRBX1NQWSAgICAgICAgOiAnW2RhdGEtc3B5PVwic2Nyb2xsXCJdJyxcbiAgICBBQ1RJVkUgICAgICAgICAgOiAnLmFjdGl2ZScsXG4gICAgTkFWX0xJU1RfR1JPVVAgIDogJy5uYXYsIC5saXN0LWdyb3VwJyxcbiAgICBOQVZfTElOS1MgICAgICAgOiAnLm5hdi1saW5rJyxcbiAgICBMSVNUX0lURU1TICAgICAgOiAnLmxpc3QtZ3JvdXAtaXRlbScsXG4gICAgRFJPUERPV04gICAgICAgIDogJy5kcm9wZG93bicsXG4gICAgRFJPUERPV05fSVRFTVMgIDogJy5kcm9wZG93bi1pdGVtJyxcbiAgICBEUk9QRE9XTl9UT0dHTEUgOiAnLmRyb3Bkb3duLXRvZ2dsZSdcbiAgfVxuXG4gIGNvbnN0IE9mZnNldE1ldGhvZCA9IHtcbiAgICBPRkZTRVQgICA6ICdvZmZzZXQnLFxuICAgIFBPU0lUSU9OIDogJ3Bvc2l0aW9uJ1xuICB9XG5cblxuICAvKipcbiAgICogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gICAqIENsYXNzIERlZmluaXRpb25cbiAgICogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gICAqL1xuXG4gIGNsYXNzIFNjcm9sbFNweSB7XG5cbiAgICBjb25zdHJ1Y3RvcihlbGVtZW50LCBjb25maWcpIHtcbiAgICAgIHRoaXMuX2VsZW1lbnQgICAgICAgPSBlbGVtZW50XG4gICAgICB0aGlzLl9zY3JvbGxFbGVtZW50ID0gZWxlbWVudC50YWdOYW1lID09PSAnQk9EWScgPyB3aW5kb3cgOiBlbGVtZW50XG4gICAgICB0aGlzLl9jb25maWcgICAgICAgID0gdGhpcy5fZ2V0Q29uZmlnKGNvbmZpZylcbiAgICAgIHRoaXMuX3NlbGVjdG9yICAgICAgPSBgJHt0aGlzLl9jb25maWcudGFyZ2V0fSAke1NlbGVjdG9yLk5BVl9MSU5LU30sYFxuICAgICAgICAgICAgICAgICAgICAgICAgICArIGAke3RoaXMuX2NvbmZpZy50YXJnZXR9ICR7U2VsZWN0b3IuTElTVF9JVEVNU30sYFxuICAgICAgICAgICAgICAgICAgICAgICAgICArIGAke3RoaXMuX2NvbmZpZy50YXJnZXR9ICR7U2VsZWN0b3IuRFJPUERPV05fSVRFTVN9YFxuICAgICAgdGhpcy5fb2Zmc2V0cyAgICAgICA9IFtdXG4gICAgICB0aGlzLl90YXJnZXRzICAgICAgID0gW11cbiAgICAgIHRoaXMuX2FjdGl2ZVRhcmdldCAgPSBudWxsXG4gICAgICB0aGlzLl9zY3JvbGxIZWlnaHQgID0gMFxuXG4gICAgICAkKHRoaXMuX3Njcm9sbEVsZW1lbnQpLm9uKEV2ZW50LlNDUk9MTCwgKGV2ZW50KSA9PiB0aGlzLl9wcm9jZXNzKGV2ZW50KSlcblxuICAgICAgdGhpcy5yZWZyZXNoKClcbiAgICAgIHRoaXMuX3Byb2Nlc3MoKVxuICAgIH1cblxuXG4gICAgLy8gZ2V0dGVyc1xuXG4gICAgc3RhdGljIGdldCBWRVJTSU9OKCkge1xuICAgICAgcmV0dXJuIFZFUlNJT05cbiAgICB9XG5cbiAgICBzdGF0aWMgZ2V0IERlZmF1bHQoKSB7XG4gICAgICByZXR1cm4gRGVmYXVsdFxuICAgIH1cblxuXG4gICAgLy8gcHVibGljXG5cbiAgICByZWZyZXNoKCkge1xuICAgICAgY29uc3QgYXV0b01ldGhvZCA9IHRoaXMuX3Njcm9sbEVsZW1lbnQgIT09IHRoaXMuX3Njcm9sbEVsZW1lbnQud2luZG93ID9cbiAgICAgICAgT2Zmc2V0TWV0aG9kLlBPU0lUSU9OIDogT2Zmc2V0TWV0aG9kLk9GRlNFVFxuXG4gICAgICBjb25zdCBvZmZzZXRNZXRob2QgPSB0aGlzLl9jb25maWcubWV0aG9kID09PSAnYXV0bycgP1xuICAgICAgICBhdXRvTWV0aG9kIDogdGhpcy5fY29uZmlnLm1ldGhvZFxuXG4gICAgICBjb25zdCBvZmZzZXRCYXNlID0gb2Zmc2V0TWV0aG9kID09PSBPZmZzZXRNZXRob2QuUE9TSVRJT04gP1xuICAgICAgICB0aGlzLl9nZXRTY3JvbGxUb3AoKSA6IDBcblxuICAgICAgdGhpcy5fb2Zmc2V0cyA9IFtdXG4gICAgICB0aGlzLl90YXJnZXRzID0gW11cblxuICAgICAgdGhpcy5fc2Nyb2xsSGVpZ2h0ID0gdGhpcy5fZ2V0U2Nyb2xsSGVpZ2h0KClcblxuICAgICAgY29uc3QgdGFyZ2V0cyA9ICQubWFrZUFycmF5KCQodGhpcy5fc2VsZWN0b3IpKVxuXG4gICAgICB0YXJnZXRzXG4gICAgICAgIC5tYXAoKGVsZW1lbnQpID0+IHtcbiAgICAgICAgICBsZXQgdGFyZ2V0XG4gICAgICAgICAgY29uc3QgdGFyZ2V0U2VsZWN0b3IgPSBVdGlsLmdldFNlbGVjdG9yRnJvbUVsZW1lbnQoZWxlbWVudClcblxuICAgICAgICAgIGlmICh0YXJnZXRTZWxlY3Rvcikge1xuICAgICAgICAgICAgdGFyZ2V0ID0gJCh0YXJnZXRTZWxlY3RvcilbMF1cbiAgICAgICAgICB9XG5cbiAgICAgICAgICBpZiAodGFyZ2V0KSB7XG4gICAgICAgICAgICBjb25zdCB0YXJnZXRCQ1IgPSB0YXJnZXQuZ2V0Qm91bmRpbmdDbGllbnRSZWN0KClcbiAgICAgICAgICAgIGlmICh0YXJnZXRCQ1Iud2lkdGggfHwgdGFyZ2V0QkNSLmhlaWdodCkge1xuICAgICAgICAgICAgICAvLyB0b2RvIChmYXQpOiByZW1vdmUgc2tldGNoIHJlbGlhbmNlIG9uIGpRdWVyeSBwb3NpdGlvbi9vZmZzZXRcbiAgICAgICAgICAgICAgcmV0dXJuIFtcbiAgICAgICAgICAgICAgICAkKHRhcmdldClbb2Zmc2V0TWV0aG9kXSgpLnRvcCArIG9mZnNldEJhc2UsXG4gICAgICAgICAgICAgICAgdGFyZ2V0U2VsZWN0b3JcbiAgICAgICAgICAgICAgXVxuICAgICAgICAgICAgfVxuICAgICAgICAgIH1cbiAgICAgICAgICByZXR1cm4gbnVsbFxuICAgICAgICB9KVxuICAgICAgICAuZmlsdGVyKChpdGVtKSAgPT4gaXRlbSlcbiAgICAgICAgLnNvcnQoKGEsIGIpICAgID0+IGFbMF0gLSBiWzBdKVxuICAgICAgICAuZm9yRWFjaCgoaXRlbSkgPT4ge1xuICAgICAgICAgIHRoaXMuX29mZnNldHMucHVzaChpdGVtWzBdKVxuICAgICAgICAgIHRoaXMuX3RhcmdldHMucHVzaChpdGVtWzFdKVxuICAgICAgICB9KVxuICAgIH1cblxuICAgIGRpc3Bvc2UoKSB7XG4gICAgICAkLnJlbW92ZURhdGEodGhpcy5fZWxlbWVudCwgREFUQV9LRVkpXG4gICAgICAkKHRoaXMuX3Njcm9sbEVsZW1lbnQpLm9mZihFVkVOVF9LRVkpXG5cbiAgICAgIHRoaXMuX2VsZW1lbnQgICAgICAgPSBudWxsXG4gICAgICB0aGlzLl9zY3JvbGxFbGVtZW50ID0gbnVsbFxuICAgICAgdGhpcy5fY29uZmlnICAgICAgICA9IG51bGxcbiAgICAgIHRoaXMuX3NlbGVjdG9yICAgICAgPSBudWxsXG4gICAgICB0aGlzLl9vZmZzZXRzICAgICAgID0gbnVsbFxuICAgICAgdGhpcy5fdGFyZ2V0cyAgICAgICA9IG51bGxcbiAgICAgIHRoaXMuX2FjdGl2ZVRhcmdldCAgPSBudWxsXG4gICAgICB0aGlzLl9zY3JvbGxIZWlnaHQgID0gbnVsbFxuICAgIH1cblxuXG4gICAgLy8gcHJpdmF0ZVxuXG4gICAgX2dldENvbmZpZyhjb25maWcpIHtcbiAgICAgIGNvbmZpZyA9ICQuZXh0ZW5kKHt9LCBEZWZhdWx0LCBjb25maWcpXG5cbiAgICAgIGlmICh0eXBlb2YgY29uZmlnLnRhcmdldCAhPT0gJ3N0cmluZycpIHtcbiAgICAgICAgbGV0IGlkID0gJChjb25maWcudGFyZ2V0KS5hdHRyKCdpZCcpXG4gICAgICAgIGlmICghaWQpIHtcbiAgICAgICAgICBpZCA9IFV0aWwuZ2V0VUlEKE5BTUUpXG4gICAgICAgICAgJChjb25maWcudGFyZ2V0KS5hdHRyKCdpZCcsIGlkKVxuICAgICAgICB9XG4gICAgICAgIGNvbmZpZy50YXJnZXQgPSBgIyR7aWR9YFxuICAgICAgfVxuXG4gICAgICBVdGlsLnR5cGVDaGVja0NvbmZpZyhOQU1FLCBjb25maWcsIERlZmF1bHRUeXBlKVxuXG4gICAgICByZXR1cm4gY29uZmlnXG4gICAgfVxuXG4gICAgX2dldFNjcm9sbFRvcCgpIHtcbiAgICAgIHJldHVybiB0aGlzLl9zY3JvbGxFbGVtZW50ID09PSB3aW5kb3cgP1xuICAgICAgICAgIHRoaXMuX3Njcm9sbEVsZW1lbnQucGFnZVlPZmZzZXQgOiB0aGlzLl9zY3JvbGxFbGVtZW50LnNjcm9sbFRvcFxuICAgIH1cblxuICAgIF9nZXRTY3JvbGxIZWlnaHQoKSB7XG4gICAgICByZXR1cm4gdGhpcy5fc2Nyb2xsRWxlbWVudC5zY3JvbGxIZWlnaHQgfHwgTWF0aC5tYXgoXG4gICAgICAgIGRvY3VtZW50LmJvZHkuc2Nyb2xsSGVpZ2h0LFxuICAgICAgICBkb2N1bWVudC5kb2N1bWVudEVsZW1lbnQuc2Nyb2xsSGVpZ2h0XG4gICAgICApXG4gICAgfVxuXG4gICAgX2dldE9mZnNldEhlaWdodCgpIHtcbiAgICAgIHJldHVybiB0aGlzLl9zY3JvbGxFbGVtZW50ID09PSB3aW5kb3cgP1xuICAgICAgICAgIHdpbmRvdy5pbm5lckhlaWdodCA6IHRoaXMuX3Njcm9sbEVsZW1lbnQuZ2V0Qm91bmRpbmdDbGllbnRSZWN0KCkuaGVpZ2h0XG4gICAgfVxuXG4gICAgX3Byb2Nlc3MoKSB7XG4gICAgICBjb25zdCBzY3JvbGxUb3AgICAgPSB0aGlzLl9nZXRTY3JvbGxUb3AoKSArIHRoaXMuX2NvbmZpZy5vZmZzZXRcbiAgICAgIGNvbnN0IHNjcm9sbEhlaWdodCA9IHRoaXMuX2dldFNjcm9sbEhlaWdodCgpXG4gICAgICBjb25zdCBtYXhTY3JvbGwgICAgPSB0aGlzLl9jb25maWcub2Zmc2V0XG4gICAgICAgICsgc2Nyb2xsSGVpZ2h0XG4gICAgICAgIC0gdGhpcy5fZ2V0T2Zmc2V0SGVpZ2h0KClcblxuICAgICAgaWYgKHRoaXMuX3Njcm9sbEhlaWdodCAhPT0gc2Nyb2xsSGVpZ2h0KSB7XG4gICAgICAgIHRoaXMucmVmcmVzaCgpXG4gICAgICB9XG5cbiAgICAgIGlmIChzY3JvbGxUb3AgPj0gbWF4U2Nyb2xsKSB7XG4gICAgICAgIGNvbnN0IHRhcmdldCA9IHRoaXMuX3RhcmdldHNbdGhpcy5fdGFyZ2V0cy5sZW5ndGggLSAxXVxuXG4gICAgICAgIGlmICh0aGlzLl9hY3RpdmVUYXJnZXQgIT09IHRhcmdldCkge1xuICAgICAgICAgIHRoaXMuX2FjdGl2YXRlKHRhcmdldClcbiAgICAgICAgfVxuICAgICAgICByZXR1cm5cbiAgICAgIH1cblxuICAgICAgaWYgKHRoaXMuX2FjdGl2ZVRhcmdldCAmJiBzY3JvbGxUb3AgPCB0aGlzLl9vZmZzZXRzWzBdICYmIHRoaXMuX29mZnNldHNbMF0gPiAwKSB7XG4gICAgICAgIHRoaXMuX2FjdGl2ZVRhcmdldCA9IG51bGxcbiAgICAgICAgdGhpcy5fY2xlYXIoKVxuICAgICAgICByZXR1cm5cbiAgICAgIH1cblxuICAgICAgZm9yIChsZXQgaSA9IHRoaXMuX29mZnNldHMubGVuZ3RoOyBpLS07KSB7XG4gICAgICAgIGNvbnN0IGlzQWN0aXZlVGFyZ2V0ID0gdGhpcy5fYWN0aXZlVGFyZ2V0ICE9PSB0aGlzLl90YXJnZXRzW2ldXG4gICAgICAgICAgICAmJiBzY3JvbGxUb3AgPj0gdGhpcy5fb2Zmc2V0c1tpXVxuICAgICAgICAgICAgJiYgKHRoaXMuX29mZnNldHNbaSArIDFdID09PSB1bmRlZmluZWQgfHxcbiAgICAgICAgICAgICAgICBzY3JvbGxUb3AgPCB0aGlzLl9vZmZzZXRzW2kgKyAxXSlcblxuICAgICAgICBpZiAoaXNBY3RpdmVUYXJnZXQpIHtcbiAgICAgICAgICB0aGlzLl9hY3RpdmF0ZSh0aGlzLl90YXJnZXRzW2ldKVxuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuXG4gICAgX2FjdGl2YXRlKHRhcmdldCkge1xuICAgICAgdGhpcy5fYWN0aXZlVGFyZ2V0ID0gdGFyZ2V0XG5cbiAgICAgIHRoaXMuX2NsZWFyKClcblxuICAgICAgbGV0IHF1ZXJpZXMgPSB0aGlzLl9zZWxlY3Rvci5zcGxpdCgnLCcpXG4gICAgICBxdWVyaWVzICAgICA9IHF1ZXJpZXMubWFwKChzZWxlY3RvcikgPT4ge1xuICAgICAgICByZXR1cm4gYCR7c2VsZWN0b3J9W2RhdGEtdGFyZ2V0PVwiJHt0YXJnZXR9XCJdLGAgK1xuICAgICAgICAgICAgICAgYCR7c2VsZWN0b3J9W2hyZWY9XCIke3RhcmdldH1cIl1gXG4gICAgICB9KVxuXG4gICAgICBjb25zdCAkbGluayA9ICQocXVlcmllcy5qb2luKCcsJykpXG5cbiAgICAgIGlmICgkbGluay5oYXNDbGFzcyhDbGFzc05hbWUuRFJPUERPV05fSVRFTSkpIHtcbiAgICAgICAgJGxpbmsuY2xvc2VzdChTZWxlY3Rvci5EUk9QRE9XTikuZmluZChTZWxlY3Rvci5EUk9QRE9XTl9UT0dHTEUpLmFkZENsYXNzKENsYXNzTmFtZS5BQ1RJVkUpXG4gICAgICAgICRsaW5rLmFkZENsYXNzKENsYXNzTmFtZS5BQ1RJVkUpXG4gICAgICB9IGVsc2Uge1xuICAgICAgICAvLyBTZXQgdHJpZ2dlcmVkIGxpbmsgYXMgYWN0aXZlXG4gICAgICAgICRsaW5rLmFkZENsYXNzKENsYXNzTmFtZS5BQ1RJVkUpXG4gICAgICAgIC8vIFNldCB0cmlnZ2VyZWQgbGlua3MgcGFyZW50cyBhcyBhY3RpdmVcbiAgICAgICAgLy8gV2l0aCBib3RoIDx1bD4gYW5kIDxuYXY+IG1hcmt1cCBhIHBhcmVudCBpcyB0aGUgcHJldmlvdXMgc2libGluZyBvZiBhbnkgbmF2IGFuY2VzdG9yXG4gICAgICAgICRsaW5rLnBhcmVudHMoU2VsZWN0b3IuTkFWX0xJU1RfR1JPVVApLnByZXYoYCR7U2VsZWN0b3IuTkFWX0xJTktTfSwgJHtTZWxlY3Rvci5MSVNUX0lURU1TfWApLmFkZENsYXNzKENsYXNzTmFtZS5BQ1RJVkUpXG4gICAgICB9XG5cbiAgICAgICQodGhpcy5fc2Nyb2xsRWxlbWVudCkudHJpZ2dlcihFdmVudC5BQ1RJVkFURSwge1xuICAgICAgICByZWxhdGVkVGFyZ2V0OiB0YXJnZXRcbiAgICAgIH0pXG4gICAgfVxuXG4gICAgX2NsZWFyKCkge1xuICAgICAgJCh0aGlzLl9zZWxlY3RvcikuZmlsdGVyKFNlbGVjdG9yLkFDVElWRSkucmVtb3ZlQ2xhc3MoQ2xhc3NOYW1lLkFDVElWRSlcbiAgICB9XG5cblxuICAgIC8vIHN0YXRpY1xuXG4gICAgc3RhdGljIF9qUXVlcnlJbnRlcmZhY2UoY29uZmlnKSB7XG4gICAgICByZXR1cm4gdGhpcy5lYWNoKGZ1bmN0aW9uICgpIHtcbiAgICAgICAgbGV0IGRhdGEgICAgICA9ICQodGhpcykuZGF0YShEQVRBX0tFWSlcbiAgICAgICAgY29uc3QgX2NvbmZpZyA9IHR5cGVvZiBjb25maWcgPT09ICdvYmplY3QnICYmIGNvbmZpZ1xuXG4gICAgICAgIGlmICghZGF0YSkge1xuICAgICAgICAgIGRhdGEgPSBuZXcgU2Nyb2xsU3B5KHRoaXMsIF9jb25maWcpXG4gICAgICAgICAgJCh0aGlzKS5kYXRhKERBVEFfS0VZLCBkYXRhKVxuICAgICAgICB9XG5cbiAgICAgICAgaWYgKHR5cGVvZiBjb25maWcgPT09ICdzdHJpbmcnKSB7XG4gICAgICAgICAgaWYgKGRhdGFbY29uZmlnXSA9PT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICB0aHJvdyBuZXcgRXJyb3IoYE5vIG1ldGhvZCBuYW1lZCBcIiR7Y29uZmlnfVwiYClcbiAgICAgICAgICB9XG4gICAgICAgICAgZGF0YVtjb25maWddKClcbiAgICAgICAgfVxuICAgICAgfSlcbiAgICB9XG5cblxuICB9XG5cblxuICAvKipcbiAgICogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gICAqIERhdGEgQXBpIGltcGxlbWVudGF0aW9uXG4gICAqIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuICAgKi9cblxuICAkKHdpbmRvdykub24oRXZlbnQuTE9BRF9EQVRBX0FQSSwgKCkgPT4ge1xuICAgIGNvbnN0IHNjcm9sbFNweXMgPSAkLm1ha2VBcnJheSgkKFNlbGVjdG9yLkRBVEFfU1BZKSlcblxuICAgIGZvciAobGV0IGkgPSBzY3JvbGxTcHlzLmxlbmd0aDsgaS0tOykge1xuICAgICAgY29uc3QgJHNweSA9ICQoc2Nyb2xsU3B5c1tpXSlcbiAgICAgIFNjcm9sbFNweS5falF1ZXJ5SW50ZXJmYWNlLmNhbGwoJHNweSwgJHNweS5kYXRhKCkpXG4gICAgfVxuICB9KVxuXG5cbiAgLyoqXG4gICAqIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuICAgKiBqUXVlcnlcbiAgICogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gICAqL1xuXG4gICQuZm5bTkFNRV0gICAgICAgICAgICAgPSBTY3JvbGxTcHkuX2pRdWVyeUludGVyZmFjZVxuICAkLmZuW05BTUVdLkNvbnN0cnVjdG9yID0gU2Nyb2xsU3B5XG4gICQuZm5bTkFNRV0ubm9Db25mbGljdCAgPSBmdW5jdGlvbiAoKSB7XG4gICAgJC5mbltOQU1FXSA9IEpRVUVSWV9OT19DT05GTElDVFxuICAgIHJldHVybiBTY3JvbGxTcHkuX2pRdWVyeUludGVyZmFjZVxuICB9XG5cbiAgcmV0dXJuIFNjcm9sbFNweVxuXG59KShqUXVlcnkpXG5cbmV4cG9ydCBkZWZhdWx0IFNjcm9sbFNweVxuIiwiaW1wb3J0IFV0aWwgZnJvbSAnLi91dGlsJ1xuXG5cbi8qKlxuICogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiAqIEJvb3RzdHJhcCAodjQuMC4wLWJldGEpOiB0YWIuanNcbiAqIExpY2Vuc2VkIHVuZGVyIE1JVCAoaHR0cHM6Ly9naXRodWIuY29tL3R3YnMvYm9vdHN0cmFwL2Jsb2IvbWFzdGVyL0xJQ0VOU0UpXG4gKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuICovXG5cbmNvbnN0IFRhYiA9ICgoJCkgPT4ge1xuXG5cbiAgLyoqXG4gICAqIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuICAgKiBDb25zdGFudHNcbiAgICogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gICAqL1xuXG4gIGNvbnN0IE5BTUUgICAgICAgICAgICAgICAgPSAndGFiJ1xuICBjb25zdCBWRVJTSU9OICAgICAgICAgICAgID0gJzQuMC4wLWJldGEnXG4gIGNvbnN0IERBVEFfS0VZICAgICAgICAgICAgPSAnYnMudGFiJ1xuICBjb25zdCBFVkVOVF9LRVkgICAgICAgICAgID0gYC4ke0RBVEFfS0VZfWBcbiAgY29uc3QgREFUQV9BUElfS0VZICAgICAgICA9ICcuZGF0YS1hcGknXG4gIGNvbnN0IEpRVUVSWV9OT19DT05GTElDVCAgPSAkLmZuW05BTUVdXG4gIGNvbnN0IFRSQU5TSVRJT05fRFVSQVRJT04gPSAxNTBcblxuICBjb25zdCBFdmVudCA9IHtcbiAgICBISURFICAgICAgICAgICA6IGBoaWRlJHtFVkVOVF9LRVl9YCxcbiAgICBISURERU4gICAgICAgICA6IGBoaWRkZW4ke0VWRU5UX0tFWX1gLFxuICAgIFNIT1cgICAgICAgICAgIDogYHNob3cke0VWRU5UX0tFWX1gLFxuICAgIFNIT1dOICAgICAgICAgIDogYHNob3duJHtFVkVOVF9LRVl9YCxcbiAgICBDTElDS19EQVRBX0FQSSA6IGBjbGljayR7RVZFTlRfS0VZfSR7REFUQV9BUElfS0VZfWBcbiAgfVxuXG4gIGNvbnN0IENsYXNzTmFtZSA9IHtcbiAgICBEUk9QRE9XTl9NRU5VIDogJ2Ryb3Bkb3duLW1lbnUnLFxuICAgIEFDVElWRSAgICAgICAgOiAnYWN0aXZlJyxcbiAgICBESVNBQkxFRCAgICAgIDogJ2Rpc2FibGVkJyxcbiAgICBGQURFICAgICAgICAgIDogJ2ZhZGUnLFxuICAgIFNIT1cgICAgICAgICAgOiAnc2hvdydcbiAgfVxuXG4gIGNvbnN0IFNlbGVjdG9yID0ge1xuICAgIERST1BET1dOICAgICAgICAgICAgICA6ICcuZHJvcGRvd24nLFxuICAgIE5BVl9MSVNUX0dST1VQICAgICAgICA6ICcubmF2LCAubGlzdC1ncm91cCcsXG4gICAgQUNUSVZFICAgICAgICAgICAgICAgIDogJy5hY3RpdmUnLFxuICAgIERBVEFfVE9HR0xFICAgICAgICAgICA6ICdbZGF0YS10b2dnbGU9XCJ0YWJcIl0sIFtkYXRhLXRvZ2dsZT1cInBpbGxcIl0sIFtkYXRhLXRvZ2dsZT1cImxpc3RcIl0nLFxuICAgIERST1BET1dOX1RPR0dMRSAgICAgICA6ICcuZHJvcGRvd24tdG9nZ2xlJyxcbiAgICBEUk9QRE9XTl9BQ1RJVkVfQ0hJTEQgOiAnPiAuZHJvcGRvd24tbWVudSAuYWN0aXZlJ1xuICB9XG5cblxuICAvKipcbiAgICogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gICAqIENsYXNzIERlZmluaXRpb25cbiAgICogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gICAqL1xuXG4gIGNsYXNzIFRhYiB7XG5cbiAgICBjb25zdHJ1Y3RvcihlbGVtZW50KSB7XG4gICAgICB0aGlzLl9lbGVtZW50ID0gZWxlbWVudFxuICAgIH1cblxuXG4gICAgLy8gZ2V0dGVyc1xuXG4gICAgc3RhdGljIGdldCBWRVJTSU9OKCkge1xuICAgICAgcmV0dXJuIFZFUlNJT05cbiAgICB9XG5cblxuICAgIC8vIHB1YmxpY1xuXG4gICAgc2hvdygpIHtcbiAgICAgIGlmICh0aGlzLl9lbGVtZW50LnBhcmVudE5vZGUgJiZcbiAgICAgICAgICB0aGlzLl9lbGVtZW50LnBhcmVudE5vZGUubm9kZVR5cGUgPT09IE5vZGUuRUxFTUVOVF9OT0RFICYmXG4gICAgICAgICAgJCh0aGlzLl9lbGVtZW50KS5oYXNDbGFzcyhDbGFzc05hbWUuQUNUSVZFKSB8fFxuICAgICAgICAgICQodGhpcy5fZWxlbWVudCkuaGFzQ2xhc3MoQ2xhc3NOYW1lLkRJU0FCTEVEKSkge1xuICAgICAgICByZXR1cm5cbiAgICAgIH1cblxuICAgICAgbGV0IHRhcmdldFxuICAgICAgbGV0IHByZXZpb3VzXG4gICAgICBjb25zdCBsaXN0RWxlbWVudCA9ICQodGhpcy5fZWxlbWVudCkuY2xvc2VzdChTZWxlY3Rvci5OQVZfTElTVF9HUk9VUClbMF1cbiAgICAgIGNvbnN0IHNlbGVjdG9yICAgID0gVXRpbC5nZXRTZWxlY3RvckZyb21FbGVtZW50KHRoaXMuX2VsZW1lbnQpXG5cbiAgICAgIGlmIChsaXN0RWxlbWVudCkge1xuICAgICAgICBwcmV2aW91cyA9ICQubWFrZUFycmF5KCQobGlzdEVsZW1lbnQpLmZpbmQoU2VsZWN0b3IuQUNUSVZFKSlcbiAgICAgICAgcHJldmlvdXMgPSBwcmV2aW91c1twcmV2aW91cy5sZW5ndGggLSAxXVxuICAgICAgfVxuXG4gICAgICBjb25zdCBoaWRlRXZlbnQgPSAkLkV2ZW50KEV2ZW50LkhJREUsIHtcbiAgICAgICAgcmVsYXRlZFRhcmdldDogdGhpcy5fZWxlbWVudFxuICAgICAgfSlcblxuICAgICAgY29uc3Qgc2hvd0V2ZW50ID0gJC5FdmVudChFdmVudC5TSE9XLCB7XG4gICAgICAgIHJlbGF0ZWRUYXJnZXQ6IHByZXZpb3VzXG4gICAgICB9KVxuXG4gICAgICBpZiAocHJldmlvdXMpIHtcbiAgICAgICAgJChwcmV2aW91cykudHJpZ2dlcihoaWRlRXZlbnQpXG4gICAgICB9XG5cbiAgICAgICQodGhpcy5fZWxlbWVudCkudHJpZ2dlcihzaG93RXZlbnQpXG5cbiAgICAgIGlmIChzaG93RXZlbnQuaXNEZWZhdWx0UHJldmVudGVkKCkgfHxcbiAgICAgICAgIGhpZGVFdmVudC5pc0RlZmF1bHRQcmV2ZW50ZWQoKSkge1xuICAgICAgICByZXR1cm5cbiAgICAgIH1cblxuICAgICAgaWYgKHNlbGVjdG9yKSB7XG4gICAgICAgIHRhcmdldCA9ICQoc2VsZWN0b3IpWzBdXG4gICAgICB9XG5cbiAgICAgIHRoaXMuX2FjdGl2YXRlKFxuICAgICAgICB0aGlzLl9lbGVtZW50LFxuICAgICAgICBsaXN0RWxlbWVudFxuICAgICAgKVxuXG4gICAgICBjb25zdCBjb21wbGV0ZSA9ICgpID0+IHtcbiAgICAgICAgY29uc3QgaGlkZGVuRXZlbnQgPSAkLkV2ZW50KEV2ZW50LkhJRERFTiwge1xuICAgICAgICAgIHJlbGF0ZWRUYXJnZXQ6IHRoaXMuX2VsZW1lbnRcbiAgICAgICAgfSlcblxuICAgICAgICBjb25zdCBzaG93bkV2ZW50ID0gJC5FdmVudChFdmVudC5TSE9XTiwge1xuICAgICAgICAgIHJlbGF0ZWRUYXJnZXQ6IHByZXZpb3VzXG4gICAgICAgIH0pXG5cbiAgICAgICAgJChwcmV2aW91cykudHJpZ2dlcihoaWRkZW5FdmVudClcbiAgICAgICAgJCh0aGlzLl9lbGVtZW50KS50cmlnZ2VyKHNob3duRXZlbnQpXG4gICAgICB9XG5cbiAgICAgIGlmICh0YXJnZXQpIHtcbiAgICAgICAgdGhpcy5fYWN0aXZhdGUodGFyZ2V0LCB0YXJnZXQucGFyZW50Tm9kZSwgY29tcGxldGUpXG4gICAgICB9IGVsc2Uge1xuICAgICAgICBjb21wbGV0ZSgpXG4gICAgICB9XG4gICAgfVxuXG4gICAgZGlzcG9zZSgpIHtcbiAgICAgICQucmVtb3ZlRGF0YSh0aGlzLl9lbGVtZW50LCBEQVRBX0tFWSlcbiAgICAgIHRoaXMuX2VsZW1lbnQgPSBudWxsXG4gICAgfVxuXG5cbiAgICAvLyBwcml2YXRlXG5cbiAgICBfYWN0aXZhdGUoZWxlbWVudCwgY29udGFpbmVyLCBjYWxsYmFjaykge1xuICAgICAgY29uc3QgYWN0aXZlICAgICAgICAgID0gJChjb250YWluZXIpLmZpbmQoU2VsZWN0b3IuQUNUSVZFKVswXVxuICAgICAgY29uc3QgaXNUcmFuc2l0aW9uaW5nID0gY2FsbGJhY2tcbiAgICAgICAgJiYgVXRpbC5zdXBwb3J0c1RyYW5zaXRpb25FbmQoKVxuICAgICAgICAmJiAoYWN0aXZlICYmICQoYWN0aXZlKS5oYXNDbGFzcyhDbGFzc05hbWUuRkFERSkpXG5cbiAgICAgIGNvbnN0IGNvbXBsZXRlID0gKCkgPT4gdGhpcy5fdHJhbnNpdGlvbkNvbXBsZXRlKFxuICAgICAgICBlbGVtZW50LFxuICAgICAgICBhY3RpdmUsXG4gICAgICAgIGlzVHJhbnNpdGlvbmluZyxcbiAgICAgICAgY2FsbGJhY2tcbiAgICAgIClcblxuICAgICAgaWYgKGFjdGl2ZSAmJiBpc1RyYW5zaXRpb25pbmcpIHtcbiAgICAgICAgJChhY3RpdmUpXG4gICAgICAgICAgLm9uZShVdGlsLlRSQU5TSVRJT05fRU5ELCBjb21wbGV0ZSlcbiAgICAgICAgICAuZW11bGF0ZVRyYW5zaXRpb25FbmQoVFJBTlNJVElPTl9EVVJBVElPTilcblxuICAgICAgfSBlbHNlIHtcbiAgICAgICAgY29tcGxldGUoKVxuICAgICAgfVxuXG4gICAgICBpZiAoYWN0aXZlKSB7XG4gICAgICAgICQoYWN0aXZlKS5yZW1vdmVDbGFzcyhDbGFzc05hbWUuU0hPVylcbiAgICAgIH1cbiAgICB9XG5cbiAgICBfdHJhbnNpdGlvbkNvbXBsZXRlKGVsZW1lbnQsIGFjdGl2ZSwgaXNUcmFuc2l0aW9uaW5nLCBjYWxsYmFjaykge1xuICAgICAgaWYgKGFjdGl2ZSkge1xuICAgICAgICAkKGFjdGl2ZSkucmVtb3ZlQ2xhc3MoQ2xhc3NOYW1lLkFDVElWRSlcblxuICAgICAgICBjb25zdCBkcm9wZG93bkNoaWxkID0gJChhY3RpdmUucGFyZW50Tm9kZSkuZmluZChcbiAgICAgICAgICBTZWxlY3Rvci5EUk9QRE9XTl9BQ1RJVkVfQ0hJTERcbiAgICAgICAgKVswXVxuXG4gICAgICAgIGlmIChkcm9wZG93bkNoaWxkKSB7XG4gICAgICAgICAgJChkcm9wZG93bkNoaWxkKS5yZW1vdmVDbGFzcyhDbGFzc05hbWUuQUNUSVZFKVxuICAgICAgICB9XG5cbiAgICAgICAgYWN0aXZlLnNldEF0dHJpYnV0ZSgnYXJpYS1leHBhbmRlZCcsIGZhbHNlKVxuICAgICAgfVxuXG4gICAgICAkKGVsZW1lbnQpLmFkZENsYXNzKENsYXNzTmFtZS5BQ1RJVkUpXG4gICAgICBlbGVtZW50LnNldEF0dHJpYnV0ZSgnYXJpYS1leHBhbmRlZCcsIHRydWUpXG5cbiAgICAgIGlmIChpc1RyYW5zaXRpb25pbmcpIHtcbiAgICAgICAgVXRpbC5yZWZsb3coZWxlbWVudClcbiAgICAgICAgJChlbGVtZW50KS5hZGRDbGFzcyhDbGFzc05hbWUuU0hPVylcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgICQoZWxlbWVudCkucmVtb3ZlQ2xhc3MoQ2xhc3NOYW1lLkZBREUpXG4gICAgICB9XG5cbiAgICAgIGlmIChlbGVtZW50LnBhcmVudE5vZGUgJiZcbiAgICAgICAgICAkKGVsZW1lbnQucGFyZW50Tm9kZSkuaGFzQ2xhc3MoQ2xhc3NOYW1lLkRST1BET1dOX01FTlUpKSB7XG5cbiAgICAgICAgY29uc3QgZHJvcGRvd25FbGVtZW50ID0gJChlbGVtZW50KS5jbG9zZXN0KFNlbGVjdG9yLkRST1BET1dOKVswXVxuICAgICAgICBpZiAoZHJvcGRvd25FbGVtZW50KSB7XG4gICAgICAgICAgJChkcm9wZG93bkVsZW1lbnQpLmZpbmQoU2VsZWN0b3IuRFJPUERPV05fVE9HR0xFKS5hZGRDbGFzcyhDbGFzc05hbWUuQUNUSVZFKVxuICAgICAgICB9XG5cbiAgICAgICAgZWxlbWVudC5zZXRBdHRyaWJ1dGUoJ2FyaWEtZXhwYW5kZWQnLCB0cnVlKVxuICAgICAgfVxuXG4gICAgICBpZiAoY2FsbGJhY2spIHtcbiAgICAgICAgY2FsbGJhY2soKVxuICAgICAgfVxuICAgIH1cblxuXG4gICAgLy8gc3RhdGljXG5cbiAgICBzdGF0aWMgX2pRdWVyeUludGVyZmFjZShjb25maWcpIHtcbiAgICAgIHJldHVybiB0aGlzLmVhY2goZnVuY3Rpb24gKCkge1xuICAgICAgICBjb25zdCAkdGhpcyA9ICQodGhpcylcbiAgICAgICAgbGV0IGRhdGEgICAgPSAkdGhpcy5kYXRhKERBVEFfS0VZKVxuXG4gICAgICAgIGlmICghZGF0YSkge1xuICAgICAgICAgIGRhdGEgPSBuZXcgVGFiKHRoaXMpXG4gICAgICAgICAgJHRoaXMuZGF0YShEQVRBX0tFWSwgZGF0YSlcbiAgICAgICAgfVxuXG4gICAgICAgIGlmICh0eXBlb2YgY29uZmlnID09PSAnc3RyaW5nJykge1xuICAgICAgICAgIGlmIChkYXRhW2NvbmZpZ10gPT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgdGhyb3cgbmV3IEVycm9yKGBObyBtZXRob2QgbmFtZWQgXCIke2NvbmZpZ31cImApXG4gICAgICAgICAgfVxuICAgICAgICAgIGRhdGFbY29uZmlnXSgpXG4gICAgICAgIH1cbiAgICAgIH0pXG4gICAgfVxuXG4gIH1cblxuXG4gIC8qKlxuICAgKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiAgICogRGF0YSBBcGkgaW1wbGVtZW50YXRpb25cbiAgICogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gICAqL1xuXG4gICQoZG9jdW1lbnQpXG4gICAgLm9uKEV2ZW50LkNMSUNLX0RBVEFfQVBJLCBTZWxlY3Rvci5EQVRBX1RPR0dMRSwgZnVuY3Rpb24gKGV2ZW50KSB7XG4gICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpXG4gICAgICBUYWIuX2pRdWVyeUludGVyZmFjZS5jYWxsKCQodGhpcyksICdzaG93JylcbiAgICB9KVxuXG5cbiAgLyoqXG4gICAqIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuICAgKiBqUXVlcnlcbiAgICogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gICAqL1xuXG4gICQuZm5bTkFNRV0gICAgICAgICAgICAgPSBUYWIuX2pRdWVyeUludGVyZmFjZVxuICAkLmZuW05BTUVdLkNvbnN0cnVjdG9yID0gVGFiXG4gICQuZm5bTkFNRV0ubm9Db25mbGljdCAgPSBmdW5jdGlvbiAoKSB7XG4gICAgJC5mbltOQU1FXSA9IEpRVUVSWV9OT19DT05GTElDVFxuICAgIHJldHVybiBUYWIuX2pRdWVyeUludGVyZmFjZVxuICB9XG5cbiAgcmV0dXJuIFRhYlxuXG59KShqUXVlcnkpXG5cbmV4cG9ydCBkZWZhdWx0IFRhYlxuIiwiLyogZ2xvYmFsIFBvcHBlciAqL1xuXG5pbXBvcnQgVXRpbCBmcm9tICcuL3V0aWwnXG5cblxuLyoqXG4gKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuICogQm9vdHN0cmFwICh2NC4wLjAtYmV0YSk6IHRvb2x0aXAuanNcbiAqIExpY2Vuc2VkIHVuZGVyIE1JVCAoaHR0cHM6Ly9naXRodWIuY29tL3R3YnMvYm9vdHN0cmFwL2Jsb2IvbWFzdGVyL0xJQ0VOU0UpXG4gKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuICovXG5cbmNvbnN0IFRvb2x0aXAgPSAoKCQpID0+IHtcblxuICAvKipcbiAgICogQ2hlY2sgZm9yIFBvcHBlciBkZXBlbmRlbmN5XG4gICAqIFBvcHBlciAtIGh0dHBzOi8vcG9wcGVyLmpzLm9yZ1xuICAgKi9cbiAgaWYgKHR5cGVvZiBQb3BwZXIgPT09ICd1bmRlZmluZWQnKSB7XG4gICAgdGhyb3cgbmV3IEVycm9yKCdCb290c3RyYXAgdG9vbHRpcHMgcmVxdWlyZSBQb3BwZXIuanMgKGh0dHBzOi8vcG9wcGVyLmpzLm9yZyknKVxuICB9XG5cblxuICAvKipcbiAgICogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gICAqIENvbnN0YW50c1xuICAgKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiAgICovXG5cbiAgY29uc3QgTkFNRSAgICAgICAgICAgICAgICA9ICd0b29sdGlwJ1xuICBjb25zdCBWRVJTSU9OICAgICAgICAgICAgID0gJzQuMC4wLWJldGEnXG4gIGNvbnN0IERBVEFfS0VZICAgICAgICAgICAgPSAnYnMudG9vbHRpcCdcbiAgY29uc3QgRVZFTlRfS0VZICAgICAgICAgICA9IGAuJHtEQVRBX0tFWX1gXG4gIGNvbnN0IEpRVUVSWV9OT19DT05GTElDVCAgPSAkLmZuW05BTUVdXG4gIGNvbnN0IFRSQU5TSVRJT05fRFVSQVRJT04gPSAxNTBcbiAgY29uc3QgQ0xBU1NfUFJFRklYICAgICAgICA9ICdicy10b29sdGlwJ1xuICBjb25zdCBCU0NMU19QUkVGSVhfUkVHRVggPSBuZXcgUmVnRXhwKGAoXnxcXFxccykke0NMQVNTX1BSRUZJWH1cXFxcUytgLCAnZycpXG5cbiAgY29uc3QgRGVmYXVsdFR5cGUgPSB7XG4gICAgYW5pbWF0aW9uICAgICAgICAgICA6ICdib29sZWFuJyxcbiAgICB0ZW1wbGF0ZSAgICAgICAgICAgIDogJ3N0cmluZycsXG4gICAgdGl0bGUgICAgICAgICAgICAgICA6ICcoc3RyaW5nfGVsZW1lbnR8ZnVuY3Rpb24pJyxcbiAgICB0cmlnZ2VyICAgICAgICAgICAgIDogJ3N0cmluZycsXG4gICAgZGVsYXkgICAgICAgICAgICAgICA6ICcobnVtYmVyfG9iamVjdCknLFxuICAgIGh0bWwgICAgICAgICAgICAgICAgOiAnYm9vbGVhbicsXG4gICAgc2VsZWN0b3IgICAgICAgICAgICA6ICcoc3RyaW5nfGJvb2xlYW4pJyxcbiAgICBwbGFjZW1lbnQgICAgICAgICAgIDogJyhzdHJpbmd8ZnVuY3Rpb24pJyxcbiAgICBvZmZzZXQgICAgICAgICAgICAgIDogJyhudW1iZXJ8c3RyaW5nKScsXG4gICAgY29udGFpbmVyICAgICAgICAgICA6ICcoc3RyaW5nfGVsZW1lbnR8Ym9vbGVhbiknLFxuICAgIGZhbGxiYWNrUGxhY2VtZW50ICAgOiAnKHN0cmluZ3xhcnJheSknXG4gIH1cblxuICBjb25zdCBBdHRhY2htZW50TWFwID0ge1xuICAgIEFVVE8gICA6ICdhdXRvJyxcbiAgICBUT1AgICAgOiAndG9wJyxcbiAgICBSSUdIVCAgOiAncmlnaHQnLFxuICAgIEJPVFRPTSA6ICdib3R0b20nLFxuICAgIExFRlQgICA6ICdsZWZ0J1xuICB9XG5cbiAgY29uc3QgRGVmYXVsdCA9IHtcbiAgICBhbmltYXRpb24gICAgICAgICAgIDogdHJ1ZSxcbiAgICB0ZW1wbGF0ZSAgICAgICAgICAgIDogJzxkaXYgY2xhc3M9XCJ0b29sdGlwXCIgcm9sZT1cInRvb2x0aXBcIj4nXG4gICAgICAgICAgICAgICAgICAgICAgICArICc8ZGl2IGNsYXNzPVwiYXJyb3dcIj48L2Rpdj4nXG4gICAgICAgICAgICAgICAgICAgICAgICArICc8ZGl2IGNsYXNzPVwidG9vbHRpcC1pbm5lclwiPjwvZGl2PjwvZGl2PicsXG4gICAgdHJpZ2dlciAgICAgICAgICAgICA6ICdob3ZlciBmb2N1cycsXG4gICAgdGl0bGUgICAgICAgICAgICAgICA6ICcnLFxuICAgIGRlbGF5ICAgICAgICAgICAgICAgOiAwLFxuICAgIGh0bWwgICAgICAgICAgICAgICAgOiBmYWxzZSxcbiAgICBzZWxlY3RvciAgICAgICAgICAgIDogZmFsc2UsXG4gICAgcGxhY2VtZW50ICAgICAgICAgICA6ICd0b3AnLFxuICAgIG9mZnNldCAgICAgICAgICAgICAgOiAwLFxuICAgIGNvbnRhaW5lciAgICAgICAgICAgOiBmYWxzZSxcbiAgICBmYWxsYmFja1BsYWNlbWVudCAgIDogJ2ZsaXAnXG4gIH1cblxuICBjb25zdCBIb3ZlclN0YXRlID0ge1xuICAgIFNIT1cgOiAnc2hvdycsXG4gICAgT1VUICA6ICdvdXQnXG4gIH1cblxuICBjb25zdCBFdmVudCA9IHtcbiAgICBISURFICAgICAgIDogYGhpZGUke0VWRU5UX0tFWX1gLFxuICAgIEhJRERFTiAgICAgOiBgaGlkZGVuJHtFVkVOVF9LRVl9YCxcbiAgICBTSE9XICAgICAgIDogYHNob3cke0VWRU5UX0tFWX1gLFxuICAgIFNIT1dOICAgICAgOiBgc2hvd24ke0VWRU5UX0tFWX1gLFxuICAgIElOU0VSVEVEICAgOiBgaW5zZXJ0ZWQke0VWRU5UX0tFWX1gLFxuICAgIENMSUNLICAgICAgOiBgY2xpY2ske0VWRU5UX0tFWX1gLFxuICAgIEZPQ1VTSU4gICAgOiBgZm9jdXNpbiR7RVZFTlRfS0VZfWAsXG4gICAgRk9DVVNPVVQgICA6IGBmb2N1c291dCR7RVZFTlRfS0VZfWAsXG4gICAgTU9VU0VFTlRFUiA6IGBtb3VzZWVudGVyJHtFVkVOVF9LRVl9YCxcbiAgICBNT1VTRUxFQVZFIDogYG1vdXNlbGVhdmUke0VWRU5UX0tFWX1gXG4gIH1cblxuICBjb25zdCBDbGFzc05hbWUgPSB7XG4gICAgRkFERSA6ICdmYWRlJyxcbiAgICBTSE9XIDogJ3Nob3cnXG4gIH1cblxuICBjb25zdCBTZWxlY3RvciA9IHtcbiAgICBUT09MVElQICAgICAgIDogJy50b29sdGlwJyxcbiAgICBUT09MVElQX0lOTkVSIDogJy50b29sdGlwLWlubmVyJyxcbiAgICBBUlJPVyAgICAgICAgIDogJy5hcnJvdydcbiAgfVxuXG4gIGNvbnN0IFRyaWdnZXIgPSB7XG4gICAgSE9WRVIgIDogJ2hvdmVyJyxcbiAgICBGT0NVUyAgOiAnZm9jdXMnLFxuICAgIENMSUNLICA6ICdjbGljaycsXG4gICAgTUFOVUFMIDogJ21hbnVhbCdcbiAgfVxuXG5cbiAgLyoqXG4gICAqIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuICAgKiBDbGFzcyBEZWZpbml0aW9uXG4gICAqIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuICAgKi9cblxuICBjbGFzcyBUb29sdGlwIHtcblxuICAgIGNvbnN0cnVjdG9yKGVsZW1lbnQsIGNvbmZpZykge1xuXG4gICAgICAvLyBwcml2YXRlXG4gICAgICB0aGlzLl9pc0VuYWJsZWQgICAgID0gdHJ1ZVxuICAgICAgdGhpcy5fdGltZW91dCAgICAgICA9IDBcbiAgICAgIHRoaXMuX2hvdmVyU3RhdGUgICAgPSAnJ1xuICAgICAgdGhpcy5fYWN0aXZlVHJpZ2dlciA9IHt9XG4gICAgICB0aGlzLl9wb3BwZXIgICAgICAgID0gbnVsbFxuXG4gICAgICAvLyBwcm90ZWN0ZWRcbiAgICAgIHRoaXMuZWxlbWVudCA9IGVsZW1lbnRcbiAgICAgIHRoaXMuY29uZmlnICA9IHRoaXMuX2dldENvbmZpZyhjb25maWcpXG4gICAgICB0aGlzLnRpcCAgICAgPSBudWxsXG5cbiAgICAgIHRoaXMuX3NldExpc3RlbmVycygpXG5cbiAgICB9XG5cblxuICAgIC8vIGdldHRlcnNcblxuICAgIHN0YXRpYyBnZXQgVkVSU0lPTigpIHtcbiAgICAgIHJldHVybiBWRVJTSU9OXG4gICAgfVxuXG4gICAgc3RhdGljIGdldCBEZWZhdWx0KCkge1xuICAgICAgcmV0dXJuIERlZmF1bHRcbiAgICB9XG5cbiAgICBzdGF0aWMgZ2V0IE5BTUUoKSB7XG4gICAgICByZXR1cm4gTkFNRVxuICAgIH1cblxuICAgIHN0YXRpYyBnZXQgREFUQV9LRVkoKSB7XG4gICAgICByZXR1cm4gREFUQV9LRVlcbiAgICB9XG5cbiAgICBzdGF0aWMgZ2V0IEV2ZW50KCkge1xuICAgICAgcmV0dXJuIEV2ZW50XG4gICAgfVxuXG4gICAgc3RhdGljIGdldCBFVkVOVF9LRVkoKSB7XG4gICAgICByZXR1cm4gRVZFTlRfS0VZXG4gICAgfVxuXG4gICAgc3RhdGljIGdldCBEZWZhdWx0VHlwZSgpIHtcbiAgICAgIHJldHVybiBEZWZhdWx0VHlwZVxuICAgIH1cblxuXG4gICAgLy8gcHVibGljXG5cbiAgICBlbmFibGUoKSB7XG4gICAgICB0aGlzLl9pc0VuYWJsZWQgPSB0cnVlXG4gICAgfVxuXG4gICAgZGlzYWJsZSgpIHtcbiAgICAgIHRoaXMuX2lzRW5hYmxlZCA9IGZhbHNlXG4gICAgfVxuXG4gICAgdG9nZ2xlRW5hYmxlZCgpIHtcbiAgICAgIHRoaXMuX2lzRW5hYmxlZCA9ICF0aGlzLl9pc0VuYWJsZWRcbiAgICB9XG5cbiAgICB0b2dnbGUoZXZlbnQpIHtcbiAgICAgIGlmIChldmVudCkge1xuICAgICAgICBjb25zdCBkYXRhS2V5ID0gdGhpcy5jb25zdHJ1Y3Rvci5EQVRBX0tFWVxuICAgICAgICBsZXQgY29udGV4dCA9ICQoZXZlbnQuY3VycmVudFRhcmdldCkuZGF0YShkYXRhS2V5KVxuXG4gICAgICAgIGlmICghY29udGV4dCkge1xuICAgICAgICAgIGNvbnRleHQgPSBuZXcgdGhpcy5jb25zdHJ1Y3RvcihcbiAgICAgICAgICAgIGV2ZW50LmN1cnJlbnRUYXJnZXQsXG4gICAgICAgICAgICB0aGlzLl9nZXREZWxlZ2F0ZUNvbmZpZygpXG4gICAgICAgICAgKVxuICAgICAgICAgICQoZXZlbnQuY3VycmVudFRhcmdldCkuZGF0YShkYXRhS2V5LCBjb250ZXh0KVxuICAgICAgICB9XG5cbiAgICAgICAgY29udGV4dC5fYWN0aXZlVHJpZ2dlci5jbGljayA9ICFjb250ZXh0Ll9hY3RpdmVUcmlnZ2VyLmNsaWNrXG5cbiAgICAgICAgaWYgKGNvbnRleHQuX2lzV2l0aEFjdGl2ZVRyaWdnZXIoKSkge1xuICAgICAgICAgIGNvbnRleHQuX2VudGVyKG51bGwsIGNvbnRleHQpXG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgY29udGV4dC5fbGVhdmUobnVsbCwgY29udGV4dClcbiAgICAgICAgfVxuXG4gICAgICB9IGVsc2Uge1xuXG4gICAgICAgIGlmICgkKHRoaXMuZ2V0VGlwRWxlbWVudCgpKS5oYXNDbGFzcyhDbGFzc05hbWUuU0hPVykpIHtcbiAgICAgICAgICB0aGlzLl9sZWF2ZShudWxsLCB0aGlzKVxuICAgICAgICAgIHJldHVyblxuICAgICAgICB9XG5cbiAgICAgICAgdGhpcy5fZW50ZXIobnVsbCwgdGhpcylcbiAgICAgIH1cbiAgICB9XG5cbiAgICBkaXNwb3NlKCkge1xuICAgICAgY2xlYXJUaW1lb3V0KHRoaXMuX3RpbWVvdXQpXG5cbiAgICAgICQucmVtb3ZlRGF0YSh0aGlzLmVsZW1lbnQsIHRoaXMuY29uc3RydWN0b3IuREFUQV9LRVkpXG5cbiAgICAgICQodGhpcy5lbGVtZW50KS5vZmYodGhpcy5jb25zdHJ1Y3Rvci5FVkVOVF9LRVkpXG4gICAgICAkKHRoaXMuZWxlbWVudCkuY2xvc2VzdCgnLm1vZGFsJykub2ZmKCdoaWRlLmJzLm1vZGFsJylcblxuICAgICAgaWYgKHRoaXMudGlwKSB7XG4gICAgICAgICQodGhpcy50aXApLnJlbW92ZSgpXG4gICAgICB9XG5cbiAgICAgIHRoaXMuX2lzRW5hYmxlZCAgICAgPSBudWxsXG4gICAgICB0aGlzLl90aW1lb3V0ICAgICAgID0gbnVsbFxuICAgICAgdGhpcy5faG92ZXJTdGF0ZSAgICA9IG51bGxcbiAgICAgIHRoaXMuX2FjdGl2ZVRyaWdnZXIgPSBudWxsXG4gICAgICBpZiAodGhpcy5fcG9wcGVyICE9PSBudWxsKSB7XG4gICAgICAgIHRoaXMuX3BvcHBlci5kZXN0cm95KClcbiAgICAgIH1cbiAgICAgIHRoaXMuX3BvcHBlciAgICAgICAgPSBudWxsXG5cbiAgICAgIHRoaXMuZWxlbWVudCA9IG51bGxcbiAgICAgIHRoaXMuY29uZmlnICA9IG51bGxcbiAgICAgIHRoaXMudGlwICAgICA9IG51bGxcbiAgICB9XG5cbiAgICBzaG93KCkge1xuICAgICAgaWYgKCQodGhpcy5lbGVtZW50KS5jc3MoJ2Rpc3BsYXknKSA9PT0gJ25vbmUnKSB7XG4gICAgICAgIHRocm93IG5ldyBFcnJvcignUGxlYXNlIHVzZSBzaG93IG9uIHZpc2libGUgZWxlbWVudHMnKVxuICAgICAgfVxuXG4gICAgICBjb25zdCBzaG93RXZlbnQgPSAkLkV2ZW50KHRoaXMuY29uc3RydWN0b3IuRXZlbnQuU0hPVylcbiAgICAgIGlmICh0aGlzLmlzV2l0aENvbnRlbnQoKSAmJiB0aGlzLl9pc0VuYWJsZWQpIHtcbiAgICAgICAgJCh0aGlzLmVsZW1lbnQpLnRyaWdnZXIoc2hvd0V2ZW50KVxuXG4gICAgICAgIGNvbnN0IGlzSW5UaGVEb20gPSAkLmNvbnRhaW5zKFxuICAgICAgICAgIHRoaXMuZWxlbWVudC5vd25lckRvY3VtZW50LmRvY3VtZW50RWxlbWVudCxcbiAgICAgICAgICB0aGlzLmVsZW1lbnRcbiAgICAgICAgKVxuXG4gICAgICAgIGlmIChzaG93RXZlbnQuaXNEZWZhdWx0UHJldmVudGVkKCkgfHwgIWlzSW5UaGVEb20pIHtcbiAgICAgICAgICByZXR1cm5cbiAgICAgICAgfVxuXG4gICAgICAgIGNvbnN0IHRpcCAgID0gdGhpcy5nZXRUaXBFbGVtZW50KClcbiAgICAgICAgY29uc3QgdGlwSWQgPSBVdGlsLmdldFVJRCh0aGlzLmNvbnN0cnVjdG9yLk5BTUUpXG5cbiAgICAgICAgdGlwLnNldEF0dHJpYnV0ZSgnaWQnLCB0aXBJZClcbiAgICAgICAgdGhpcy5lbGVtZW50LnNldEF0dHJpYnV0ZSgnYXJpYS1kZXNjcmliZWRieScsIHRpcElkKVxuXG4gICAgICAgIHRoaXMuc2V0Q29udGVudCgpXG5cbiAgICAgICAgaWYgKHRoaXMuY29uZmlnLmFuaW1hdGlvbikge1xuICAgICAgICAgICQodGlwKS5hZGRDbGFzcyhDbGFzc05hbWUuRkFERSlcbiAgICAgICAgfVxuXG4gICAgICAgIGNvbnN0IHBsYWNlbWVudCAgPSB0eXBlb2YgdGhpcy5jb25maWcucGxhY2VtZW50ID09PSAnZnVuY3Rpb24nID9cbiAgICAgICAgICB0aGlzLmNvbmZpZy5wbGFjZW1lbnQuY2FsbCh0aGlzLCB0aXAsIHRoaXMuZWxlbWVudCkgOlxuICAgICAgICAgIHRoaXMuY29uZmlnLnBsYWNlbWVudFxuXG4gICAgICAgIGNvbnN0IGF0dGFjaG1lbnQgPSB0aGlzLl9nZXRBdHRhY2htZW50KHBsYWNlbWVudClcbiAgICAgICAgdGhpcy5hZGRBdHRhY2htZW50Q2xhc3MoYXR0YWNobWVudClcblxuICAgICAgICBjb25zdCBjb250YWluZXIgPSB0aGlzLmNvbmZpZy5jb250YWluZXIgPT09IGZhbHNlID8gZG9jdW1lbnQuYm9keSA6ICQodGhpcy5jb25maWcuY29udGFpbmVyKVxuXG4gICAgICAgICQodGlwKS5kYXRhKHRoaXMuY29uc3RydWN0b3IuREFUQV9LRVksIHRoaXMpXG5cbiAgICAgICAgaWYgKCEkLmNvbnRhaW5zKHRoaXMuZWxlbWVudC5vd25lckRvY3VtZW50LmRvY3VtZW50RWxlbWVudCwgdGhpcy50aXApKSB7XG4gICAgICAgICAgJCh0aXApLmFwcGVuZFRvKGNvbnRhaW5lcilcbiAgICAgICAgfVxuXG4gICAgICAgICQodGhpcy5lbGVtZW50KS50cmlnZ2VyKHRoaXMuY29uc3RydWN0b3IuRXZlbnQuSU5TRVJURUQpXG5cbiAgICAgICAgdGhpcy5fcG9wcGVyID0gbmV3IFBvcHBlcih0aGlzLmVsZW1lbnQsIHRpcCwge1xuICAgICAgICAgIHBsYWNlbWVudDogYXR0YWNobWVudCxcbiAgICAgICAgICBtb2RpZmllcnM6IHtcbiAgICAgICAgICAgIG9mZnNldDoge1xuICAgICAgICAgICAgICBvZmZzZXQ6IHRoaXMuY29uZmlnLm9mZnNldFxuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIGZsaXA6IHtcbiAgICAgICAgICAgICAgYmVoYXZpb3I6IHRoaXMuY29uZmlnLmZhbGxiYWNrUGxhY2VtZW50XG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgYXJyb3c6IHtcbiAgICAgICAgICAgICAgZWxlbWVudDogU2VsZWN0b3IuQVJST1dcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9LFxuICAgICAgICAgIG9uQ3JlYXRlOiAoZGF0YSkgPT4ge1xuICAgICAgICAgICAgaWYgKGRhdGEub3JpZ2luYWxQbGFjZW1lbnQgIT09IGRhdGEucGxhY2VtZW50KSB7XG4gICAgICAgICAgICAgIHRoaXMuX2hhbmRsZVBvcHBlclBsYWNlbWVudENoYW5nZShkYXRhKVxuICAgICAgICAgICAgfVxuICAgICAgICAgIH0sXG4gICAgICAgICAgb25VcGRhdGUgOiAoZGF0YSkgPT4ge1xuICAgICAgICAgICAgdGhpcy5faGFuZGxlUG9wcGVyUGxhY2VtZW50Q2hhbmdlKGRhdGEpXG4gICAgICAgICAgfVxuICAgICAgICB9KVxuXG4gICAgICAgICQodGlwKS5hZGRDbGFzcyhDbGFzc05hbWUuU0hPVylcblxuICAgICAgICAvLyBpZiB0aGlzIGlzIGEgdG91Y2gtZW5hYmxlZCBkZXZpY2Ugd2UgYWRkIGV4dHJhXG4gICAgICAgIC8vIGVtcHR5IG1vdXNlb3ZlciBsaXN0ZW5lcnMgdG8gdGhlIGJvZHkncyBpbW1lZGlhdGUgY2hpbGRyZW47XG4gICAgICAgIC8vIG9ubHkgbmVlZGVkIGJlY2F1c2Ugb2YgYnJva2VuIGV2ZW50IGRlbGVnYXRpb24gb24gaU9TXG4gICAgICAgIC8vIGh0dHBzOi8vd3d3LnF1aXJrc21vZGUub3JnL2Jsb2cvYXJjaGl2ZXMvMjAxNC8wMi9tb3VzZV9ldmVudF9idWIuaHRtbFxuICAgICAgICBpZiAoJ29udG91Y2hzdGFydCcgaW4gZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50KSB7XG4gICAgICAgICAgJCgnYm9keScpLmNoaWxkcmVuKCkub24oJ21vdXNlb3ZlcicsIG51bGwsICQubm9vcClcbiAgICAgICAgfVxuXG4gICAgICAgIGNvbnN0IGNvbXBsZXRlID0gKCkgPT4ge1xuICAgICAgICAgIGlmICh0aGlzLmNvbmZpZy5hbmltYXRpb24pIHtcbiAgICAgICAgICAgIHRoaXMuX2ZpeFRyYW5zaXRpb24oKVxuICAgICAgICAgIH1cbiAgICAgICAgICBjb25zdCBwcmV2SG92ZXJTdGF0ZSA9IHRoaXMuX2hvdmVyU3RhdGVcbiAgICAgICAgICB0aGlzLl9ob3ZlclN0YXRlICAgICA9IG51bGxcblxuICAgICAgICAgICQodGhpcy5lbGVtZW50KS50cmlnZ2VyKHRoaXMuY29uc3RydWN0b3IuRXZlbnQuU0hPV04pXG5cbiAgICAgICAgICBpZiAocHJldkhvdmVyU3RhdGUgPT09IEhvdmVyU3RhdGUuT1VUKSB7XG4gICAgICAgICAgICB0aGlzLl9sZWF2ZShudWxsLCB0aGlzKVxuICAgICAgICAgIH1cbiAgICAgICAgfVxuXG4gICAgICAgIGlmIChVdGlsLnN1cHBvcnRzVHJhbnNpdGlvbkVuZCgpICYmICQodGhpcy50aXApLmhhc0NsYXNzKENsYXNzTmFtZS5GQURFKSkge1xuICAgICAgICAgICQodGhpcy50aXApXG4gICAgICAgICAgICAub25lKFV0aWwuVFJBTlNJVElPTl9FTkQsIGNvbXBsZXRlKVxuICAgICAgICAgICAgLmVtdWxhdGVUcmFuc2l0aW9uRW5kKFRvb2x0aXAuX1RSQU5TSVRJT05fRFVSQVRJT04pXG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgY29tcGxldGUoKVxuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuXG4gICAgaGlkZShjYWxsYmFjaykge1xuICAgICAgY29uc3QgdGlwICAgICAgID0gdGhpcy5nZXRUaXBFbGVtZW50KClcbiAgICAgIGNvbnN0IGhpZGVFdmVudCA9ICQuRXZlbnQodGhpcy5jb25zdHJ1Y3Rvci5FdmVudC5ISURFKVxuICAgICAgY29uc3QgY29tcGxldGUgID0gKCkgPT4ge1xuICAgICAgICBpZiAodGhpcy5faG92ZXJTdGF0ZSAhPT0gSG92ZXJTdGF0ZS5TSE9XICYmIHRpcC5wYXJlbnROb2RlKSB7XG4gICAgICAgICAgdGlwLnBhcmVudE5vZGUucmVtb3ZlQ2hpbGQodGlwKVxuICAgICAgICB9XG5cbiAgICAgICAgdGhpcy5fY2xlYW5UaXBDbGFzcygpXG4gICAgICAgIHRoaXMuZWxlbWVudC5yZW1vdmVBdHRyaWJ1dGUoJ2FyaWEtZGVzY3JpYmVkYnknKVxuICAgICAgICAkKHRoaXMuZWxlbWVudCkudHJpZ2dlcih0aGlzLmNvbnN0cnVjdG9yLkV2ZW50LkhJRERFTilcbiAgICAgICAgaWYgKHRoaXMuX3BvcHBlciAhPT0gbnVsbCkge1xuICAgICAgICAgIHRoaXMuX3BvcHBlci5kZXN0cm95KClcbiAgICAgICAgfVxuXG4gICAgICAgIGlmIChjYWxsYmFjaykge1xuICAgICAgICAgIGNhbGxiYWNrKClcbiAgICAgICAgfVxuICAgICAgfVxuXG4gICAgICAkKHRoaXMuZWxlbWVudCkudHJpZ2dlcihoaWRlRXZlbnQpXG5cbiAgICAgIGlmIChoaWRlRXZlbnQuaXNEZWZhdWx0UHJldmVudGVkKCkpIHtcbiAgICAgICAgcmV0dXJuXG4gICAgICB9XG5cbiAgICAgICQodGlwKS5yZW1vdmVDbGFzcyhDbGFzc05hbWUuU0hPVylcblxuICAgICAgLy8gaWYgdGhpcyBpcyBhIHRvdWNoLWVuYWJsZWQgZGV2aWNlIHdlIHJlbW92ZSB0aGUgZXh0cmFcbiAgICAgIC8vIGVtcHR5IG1vdXNlb3ZlciBsaXN0ZW5lcnMgd2UgYWRkZWQgZm9yIGlPUyBzdXBwb3J0XG4gICAgICBpZiAoJ29udG91Y2hzdGFydCcgaW4gZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50KSB7XG4gICAgICAgICQoJ2JvZHknKS5jaGlsZHJlbigpLm9mZignbW91c2VvdmVyJywgbnVsbCwgJC5ub29wKVxuICAgICAgfVxuXG4gICAgICB0aGlzLl9hY3RpdmVUcmlnZ2VyW1RyaWdnZXIuQ0xJQ0tdID0gZmFsc2VcbiAgICAgIHRoaXMuX2FjdGl2ZVRyaWdnZXJbVHJpZ2dlci5GT0NVU10gPSBmYWxzZVxuICAgICAgdGhpcy5fYWN0aXZlVHJpZ2dlcltUcmlnZ2VyLkhPVkVSXSA9IGZhbHNlXG5cbiAgICAgIGlmIChVdGlsLnN1cHBvcnRzVHJhbnNpdGlvbkVuZCgpICYmXG4gICAgICAgICAgJCh0aGlzLnRpcCkuaGFzQ2xhc3MoQ2xhc3NOYW1lLkZBREUpKSB7XG5cbiAgICAgICAgJCh0aXApXG4gICAgICAgICAgLm9uZShVdGlsLlRSQU5TSVRJT05fRU5ELCBjb21wbGV0ZSlcbiAgICAgICAgICAuZW11bGF0ZVRyYW5zaXRpb25FbmQoVFJBTlNJVElPTl9EVVJBVElPTilcblxuICAgICAgfSBlbHNlIHtcbiAgICAgICAgY29tcGxldGUoKVxuICAgICAgfVxuXG4gICAgICB0aGlzLl9ob3ZlclN0YXRlID0gJydcblxuICAgIH1cblxuICAgIHVwZGF0ZSgpIHtcbiAgICAgIGlmICh0aGlzLl9wb3BwZXIgIT09IG51bGwpIHtcbiAgICAgICAgdGhpcy5fcG9wcGVyLnNjaGVkdWxlVXBkYXRlKClcbiAgICAgIH1cbiAgICB9XG5cbiAgICAvLyBwcm90ZWN0ZWRcblxuICAgIGlzV2l0aENvbnRlbnQoKSB7XG4gICAgICByZXR1cm4gQm9vbGVhbih0aGlzLmdldFRpdGxlKCkpXG4gICAgfVxuXG4gICAgYWRkQXR0YWNobWVudENsYXNzKGF0dGFjaG1lbnQpIHtcbiAgICAgICQodGhpcy5nZXRUaXBFbGVtZW50KCkpLmFkZENsYXNzKGAke0NMQVNTX1BSRUZJWH0tJHthdHRhY2htZW50fWApXG4gICAgfVxuXG4gICAgZ2V0VGlwRWxlbWVudCgpIHtcbiAgICAgIHJldHVybiB0aGlzLnRpcCA9IHRoaXMudGlwIHx8ICQodGhpcy5jb25maWcudGVtcGxhdGUpWzBdXG4gICAgfVxuXG4gICAgc2V0Q29udGVudCgpIHtcbiAgICAgIGNvbnN0ICR0aXAgPSAkKHRoaXMuZ2V0VGlwRWxlbWVudCgpKVxuICAgICAgdGhpcy5zZXRFbGVtZW50Q29udGVudCgkdGlwLmZpbmQoU2VsZWN0b3IuVE9PTFRJUF9JTk5FUiksIHRoaXMuZ2V0VGl0bGUoKSlcbiAgICAgICR0aXAucmVtb3ZlQ2xhc3MoYCR7Q2xhc3NOYW1lLkZBREV9ICR7Q2xhc3NOYW1lLlNIT1d9YClcbiAgICB9XG5cbiAgICBzZXRFbGVtZW50Q29udGVudCgkZWxlbWVudCwgY29udGVudCkge1xuICAgICAgY29uc3QgaHRtbCA9IHRoaXMuY29uZmlnLmh0bWxcbiAgICAgIGlmICh0eXBlb2YgY29udGVudCA9PT0gJ29iamVjdCcgJiYgKGNvbnRlbnQubm9kZVR5cGUgfHwgY29udGVudC5qcXVlcnkpKSB7XG4gICAgICAgIC8vIGNvbnRlbnQgaXMgYSBET00gbm9kZSBvciBhIGpRdWVyeVxuICAgICAgICBpZiAoaHRtbCkge1xuICAgICAgICAgIGlmICghJChjb250ZW50KS5wYXJlbnQoKS5pcygkZWxlbWVudCkpIHtcbiAgICAgICAgICAgICRlbGVtZW50LmVtcHR5KCkuYXBwZW5kKGNvbnRlbnQpXG4gICAgICAgICAgfVxuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICRlbGVtZW50LnRleHQoJChjb250ZW50KS50ZXh0KCkpXG4gICAgICAgIH1cbiAgICAgIH0gZWxzZSB7XG4gICAgICAgICRlbGVtZW50W2h0bWwgPyAnaHRtbCcgOiAndGV4dCddKGNvbnRlbnQpXG4gICAgICB9XG4gICAgfVxuXG4gICAgZ2V0VGl0bGUoKSB7XG4gICAgICBsZXQgdGl0bGUgPSB0aGlzLmVsZW1lbnQuZ2V0QXR0cmlidXRlKCdkYXRhLW9yaWdpbmFsLXRpdGxlJylcblxuICAgICAgaWYgKCF0aXRsZSkge1xuICAgICAgICB0aXRsZSA9IHR5cGVvZiB0aGlzLmNvbmZpZy50aXRsZSA9PT0gJ2Z1bmN0aW9uJyA/XG4gICAgICAgICAgdGhpcy5jb25maWcudGl0bGUuY2FsbCh0aGlzLmVsZW1lbnQpIDpcbiAgICAgICAgICB0aGlzLmNvbmZpZy50aXRsZVxuICAgICAgfVxuXG4gICAgICByZXR1cm4gdGl0bGVcbiAgICB9XG5cblxuICAgIC8vIHByaXZhdGVcblxuICAgIF9nZXRBdHRhY2htZW50KHBsYWNlbWVudCkge1xuICAgICAgcmV0dXJuIEF0dGFjaG1lbnRNYXBbcGxhY2VtZW50LnRvVXBwZXJDYXNlKCldXG4gICAgfVxuXG4gICAgX3NldExpc3RlbmVycygpIHtcbiAgICAgIGNvbnN0IHRyaWdnZXJzID0gdGhpcy5jb25maWcudHJpZ2dlci5zcGxpdCgnICcpXG5cbiAgICAgIHRyaWdnZXJzLmZvckVhY2goKHRyaWdnZXIpID0+IHtcbiAgICAgICAgaWYgKHRyaWdnZXIgPT09ICdjbGljaycpIHtcbiAgICAgICAgICAkKHRoaXMuZWxlbWVudCkub24oXG4gICAgICAgICAgICB0aGlzLmNvbnN0cnVjdG9yLkV2ZW50LkNMSUNLLFxuICAgICAgICAgICAgdGhpcy5jb25maWcuc2VsZWN0b3IsXG4gICAgICAgICAgICAoZXZlbnQpID0+IHRoaXMudG9nZ2xlKGV2ZW50KVxuICAgICAgICAgIClcblxuICAgICAgICB9IGVsc2UgaWYgKHRyaWdnZXIgIT09IFRyaWdnZXIuTUFOVUFMKSB7XG4gICAgICAgICAgY29uc3QgZXZlbnRJbiAgPSB0cmlnZ2VyID09PSBUcmlnZ2VyLkhPVkVSID9cbiAgICAgICAgICAgIHRoaXMuY29uc3RydWN0b3IuRXZlbnQuTU9VU0VFTlRFUiA6XG4gICAgICAgICAgICB0aGlzLmNvbnN0cnVjdG9yLkV2ZW50LkZPQ1VTSU5cbiAgICAgICAgICBjb25zdCBldmVudE91dCA9IHRyaWdnZXIgPT09IFRyaWdnZXIuSE9WRVIgP1xuICAgICAgICAgICAgdGhpcy5jb25zdHJ1Y3Rvci5FdmVudC5NT1VTRUxFQVZFIDpcbiAgICAgICAgICAgIHRoaXMuY29uc3RydWN0b3IuRXZlbnQuRk9DVVNPVVRcblxuICAgICAgICAgICQodGhpcy5lbGVtZW50KVxuICAgICAgICAgICAgLm9uKFxuICAgICAgICAgICAgICBldmVudEluLFxuICAgICAgICAgICAgICB0aGlzLmNvbmZpZy5zZWxlY3RvcixcbiAgICAgICAgICAgICAgKGV2ZW50KSA9PiB0aGlzLl9lbnRlcihldmVudClcbiAgICAgICAgICAgIClcbiAgICAgICAgICAgIC5vbihcbiAgICAgICAgICAgICAgZXZlbnRPdXQsXG4gICAgICAgICAgICAgIHRoaXMuY29uZmlnLnNlbGVjdG9yLFxuICAgICAgICAgICAgICAoZXZlbnQpID0+IHRoaXMuX2xlYXZlKGV2ZW50KVxuICAgICAgICAgICAgKVxuICAgICAgICB9XG5cbiAgICAgICAgJCh0aGlzLmVsZW1lbnQpLmNsb3Nlc3QoJy5tb2RhbCcpLm9uKFxuICAgICAgICAgICdoaWRlLmJzLm1vZGFsJyxcbiAgICAgICAgICAoKSA9PiB0aGlzLmhpZGUoKVxuICAgICAgICApXG4gICAgICB9KVxuXG4gICAgICBpZiAodGhpcy5jb25maWcuc2VsZWN0b3IpIHtcbiAgICAgICAgdGhpcy5jb25maWcgPSAkLmV4dGVuZCh7fSwgdGhpcy5jb25maWcsIHtcbiAgICAgICAgICB0cmlnZ2VyICA6ICdtYW51YWwnLFxuICAgICAgICAgIHNlbGVjdG9yIDogJydcbiAgICAgICAgfSlcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHRoaXMuX2ZpeFRpdGxlKClcbiAgICAgIH1cbiAgICB9XG5cbiAgICBfZml4VGl0bGUoKSB7XG4gICAgICBjb25zdCB0aXRsZVR5cGUgPSB0eXBlb2YgdGhpcy5lbGVtZW50LmdldEF0dHJpYnV0ZSgnZGF0YS1vcmlnaW5hbC10aXRsZScpXG4gICAgICBpZiAodGhpcy5lbGVtZW50LmdldEF0dHJpYnV0ZSgndGl0bGUnKSB8fFxuICAgICAgICAgdGl0bGVUeXBlICE9PSAnc3RyaW5nJykge1xuICAgICAgICB0aGlzLmVsZW1lbnQuc2V0QXR0cmlidXRlKFxuICAgICAgICAgICdkYXRhLW9yaWdpbmFsLXRpdGxlJyxcbiAgICAgICAgICB0aGlzLmVsZW1lbnQuZ2V0QXR0cmlidXRlKCd0aXRsZScpIHx8ICcnXG4gICAgICAgIClcbiAgICAgICAgdGhpcy5lbGVtZW50LnNldEF0dHJpYnV0ZSgndGl0bGUnLCAnJylcbiAgICAgIH1cbiAgICB9XG5cbiAgICBfZW50ZXIoZXZlbnQsIGNvbnRleHQpIHtcbiAgICAgIGNvbnN0IGRhdGFLZXkgPSB0aGlzLmNvbnN0cnVjdG9yLkRBVEFfS0VZXG5cbiAgICAgIGNvbnRleHQgPSBjb250ZXh0IHx8ICQoZXZlbnQuY3VycmVudFRhcmdldCkuZGF0YShkYXRhS2V5KVxuXG4gICAgICBpZiAoIWNvbnRleHQpIHtcbiAgICAgICAgY29udGV4dCA9IG5ldyB0aGlzLmNvbnN0cnVjdG9yKFxuICAgICAgICAgIGV2ZW50LmN1cnJlbnRUYXJnZXQsXG4gICAgICAgICAgdGhpcy5fZ2V0RGVsZWdhdGVDb25maWcoKVxuICAgICAgICApXG4gICAgICAgICQoZXZlbnQuY3VycmVudFRhcmdldCkuZGF0YShkYXRhS2V5LCBjb250ZXh0KVxuICAgICAgfVxuXG4gICAgICBpZiAoZXZlbnQpIHtcbiAgICAgICAgY29udGV4dC5fYWN0aXZlVHJpZ2dlcltcbiAgICAgICAgICBldmVudC50eXBlID09PSAnZm9jdXNpbicgPyBUcmlnZ2VyLkZPQ1VTIDogVHJpZ2dlci5IT1ZFUlxuICAgICAgICBdID0gdHJ1ZVxuICAgICAgfVxuXG4gICAgICBpZiAoJChjb250ZXh0LmdldFRpcEVsZW1lbnQoKSkuaGFzQ2xhc3MoQ2xhc3NOYW1lLlNIT1cpIHx8XG4gICAgICAgICBjb250ZXh0Ll9ob3ZlclN0YXRlID09PSBIb3ZlclN0YXRlLlNIT1cpIHtcbiAgICAgICAgY29udGV4dC5faG92ZXJTdGF0ZSA9IEhvdmVyU3RhdGUuU0hPV1xuICAgICAgICByZXR1cm5cbiAgICAgIH1cblxuICAgICAgY2xlYXJUaW1lb3V0KGNvbnRleHQuX3RpbWVvdXQpXG5cbiAgICAgIGNvbnRleHQuX2hvdmVyU3RhdGUgPSBIb3ZlclN0YXRlLlNIT1dcblxuICAgICAgaWYgKCFjb250ZXh0LmNvbmZpZy5kZWxheSB8fCAhY29udGV4dC5jb25maWcuZGVsYXkuc2hvdykge1xuICAgICAgICBjb250ZXh0LnNob3coKVxuICAgICAgICByZXR1cm5cbiAgICAgIH1cblxuICAgICAgY29udGV4dC5fdGltZW91dCA9IHNldFRpbWVvdXQoKCkgPT4ge1xuICAgICAgICBpZiAoY29udGV4dC5faG92ZXJTdGF0ZSA9PT0gSG92ZXJTdGF0ZS5TSE9XKSB7XG4gICAgICAgICAgY29udGV4dC5zaG93KClcbiAgICAgICAgfVxuICAgICAgfSwgY29udGV4dC5jb25maWcuZGVsYXkuc2hvdylcbiAgICB9XG5cbiAgICBfbGVhdmUoZXZlbnQsIGNvbnRleHQpIHtcbiAgICAgIGNvbnN0IGRhdGFLZXkgPSB0aGlzLmNvbnN0cnVjdG9yLkRBVEFfS0VZXG5cbiAgICAgIGNvbnRleHQgPSBjb250ZXh0IHx8ICQoZXZlbnQuY3VycmVudFRhcmdldCkuZGF0YShkYXRhS2V5KVxuXG4gICAgICBpZiAoIWNvbnRleHQpIHtcbiAgICAgICAgY29udGV4dCA9IG5ldyB0aGlzLmNvbnN0cnVjdG9yKFxuICAgICAgICAgIGV2ZW50LmN1cnJlbnRUYXJnZXQsXG4gICAgICAgICAgdGhpcy5fZ2V0RGVsZWdhdGVDb25maWcoKVxuICAgICAgICApXG4gICAgICAgICQoZXZlbnQuY3VycmVudFRhcmdldCkuZGF0YShkYXRhS2V5LCBjb250ZXh0KVxuICAgICAgfVxuXG4gICAgICBpZiAoZXZlbnQpIHtcbiAgICAgICAgY29udGV4dC5fYWN0aXZlVHJpZ2dlcltcbiAgICAgICAgICBldmVudC50eXBlID09PSAnZm9jdXNvdXQnID8gVHJpZ2dlci5GT0NVUyA6IFRyaWdnZXIuSE9WRVJcbiAgICAgICAgXSA9IGZhbHNlXG4gICAgICB9XG5cbiAgICAgIGlmIChjb250ZXh0Ll9pc1dpdGhBY3RpdmVUcmlnZ2VyKCkpIHtcbiAgICAgICAgcmV0dXJuXG4gICAgICB9XG5cbiAgICAgIGNsZWFyVGltZW91dChjb250ZXh0Ll90aW1lb3V0KVxuXG4gICAgICBjb250ZXh0Ll9ob3ZlclN0YXRlID0gSG92ZXJTdGF0ZS5PVVRcblxuICAgICAgaWYgKCFjb250ZXh0LmNvbmZpZy5kZWxheSB8fCAhY29udGV4dC5jb25maWcuZGVsYXkuaGlkZSkge1xuICAgICAgICBjb250ZXh0LmhpZGUoKVxuICAgICAgICByZXR1cm5cbiAgICAgIH1cblxuICAgICAgY29udGV4dC5fdGltZW91dCA9IHNldFRpbWVvdXQoKCkgPT4ge1xuICAgICAgICBpZiAoY29udGV4dC5faG92ZXJTdGF0ZSA9PT0gSG92ZXJTdGF0ZS5PVVQpIHtcbiAgICAgICAgICBjb250ZXh0LmhpZGUoKVxuICAgICAgICB9XG4gICAgICB9LCBjb250ZXh0LmNvbmZpZy5kZWxheS5oaWRlKVxuICAgIH1cblxuICAgIF9pc1dpdGhBY3RpdmVUcmlnZ2VyKCkge1xuICAgICAgZm9yIChjb25zdCB0cmlnZ2VyIGluIHRoaXMuX2FjdGl2ZVRyaWdnZXIpIHtcbiAgICAgICAgaWYgKHRoaXMuX2FjdGl2ZVRyaWdnZXJbdHJpZ2dlcl0pIHtcbiAgICAgICAgICByZXR1cm4gdHJ1ZVxuICAgICAgICB9XG4gICAgICB9XG5cbiAgICAgIHJldHVybiBmYWxzZVxuICAgIH1cblxuICAgIF9nZXRDb25maWcoY29uZmlnKSB7XG4gICAgICBjb25maWcgPSAkLmV4dGVuZChcbiAgICAgICAge30sXG4gICAgICAgIHRoaXMuY29uc3RydWN0b3IuRGVmYXVsdCxcbiAgICAgICAgJCh0aGlzLmVsZW1lbnQpLmRhdGEoKSxcbiAgICAgICAgY29uZmlnXG4gICAgICApXG5cbiAgICAgIGlmIChjb25maWcuZGVsYXkgJiYgdHlwZW9mIGNvbmZpZy5kZWxheSA9PT0gJ251bWJlcicpIHtcbiAgICAgICAgY29uZmlnLmRlbGF5ID0ge1xuICAgICAgICAgIHNob3cgOiBjb25maWcuZGVsYXksXG4gICAgICAgICAgaGlkZSA6IGNvbmZpZy5kZWxheVxuICAgICAgICB9XG4gICAgICB9XG5cbiAgICAgIGlmIChjb25maWcudGl0bGUgJiYgdHlwZW9mIGNvbmZpZy50aXRsZSA9PT0gJ251bWJlcicpIHtcbiAgICAgICAgY29uZmlnLnRpdGxlID0gY29uZmlnLnRpdGxlLnRvU3RyaW5nKClcbiAgICAgIH1cblxuICAgICAgaWYgKGNvbmZpZy5jb250ZW50ICYmIHR5cGVvZiBjb25maWcuY29udGVudCA9PT0gJ251bWJlcicpIHtcbiAgICAgICAgY29uZmlnLmNvbnRlbnQgPSBjb25maWcuY29udGVudC50b1N0cmluZygpXG4gICAgICB9XG5cbiAgICAgIFV0aWwudHlwZUNoZWNrQ29uZmlnKFxuICAgICAgICBOQU1FLFxuICAgICAgICBjb25maWcsXG4gICAgICAgIHRoaXMuY29uc3RydWN0b3IuRGVmYXVsdFR5cGVcbiAgICAgIClcblxuICAgICAgcmV0dXJuIGNvbmZpZ1xuICAgIH1cblxuICAgIF9nZXREZWxlZ2F0ZUNvbmZpZygpIHtcbiAgICAgIGNvbnN0IGNvbmZpZyA9IHt9XG5cbiAgICAgIGlmICh0aGlzLmNvbmZpZykge1xuICAgICAgICBmb3IgKGNvbnN0IGtleSBpbiB0aGlzLmNvbmZpZykge1xuICAgICAgICAgIGlmICh0aGlzLmNvbnN0cnVjdG9yLkRlZmF1bHRba2V5XSAhPT0gdGhpcy5jb25maWdba2V5XSkge1xuICAgICAgICAgICAgY29uZmlnW2tleV0gPSB0aGlzLmNvbmZpZ1trZXldXG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICB9XG5cbiAgICAgIHJldHVybiBjb25maWdcbiAgICB9XG5cbiAgICBfY2xlYW5UaXBDbGFzcygpIHtcbiAgICAgIGNvbnN0ICR0aXAgPSAkKHRoaXMuZ2V0VGlwRWxlbWVudCgpKVxuICAgICAgY29uc3QgdGFiQ2xhc3MgPSAkdGlwLmF0dHIoJ2NsYXNzJykubWF0Y2goQlNDTFNfUFJFRklYX1JFR0VYKVxuICAgICAgaWYgKHRhYkNsYXNzICE9PSBudWxsICYmIHRhYkNsYXNzLmxlbmd0aCA+IDApIHtcbiAgICAgICAgJHRpcC5yZW1vdmVDbGFzcyh0YWJDbGFzcy5qb2luKCcnKSlcbiAgICAgIH1cbiAgICB9XG5cbiAgICBfaGFuZGxlUG9wcGVyUGxhY2VtZW50Q2hhbmdlKGRhdGEpIHtcbiAgICAgIHRoaXMuX2NsZWFuVGlwQ2xhc3MoKVxuICAgICAgdGhpcy5hZGRBdHRhY2htZW50Q2xhc3ModGhpcy5fZ2V0QXR0YWNobWVudChkYXRhLnBsYWNlbWVudCkpXG4gICAgfVxuXG4gICAgX2ZpeFRyYW5zaXRpb24oKSB7XG4gICAgICBjb25zdCB0aXAgICAgICAgICAgICAgICAgID0gdGhpcy5nZXRUaXBFbGVtZW50KClcbiAgICAgIGNvbnN0IGluaXRDb25maWdBbmltYXRpb24gPSB0aGlzLmNvbmZpZy5hbmltYXRpb25cbiAgICAgIGlmICh0aXAuZ2V0QXR0cmlidXRlKCd4LXBsYWNlbWVudCcpICE9PSBudWxsKSB7XG4gICAgICAgIHJldHVyblxuICAgICAgfVxuICAgICAgJCh0aXApLnJlbW92ZUNsYXNzKENsYXNzTmFtZS5GQURFKVxuICAgICAgdGhpcy5jb25maWcuYW5pbWF0aW9uID0gZmFsc2VcbiAgICAgIHRoaXMuaGlkZSgpXG4gICAgICB0aGlzLnNob3coKVxuICAgICAgdGhpcy5jb25maWcuYW5pbWF0aW9uID0gaW5pdENvbmZpZ0FuaW1hdGlvblxuICAgIH1cblxuICAgIC8vIHN0YXRpY1xuXG4gICAgc3RhdGljIF9qUXVlcnlJbnRlcmZhY2UoY29uZmlnKSB7XG4gICAgICByZXR1cm4gdGhpcy5lYWNoKGZ1bmN0aW9uICgpIHtcbiAgICAgICAgbGV0IGRhdGEgICAgICA9ICQodGhpcykuZGF0YShEQVRBX0tFWSlcbiAgICAgICAgY29uc3QgX2NvbmZpZyA9IHR5cGVvZiBjb25maWcgPT09ICdvYmplY3QnICYmIGNvbmZpZ1xuXG4gICAgICAgIGlmICghZGF0YSAmJiAvZGlzcG9zZXxoaWRlLy50ZXN0KGNvbmZpZykpIHtcbiAgICAgICAgICByZXR1cm5cbiAgICAgICAgfVxuXG4gICAgICAgIGlmICghZGF0YSkge1xuICAgICAgICAgIGRhdGEgPSBuZXcgVG9vbHRpcCh0aGlzLCBfY29uZmlnKVxuICAgICAgICAgICQodGhpcykuZGF0YShEQVRBX0tFWSwgZGF0YSlcbiAgICAgICAgfVxuXG4gICAgICAgIGlmICh0eXBlb2YgY29uZmlnID09PSAnc3RyaW5nJykge1xuICAgICAgICAgIGlmIChkYXRhW2NvbmZpZ10gPT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgdGhyb3cgbmV3IEVycm9yKGBObyBtZXRob2QgbmFtZWQgXCIke2NvbmZpZ31cImApXG4gICAgICAgICAgfVxuICAgICAgICAgIGRhdGFbY29uZmlnXSgpXG4gICAgICAgIH1cbiAgICAgIH0pXG4gICAgfVxuXG4gIH1cblxuXG4gIC8qKlxuICAgKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiAgICogalF1ZXJ5XG4gICAqIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuICAgKi9cblxuICAkLmZuW05BTUVdICAgICAgICAgICAgID0gVG9vbHRpcC5falF1ZXJ5SW50ZXJmYWNlXG4gICQuZm5bTkFNRV0uQ29uc3RydWN0b3IgPSBUb29sdGlwXG4gICQuZm5bTkFNRV0ubm9Db25mbGljdCAgPSBmdW5jdGlvbiAoKSB7XG4gICAgJC5mbltOQU1FXSA9IEpRVUVSWV9OT19DT05GTElDVFxuICAgIHJldHVybiBUb29sdGlwLl9qUXVlcnlJbnRlcmZhY2VcbiAgfVxuXG4gIHJldHVybiBUb29sdGlwXG5cbn0pKGpRdWVyeSlcblxuZXhwb3J0IGRlZmF1bHQgVG9vbHRpcFxuIiwiaW1wb3J0IFRvb2x0aXAgZnJvbSAnLi90b29sdGlwJ1xuXG5cbi8qKlxuICogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiAqIEJvb3RzdHJhcCAodjQuMC4wLWJldGEpOiBwb3BvdmVyLmpzXG4gKiBMaWNlbnNlZCB1bmRlciBNSVQgKGh0dHBzOi8vZ2l0aHViLmNvbS90d2JzL2Jvb3RzdHJhcC9ibG9iL21hc3Rlci9MSUNFTlNFKVxuICogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiAqL1xuXG5jb25zdCBQb3BvdmVyID0gKCgkKSA9PiB7XG5cblxuICAvKipcbiAgICogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gICAqIENvbnN0YW50c1xuICAgKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiAgICovXG5cbiAgY29uc3QgTkFNRSAgICAgICAgICAgICAgICA9ICdwb3BvdmVyJ1xuICBjb25zdCBWRVJTSU9OICAgICAgICAgICAgID0gJzQuMC4wLWJldGEnXG4gIGNvbnN0IERBVEFfS0VZICAgICAgICAgICAgPSAnYnMucG9wb3ZlcidcbiAgY29uc3QgRVZFTlRfS0VZICAgICAgICAgICA9IGAuJHtEQVRBX0tFWX1gXG4gIGNvbnN0IEpRVUVSWV9OT19DT05GTElDVCAgPSAkLmZuW05BTUVdXG4gIGNvbnN0IENMQVNTX1BSRUZJWCAgICAgICAgPSAnYnMtcG9wb3ZlcidcbiAgY29uc3QgQlNDTFNfUFJFRklYX1JFR0VYICA9IG5ldyBSZWdFeHAoYChefFxcXFxzKSR7Q0xBU1NfUFJFRklYfVxcXFxTK2AsICdnJylcblxuICBjb25zdCBEZWZhdWx0ID0gJC5leHRlbmQoe30sIFRvb2x0aXAuRGVmYXVsdCwge1xuICAgIHBsYWNlbWVudCA6ICdyaWdodCcsXG4gICAgdHJpZ2dlciAgIDogJ2NsaWNrJyxcbiAgICBjb250ZW50ICAgOiAnJyxcbiAgICB0ZW1wbGF0ZSAgOiAnPGRpdiBjbGFzcz1cInBvcG92ZXJcIiByb2xlPVwidG9vbHRpcFwiPidcbiAgICAgICAgICAgICAgKyAnPGRpdiBjbGFzcz1cImFycm93XCI+PC9kaXY+J1xuICAgICAgICAgICAgICArICc8aDMgY2xhc3M9XCJwb3BvdmVyLWhlYWRlclwiPjwvaDM+J1xuICAgICAgICAgICAgICArICc8ZGl2IGNsYXNzPVwicG9wb3Zlci1ib2R5XCI+PC9kaXY+PC9kaXY+J1xuICB9KVxuXG4gIGNvbnN0IERlZmF1bHRUeXBlID0gJC5leHRlbmQoe30sIFRvb2x0aXAuRGVmYXVsdFR5cGUsIHtcbiAgICBjb250ZW50IDogJyhzdHJpbmd8ZWxlbWVudHxmdW5jdGlvbiknXG4gIH0pXG5cbiAgY29uc3QgQ2xhc3NOYW1lID0ge1xuICAgIEZBREUgOiAnZmFkZScsXG4gICAgU0hPVyA6ICdzaG93J1xuICB9XG5cbiAgY29uc3QgU2VsZWN0b3IgPSB7XG4gICAgVElUTEUgICA6ICcucG9wb3Zlci1oZWFkZXInLFxuICAgIENPTlRFTlQgOiAnLnBvcG92ZXItYm9keSdcbiAgfVxuXG4gIGNvbnN0IEV2ZW50ID0ge1xuICAgIEhJREUgICAgICAgOiBgaGlkZSR7RVZFTlRfS0VZfWAsXG4gICAgSElEREVOICAgICA6IGBoaWRkZW4ke0VWRU5UX0tFWX1gLFxuICAgIFNIT1cgICAgICAgOiBgc2hvdyR7RVZFTlRfS0VZfWAsXG4gICAgU0hPV04gICAgICA6IGBzaG93biR7RVZFTlRfS0VZfWAsXG4gICAgSU5TRVJURUQgICA6IGBpbnNlcnRlZCR7RVZFTlRfS0VZfWAsXG4gICAgQ0xJQ0sgICAgICA6IGBjbGljayR7RVZFTlRfS0VZfWAsXG4gICAgRk9DVVNJTiAgICA6IGBmb2N1c2luJHtFVkVOVF9LRVl9YCxcbiAgICBGT0NVU09VVCAgIDogYGZvY3Vzb3V0JHtFVkVOVF9LRVl9YCxcbiAgICBNT1VTRUVOVEVSIDogYG1vdXNlZW50ZXIke0VWRU5UX0tFWX1gLFxuICAgIE1PVVNFTEVBVkUgOiBgbW91c2VsZWF2ZSR7RVZFTlRfS0VZfWBcbiAgfVxuXG5cbiAgLyoqXG4gICAqIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuICAgKiBDbGFzcyBEZWZpbml0aW9uXG4gICAqIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuICAgKi9cblxuICBjbGFzcyBQb3BvdmVyIGV4dGVuZHMgVG9vbHRpcCB7XG5cblxuICAgIC8vIGdldHRlcnNcblxuICAgIHN0YXRpYyBnZXQgVkVSU0lPTigpIHtcbiAgICAgIHJldHVybiBWRVJTSU9OXG4gICAgfVxuXG4gICAgc3RhdGljIGdldCBEZWZhdWx0KCkge1xuICAgICAgcmV0dXJuIERlZmF1bHRcbiAgICB9XG5cbiAgICBzdGF0aWMgZ2V0IE5BTUUoKSB7XG4gICAgICByZXR1cm4gTkFNRVxuICAgIH1cblxuICAgIHN0YXRpYyBnZXQgREFUQV9LRVkoKSB7XG4gICAgICByZXR1cm4gREFUQV9LRVlcbiAgICB9XG5cbiAgICBzdGF0aWMgZ2V0IEV2ZW50KCkge1xuICAgICAgcmV0dXJuIEV2ZW50XG4gICAgfVxuXG4gICAgc3RhdGljIGdldCBFVkVOVF9LRVkoKSB7XG4gICAgICByZXR1cm4gRVZFTlRfS0VZXG4gICAgfVxuXG4gICAgc3RhdGljIGdldCBEZWZhdWx0VHlwZSgpIHtcbiAgICAgIHJldHVybiBEZWZhdWx0VHlwZVxuICAgIH1cblxuXG4gICAgLy8gb3ZlcnJpZGVzXG5cbiAgICBpc1dpdGhDb250ZW50KCkge1xuICAgICAgcmV0dXJuIHRoaXMuZ2V0VGl0bGUoKSB8fCB0aGlzLl9nZXRDb250ZW50KClcbiAgICB9XG5cbiAgICBhZGRBdHRhY2htZW50Q2xhc3MoYXR0YWNobWVudCkge1xuICAgICAgJCh0aGlzLmdldFRpcEVsZW1lbnQoKSkuYWRkQ2xhc3MoYCR7Q0xBU1NfUFJFRklYfS0ke2F0dGFjaG1lbnR9YClcbiAgICB9XG5cbiAgICBnZXRUaXBFbGVtZW50KCkge1xuICAgICAgcmV0dXJuIHRoaXMudGlwID0gdGhpcy50aXAgfHwgJCh0aGlzLmNvbmZpZy50ZW1wbGF0ZSlbMF1cbiAgICB9XG5cbiAgICBzZXRDb250ZW50KCkge1xuICAgICAgY29uc3QgJHRpcCA9ICQodGhpcy5nZXRUaXBFbGVtZW50KCkpXG5cbiAgICAgIC8vIHdlIHVzZSBhcHBlbmQgZm9yIGh0bWwgb2JqZWN0cyB0byBtYWludGFpbiBqcyBldmVudHNcbiAgICAgIHRoaXMuc2V0RWxlbWVudENvbnRlbnQoJHRpcC5maW5kKFNlbGVjdG9yLlRJVExFKSwgdGhpcy5nZXRUaXRsZSgpKVxuICAgICAgdGhpcy5zZXRFbGVtZW50Q29udGVudCgkdGlwLmZpbmQoU2VsZWN0b3IuQ09OVEVOVCksIHRoaXMuX2dldENvbnRlbnQoKSlcblxuICAgICAgJHRpcC5yZW1vdmVDbGFzcyhgJHtDbGFzc05hbWUuRkFERX0gJHtDbGFzc05hbWUuU0hPV31gKVxuICAgIH1cblxuICAgIC8vIHByaXZhdGVcblxuICAgIF9nZXRDb250ZW50KCkge1xuICAgICAgcmV0dXJuIHRoaXMuZWxlbWVudC5nZXRBdHRyaWJ1dGUoJ2RhdGEtY29udGVudCcpXG4gICAgICAgIHx8ICh0eXBlb2YgdGhpcy5jb25maWcuY29udGVudCA9PT0gJ2Z1bmN0aW9uJyA/XG4gICAgICAgICAgICAgIHRoaXMuY29uZmlnLmNvbnRlbnQuY2FsbCh0aGlzLmVsZW1lbnQpIDpcbiAgICAgICAgICAgICAgdGhpcy5jb25maWcuY29udGVudClcbiAgICB9XG5cbiAgICBfY2xlYW5UaXBDbGFzcygpIHtcbiAgICAgIGNvbnN0ICR0aXAgPSAkKHRoaXMuZ2V0VGlwRWxlbWVudCgpKVxuICAgICAgY29uc3QgdGFiQ2xhc3MgPSAkdGlwLmF0dHIoJ2NsYXNzJykubWF0Y2goQlNDTFNfUFJFRklYX1JFR0VYKVxuICAgICAgaWYgKHRhYkNsYXNzICE9PSBudWxsICYmIHRhYkNsYXNzLmxlbmd0aCA+IDApIHtcbiAgICAgICAgJHRpcC5yZW1vdmVDbGFzcyh0YWJDbGFzcy5qb2luKCcnKSlcbiAgICAgIH1cbiAgICB9XG5cblxuICAgIC8vIHN0YXRpY1xuXG4gICAgc3RhdGljIF9qUXVlcnlJbnRlcmZhY2UoY29uZmlnKSB7XG4gICAgICByZXR1cm4gdGhpcy5lYWNoKGZ1bmN0aW9uICgpIHtcbiAgICAgICAgbGV0IGRhdGEgICAgICA9ICQodGhpcykuZGF0YShEQVRBX0tFWSlcbiAgICAgICAgY29uc3QgX2NvbmZpZyA9IHR5cGVvZiBjb25maWcgPT09ICdvYmplY3QnID8gY29uZmlnIDogbnVsbFxuXG4gICAgICAgIGlmICghZGF0YSAmJiAvZGVzdHJveXxoaWRlLy50ZXN0KGNvbmZpZykpIHtcbiAgICAgICAgICByZXR1cm5cbiAgICAgICAgfVxuXG4gICAgICAgIGlmICghZGF0YSkge1xuICAgICAgICAgIGRhdGEgPSBuZXcgUG9wb3Zlcih0aGlzLCBfY29uZmlnKVxuICAgICAgICAgICQodGhpcykuZGF0YShEQVRBX0tFWSwgZGF0YSlcbiAgICAgICAgfVxuXG4gICAgICAgIGlmICh0eXBlb2YgY29uZmlnID09PSAnc3RyaW5nJykge1xuICAgICAgICAgIGlmIChkYXRhW2NvbmZpZ10gPT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgdGhyb3cgbmV3IEVycm9yKGBObyBtZXRob2QgbmFtZWQgXCIke2NvbmZpZ31cImApXG4gICAgICAgICAgfVxuICAgICAgICAgIGRhdGFbY29uZmlnXSgpXG4gICAgICAgIH1cbiAgICAgIH0pXG4gICAgfVxuICB9XG5cblxuICAvKipcbiAgICogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gICAqIGpRdWVyeVxuICAgKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiAgICovXG5cbiAgJC5mbltOQU1FXSAgICAgICAgICAgICA9IFBvcG92ZXIuX2pRdWVyeUludGVyZmFjZVxuICAkLmZuW05BTUVdLkNvbnN0cnVjdG9yID0gUG9wb3ZlclxuICAkLmZuW05BTUVdLm5vQ29uZmxpY3QgID0gZnVuY3Rpb24gKCkge1xuICAgICQuZm5bTkFNRV0gPSBKUVVFUllfTk9fQ09ORkxJQ1RcbiAgICByZXR1cm4gUG9wb3Zlci5falF1ZXJ5SW50ZXJmYWNlXG4gIH1cblxuICByZXR1cm4gUG9wb3ZlclxuXG59KShqUXVlcnkpXG5cbmV4cG9ydCBkZWZhdWx0IFBvcG92ZXJcbiJdfQ==
