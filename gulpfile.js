(function () {
  const gulp = require('gulp'),
      image = require('gulp-image'),
      connect = require('gulp-connect'),
      open = require('gulp-open'),
      jade = require('gulp-jade'),
      rename = require('gulp-rename'),
      path = require('gulp-path'),
      watch = require('gulp-watch'),
      sass = require('gulp-sass'),
      sourcemaps = require('gulp-sourcemaps'),
      concat = require('gulp-concat'),
      clean = require('gulp-clean-css'),
      uglify = require('gulp-uglify'),
      babel = require('gulp-babel'),
      wrapper = require('gulp-wrapper'),
      autoprefixer = require('gulp-autoprefixer'),
      paths = {
        root: './',
        build: {
          root: './',
          jade: './',
          styles: 'assets/css/',
          scripts: 'assets/js/',
          image: 'assets/img/'
        },
        source: {
          root: 'source/',
          jade: 'source/jade/',
          styles: 'source/css/',
          scripts: 'source/js/',
          image: 'source/img/',
          lib: 'source/lib/'
        }
      };

  // JADE
  gulp.task('jade', function () {
    gulp.src([
      paths.source.jade + '*.jade',
      '!' + paths.source.jade + 'head.jade',
      '!' + paths.source.jade + 'header.jade',
      '!' + paths.source.jade + 'footer.jade'
    ])
        .pipe(jade({
          pretty: true
        }))
        .pipe(gulp.dest(paths.build.jade));
  });

  // CSS
  gulp.task('css', function () {
    gulp.src(paths.source.styles + 'style.scss')
        .pipe(sourcemaps.init())
        .pipe(sass().on('error', sass.logError))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(paths.build.styles));
  });

  // JS
  gulp.task('js', function () {
    gulp.src([
      paths.source.scripts + 'common.js'
    ])
        .pipe(sourcemaps.init())
        .pipe(concat('common.js'))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(paths.build.scripts));
  });

  // IMG
  gulp.task('img', function () {
    gulp.src(paths.source.image + '*.*')
        .pipe(image({
          pngquant: true,
          optipng: false,
          zopflipng: true,
          jpegRecompress: false,
          jpegoptim: true,
          mozjpeg: true,
          gifsicle: true,
          svgo: true,
          concurrent: 10
        }))
        .pipe(gulp.dest(paths.build.image));
  });

  // BOOTSTRAP
  gulp.task('bootstrapStyles', function () {
    gulp.src(paths.source.lib + 'bootstrap/scss/bootstrap.scss')
        .pipe(sourcemaps.init())
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer({
          browsers: [
            //
            // Official browser support policy:
            // https://getbootstrap.com/docs/4.0/getting-started/browsers-devices/#supported-browsers
            //
            'Chrome >= 45', // Exact version number here is kinda arbitrary
            'Firefox ESR',
            // Note: Edge versions in Autoprefixer & Can I Use refer to the EdgeHTML rendering engine version,
            // NOT the Edge app version shown in Edge's "About" screen.
            // For example, at the time of writing, Edge 20 on an up-to-date system uses EdgeHTML 12.
            // See also https://github.com/Fyrd/caniuse/issues/1928
            'Edge >= 12',
            'Explorer >= 10',
            // Out of leniency, we prefix these 1 version further back than the official policy.
            'iOS >= 9',
            'Safari >= 9',
            // The following remain NOT officially supported, but we're lenient and include their prefixes to avoid severely breaking in them.
            'Android >= 4.4',
            'Opera >= 30'
          ]
        }))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(paths.build.styles));
  });
  gulp.task('bootstrapScripts', function () {
    gulp.src([
      paths.source.lib + 'bootstrap/js/util.js',
      paths.source.lib + 'bootstrap/js/alert.js',
      paths.source.lib + 'bootstrap/js/button.js',
      paths.source.lib + 'bootstrap/js/carousel.js',
      paths.source.lib + 'bootstrap/js/collapse.js',
      paths.source.lib + 'bootstrap/js/dropdown.js',
      paths.source.lib + 'bootstrap/js/modal.js',
      paths.source.lib + 'bootstrap/js/scrollspy.js',
      paths.source.lib + 'bootstrap/js/tab.js',
      paths.source.lib + 'bootstrap/js/tooltip.js',
      paths.source.lib + 'bootstrap/js/popover.js'
    ])
        .pipe(sourcemaps.init())
        .pipe(babel({
          "presets": [
            [
              "es2015",
              {
                "modules": false,
                "loose": true
              }
            ]
          ],
          "plugins": [
            "transform-es2015-modules-strip"
          ]
        }))
        .pipe(concat('bootstrap.js'))
        .pipe(wrapper({
          header: '+function(){\n',
          footer: '\n}()'
        }))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(paths.build.scripts));
  });

  // JQUERY
  gulp.task('jquery', function () {
    gulp.src([
      paths.source.lib + "jquery/jquery-3.2.1.js"
    ])
        .pipe(sourcemaps.init())
        .pipe(concat('jquery.js'))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(paths.build.scripts));
  });

  // POPPER
  gulp.task('popper', function () {
    gulp.src([
      paths.source.lib + "popper/popper.js"
    ])
        .pipe(babel({
          "presets": [
            [
              "es2015",
              {
                "modules": false,
                "loose": true
              }
            ]
          ],
          "plugins": [
            "transform-es2015-modules-strip"
          ]
        }))
        .pipe(sourcemaps.init())
        .pipe(concat('popper.js'))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(paths.build.scripts));
  });

  // MIN STYLES
  gulp.task('minCSS', function () {
    gulp.src([
      paths.build.styles + 'bootstrap.css',
      paths.build.styles + 'style.css'
    ])
        .pipe(concat('all.css'))
        .pipe(clean({
          level: {
            1: {
              cleanupCharsets: true, // controls `@charset` moving to the front of a stylesheet; defaults to `true`
              normalizeUrls: true, // controls URL normalization; defaults to `true`
              optimizeBackground: true, // controls `background` property optimizations; defaults to `true`
              optimizeBorderRadius: true, // controls `border-radius` property optimizations; defaults to `true`
              optimizeFilter: true, // controls `filter` property optimizations; defaults to `true`
              optimizeFont: true, // controls `font` property optimizations; defaults to `true`
              optimizeFontWeight: true, // controls `font-weight` property optimizations; defaults to `true`
              optimizeOutline: true, // controls `outline` property optimizations; defaults to `true`
              removeEmpty: true, // controls removing empty rules and nested blocks; defaults to `true`
              removeNegativePaddings: true, // controls removing negative paddings; defaults to `true`
              removeQuotes: true, // controls removing quotes when unnecessary; defaults to `true`
              removeWhitespace: true, // controls removing unused whitespace; defaults to `true`
              replaceMultipleZeros: true, // contols removing redundant zeros; defaults to `true`
              replaceTimeUnits: true, // controls replacing time units with shorter values; defaults to `true`
              replaceZeroUnits: true, // controls replacing zero values with units; defaults to `true`
              roundingPrecision: false, // rounds pixel values to `N` decimal places; `false` disables rounding; defaults to `false`
              selectorsSortingMethod: 'standard', // denotes selector sorting method; can be `'natural'` or `'standard'`, `'none'`, or false (the last two since 4.1.0); defaults to `'standard'`
              specialComments: 0, // denotes a number of /*! ... */ comments preserved; defaults to `all`
              tidyAtRules: true, // controls at-rules (e.g. `@charset`, `@import`) optimizing; defaults to `true`
              tidyBlockScopes: true, // controls block scopes (e.g. `@media`) optimizing; defaults to `true`
              tidySelectors: true, // controls selectors optimizing; defaults to `true`,
              transform: function () {} // defines a callback for fine-grained property optimization; defaults to no-op
            }
          }
        }))
        .pipe(gulp.dest(paths.build.styles));
  });

  // MIN SCRIPTS
  gulp.task('minJS', function () {
    gulp.src([
      paths.build.scripts + "jquery.js",
      paths.build.scripts + "popper.js",
      paths.build.scripts + "bootstrap.js",
      paths.build.scripts + "common.js"
    ])
        .pipe(concat('all.js'))
        .pipe(uglify())
        .pipe(gulp.dest(paths.build.scripts));
  });

  // BUILD
  gulp.task('dev', ['jade', 'css', 'js', 'img', 'bootstrapStyles', 'bootstrapScripts', 'jquery', 'popper']);

  // PROD
  gulp.task('prod', ['minCSS', 'minJS']);

  // Watch
  gulp.task('watch', function () {
    // Watch/build
    gulp.watch(paths.source.jade + '*.jade', ['jade']);
    gulp.watch(paths.source.styles + '*.scss', ['css']);
    gulp.watch(paths.source.scripts + '*.js', ['js']);
    gulp.watch(paths.source.image + '*.*', ['img']);
    gulp.watch(paths.source.lib + 'bootstrap/scss/*.scss', ['bootstrapStyles']);
    gulp.watch(paths.source.lib + 'bootstrap/scss/mixins/*.scss', ['bootstrapStyles']);
    gulp.watch(paths.source.lib + 'bootstrap/scss/utilities/*.scss', ['bootstrapStyles']);
    gulp.watch(paths.source.lib + 'bootstrap/js/*.js', ['bootstrapScripts']);
    gulp.watch(paths.source.lib + 'jquery/*.js', ['jquery']);
    gulp.watch(paths.source.lib + 'popper/*.js', ['popper']);

    // Watch/reload
    watch([
      paths.build.jade + '*.html',
      paths.build.styles + '*.css',
      paths.build.scripts + '*.js',
      paths.build.image + '*.*'
    ]).pipe(connect.reload());
  });

  // Connect
  gulp.task('connect', function () {
    return connect.server({
      root: [paths.root],
      livereload: true,
      port: '3000'
    });
  });

  // Open
  gulp.task('open', function () {
    return gulp.src('./index.html').pipe(open({uri: 'http://localhost:3000/index.html'}));
  });

  // Server
  gulp.task('server', ['dev', 'watch', 'connect', 'open']);
})();